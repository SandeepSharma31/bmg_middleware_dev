package com.bmg.esb.PublishDataToSmartSuspense.beans;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
