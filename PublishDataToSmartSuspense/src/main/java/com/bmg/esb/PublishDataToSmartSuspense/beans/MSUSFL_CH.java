package com.bmg.esb.PublishDataToSmartSuspense.beans;

public class MSUSFL_CH {
	private String companyCode;
	private String deferredType;
	private String reference;
	private long sequenceNo;
	private String statIdyy;
	private long statIdnum;
	private String songTitle;
	private String composers;
	private String sourceCode;
	private String societyWorkno;
	private long amount;
	private long percPaidToUS;
	private String note;
	private long incomeType;
	private long periodAdded;
	private long incomePeriod;
	private long SUSA_ID;

	public long getSUSA_ID() {
		return SUSA_ID;
	}

	public void setSUSA_ID(long sUSA_ID) {
		SUSA_ID = sUSA_ID;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getDeferredType() {
		return deferredType;
	}

	public void setDeferredType(String deferredType) {
		this.deferredType = deferredType;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public long getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(long sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getStatIdyy() {
		return statIdyy;
	}

	public void setStatIdyy(String statIdyy) {
		this.statIdyy = statIdyy;
	}

	public long getStatIdnum() {
		return statIdnum;
	}

	public void setStatIdnum(long statIdnum) {
		this.statIdnum = statIdnum;
	}

	public String getSongTitle() {
		return songTitle;
	}

	public void setSongTitle(String songTitle) {
		this.songTitle = songTitle;
	}

	public String getComposers() {
		return composers;
	}

	public void setComposers(String composers) {
		this.composers = composers;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public String getSocietyWorkno() {
		return societyWorkno;
	}

	public void setSocietyWorkno(String societyWorkno) {
		this.societyWorkno = societyWorkno;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public long getPercPaidToUS() {
		return percPaidToUS;
	}

	public void setPercPaidToUS(long percPaidToUS) {
		this.percPaidToUS = percPaidToUS;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public long getIncomeType() {
		return incomeType;
	}

	public void setIncomeType(long incomeType) {
		this.incomeType = incomeType;
	}

	public long getPeriodAdded() {
		return periodAdded;
	}

	public void setPeriodAdded(long periodAdded) {
		this.periodAdded = periodAdded;
	}

	public long getIncomePeriod() {
		return incomePeriod;
	}

	public void setIncomePeriod(long incomePeriod) {
		this.incomePeriod = incomePeriod;
	}

}
