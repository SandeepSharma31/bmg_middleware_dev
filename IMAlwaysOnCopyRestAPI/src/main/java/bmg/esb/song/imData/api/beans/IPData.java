package bmg.esb.song.imData.api.beans;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;

/**
 * This is a bean class used for the IP data coming from the database 
 *
 */
public class IPData
{
	private transient boolean isChild = false;
	private transient int code;
	int ipCode;
	long sequence;
	String name;
	String surname;
	String firstName;
	String middleName;
	long chainId;
	String territory;
	String territoryName;
	String originTerritory;
	String capacityCode;
	String controlled;
	float mechOwned;
	float perfOwned;
	float syncOwned;
	float otherOwned;
	String ipType;
	IPData[] children;
	String ipLink;
	String parentIpLink;
	ArrayList<String> parentIpLinkList;
	//int ipId;
	
	public ArrayList<String> getParentIpLinkList() {
		return parentIpLinkList;
	}

	public void setParentIpLinkList(ArrayList<String> parentIpLinkList) {
		this.parentIpLinkList = parentIpLinkList;
	}

	public int getCode()
	{
		return code;
	}

	public void setCode(int code)
	{
		this.code = code;
	}

	/*public int getIpId()
	{
		return ipId;
	}

	public void setIpId(int ipId)
	{
		this.ipId = ipId;
	}*/

	public String getControlled()
	{
		return controlled;
	}

	public String getIpLink() {
		return ipLink;
	}

	public void setIpLink(String str) {
		this.ipLink = str;
	}

	public String getParentIpLink() {
		return parentIpLink;
	}

	public void setParentIpLink(String str) {
		this.parentIpLink = str;
	}

	public long getSequence()
	{
		return sequence;
	}

	public void setSequence(long sequence)
	{
		this.sequence = sequence;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public long getChainId()
	{
		return chainId;
	}

	public void setChainId(long chainId)
	{
		this.chainId = chainId;
	}

	public String getTerritory()
	{
		return territory;
	}

	public void setTerritory(String territory)
	{
		this.territory = territory;
	}

	public String getTerritoryName()
	{
		return territoryName;
	}

	public void setTerritoryName(String territoryName)
	{
		this.territoryName = territoryName;
	}

	public String getOriginTerritory()
	{
		return originTerritory;
	}

	public void setOriginTerritory(String originTerritory)
	{
		this.originTerritory = originTerritory;
	}

	public String getCapacityCode()
	{
		return capacityCode;
	}

	public void setCapacityCode(String capacityCode)
	{
		this.capacityCode = capacityCode;
	}

	public String isControlled()
	{
		return controlled;
	}

	public void setControlled(String controlled)
	{
		this.controlled = controlled;
	}

	public float getMechOwned()
	{
		return mechOwned;
	}

	public void setMechOwned(float mechOwned)
	{
		this.mechOwned = mechOwned;
	}

	public float getPerfOwned()
	{
		return perfOwned;
	}

	public void setPerfOwned(float perfOwned)
	{
		this.perfOwned = perfOwned;
	}

	public float getSyncOwned()
	{
		return syncOwned;
	}

	public void setSyncOwned(float syncOwned)
	{
		this.syncOwned = syncOwned;
	}

	public float getOtherOwned()
	{
		return otherOwned;
	}

	public void setOtherOwned(float otherOwned)
	{
		this.otherOwned = otherOwned;
	}

	
	public IPData[] getChildren()
	{
		return children;
	}

	public void setChildren(IPData[] children)
	{
		this.children = children;
	}

	public String getSurname()
	{
		return surname;
	}

	public void setSurname(String surname)
	{
		this.surname = surname;
	}

	public boolean isChild()
	{
		return isChild;
	}

	public void setChild(boolean isChild)
	{
		this.isChild = isChild;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public int getIpCode() {
		return ipCode;
	}

	public void setIpCode(int ipCode) {
		this.ipCode = ipCode;
	}
	public String getIpType() {
		return ipType;
	}

	public void setIpType(String ipType) {
		this.ipType = ipType;
	}
}
