package bmg.esb.song.imData.api.impl;


import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import bmg.esb.song.imData.api.bo.SongApiBO;
import bmg.esb.song.imData.api.dao.ApiDaoImpl;
import bmg.esb.song.imData.api.literals.RestApiConstants;


@Path("/api")
public class SongApi_SingleURL {/*

	public static Logger logger = Logger.getLogger(SongApi_SingleURL.class.getName());

	
	 * This method is used to get Json songs data based on song code
	 

	@GET
	@Path("/songs")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSongsJson(@Context  UriInfo info) throws Exception{
		String responseStr=null;
		SongApiBO songBo=null;
		try{
			
			String  song_code=info.getQueryParameters(). getFirst("code");
			String  title=info.getQueryParameters(). getFirst("title");
			String  composer=info.getQueryParameters(). getFirst("composer");
			String  start=info.getQueryParameters().getFirst("startdate");
			String  end=info.getQueryParameters().getFirst("enddate");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");
			String  filter=info.getQueryParameters().getFirst("filter");

			songBo=new SongApiBO();

			if(song_code!=null && !song_code.equals("")){
				
				String[] perpage=getPageAndPerPageVals(page,per_page);
				
				System.out.println("id-->"+song_code+"--page-->"+perpage[0]+"--per_page-->"+perpage[1]+"--withIP-->"+withIP);
				responseStr=songBo.getSongsJsonByID(song_code,perpage[0],perpage[1],getWithIPStrVal(withIP),apiDaoImpl).toString();

			} else if(title!=null && !title.equals("")){

				String[] perpage=getPageAndPerPageVals(page,per_page);
				
				System.out.println("title-->"+title+"--page-->"+perpage[0]+"--per_page-->"+perpage[1]+"--withIP-->"+withIP);
				responseStr=songBo.getSongsJsonByTitle(title,perpage[0],perpage[1],getWithIPStrVal(withIP),apiDaoImpl).toString();

			} else if(composer!=null && !composer.equals("")){

				String[] perpage=getPageAndPerPageVals(page,per_page);
				
				System.out.println("composer-->"+composer+"--page-->"+perpage[0]+"--per_page-->"+perpage[1]+"--withIP-->"+withIP);
				responseStr=songBo.getSongsJsonByComposer(composer,perpage[0],perpage[1],getWithIPStrVal(withIP),apiDaoImpl).toString();

			} else if((song_code!=null && !song_code.equals("")) && (title!=null && !title.equals("")) ){

				String[] perpage=getPageAndPerPageVals(page,per_page);

				System.out.println("song_code-->"+song_code+"--title-->"+title+"---page-->"+perpage[0]+"--per_page-->"+perpage[1]+"--withIP-->"+withIP);
				responseStr=songBo.getSongsJsonByIdAndTitle(song_code,title,perpage[0],perpage[1],getWithIPStrVal(withIP),filter,apiDaoImpl).toString();

			} else if((song_code!=null && !song_code.equals("")) && (composer!=null && !composer.equals("")) ){

				String[] perpage=getPageAndPerPageVals(page,per_page);
				
				System.out.println("song_code-->"+song_code+"--composer-->"+composer+"---page-->"+perpage[0]+"--per_page-->"+perpage[1]+"--withIP-->"+withIP);
				responseStr=songBo.getSongsJsonByIdAndComposer(song_code,composer,perpage[0],perpage[1],getWithIPStrVal(withIP),filter,apiDaoImpl).toString();

			} else if((title!=null && !title.equals("")) && (composer!=null && !composer.equals("")) ){

				String[] perpage=getPageAndPerPageVals(page,per_page);

				System.out.println("title-->"+title+"--composer-->"+composer+"---page-->"+perpage[0]+"--per_page-->"+perpage[1]+"--withIP-->"+withIP);
				responseStr=songBo.getSongsJsonByTitleAndComposer(title,composer,perpage[0],perpage[1],getWithIPStrVal(withIP),filter,apiDaoImpl).toString();

			} else if((song_code!=null && !song_code.equals("")) &&(title!=null && !title.equals("")) && (composer!=null && !composer.equals("")) ){

				String[] perpage=getPageAndPerPageVals(page,per_page);
				
				System.out.println("song_code-->"+song_code+"title-->"+title+"--composer-->"+composer+"---page-->"+perpage[0]+"--per_page-->"+perpage[1]+"--withIP-->"+withIP);
				responseStr=songBo.getSongsJsonByCodeTitleAndComposer(song_code,title,composer,perpage[0],perpage[1],filter,getWithIPStrVal(withIP),apiDaoImpl).toString();

			} else if((start!=null && !start.equals("")) &&  (end!=null && !end.equals(""))){

				String[] perpage=getPageAndPerPageVals(page,per_page);
				
				System.out.println("start-->"+start+"-end-->"+end+"---page-->"+perpage[0]+"--per_page-->"+perpage[1]+"--withIP-->"+withIP);
				responseStr=songBo.getSongsJsonByDateRange(start,end,perpage[0],perpage[1],getWithIPStrVal(withIP),apiDaoImpl).toString();

			} else if( (page!=null && !page.equals("0")) && (per_page!=null && !per_page.equals("0"))){

				System.out.println("---page-->"+page+"--per_page-->"+per_page+"--withIP-->"+withIP);
				responseStr=songBo.getAllSongsJson(page,per_page,getWithIPStrVal(withIP),apiDaoImpl).toString();

			} else {
				System.out.println("all-->");
				page="1";
				per_page="1000";
				responseStr=songBo.getAllSongsJson(page,per_page,withIP,apiDaoImpl).toString();
			} 


		}catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByCode() : message: "+ex.getMessage());
			responseStr=ex.getMessage();
			//throw new Exception(ex.getMessage());
		}

		logger.info("SongApi : End :getSongsByCode() : responseStr: "+responseStr);
		return Response.status(201).type("text/html").entity(responseStr).build();
	}


	public String getWithIPStrVal(String withIP){
		if(withIP!=null && !withIP.equals("") && withIP.equals("Y")){

		}else {
			withIP="N";
		}
		return withIP;
	}
	public String[] getPageAndPerPageVals(String page,String per_page ){
		String[] strArr=new String[2];
		if((page!=null && !page.equals("0")) && (per_page!=null && !per_page.equals("0"))){
		} else{
			page="1";
			per_page="1000";
		}
		strArr[0]=page;
		strArr[1]=per_page;
		return strArr;
	}
	
	 * This method is used to get Json songs data based on song code
	 

	@GET
	@Path("/code")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByCode(@Context  UriInfo info) throws Exception{

		logger.info("SongApi : Start :getSongsByCode()");
		String responseStr=null;
		SongApiBO songBo=null;
		try{

			List<String> jsonStr=null;
			String  song_code=info.getQueryParameters(). getFirst("code");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");

			logger.info("SongApi : getSongsByCode() :Input params: song_code:"+song_code);
			songBo=new SongApiBO();

			if(song_code!=null && !song_code.equals("")){

				if((page!=null && !page.equals("0")) && (per_page!=null && !per_page.equals("0"))){

				} else{
					page="1";
					per_page="1000";
				}

				if(withIP!=null && !withIP.equals("") && withIP.equals("Y")){

				}else {
					withIP="N";
				}
				System.out.println("id-->"+song_code+"--page-->"+page+"--per_page-->"+per_page+"--withIP-->"+withIP);
				jsonStr=songBo.getSongsJsonByID(song_code,page,per_page,withIP,apiDaoImpl);
				responseStr=jsonStr.toString();

			} else {
				responseStr=RestApiConstants.BAD_REQUEST_MSG;
				logger.error("SongApiBO : ERROR :getSongsByCode() : message: "+responseStr);
			}

		}catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByCode() : message: "+ex.getMessage());
			responseStr=ex.getMessage();
			//throw new Exception(ex.getMessage());
		}

		logger.info("SongApi : End :getSongsByCode() : responseStr: "+responseStr);
		return Response.status(201).type("text/html").entity(responseStr).build();
	}


	 * This method is used to get Json songs data based on song title


	@GET
	@Path("/title")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByTitle(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getSongsByTitle()");
		String responseStr=null;
		SongApiBO songBo=null;
		try{
			List<String> jsonStr=null;
			String  title=info.getQueryParameters(). getFirst("title");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");

			logger.info("SongApi : getSongsByTitle() :Input params: title:"+title);
			songBo=new SongApiBO();
			System.out.println("title-->"+title);
			if(title!=null && !title.equals("")){
				if((page!=null && !page.equals("0")) && (per_page!=null && !per_page.equals("0"))){

				} else{
					page="1";
					per_page="1000";
				}

				if(withIP!=null && !withIP.equals("") && withIP.equals("Y")){

				}else {
					withIP="N";
				}
				System.out.println("title-->"+title+"--page-->"+page+"--per_page-->"+per_page+"--withIP-->"+withIP);
				jsonStr=songBo.getSongsJsonByTitle(title,page,per_page,withIP,apiDaoImpl);
				responseStr=jsonStr.toString();
			} else {
				responseStr=RestApiConstants.BAD_REQUEST_MSG;
				logger.error("SongApi : ERROR :getSongsByTitle() : message: "+responseStr);
			}

		}catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByCode() : message: "+ex.getMessage());
			responseStr=ex.getMessage();
			//throw new Exception(ex.getMessage());
		}
		logger.info("SongApi : End :getSongsByTitle() : responseStr: "+responseStr);
		return Response.status(201).type("text/html").entity(responseStr).build();
	}
	 

	
	 * This method is used to get Json songs data based on song composer
	 

	@GET
	@Path("/composer")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByComposer(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getSongsByComposer()");
		String responseStr=null;
		SongApiBO songBo=null;
		try{
			List<String> jsonStr=null;
			String  composer=info.getQueryParameters(). getFirst("composer");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");

			logger.info("SongApi : getSongsByComposer() :Input params: composer:"+composer);
			songBo=new SongApiBO();
			System.out.println("composer-->"+composer);
			if(composer!=null && !composer.equals("")){

				if((page!=null && !page.equals("0")) && (per_page!=null && !per_page.equals("0"))){

				} else{
					page="1";
					per_page="1000";
				}

				if(withIP!=null && !withIP.equals("") && withIP.equals("Y")){

				}else {
					withIP="N";
				}
				jsonStr=songBo.getSongsJsonByComposer(composer,page,per_page,withIP,apiDaoImpl);
				responseStr=jsonStr.toString();
			} else {
				responseStr=RestApiConstants.BAD_REQUEST_MSG;
				logger.error("SongApi : ERROR :getSongsByTitle() : message: "+responseStr);
			}

		}catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByComposer() : message: "+ex.getMessage());
			responseStr=ex.getMessage();
			//throw new Exception(ex.getMessage());
		}
		logger.info("SongApi : End :getSongsByComposer() : responseStr: "+responseStr);
		return Response.status(201).type("text/html").entity(responseStr).build();
	}
	 
	
	 * This method is used to get Json songs data based on song code and title
	 

	@GET
	@Path("/codeAndTitle")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByCodeAndTitle(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getSongsByCodeAndTitle()");
		String responseStr=null;
		SongApiBO songBo=null;
		try{
			List<String> jsonStr=null;
			String  code=info.getQueryParameters(). getFirst("code");
			String  title=info.getQueryParameters(). getFirst("title");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");

			logger.info("SongApi : getSongsByCodeAndTitle() :Input params: code:"+code+":title"+title);
			songBo=new SongApiBO();
			System.out.println("code-->"+code+"--title--->"+title);
			if((code!=null && !code.equals("")) && (title!=null && !title.equals("")) ){

				if((page!=null && !page.equals("0")) && (per_page!=null && !per_page.equals("0"))){

				} else{
					page="1";
					per_page="1000";
				}

				if(withIP!=null && !withIP.equals("") && withIP.equals("Y")){

				}else {
					withIP="N";
				}

				jsonStr=songBo.getSongsJsonByIdAndTitle(code,title,page,per_page,withIP,apiDaoImpl);
				responseStr=jsonStr.toString();
			} else {
				responseStr=RestApiConstants.BAD_REQUEST_MSG;
				logger.error("SongApi : ERROR :getSongsByCodeAndTitle() : message: "+responseStr);
			}

		}catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByCodeAndTitle() : message: "+ex.getMessage());
			responseStr=ex.getMessage();
			//throw new Exception(ex.getMessage());
		}
		logger.info("SongApi : End :getSongsByCodeAndTitle() : responseStr: "+responseStr);
		return Response.status(201).type("text/html").entity(responseStr).build();
	}
	 
	
	 * This method is used to get Json songs data based on song code and composer
	 
	
	@GET
	@Path("/codeAndComposer")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByCodeAndComposer(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getSongsByCodeAndComposer()");
		String responseStr=null;
		SongApiBO songBo=null;
		try{
			List<String> jsonStr=null;
			String  code =info.getQueryParameters(). getFirst("code");
			String  composer =info.getQueryParameters(). getFirst("composer");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");

			logger.info("SongApi : getSongsByCodeAndComposer() :Input params: code:"+code+":composer"+composer);
			songBo=new SongApiBO();
			System.out.println("code-->"+code+"--composer--->"+composer);
			if((code!=null && !code.equals("")) && (composer!=null && !composer.equals("")) ){

				if((page!=null && !page.equals("0")) && (per_page!=null && !per_page.equals("0"))){

				} else{
					page="1";
					per_page="1000";
				}

				if(withIP!=null && !withIP.equals("") && withIP.equals("Y")){

				}else {
					withIP="N";
				}

				jsonStr=songBo.getSongsJsonByIdAndComposer(code,composer,page,per_page,withIP,apiDaoImpl);
				responseStr=jsonStr.toString();
			} else {
				responseStr=RestApiConstants.BAD_REQUEST_MSG;
				logger.error("SongApi : ERROR :getSongsByCodeAndComposer() : message: "+responseStr);
			}

		}catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByCodeAndComposer() : message: "+ex.getMessage());
			responseStr=ex.getMessage();
			//throw new Exception(ex.getMessage());
		}
		logger.info("SongApi : End :getSongsByCodeAndComposer() : responseStr: "+responseStr);
		return Response.status(201).type("text/html").entity(responseStr).build();
	}

	 
	
	 * This method is used to get Json songs data based on title and composer
	 

	@GET
	@Path("/titleAndComposer")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByTitleAndComposer(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getSongsByTitleAndComposer()");
		String responseStr=null;
		SongApiBO songBo=null;
		try{
			List<String> jsonStr=null;
			String  title =info.getQueryParameters(). getFirst("title");
			String  composer =info.getQueryParameters(). getFirst("composer");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");

			logger.info("SongApi : getSongsByTitleAndComposer() :Input params: title:"+title+":composer"+composer);
			songBo=new SongApiBO();
			System.out.println("title-->"+title+"--composer--->"+composer);
			if((title!=null && !title.equals("")) && (composer!=null && !composer.equals("")) ){

				if((page!=null && !page.equals("0")) && (per_page!=null && !per_page.equals("0"))){

				} else{
					page="1";
					per_page="1000";
				}

				if(withIP!=null && !withIP.equals("") && withIP.equals("Y")){

				}else {
					withIP="N";
				}

				jsonStr=songBo.getSongsJsonByTitleAndComposer(title,composer,page,per_page,withIP,apiDaoImpl);
				responseStr=jsonStr.toString();
			} else {
				responseStr=RestApiConstants.BAD_REQUEST_MSG;
				logger.error("SongApi : ERROR :getSongsByTitle() : message: "+responseStr);
			}

		}catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByTitleAndComposer() : message: "+ex.getMessage());
			responseStr=ex.getMessage();
			//throw new Exception(ex.getMessage());
		}
		logger.info("SongApi : End :getSongsByTitleAndComposer() : responseStr: "+responseStr);
		return Response.status(201).type("text/html").entity(responseStr).build();
	}

	
	 * This method is used to get Json songs data based on song code,title and composer
	 

	@GET
	@Path("/codeTitleAndComposer")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByCodeTitleAndComposer(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getSongsByCodeTitleAndComposer()");
		String responseStr=null;
		SongApiBO songBo=null;
		try{
			List<String> jsonStr=null;
			String  code =info.getQueryParameters(). getFirst("code");
			String  title =info.getQueryParameters(). getFirst("title");
			String  composer =info.getQueryParameters(). getFirst("composer");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");

			logger.info("SongApi : getSongsByCodeTitleAndComposer() :Input params: code:"+code+":title:"+title+":composer"+composer);
			songBo=new SongApiBO();
			System.out.println("code-->"+code+"--title-->"+title+"--composer--->"+composer);
			if((code!=null && !code.equals("")) &&(title!=null && !title.equals("")) && (composer!=null && !composer.equals("")) ){

				if((page!=null && !page.equals("0")) && (per_page!=null && !per_page.equals("0"))){

				} else{
					page="1";
					per_page="1000";
				}

				if(withIP!=null && !withIP.equals("") && withIP.equals("Y")){

				}else {
					withIP="N";
				}

				jsonStr=songBo.getSongsJsonByCodeTitleAndComposer(code,title,composer,page,per_page,withIP,apiDaoImpl);
				responseStr=jsonStr.toString();
			} else {
				responseStr=RestApiConstants.BAD_REQUEST_MSG;
				logger.error("SongApi : ERROR :getSongsByCodeTitleAndComposer() : message: "+responseStr);
			}

		}catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByCodeTitleAndComposer() : message: "+ex.getMessage());
			responseStr=ex.getMessage();
			//throw new Exception(ex.getMessage());
		}
		logger.info("SongApi : End :getSongsByCodeTitleAndComposer() : responseStr: "+responseStr);
		return Response.status(201).type("text/html").entity(responseStr).build();
	}

	
	 * This method is used to get Json songs data based on date range - in between start and end dates
	 

	@GET
	@Path("/date")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByDateRange(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getSongsByDateRange()");
		String responseStr=null;
		SongApiBO songBo=null;
		try{
			List<String> jsonStr=null;

			String  start=info.getQueryParameters().getFirst("startdate");
			String  end=info.getQueryParameters().getFirst("enddate");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");

			System.out.println("start-->"+start+"--end-->"+end+"--page--->"+page+"--per_page--->"+per_page);
			logger.info("SongApi : Start :getSongsByDateRange() : Inuputs: start :"+": end :"+end+" : page :"+page+" : per_page :"+per_page);
			songBo=new SongApiBO();
			if((start!=null && !start.equals("")) &&  (end!=null && !end.equals(""))){

				if((page!=null && !page.equals("0")) && (per_page!=null && !per_page.equals("0"))){

				} else{
					page="1";
					per_page="1000";
				}
				System.out.println("if-->start-->"+start+"--end-->"+end+"--page--->"+page+"--per_page--->"+per_page);
				logger.info("SongApi : Start in IF :getSongsByDateRange() : Inuputs: start :"+": end :"+end+" : page :"+page+" : per_page :"+per_page);
				jsonStr=songBo.getSongsJsonByDateRange(start,end,page,per_page,withIP,apiDaoImpl);
				responseStr=jsonStr.toString();

			} else {
				responseStr=RestApiConstants.BAD_REQUEST_MSG;
				logger.error("SongApi : ERROR :getSongsByDateRange() : message: "+responseStr);
			}

		} catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByDateRange() : message: "+ex.getMessage());
			responseStr=ex.getMessage();
			//throw new Exception(ex.getMessage());

		}
		logger.info("SongApi : End :getSongsByDateRange() : responseStr: "+responseStr);
		return Response.status(201).type("text/html").entity(responseStr).build();
	}
	 

	
	 * This method is used to get all Json songs data 
	 

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getAllSongs(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getAllSongs()");
		String responseStr=null;
		SongApiBO songBo=null;
		try{
			List<String> jsonStr=null;
			songBo=new SongApiBO();
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");
			String  withIP=info.getQueryParameters(). getFirst("withIP");

			logger.info("SongApi : Start :getSongsByDateRange() : Inuputs:page :"+page+" : per_page :"+per_page);
			if( (page!=null && !page.equals("0")) && (per_page!=null && !per_page.equals("0"))){

				System.out.println("----page-->"+page+"----per_page>"+per_page);
				jsonStr=songBo.getAllSongsJson(page,per_page,withIP,apiDaoImpl);
			} else {
				System.out.println("all-->");
				page="1";
				per_page="1000";
				jsonStr=songBo.getAllSongsJson(page,per_page,withIP,apiDaoImpl);
			} 
			if(jsonStr!=null && !jsonStr.equals(""))
				responseStr=jsonStr.toString();

		} catch(Exception ex){


			logger.info("SongApi : ERROR :getAllSongs() : message: "+ex.getMessage());
			responseStr=ex.getMessage();
			//throw new Exception(ex.getMessage());

		}
		logger.info("SongApi : End :getAllSongs() : responseStr: "+responseStr);
		return Response.status(201).type("text/html").entity(responseStr).build();
	}


	public ApiDaoImpl apiDaoImpl;

	public ApiDaoImpl getApiDaoImpl() {
		return apiDaoImpl;
	}

	public void setApiDaoImpl(ApiDaoImpl apiDaoImpl) {
		this.apiDaoImpl = apiDaoImpl;
	}

*/}
