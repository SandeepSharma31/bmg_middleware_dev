package bmg.esb.song.imData.api.bo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import bmg.esb.song.imData.api.beans.*;
import bmg.esb.song.imData.api.dao.ApiDaoImpl;
import bmg.esb.song.imData.api.impl.SongApi_SingleURL;
import bmg.esb.song.imData.api.literals.RestApiConstants;
import bmg.esb.song.imData.api.utility.JsonGenerator;

public class SongApiBO {

	public static Logger logger = Logger.getLogger(SongApiBO.class.getName());

	public static int countById;
	public static int countByTimeStamp;	
	public static int countAll;

	public static List<Map<String, Object>> inputListById = new ArrayList<Map<String,Object>>();
	public static List<Map<String, Object>> inputListByTimeStamp = new ArrayList<Map<String,Object>>();
	public static List<Map<String, Object>> inputListForAll =new ArrayList<Map<String,Object>>();

	/*
	 * This method is used to get Json songs data based on song code
	 */
	public List<String>  getSongsJsonByID(String id,String pageNo, String perPage,String withIP,ApiDaoImpl apiDaoImpl) throws Exception{

		logger.info("SongApiBO : Start :getSongsJsonByID() :  id :"+id);
		List<String> json=null;
		try {
			
			int[] startEndRecNumbers=getStartAndEndRecordNumbers(pageNo,  perPage);
			List<Map<String, Object>> inputlist=apiDaoImpl.getSongsById(id,withIP,startEndRecNumbers);
			if(inputlist.size()==0)
				throw new Exception(RestApiConstants.SONG_CODE_DATA_NOT_AVAILABLE_MSG+id+":"+pageNo);
			json=getJson(inputlist,withIP);
		} catch(Exception ex){
			logger.info("SongApiBO : ERROR :getSongsJsonByID() : message: "+ex.getMessage());
			throw new Exception(ex.getMessage());
		}
		logger.info("SongApiBO : End :getSongsJsonByID() ");
		return json;
	}


	/*
	 * This method is used to get Json songs data based on song title
	 */
	public List<String>  getSongsJsonByTitle(String title,String pageNo, String perPage,String withIP,ApiDaoImpl apiDaoImpl) throws Exception{

		logger.info("SongApiBO : Start :getSongsJsonByTitle() :  title :"+title);
		List<String> json=null;
		try {
			int[] startEndRecNumbers=getStartAndEndRecordNumbers(pageNo,  perPage);
			List<Map<String, Object>> inputlist=apiDaoImpl.getSongsByTitle(title,withIP,startEndRecNumbers);
			if(inputlist.size()==0)
				throw new Exception(RestApiConstants.SONG_TITLE_DATA_NOT_AVAILABLE_MSG+title+":"+pageNo);
			json=getJson(inputlist,withIP);
		} catch(Exception ex){
			logger.info("SongApiBO : ERROR :getSongsJsonByTitle() : message: "+ex.getMessage());
			throw new Exception(ex.getMessage());
		}
		logger.info("SongApiBO : End :getSongsJsonByTitle()  ");
		return json;
	}


	/*
	 * This method is used to get Json songs data based on song composer
	 */
	public List<String>  getSongsJsonByComposer(String composer,String pageNo, String perPage,String withIP,ApiDaoImpl apiDaoImpl) throws Exception{

		logger.info("SongApiBO : Start :getSongsJsonByComposer() :  composer :"+composer);
		List<String> json=null;
		try {
			int[] startEndRecNumbers=getStartAndEndRecordNumbers(pageNo,  perPage);
			List<Map<String, Object>> inputlist=apiDaoImpl.getSongsByComposer(composer,withIP,startEndRecNumbers);
			if(inputlist.size()==0)
				throw new Exception(RestApiConstants.SONG_COMPOSER_DATA_NOT_AVAILABLE_MSG+composer+":"+pageNo);
			json=getJson(inputlist,withIP);
		} catch(Exception ex){
			logger.info("SongApiBO : ERROR :getSongsJsonByComposer() : message: "+ex.getMessage());
			throw new Exception(ex.getMessage());
		}
		logger.info("SongApiBO : End :getSongsJsonByComposer()");
		return json;
	}


	/*
	 * This method is used to get Json songs data based on song code and title
	 */

	public List<String>  getSongsJsonByIdAndTitle(String id,String title,String pageNo, String perPage,String withIP,String filter,ApiDaoImpl apiDaoImpl) throws Exception{

		logger.info("SongApiBO : getSongsJsonByIdAndTitle() :Input params: id:"+id+":title"+title);
		List<String> json=null;
		try {
			int[] startEndRecNumbers=getStartAndEndRecordNumbers(pageNo,  perPage);
			List<Map<String, Object>> inputlist=apiDaoImpl.getSongsByIdAndTitle(id,title,withIP,startEndRecNumbers,filter);
			if(inputlist.size()==0)
				throw new Exception(RestApiConstants.SONG_CODE_TITLE_DATA_NOT_AVAILABLE_MSG+id+":"+title+":"+pageNo);
			json=getJson(inputlist,withIP);
		} catch(Exception ex){
			logger.info("SongApiBO : ERROR :getSongsJsonByIdAndTitle() : message: "+ex.getMessage());
			throw new Exception(ex.getMessage());
		}
		logger.info("SongApiBO : End :getSongsJsonByIdAndTitle() ");
		return json;
	}



	/*
	 * This method is used to get Json songs data based on song code and composer
	 */

	public List<String>  getSongsJsonByIdAndComposer(String id,String composer,String pageNo, String perPage,String withIP,String filter,ApiDaoImpl apiDaoImpl) throws Exception{

		logger.info("SongApiBO : Start :getSongsJsonByIdAndComposer() :  id:"+id+":composer"+composer);
		List<String> json=null;
		try {
			int[] startEndRecNumbers=getStartAndEndRecordNumbers(pageNo,  perPage);
			List<Map<String, Object>> inputlist=apiDaoImpl.getSongsByIdAndComposer(id,composer,withIP,startEndRecNumbers,filter);
			if(inputlist.size()==0)
				throw new Exception(RestApiConstants.SONG_CODE_COMPOSER_DATA_NOT_AVAILABLE_MSG+id+":"+composer+":"+pageNo);
			json=getJson(inputlist,withIP);
		} catch(Exception ex){
			logger.info("SongApiBO : ERROR :getSongsJsonByIdAndComposer() : message: "+ex.getMessage());
			throw new Exception(ex.getMessage());
		}
		logger.info("SongApiBO : End :getSongsJsonByIdAndComposer()  ");
		return json;
	}


	/*
	 * This method is used to get Json songs data based on title and composer
	 */

	public List<String>  getSongsJsonByTitleAndComposer(String title,String composer,String pageNo, String perPage,String withIP,String filter,ApiDaoImpl apiDaoImpl) throws Exception{

		logger.info("SongApiBO : Start :getSongsJsonByTitleAndComposer() :  title:"+title+":composer"+composer);
		List<String> json=null;
		try {
			int[] startEndRecNumbers=getStartAndEndRecordNumbers(pageNo,  perPage);
			List<Map<String, Object>> inputlist=apiDaoImpl.getSongsByTitleAndComposer(title,composer,withIP,startEndRecNumbers,filter);
			if(inputlist.size()==0)
				throw new Exception(RestApiConstants.SONG_TITLE_COMPOSER_DATA_NOT_AVAILABLE_MSG+title+":"+composer+":"+pageNo);
			json=getJson(inputlist,withIP);
		} catch(Exception ex){
			logger.info("SongApiBO : ERROR :getSongsJsonByTitleAndComposer() : message: "+ex.getMessage());
			throw new Exception(ex.getMessage());
		}
		logger.info("SongApiBO : End :getSongsJsonByTitleAndComposer() ");
		return json;
	}


	/*
	 * This method is used to get Json songs data based on song code,title and composer
	 */

	public List<String>  getSongsJsonByCodeTitleAndComposer(String id,String title,String composer,String pageNo, String perPage,String withIP,String filter,ApiDaoImpl apiDaoImpl) throws Exception{

		logger.info("SongApiBO : Start :getSongsJsonByCodeTitleAndComposer() :  id:"+id+":title:"+title+":composer"+composer);
		List<String> json=null;
		try {
			int[] startEndRecNumbers=getStartAndEndRecordNumbers(pageNo,  perPage);
			List<Map<String, Object>> inputlist=apiDaoImpl.getSongsByCodeTitleAndComposer(id,title,composer,withIP,startEndRecNumbers,filter);
			if(inputlist.size()==0)
				throw new Exception(RestApiConstants.SONG_CODE_TITLE_COMPOSER_DATA_NOT_AVAILABLE_MSG+id+":"+title+":"+composer+":"+pageNo);
			json=getJson(inputlist,withIP);
		} catch(Exception ex){
			logger.info("SongApiBO : ERROR :getSongsJsonByCodeTitleAndComposer() : message: "+ex.getMessage());
			throw new Exception(ex.getMessage());
		}
		logger.info("SongApiBO : End :getSongsJsonByCodeTitleAndComposer() ");
		return json;
	}

	/*
	 * This method is used to get Json songs data based on on date range - in between start and end dates
	 */


	public List<String> getSongsJsonByDateRange(String startDate,String endDate,String pageNo, String perPage,String withIP,ApiDaoImpl apiDaoImpl) throws Exception{

		logger.info("SongApiBO : Start :getSongsJsonByDateRange() :   start :"+startDate+": end :"+startDate+" : page :"+pageNo+" : per_page :"+perPage);

		List<String> json=null;
		try{
			
			int[] startEndRecNumbers=getStartAndEndRecordNumbers(pageNo,  perPage);
			logger.info("SongApiBO : Start :getSongsJsonByDateRange() :   startEndRecNumbers :"+startEndRecNumbers);
			List<Map<String, Object>> inputlist=apiDaoImpl.getSongByDateRange(startDate, endDate,withIP,startEndRecNumbers);
			if(inputlist.size()==0)
				throw new Exception(RestApiConstants.SONG_DATE_RANGE_DATA_NOT_AVAILABLE_MSG+startDate+":"+endDate);
			json=getJson(inputlist,withIP);
		}catch(Exception ex){
			logger.info("SongApiBO : ERROR :getSongsJsonByDateRange() : message: "+ex.getMessage());
			throw new Exception(ex.getMessage());
		}
		logger.info("SongApiBO : End :getSongsJsonByDateRange()");
		return json;
	}

	/*
	 * This method is used to get Json songs data for all songs - 
	 */

	public List<String> getAllSongsJson(String pageNo, String perPage,String withIP,ApiDaoImpl apiDaoImpl) throws Exception{
		logger.info("SongApiBO : Start :getAllSongsJson() :   page :"+pageNo+" : per_page :"+perPage);
		List<Map<String, Object>> inputlist=null;
		List<String> json=null;
		try{
			int[] startEndRecNumbers=getStartAndEndRecordNumbers(pageNo,  perPage);
			logger.info("SongApiBO : Start :getAllSongsJson() :   startEndRecNumbers :"+startEndRecNumbers);
			inputlist=apiDaoImpl.getAllSongs(withIP,startEndRecNumbers);
			if(inputlist.size()==0)
				throw new Exception(RestApiConstants.SONG_ALL_DATA_NOT_AVAILABLE_MSG);
			json=getJson(inputlist,withIP);
			
		}catch(Exception ex){
			logger.info("SongApiBO : ERROR :getAllSongsJson() : message: "+ex.getMessage());
			throw new Exception(ex.getMessage());
		}
		logger.info("SongApiBO : End :getAllSongsJson()");
		return  json;
	}
	
	



	public List<String> getJson(List<Map<String, Object>> inputList, String withIP){
		logger.info("SongApiBO : Start :getJson()");
		logger.info("inputlist size: "+inputList.size());
		JsonGenerator jsonGen=new JsonGenerator();
		logger.info("SongApiBO : End :getJson()");
		return jsonGen.generateJsonObject(inputList,withIP);
	}

	

	public int[] getStartAndEndRecordNumbers(String pageno, String perpage) throws Exception{
		logger.info("SongApiBO : Start :getStartAndEndRecordNumbers() :   pageno :"+pageno+" : perpage :"+perpage);	
		//List<Map<String, Object>> acatulInputlist=null;
		int[] startEndRecNum=null;
		try{
			
			int pageNo=Integer.parseInt(pageno);
			int perPage=Integer.parseInt(perpage);
			/*int startRecordNumber =(pageNo-1)*perPage+1;
			int endRecordNumber= startRecordNumber+(perPage-1);*/
			int startRecordNumber =(pageNo-1)*perPage;
			int endRecordNumber= perPage;
			
			logger.info("SongApiBO : Start :getStartAndEndRecordNumbers() :   startRecordNumber :"+startRecordNumber+" : endRecordNumber :"+endRecordNumber);
			startEndRecNum= new int[2];
			startEndRecNum[0]=startRecordNumber;
			startEndRecNum[1]=endRecordNumber;
			
		}catch(Exception ex){
			logger.info("SongApiBO : ERROR :getActualSongsDataList() :   message: "+ex.getMessage());
			throw new Exception(ex.getMessage()) ;
		}
		logger.info("SongApiBO : End :getActualSongsDataList()");
		return startEndRecNum;
	}
	



}
