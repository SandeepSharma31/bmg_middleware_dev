package bmg.esb.song.imData.api.dao;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

import bmg.esb.song.imData.api.bo.SongApiBO;
import bmg.esb.song.imData.api.literals.RestApiConstants;


public class ApiDaoImpl implements ApiDao{

	public static Logger logger = Logger.getLogger(ApiDaoImpl.class.getName());

	public JdbcTemplate jdbcTemplate;
	public DataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/*
	 * This method is used to fetch songs data based on song code with optional/mandatory IP and pagination from the db
	 */
	@Override
	public List<Map<String, Object>> getSongsById(String songId,String withIP,int[] startEndRecNumbers) throws Exception {
		logger.info("ApiDaoImpl : Start :getSongById() songId :"+songId);
		List<Map<String, Object>> songData = null;
		Connection conn=null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try {

			songData=new ArrayList<Map<String,Object>>();
			Map<String,Object> map=null;
			
			logger.info("ApiDaoImpl : getSongsById() : getConnection:  start :");
			conn=getConnection();
			String sql=null;

			if(withIP.equalsIgnoreCase("Y")){
				sql=RestApiConstants.SQL_SONG_CODE_WITH_IP_QUERY;
			}else if(withIP.equalsIgnoreCase("N")){	
				sql=RestApiConstants.SQL_SONG_CODE_WITHOUT_IP_QUERY;
			}
			logger.info("ApiDaoImpl : getSongsById() : Sql Query : "+sql);
			
			pst=conn.prepareStatement(sql);
			
			if(withIP.equalsIgnoreCase("Y")){
				pst.setString(1, songId);
			}else if(withIP.equalsIgnoreCase("N")){	
				//pst.setString(1, convertToDblQuoteStr(songId));
				pst.setString(1, songId);
			}
			pst.setInt(2, startEndRecNumbers[0]);
			pst.setInt(3, startEndRecNumbers[1]);
			
			//long queryStartTime=System.currentTimeMillis();
			rs=pst.executeQuery();
			while(rs.next()){
				if(withIP.equalsIgnoreCase("Y")){
					map=getMapObjForEachRowForSongDataWithIP(rs);
				}if(withIP.equalsIgnoreCase("N")){
					map=getMapObjForEachRowForSongDataWithoutIP(rs);
				}

				songData.add(map);
			}
			
			//logger.info("ApiDaoImpl : getSongsById() : SQL query processing time : "+(System.currentTimeMillis()-queryStartTime));
		} catch (Exception ex) {
			logger.info("ApiDaoImpl : ERROR :getSongById() : message: "+RestApiConstants.SONG_CODE_ERROR_MSG+songId);
			logger.info("ApiDaoImpl : ERROR :getSongById() : exception: "+ex.getMessage());
			throw new Exception(RestApiConstants.SONG_CODE_ERROR_MSG+songId);
		} finally {

			closeConnections(rs,pst,conn);
		}
		logger.info("ApiDaoImpl : End :getSongById() songData size :"+songData.size());
		return songData;

	}


	/*
	 * This method is used to fetch songs data based on song title from the db
	 */
	@Override
	public List<Map<String, Object>> getSongsByTitle(String songTitle,String withIP,int[] startEndRecNumbers) throws Exception {
		logger.info("ApiDaoImpl : Start :getSongsByTitle() songTitle :"+songTitle);
		List<Map<String, Object>> songData = null;
		Connection conn=null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try {
			songData=new ArrayList<Map<String,Object>>();
			Map<String,Object> map=null;
			
			conn=getConnection();
			
			String sql=null;

			if(withIP.equalsIgnoreCase("Y")){
				sql=RestApiConstants.SQL_SONG_TITLE_WITH_IP_QUERY;
			}else if(withIP.equalsIgnoreCase("N")){	
				sql=RestApiConstants.SQL_SONG_DATA_WITHOUT_IP_QUERY;
			}
			logger.info("ApiDaoImpl : getSongsByTitle() : Sql Query : "+sql);
			pst=conn.prepareStatement(sql);
			
			if(withIP.equalsIgnoreCase("Y")){
				pst.setString(1,getFulMatchForlike(songTitle));
			}else if(withIP.equalsIgnoreCase("N")){	
				pst.setString(1,convertToDblQuoteStr(songTitle));
			}
			pst.setInt(2,startEndRecNumbers[0]);
			pst.setInt(3,startEndRecNumbers[1]);
			//long queryStartTime=System.currentTimeMillis();
			rs=pst.executeQuery();
			while(rs.next()){
				if(withIP.equalsIgnoreCase("Y")){
					map=getMapObjForEachRowForSongDataWithIP(rs);
				}if(withIP.equalsIgnoreCase("N")){
					map=getMapObjForEachRowForSongDataWithoutIP(rs);
				}

				songData.add(map);
			}

			//logger.info("ApiDaoImpl : getSongsByTitle() : SQL query processing time : "+(System.currentTimeMillis()-queryStartTime));
		} catch (Exception ex) {
			logger.info("ApiDaoImpl : ERROR :getSongsByTitle() : message: "+RestApiConstants.SONG_TITLE_ERROR_MSG+songTitle);
			logger.info("ApiDaoImpl : ERROR :getSongsByTitle() : exception: "+ex.getMessage());
			ex.printStackTrace();
			throw new Exception(RestApiConstants.SONG_TITLE_ERROR_MSG+songTitle);
		} finally {

			closeConnections(rs,pst,conn);
		}
		logger.info("ApiDaoImpl : End :getSongsByTitle() songData size :"+songData.size());
		return songData;

	}
	


	/*
	 * This method is used to fetch songs data based on song composer from the db
	 */
	@Override
	public List<Map<String, Object>> getSongsByComposer(String songComposer,String withIP,int[] startEndRecNumbers) throws Exception {
		logger.info("ApiDaoImpl : Start :getSongsByComposer() songComposer :"+songComposer);
		List<Map<String, Object>> songData = null;
		Connection conn=null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try {

			songData=new ArrayList<Map<String,Object>>();
			Map<String,Object> map=null;
			conn=getConnection();
			
			String sql=null;

			if(withIP.equalsIgnoreCase("Y")){
				sql=RestApiConstants.SQL_SONG_COMPOSER_WITH_IP_QUERY;
			}else if(withIP.equalsIgnoreCase("N")){	
				sql=RestApiConstants.SQL_SONG_DATA_WITHOUT_IP_QUERY;
			}
			logger.info("ApiDaoImpl : getSongsByComposer() : Sql Query : "+sql);
			pst=conn.prepareStatement(sql);
			
			if(withIP.equalsIgnoreCase("Y")){
				pst.setString(1, getFulMatchForlike(songComposer));
			}else if(withIP.equalsIgnoreCase("N")){	
				pst.setString(1, convertToDblQuoteStr(songComposer));
			}
			
			pst.setInt(2, startEndRecNumbers[0]);
			pst.setInt(3, startEndRecNumbers[1]);
			//long queryStartTime=System.currentTimeMillis();
			rs=pst.executeQuery();
			while(rs.next()){
				if(withIP.equalsIgnoreCase("Y")){
					map=getMapObjForEachRowForSongDataWithIP(rs);
				}if(withIP.equalsIgnoreCase("N")){
					map=getMapObjForEachRowForSongDataWithoutIP(rs);
				}

				songData.add(map);
			}
			//logger.info("ApiDaoImpl : getSongsByComposer() : SQL query processing time : "+(System.currentTimeMillis()-queryStartTime));
		} catch (Exception ex) {
			logger.info("ApiDaoImpl : ERROR :getSongsByComposer() : message: "+RestApiConstants.SONG_COMPOSER_ERROR_MSG+songComposer);
			logger.info("ApiDaoImpl : ERROR :getSongsByComposer() : exception: "+ex.getMessage());
			throw new Exception(RestApiConstants.SONG_COMPOSER_ERROR_MSG+songComposer);
		} finally {

			closeConnections(rs,pst,conn);
		}
		logger.info("ApiDaoImpl : End :getSongsByComposer() songData size :"+songData.size());
		return songData;

	}


	/*
	 * This method is used to fetch songs data based on song id and title from the db
	 */
	@Override
	public List<Map<String, Object>> getSongsByIdAndTitle(String id, String title,String withIP,int[] startEndRecNumbers,String sqlFilterForAndOr) throws Exception {
		logger.info("ApiDaoImpl : Start :getSongsByIdAndTitle() id :"+id+":title"+title);
		List<Map<String, Object>> songData = null;
		Connection conn=null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try {
			songData=new ArrayList<Map<String,Object>>();
			Map<String,Object> map=null;
			conn=getConnection();
			
			
			String sql=null;
			
			if(withIP.equalsIgnoreCase("Y")){
				sql=replaceWithANDorOR(RestApiConstants.SQL_SONG_ID_TITLE_WITH_IP_QUERY,sqlFilterForAndOr);
			}else if(withIP.equalsIgnoreCase("N")){	
				sql=RestApiConstants.SQL_SONG_DATA_WITHOUT_IP_QUERY;
			}
			logger.info("ApiDaoImpl : getSongsByIdAndTitle() : Sql Query : "+sql);
			pst=conn.prepareStatement(sql);

			if(withIP.equalsIgnoreCase("Y")){
				pst.setString(1, getFulMatchForlike(id));
				pst.setString(2, getFulMatchForlike(title));
				pst.setInt(3, startEndRecNumbers[0]);
				pst.setInt(4, startEndRecNumbers[1]);
			}else if(withIP.equalsIgnoreCase("N")){	
				pst.setString(1, replaceWithANDorORFortwoparams(convertToDblQuoteStr(id), convertToDblQuoteStr(title), sqlFilterForAndOr));
				pst.setInt(2, startEndRecNumbers[0]);
				pst.setInt(3, startEndRecNumbers[1]);
			}
			
			//long queryStartTime=System.currentTimeMillis();
			rs=pst.executeQuery();
			while(rs.next()){
				if(withIP.equalsIgnoreCase("Y")){
					map=getMapObjForEachRowForSongDataWithIP(rs);
				}if(withIP.equalsIgnoreCase("N")){
					map=getMapObjForEachRowForSongDataWithoutIP(rs);
				}

				songData.add(map);
			}
			//logger.info("ApiDaoImpl : getSongsByIdAndTitle() : SQL query processing time : "+(System.currentTimeMillis()-queryStartTime));
		} catch (Exception ex) {
			logger.info("ApiDaoImpl : ERROR :getSongsByIdAndTitle() : message: "+RestApiConstants.SONG_ID_TITLE_ERROR_MSG+id+":"+title);
			logger.info("ApiDaoImpl : ERROR :getSongsByIdAndTitle() : exception: "+ex.getMessage());
			throw new Exception(RestApiConstants.SONG_ID_TITLE_ERROR_MSG+id+":"+title);
		} finally {

			closeConnections(rs,pst,conn);
		}
		logger.info("ApiDaoImpl : End :getSongsByIdAndTitle() songData size :"+songData.size());
		return songData;

	}


	/*
	 * This method is used to fetch songs data based on song id and composer from the db
	 */
	@Override
	public List<Map<String, Object>> getSongsByIdAndComposer(String id, String composer,String withIP,int[] startEndRecNumbers,String sqlFilterForAndOr) throws Exception {
		logger.info("ApiDaoImpl : Start :getSongsByIdAndComposer() id :"+id+":composer"+composer);
		List<Map<String, Object>> songData = null;
		Connection conn=null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try {
			songData=new ArrayList<Map<String,Object>>();
			Map<String,Object> map=null;
			conn=getConnection();
			
			String sql=null;

			if(withIP.equalsIgnoreCase("Y")){
				sql=replaceWithANDorOR(RestApiConstants.SQL_SONG_ID_COMPOSER_WITH_IP_QUERY,sqlFilterForAndOr);
			}else if(withIP.equalsIgnoreCase("N")){	
				sql=RestApiConstants.SQL_SONG_DATA_WITHOUT_IP_QUERY;
			}
			logger.info("ApiDaoImpl : getSongsByIdAndComposer() : Sql Query : "+sql);
			pst=conn.prepareStatement(sql);
			if(withIP.equalsIgnoreCase("Y")){
				pst.setString(1, getFulMatchForlike(id));
				pst.setString(2, getFulMatchForlike(composer));
				pst.setInt(3, startEndRecNumbers[0]);
				pst.setInt(4, startEndRecNumbers[1]);
			}else if(withIP.equalsIgnoreCase("N")){	
				pst.setString(1, replaceWithANDorORFortwoparams(convertToDblQuoteStr(id), convertToDblQuoteStr(composer), sqlFilterForAndOr));
				pst.setInt(2, startEndRecNumbers[0]);
				pst.setInt(3, startEndRecNumbers[1]);
			}
			
			//long queryStartTime=System.currentTimeMillis();
			rs=pst.executeQuery();
			while(rs.next()){
				if(withIP.equalsIgnoreCase("Y")){
					map=getMapObjForEachRowForSongDataWithIP(rs);
				}if(withIP.equalsIgnoreCase("N")){
					map=getMapObjForEachRowForSongDataWithoutIP(rs);
				}

				songData.add(map);
			}
			//logger.info("ApiDaoImpl : getSongsByIdAndComposer() : SQL query processing time : "+(System.currentTimeMillis()-queryStartTime));
		} catch (Exception ex) {
			logger.info("ApiDaoImpl : ERROR :getSongsByIdAndTitle() : message: "+RestApiConstants.SONG_ID_COMPOSER_ERROR_MSG+id+":"+composer);
			logger.info("ApiDaoImpl : ERROR :getSongsByIdAndTitle() : exception: "+ex.getMessage());
			throw new Exception(RestApiConstants.SONG_ID_COMPOSER_ERROR_MSG+id+":"+composer);
		} finally {
			closeConnections(rs,pst,conn);
		}
		logger.info("ApiDaoImpl : End :getSongsByIdAndComposer() songData size :"+songData.size());
		return songData;

	}


	/*
	 * This method is used to fetch songs data based on song title and composer from the db
	 */
	@Override
	public List<Map<String, Object>> getSongsByTitleAndComposer(String title, String composer,String withIP,int[] startEndRecNumbers,String sqlFilterForAndOr) throws Exception {
		logger.info("ApiDaoImpl : Start :getSongsByTitleAndComposer() title :"+title+":composer:"+composer);
		List<Map<String, Object>> songData = null;
		Connection conn=null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try {
			songData=new ArrayList<Map<String,Object>>();
			Map<String,Object> map=null;
			conn=getConnection();
			
			String sql=null;

			if(withIP.equalsIgnoreCase("Y")){
				sql=replaceWithANDorOR(RestApiConstants.SQL_SONG_TITLE_COMPOSER_WITH_IP_QUERY,sqlFilterForAndOr);
			}else if(withIP.equalsIgnoreCase("N")){	
				sql=RestApiConstants.SQL_SONG_DATA_WITHOUT_IP_QUERY;
			}
			logger.info("ApiDaoImpl : getSongsByIdAndComposer() : Sql Query : "+sql);
			pst=conn.prepareStatement(sql);
			if(withIP.equalsIgnoreCase("Y")){
				pst.setString(1, getFulMatchForlike(title));
				pst.setString(2, getFulMatchForlike(composer));
				pst.setInt(3, startEndRecNumbers[0]);
				pst.setInt(4, startEndRecNumbers[1]);
			}else if(withIP.equalsIgnoreCase("N")){	
				pst.setString(1, replaceWithANDorORFortwoparams(convertToDblQuoteStr(title), convertToDblQuoteStr(composer), sqlFilterForAndOr));
				pst.setInt(2, startEndRecNumbers[0]);
				pst.setInt(3, startEndRecNumbers[1]);
			}
			
			//long queryStartTime=System.currentTimeMillis();
			rs=pst.executeQuery();
			while(rs.next()){
				if(withIP.equalsIgnoreCase("Y")){
					map=getMapObjForEachRowForSongDataWithIP(rs);
				}if(withIP.equalsIgnoreCase("N")){
					map=getMapObjForEachRowForSongDataWithoutIP(rs);
				}

				songData.add(map);
			}
			//logger.info("ApiDaoImpl : getSongsByTitleAndComposer() : SQL query processing time : "+(System.currentTimeMillis()-queryStartTime));
		} catch (Exception ex) {
			logger.info("ApiDaoImpl : ERROR :getSongsByTitleAndComposer() : message: "+RestApiConstants.SONG_TITLE_COMPOSER_ERROR_MSG+title+":"+composer);
			logger.info("ApiDaoImpl : ERROR :getSongsByTitleAndComposer() : exception: "+ex.getMessage());
			throw new Exception(RestApiConstants.SONG_TITLE_COMPOSER_ERROR_MSG+title+":"+composer);
		} finally {

			closeConnections(rs,pst,conn);
		}
		logger.info("ApiDaoImpl : End :getSongsByTitleAndComposer() songData size :"+songData.size());
		return songData;

	}



	/*
	 * This method is used to fetch songs data based on song id , title and composer from the db
	 */
	@Override
	public List<Map<String, Object>> getSongsByCodeTitleAndComposer(String id,String title, String composer,String withIP,int[] startEndRecNumbers,String sqlFilterForAndOr) throws Exception {
		logger.info("ApiDaoImpl : Start :getSongsByCodeTitleAndComposer() id :"+id+":title:"+title+":composer:"+composer);
		List<Map<String, Object>> songData = null;
		Connection conn=null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try {
			songData=new ArrayList<Map<String,Object>>();
			Map<String,Object> map=null;
			conn=getConnection();
		
			String sql=null;

			if(withIP.equalsIgnoreCase("Y")){
				sql=replaceWithANDorOR(RestApiConstants.SQL_SONG_ID_TITLE_COMPOSER_WITH_IP_QUERY,sqlFilterForAndOr);
			}else if(withIP.equalsIgnoreCase("N")){	
				sql=RestApiConstants.SQL_SONG_DATA_WITHOUT_IP_QUERY;
			}
			logger.info("ApiDaoImpl : getSongsByCodeTitleAndComposer() : Sql Query : "+sql);
			logger.info("ApiDaoImpl : getSongsByCodeTitleAndComposer() : string replace : "+replaceWithANDorORForallparams(convertToDblQuoteStr(id),convertToDblQuoteStr(title), convertToDblQuoteStr(composer), sqlFilterForAndOr));
			pst=conn.prepareStatement(sql);
			
			if(withIP.equalsIgnoreCase("Y")){
				pst.setString(1, getFulMatchForlike(id));
				pst.setString(2, getFulMatchForlike(title));
				pst.setString(3, getFulMatchForlike(composer));
				pst.setInt(4, startEndRecNumbers[0]);
				pst.setInt(5, startEndRecNumbers[1]);
			}else if(withIP.equalsIgnoreCase("N")){	
				pst.setString(1, replaceWithANDorORForallparams(convertToDblQuoteStr(id),convertToDblQuoteStr(title), convertToDblQuoteStr(composer), sqlFilterForAndOr));
				pst.setInt(2, startEndRecNumbers[0]);
				pst.setInt(3, startEndRecNumbers[1]);
			}
			
			//long queryStartTime=System.currentTimeMillis();
			rs=pst.executeQuery();
			while(rs.next()){
				if(withIP.equalsIgnoreCase("Y")){
					map=getMapObjForEachRowForSongDataWithIP(rs);
				}if(withIP.equalsIgnoreCase("N")){
					map=getMapObjForEachRowForSongDataWithoutIP(rs);
				}

				songData.add(map);
			}
			
			//logger.info("ApiDaoImpl : getSongsByCodeTitleAndComposer() : SQL query processing time : "+(System.currentTimeMillis()-queryStartTime));
		} catch (Exception ex) {
			logger.info("ApiDaoImpl : ERROR :getSongsByCodeTitleAndComposer() : message: "+RestApiConstants.SONG_ID_TITLE_COMPOSER_ERROR_MSG+id+":"+title+":"+composer);
			logger.info("ApiDaoImpl : ERROR :getSongsByCodeTitleAndComposer() : exception: "+ex.getMessage());
			throw new Exception(RestApiConstants.SONG_ID_TITLE_COMPOSER_ERROR_MSG+id+":"+title+":"+composer);
		} finally {

			closeConnections(rs,pst,conn);
		}
		logger.info("ApiDaoImpl : End :getSongsByCodeTitleAndComposer() songData size :"+songData.size());
		return songData;

	}


	/*
	 * This method is used to fetch songs data based on date range in between start and end dates from the db
	 */
	@Override
	public List<Map<String, Object>> getSongByDateRange(String amendedDate1,String amendedDate2, String withIP,int[] startEndRecNumbers) throws Exception{
		logger.info("ApiDaoImpl : Start :getSongByDateRange() :  amendedDate1 :"+amendedDate1+" : amendedDate2 :"+amendedDate2);
		logger.info("ApiDaoImpl : Start :getSongByDateRange() :  startEndRecNumbers[0] :"+startEndRecNumbers[0]+" : startEndRecNumbers[1] :"+startEndRecNumbers[1]);
		List<Map<String, Object>> songData = null;
		Connection conn=null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try {
			songData=new ArrayList<Map<String,Object>>();
			Map<String,Object> map=null;

			conn=getConnection();
			
			String sql=null;

			
			if(withIP.equalsIgnoreCase("Y")){
				sql=RestApiConstants.SQL_SONG_TIMESTAMP_WITH_IP_QUERY;
			}else if(withIP.equalsIgnoreCase("N")){	
				sql=RestApiConstants.SQL_SONG_TIMESTAMP_WITHOUT_IP_QUERY;
			}
			logger.info("ApiDaoImpl : getSongByDateRange() : Sql Query : "+sql);
			pst=conn.prepareStatement(sql);
			pst.setString(1, amendedDate1);
			pst.setString(2, amendedDate2);
			pst.setInt(3, startEndRecNumbers[0]);
			pst.setInt(4, startEndRecNumbers[1]);
			//long queryStartTime=System.currentTimeMillis();
			rs=pst.executeQuery();
			
			while(rs.next()){
				if(withIP.equalsIgnoreCase("Y")){
					map=getMapObjForEachRow(rs);
				}if(withIP.equalsIgnoreCase("N")){
					map=getMapObjForEachRowForDateAndAll(rs);
				}
				songData.add(map);
			}
			//logger.info("ApiDaoImpl : getSongByDateRange() : SQL query processing time : "+(System.currentTimeMillis()-queryStartTime));
		} catch (Exception ex) {
			logger.info("ApiDaoImpl : ERROR :getSongByDateRange() : message: "+RestApiConstants.SONG_DATE_RANGE_ERROR_MSG+amendedDate1+":"+amendedDate2);
			logger.info("ApiDaoImpl : ERROR :getSongByDateRange() : exception: "+ex.getMessage());
			throw new Exception(RestApiConstants.SONG_DATE_RANGE_ERROR_MSG+amendedDate1+":"+amendedDate2);
		} finally {
			closeConnections(rs,pst,conn);
		}
		logger.info("ApiDaoImpl : End :getSongByDateRange() songData size:"+songData.size());
		return songData;
	}


	/*
	 * This method is used to fetch  all songs data from the db
	 */
	@Override
	public List<Map<String, Object>> getAllSongs(String withIP,int[] startEndRecNumbers) throws Exception{
		logger.info("ApiDaoImpl : Start :getAllSongs() ");
		List<Map<String, Object>> songData = null;
		Connection conn=null;
		PreparedStatement pst=null;
		ResultSet rs=null;
		try {
			songData=new ArrayList<Map<String,Object>>();
			Map<String,Object> map=null;
			long timeStart=System.currentTimeMillis();
			logger.info("ApiDaoImpl : getAllSongs() : getConnection:  start :"+timeStart);
			conn=getConnection();
			logger.info("ApiDaoImpl : getAllSongs() : time taken for getting Connection : "+(System.currentTimeMillis()-timeStart));
		
			String sql=null;

			if(withIP.equalsIgnoreCase("Y")){
				sql=RestApiConstants.SQL_SONG_ALL_WITH_IP_QUERY;
			}else if(withIP.equalsIgnoreCase("N")){	
				sql=RestApiConstants.SQL_SONG_ALL_WITHOUT_IP_QUERY;
			}
			logger.info("ApiDaoImpl : getAllSongs() : Sql Query : "+sql);
			pst=conn.prepareStatement(sql);
			pst.setInt(1, startEndRecNumbers[0]);
			pst.setInt(2, startEndRecNumbers[1]);
			//long queryStartTime=System.currentTimeMillis();
			rs=pst.executeQuery();
			while(rs.next()){
				if(withIP.equalsIgnoreCase("Y")){
					map=getMapObjForEachRow(rs);
				}if(withIP.equalsIgnoreCase("N")){
					map=getMapObjForEachRowForDateAndAll(rs);
				}
				songData.add(map);
			}
			//logger.info("ApiDaoImpl : getAllSongs() : SQL query processing time : "+(System.currentTimeMillis()-queryStartTime));
		} catch (Exception ex) {
			logger.info("ApiDaoImpl : ERROR :getAllSongs() : message: "+RestApiConstants.SONG_ALL_ERROR_MSG);
			logger.info("ApiDaoImpl : ERROR :getAllSongs() : exception: "+ex.getMessage());
			ex.printStackTrace();
			throw new Exception(RestApiConstants.SONG_ALL_ERROR_MSG);
		} finally {

			closeConnections(rs,pst,conn);
		}
		logger.info("ApiDaoImpl : End :getAllSongs() :songData size :"+songData.size());
		return songData;
	}

	public Map<String, Object> getMapObjForEachRowForSongDataWithIP(ResultSet rs) throws Exception{
		Map<String, Object> map=null;
		//logger.info("ApiDaoImpl : Start :getMapObjForEachRow() ");
		try{
			map=new LinkedHashMap<String, Object>();
			map.put("DG_SONG_CODE", rs.getString(2));
			map.put("DG_SEQUENCE_NO", rs.getString(3));
			map.put("DG_IPS_NAME", rs.getString(4));
			map.put("DG_CHAIN_ID", rs.getString(5));
			map.put("DG_TERR_CODE", rs.getString(6));
			map.put("DG_ORIG_TERRITORY", rs.getString(7));
			map.put("DG_CAPACITY_CODE", rs.getString(8));
			map.put("DG_CONTROLLED", rs.getString(9));
			map.put("DG_MECH_OWNED", rs.getString(10));
			map.put("DG_PERF_OWNED", rs.getString(11));
			map.put("DG_SYNC_OWNED", rs.getString(12));
			map.put("DG_OTHER_OWNED", rs.getString(13));
			map.put("DG_IPS_SURNAME", rs.getString(14));
			map.put("DG_IPLINK_ID", rs.getString(15));
			map.put("DG_PARENT_IPLINK_ID", rs.getString(16));
			map.put("SNGA_SONG_TITLE", rs.getString(17));
			map.put("SNGA_SONG_TYPE", rs.getString(18));
			map.put("SNGA_ORIGINAL_SONG", rs.getString(19));
			map.put("SNGA_DATE_CREATED", rs.getString(20));
			map.put("SNGA_OWNING_SYS_TERR", rs.getString(21));
			map.put("SNGA_DATE_LOCAL_PUB", rs.getString(22));
			map.put("SNGA_FIRST_RECORDING_DATE", rs.getString(23));
			map.put("SNGA_LEAD_SHEET", rs.getString(24));
			map.put("SNGA_COMPOSERS", rs.getString(25));
			map.put("SNGA_DATE_ADDED", rs.getString(26));
			map.put("SNGA_ADDED_BY", rs.getString(27));
			map.put("SNGA_TIME_ADDED", rs.getString(28));
			map.put("SNGA_DATE_AMENDED", rs.getString(29));
			map.put("SNGA_AMENDED_BY", rs.getString(30));
			map.put("SNGA_TIME_AMENDED", rs.getString(31));
			map.put("SNGA_PRINCIPAL_ARTIST", rs.getString(32));
			map.put("SNGA_DELETE_IND", rs.getString(33));
			map.put("SNGA_SNGL_ALBUM_TITLE", rs.getString(34));
			map.put("SNGA_COUNTRY_CODE", rs.getString(35));
			map.put("SNGA_SONG_STATUS", rs.getString(36));
			map.put("SNGA_ISWC_CODE", rs.getString(37));
			map.put("SNGA_TERRITORY_CONTD", rs.getString(38));
			map.put("SNGA_LABEL_CODE", rs.getString(39));
			map.put("DG_COMPOSER_CODE", rs.getString(40));
			map.put("DG_TERS_TERRI_DESC", rs.getString(41));
			map.put("DG_COMA_CNAME1", rs.getString(42));
			map.put("DG_COMA_CNAME2", rs.getString(43));
			map.put("DG_IP_CODE_1", rs.getString(44));
			map.put("DG_IP_TYPE_1", rs.getString(45));
			map.put("DG_RECORD_LABELNAME", rs.getString(46));
			map.put("DG_TERRITORYCONTROLLEDNAME", rs.getString(47));
			
			
			
		} catch (Exception ex) {
			logger.info("ApiDaoImpl : ERROR :getMapObjForEachRowForSongDataWithIP() : message: "+ex.getMessage());
			throw new Exception(RestApiConstants.SONG_MAP_OBJ_ERROR_MSG);
		} 
		//logger.info("ApiDaoImpl : End :getMapObjForEachRow()");
		return map;
	}

	public Map<String, Object> getMapObjForEachRowForSongDataWithoutIP(ResultSet rs) throws Exception{
		Map<String, Object> map=null;
		//logger.info("ApiDaoImpl : Start :getMapObjForEachRow() ");
		try{
			map=new LinkedHashMap<String, Object>();
			map.put("DG_SONG_CODE", rs.getString(1));
			map.put("SNGA_OWNING_SYS_TERR", rs.getString(2));
			map.put("SNGA_SONG_TITLE", rs.getString(3));
			map.put("SNGA_ISWC_CODE", rs.getString(4));
			map.put("SNGA_COMPOSERS", rs.getString(5));
			map.put("SNGA_PRINCIPAL_ARTIST", rs.getString(6));
			map.put("SNGA_SNGL_ALBUM_TITLE", rs.getString(7));
			map.put("SNGA_SONG_STATUS", rs.getString(8));
			map.put("SNGA_DELETE_IND", rs.getString(9));
			map.put("SNGA_DATE_ADDED", rs.getString(10));
			map.put("SNGA_ADDED_BY", rs.getString(11));
			map.put("SNGA_DATE_AMENDED", rs.getString(12));
			map.put("SNGA_AMENDED_BY", rs.getString(13));
			map.put("SNGA_LABEL_CODE", rs.getString(14));
			map.put("SNGA_TERRITORY_CONTD", rs.getString(15));
			map.put("TERS_TERRI_DESC", rs.getString(16));
			/*map.put("DG_SONG_CODE", rs.getString("SNGA_SONG_CODE"));
			map.put("SNGA_OWNING_SYS_TERR", rs.getString("SNGA_OWNING_SYS_TERR"));
			map.put("SNGA_SONG_TITLE", rs.getString("SNGA_SONG_TITLE"));
			map.put("SNGA_ISWC_CODE", rs.getString("SNGA_ISWC_CODE"));
			map.put("SNGA_COMPOSERS", rs.getString("SNGA_COMPOSERS"));
			map.put("SNGA_PRINCIPAL_ARTIST", rs.getString("SNGA_PRINCIPAL_ARTIST"));
			map.put("SNGA_SNGL_ALBUM_TITLE", rs.getString("SNGA_SNGL_ALBUM_TITLE"));
			map.put("SNGA_SONG_STATUS", rs.getString("SNGA_SONG_STATUS"));
			map.put("SNGA_DELETE_IND", rs.getString("SNGA_DELETE_IND"));
			map.put("SNGA_DATE_ADDED", rs.getString("SNGA_DATE_ADDED"));
			map.put("SNGA_ADDED_BY", rs.getString("SNGA_ADDED_BY"));
			map.put("SNGA_DATE_AMENDED", rs.getString("SNGA_DATE_AMENDED"));
			map.put("SNGA_AMENDED_BY", rs.getString("SNGA_AMENDED_BY"));
			map.put("SNGA_LABEL_CODE", rs.getString("SNGA_LABEL_CODE"));*/
			
			
		} catch (Exception ex) {
			logger.info("ApiDaoImpl : ERROR :getMapObjForEachRow() : message: "+ex.getMessage());
			throw new Exception(RestApiConstants.SONG_MAP_OBJ_ERROR_MSG);
		} 
		//logger.info("ApiDaoImpl : End :getMapObjForEachRow()");
		return map;
	}

	public Map<String, Object> getMapObjForEachRow(ResultSet rs) throws Exception{
		Map<String, Object> map=null;
		//logger.info("ApiDaoImpl : Start :getMapObjForEachRow() ");
		try{
			//System.out.println("--again change--DG_SONG_CODE-->");
			map=new LinkedHashMap<String, Object>();
			map.put("DG_SONG_CODE", rs.getString(2));
			map.put("DG_SEQUENCE_NO", rs.getString(3));
			map.put("DG_IPS_NAME", rs.getString(4));
			map.put("DG_CHAIN_ID", rs.getString(5));
			map.put("DG_TERR_CODE", rs.getString(6));
			map.put("DG_ORIG_TERRITORY", rs.getString(7));
			map.put("DG_CAPACITY_CODE", rs.getString(8));
			map.put("DG_CONTROLLED", rs.getString(9));
			map.put("DG_MECH_OWNED", rs.getString(10));
			map.put("DG_PERF_OWNED", rs.getString(11));
			map.put("DG_SYNC_OWNED", rs.getString(12));
			map.put("DG_OTHER_OWNED", rs.getString(13));
			map.put("DG_IPS_SURNAME", rs.getString(14));
			map.put("DG_IPLINK_ID", rs.getString(15));
			map.put("DG_PARENT_IPLINK_ID", rs.getString(16));
			map.put("SNGA_SONG_TITLE", rs.getString(17));
			map.put("SNGA_SONG_TYPE", rs.getString(18));
			map.put("SNGA_ORIGINAL_SONG", rs.getString(19));
			map.put("SNGA_DATE_CREATED", rs.getString(20));
			map.put("SNGA_OWNING_SYS_TERR", rs.getString(21));
			map.put("SNGA_DATE_LOCAL_PUB", rs.getString(22));
			map.put("SNGA_FIRST_RECORDING_DATE", rs.getString(23));
			map.put("SNGA_LEAD_SHEET", rs.getString(24));
			map.put("SNGA_COMPOSERS", rs.getString(25));
			map.put("SNGA_DATE_ADDED", rs.getString(26));
			map.put("SNGA_ADDED_BY", rs.getString(27));
			map.put("SNGA_TIME_ADDED", rs.getString(28));
			map.put("SNGA_DATE_AMENDED", rs.getString(29));
			map.put("SNGA_AMENDED_BY", rs.getString(30));
			map.put("SNGA_TIME_AMENDED", rs.getString(31));
			map.put("SNGA_PRINCIPAL_ARTIST", rs.getString(32));
			map.put("SNGA_DELETE_IND", rs.getString(33));
			map.put("SNGA_SNGL_ALBUM_TITLE", rs.getString(34));
			map.put("SNGA_COUNTRY_CODE", rs.getString(35));
			map.put("SNGA_SONG_STATUS", rs.getString(36));
			map.put("SNGA_ISWC_CODE", rs.getString(37));
			map.put("SNGA_TERRITORY_CONTD", rs.getString(38));
			map.put("SNGA_LABEL_CODE", rs.getString(39));
			map.put("DG_COMPOSER_CODE", rs.getString(40));
			map.put("DG_TERS_TERRI_DESC", rs.getString(41));
			map.put("DG_COMA_CNAME1", rs.getString(42));
			map.put("DG_COMA_CNAME2", rs.getString(43));
			map.put("DG_IP_CODE_1", rs.getString(44));
			map.put("DG_IP_TYPE_1", rs.getString(45));
			map.put("DG_RECORD_LABELNAME", rs.getString(46));
			map.put("DG_TERRITORYCONTROLLEDNAME", rs.getString(47));
			

		} catch (Exception ex) {
			logger.info("ApiDaoImpl : ERROR :getMapObjForEachRow() : message: "+ex.getMessage());
			throw new Exception(RestApiConstants.SONG_MAP_OBJ_ERROR_MSG);
		}
		//logger.info("ApiDaoImpl : End :getMapObjForEachRow()");
		return map;
	}
	public Map<String, Object> getMapObjForEachRowForDateAndAll(ResultSet rs) throws Exception{
		Map<String, Object> map=null;
		//logger.info("ApiDaoImpl : Start :getMapObjForEachRow() ");
		try{
			
			map=new LinkedHashMap<String, Object>();
			map.put("DG_SONG_CODE", rs.getString("SNGA_SONG_CODE"));
			map.put("SNGA_OWNING_SYS_TERR", rs.getString("SNGA_OWNING_SYS_TERR"));
			map.put("SNGA_SONG_TITLE", rs.getString("SNGA_SONG_TITLE"));
			map.put("SNGA_ISWC_CODE", rs.getString("SNGA_ISWC_CODE"));
			map.put("SNGA_COMPOSERS", rs.getString("SNGA_COMPOSERS"));
			map.put("SNGA_PRINCIPAL_ARTIST", rs.getString("SNGA_PRINCIPAL_ARTIST"));
			map.put("SNGA_SNGL_ALBUM_TITLE", rs.getString("SNGA_SNGL_ALBUM_TITLE"));
			map.put("SNGA_SONG_STATUS", rs.getString("SNGA_SONG_STATUS"));
			map.put("SNGA_DELETE_IND", rs.getString("SNGA_DELETE_IND"));
			map.put("SNGA_DATE_ADDED", rs.getString("SNGA_DATE_ADDED"));
			map.put("SNGA_ADDED_BY", rs.getString("SNGA_ADDED_BY"));
			map.put("SNGA_DATE_AMENDED", rs.getString("SNGA_DATE_AMENDED"));
			map.put("SNGA_AMENDED_BY", rs.getString("SNGA_AMENDED_BY"));
			map.put("SNGA_LABEL_CODE", rs.getString("SNGA_LABEL_CODE"));
			map.put("SNGA_TERRITORY_CONTD", rs.getString("SNGA_TERRITORY_CONTD"));
			map.put("TERS_TERRI_DESC", rs.getString("TERS_TERRI_DESC"));
			
			
			
		} catch (Exception ex) {
			logger.info("ApiDaoImpl : ERROR :getMapObjForEachRow() : message: "+ex.getMessage());
			throw new Exception(RestApiConstants.SONG_MAP_OBJ_ERROR_MSG);
		}
		//logger.info("ApiDaoImpl : End :getMapObjForEachRow() map:size"+map.size());
		return map;
	}
	
	public Connection getConnection()throws Exception{
		Connection conn=null;
		try {
			conn=dataSource.getConnection();
			if(conn==null)
				throw new SQLException(RestApiConstants.DATASOURCE_NOT_AVAILABLE);
		}catch (Exception ex){
			throw new SQLException(RestApiConstants.DATASOURCE_NOT_AVAILABLE);
		}
		return conn;
	}
	public String getFulMatchForlike(String param){
		String full="%"+param+"%";
		return full;
	}
	
	public  String convertToDblQuoteStr(String param){
		String full=null;
		try{


			if(param.indexOf(" ")!= -1 || param.indexOf(",")!= -1 || param.indexOf(":")!= -1 || param.indexOf(";")!= -1 || param.indexOf(".")!= -1){

				if(param.indexOf(" ")!= -1){
					param= param.replaceAll(" ", "*\""+" AND "+"\"");
				} 
				

				if(param.indexOf(",")!= -1){
					param= param.replaceAll(",", "*\""+" AND "+"\"");
				}
				

				if(param.indexOf(":")!= -1){
					param= param.replaceAll(":", "*\""+" AND "+"\"");
				}
				

				if(param.indexOf(";")!= -1){
					param= param.replaceAll(";", "*\""+" AND "+"\"");
				}

				if(param.indexOf(".")!= -1){
					param= param.replaceAll(Pattern.quote("."), "*\""+" AND "+"\"");
				}
				full=param;
			
			} else {
				full=param;
			}
			full=("\""+full+"*\"");
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		return full;
	}

	
	public String replaceWithANDorORFortwoparams(String param1,String param2,String param){
		String retVal=null;
	
		if(param!=null && param.equalsIgnoreCase("AND"))
			retVal=param1+" AND "+param2;
		else if(param!=null && param.equalsIgnoreCase("OR"))
			retVal=param1+" OR "+param2;
		else 
			retVal=param1+" OR "+param2;
			
		return retVal;
	}
	public String replaceWithANDorORForallparams(String param1,String param2, String param3,String param){
		String retVal=null;
		if(param!=null && param.equalsIgnoreCase("AND"))
			retVal=param1+" AND "+param2+" AND "+param3;
		else if(param!=null && param.equalsIgnoreCase("OR"))
			retVal=param1+" OR "+param2+" OR "+param3;
		else 
			retVal=param1+" OR "+param2+" OR "+param3;
			
		return retVal;
	}
	public String replaceWithANDorOR(String str,String param){
		String retVal=null;
	
		if(param!=null && param.equalsIgnoreCase("AND"))
			retVal=str.replace("@", "AND");
		else if(param!=null && param.equalsIgnoreCase("OR"))
			retVal=str.replace("@", "OR");
		else 
			retVal=str.replace("@", "OR");
			
		return retVal;
	}
	protected void closeConnections(ResultSet rs,PreparedStatement pstmt,Connection conn){

		if(null != rs)
			try{
				rs.close();
			}
		catch(SQLException sqe){
			//	sqe.printStackTrace();
		}
		if(null != pstmt)
			try{
				pstmt.close();
			}
		catch(SQLException sqe){
			//sqe.printStackTrace();
		}
		if(null != conn)
			try{
				conn.close();
			}
		catch(SQLException sqe){
			//sqe.printStackTrace();
		}
	}
	
	
}
