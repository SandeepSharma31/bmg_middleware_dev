package bmg.esb.song.imData.api.impl;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import bmg.esb.song.imData.api.bo.SongApiBO;
import bmg.esb.song.imData.api.dao.ApiDaoImpl;
import bmg.esb.song.imData.api.literals.RestApiConstants;


@Path("/songs/data")
public class SongApi {

	public static Logger logger = Logger.getLogger(SongApi.class.getName());

	/*
	 * This method is used to get Json songs data based on song code
	 */

	@GET
	@Path("/code")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByCode(@Context  UriInfo info) throws Exception{

		logger.info("SongApi : Start :getSongsByCode()");
		String responseStr=null;
		SongApiBO songBo=null;
		int status=0;
	
		try{

			List<String> jsonStr=null;
			String  song_code=info.getQueryParameters(). getFirst("code");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");
			
			
			logger.info("API Version:2.9.0");
			
			logger.info("SongApi : getSongsByCode() :Input params: song_code:"+song_code+": withIP:"+withIP);
			songBo=new SongApiBO();

			if(song_code!=null && !song_code.equals("")){

				String[] perpage=getPageAndPerPageVals(page,per_page);
				
				logger.info("id: "+song_code+" :page: "+page+" :per_page: "+per_page+" :withIP: "+withIP);
				jsonStr=songBo.getSongsJsonByID(song_code,perpage[0],perpage[1],getWithIPStrVal(withIP),apiDaoImpl);
				responseStr=jsonStr.toString();
				status=RestApiConstants.success;

			} else {
				
				responseStr =errorJson(RestApiConstants.BAD_REQUEST_MSG);
				status=RestApiConstants.fail;
				logger.error("SongApiBO : ERROR :getSongsByCode() : message: "+responseStr);
			}

		}catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByCode() : message: "+ex.getMessage());
			status=RestApiConstants.fail;
			responseStr = errorJson(ex.getMessage());
		}
		//logger.info("SongApi : End :getSongsByCode() : responseStr: "+responseStr);
		return Response.status(status).type("application/json").entity(responseStr).build();
	}

	/*
	 * This method is used to get Json songs data based on song title
	 */

	@GET
	@Path("/title")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByTitle(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getSongsByTitle()");
		String responseStr=null;
		SongApiBO songBo=null;
		Map<String, String> map =null;
		int status=0;
		try{
			List<String> jsonStr=null;
			String  title=info.getQueryParameters(). getFirst("title");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");

			logger.info("SongApi : getSongsByTitle() :Input params: title:"+title+": withIP:"+withIP);
			songBo=new SongApiBO();
			
			if(title!=null && !title.equals("")){
				
				String[] perpage=getPageAndPerPageVals(page,per_page);
				logger.info("title: "+title+" :page: "+page+" :per_page: "+per_page+" :withIP: "+withIP);
				jsonStr=songBo.getSongsJsonByTitle(title,perpage[0],perpage[1],getWithIPStrVal(withIP),apiDaoImpl);
				responseStr=jsonStr.toString();
				status=RestApiConstants.success;
			} else {
			
				responseStr =errorJson(RestApiConstants.BAD_REQUEST_MSG);
				status=RestApiConstants.fail;
				logger.error("SongApi : ERROR :getSongsByTitle() : message: "+responseStr);
			}

		}catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByCode() : message: "+ex.getMessage());
			status=RestApiConstants.fail;
			responseStr = errorJson(ex.getMessage());
		}
		//logger.info("SongApi : End :getSongsByTitle() : responseStr: "+responseStr);
		return Response.status(status).type("application/json").entity(responseStr).build();
	}


	/*
	 * This method is used to get Json songs data based on song composer
	 */

	@GET
	@Path("/composer")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByComposer(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getSongsByComposer()");
		String responseStr=null;
		SongApiBO songBo=null;
		Map<String, String> map = null;
		int status=0;
		try{
			List<String> jsonStr=null;
			String  composer=info.getQueryParameters(). getFirst("composer");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");

			logger.info("SongApi : getSongsByComposer() :Input params: composer:"+composer+": withIP:"+withIP);
			songBo=new SongApiBO();
			
			if(composer!=null && !composer.equals("")){

				String[] perpage=getPageAndPerPageVals(page,per_page);
				
				jsonStr=songBo.getSongsJsonByComposer(composer,perpage[0],perpage[1],getWithIPStrVal(withIP),apiDaoImpl);
				responseStr=jsonStr.toString();
				status=RestApiConstants.success;
			} else {
				responseStr =errorJson( RestApiConstants.BAD_REQUEST_MSG);
				status=400;
				logger.error("SongApi : ERROR :getSongsByTitle() : message: "+responseStr);
			}

		}catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByComposer() : message: "+ex.getMessage());
			status=RestApiConstants.fail;
			responseStr = errorJson( ex.getMessage());
		}
		//logger.info("SongApi : End :getSongsByComposer() : responseStr: "+responseStr);
		return Response.status(status).type("application/json").entity(responseStr).build();
	}

	/*
	 * This method is used to get Json songs data based on song code and title
	 */

	@GET
	@Path("/codeAndTitle")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByCodeAndTitle(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getSongsByCodeAndTitle()");
		String responseStr=null;
		SongApiBO songBo=null;
		Map<String, String> map = null;
		int status=0;
		try{
			List<String> jsonStr=null;
			String  code=info.getQueryParameters(). getFirst("code");
			String  title=info.getQueryParameters(). getFirst("title");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");
			String  filter=info.getQueryParameters().getFirst("filter");

			logger.info("SongApi : getSongsByCodeAndTitle() :Input params: code:"+code+": title:"+title+": filter: "+filter+": withIP:"+withIP);
			songBo=new SongApiBO();
			
			if((code!=null && !code.equals("")) && (title!=null && !title.equals("")) ){

				String[] perpage=getPageAndPerPageVals(page,per_page);

				jsonStr=songBo.getSongsJsonByIdAndTitle(code,title,perpage[0],perpage[1],getWithIPStrVal(withIP),filter,apiDaoImpl);
				responseStr=jsonStr.toString();
				status=RestApiConstants.success;
			} else {
				
				responseStr =errorJson( RestApiConstants.BAD_REQUEST_MSG);
				status=RestApiConstants.fail;
				logger.error("SongApi : ERROR :getSongsByCodeAndTitle() : message: "+responseStr);
			}

		}catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByCodeAndTitle() : message: "+ex.getMessage());
			status=RestApiConstants.fail;
			
			responseStr = errorJson(ex.getMessage());
		}
		//logger.info("SongApi : End :getSongsByCodeAndTitle() : responseStr: "+responseStr);
		return Response.status(status).type("application/json").entity(responseStr).build();
	}

	/*
	 * This method is used to get Json songs data based on song code and composer
	 */

	@GET
	@Path("/codeAndComposer")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByCodeAndComposer(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getSongsByCodeAndComposer()");
		String responseStr=null;
		SongApiBO songBo=null;
		int status=0;
		Map<String, String> map = null;
		try{
			List<String> jsonStr=null;
			String  code =info.getQueryParameters(). getFirst("code");
			String  composer =info.getQueryParameters(). getFirst("composer");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");
			String  filter=info.getQueryParameters().getFirst("filter");
			
			logger.info("SongApi : getSongsByCodeAndComposer() :Input params: code:"+code+":composer"+composer+": withIP:"+withIP);
			songBo=new SongApiBO();
			
			
			if((code!=null && !code.equals("")) && (composer!=null && !composer.equals("")) ){

				String[] perpage=getPageAndPerPageVals(page,per_page);

				jsonStr=songBo.getSongsJsonByIdAndComposer(code,composer,perpage[0],perpage[1],getWithIPStrVal(withIP),filter,apiDaoImpl);
				responseStr=jsonStr.toString();
				status=RestApiConstants.success;
			} else {
				
				responseStr =errorJson( RestApiConstants.BAD_REQUEST_MSG);
				status=RestApiConstants.fail;
				logger.error("SongApi : ERROR :getSongsByCodeAndComposer() : message: "+responseStr);
			}

		}catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByCodeAndComposer() : message: "+ex.getMessage());
			status=RestApiConstants.fail;
			
			responseStr = errorJson( ex.getMessage());
		}
		//logger.info("SongApi : End :getSongsByCodeAndComposer() : responseStr: "+responseStr);
		return Response.status(status).type("application/json").entity(responseStr).build();
	}


	/*
	 * This method is used to get Json songs data based on title and composer
	 */

	@GET
	@Path("/titleAndComposer")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByTitleAndComposer(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getSongsByTitleAndComposer()");
		String responseStr=null;
		SongApiBO songBo=null;
		int status=0;
		Map<String, String> map = null;
		try{
			List<String> jsonStr=null;
			String  title =info.getQueryParameters(). getFirst("title");
			String  composer =info.getQueryParameters(). getFirst("composer");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");
			String  filter=info.getQueryParameters().getFirst("filter");
			
			logger.info("SongApi : getSongsByTitleAndComposer() :Input params: title:"+title+":composer"+composer+": withIP:"+withIP);
			songBo=new SongApiBO();
			
			
			if((title!=null && !title.equals("")) && (composer!=null && !composer.equals("")) ){

				String[] perpage=getPageAndPerPageVals(page,per_page);

				jsonStr=songBo.getSongsJsonByTitleAndComposer(title,composer,perpage[0],perpage[1],getWithIPStrVal(withIP),filter,apiDaoImpl);
				responseStr=jsonStr.toString();
				status=RestApiConstants.success;
			} else {
				
				responseStr =errorJson( RestApiConstants.BAD_REQUEST_MSG);
				status=RestApiConstants.fail;
				logger.error("SongApi : ERROR :getSongsByTitle() : message: "+responseStr);
			}

		}catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByTitleAndComposer() : message: "+ex.getMessage());
			status=RestApiConstants.fail;
			
			responseStr = errorJson( ex.getMessage());
		}
		//logger.info("SongApi : End :getSongsByTitleAndComposer() : responseStr: "+responseStr);
		return Response.status(status).type("application/json").entity(responseStr).build();
	}

	/*
	 * This method is used to get Json songs data based on song code,title and composer
	 */

	@GET
	@Path("/codeTitleAndComposer")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByCodeTitleAndComposer(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getSongsByCodeTitleAndComposer()");
		String responseStr=null;
		SongApiBO songBo=null;
		int status=0;
		Map<String, String> map = null;
		try{
			List<String> jsonStr=null;
			String  code =info.getQueryParameters(). getFirst("code");
			String  title =info.getQueryParameters(). getFirst("title");
			String  composer =info.getQueryParameters(). getFirst("composer");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");
			String  filter=info.getQueryParameters().getFirst("filter");
			
			logger.info("SongApi : getSongsByCodeTitleAndComposer() :Input params: code:"+code+":title:"+title+":composer"+composer+": withIP:"+withIP);
			songBo=new SongApiBO();
			
			
			if((code!=null && !code.equals("")) &&(title!=null && !title.equals("")) && (composer!=null && !composer.equals("")) ){

				String[] perpage=getPageAndPerPageVals(page,per_page);

				jsonStr=songBo.getSongsJsonByCodeTitleAndComposer(code,title,composer,perpage[0],perpage[1],getWithIPStrVal(withIP),filter,apiDaoImpl);
				responseStr=jsonStr.toString();
				status=RestApiConstants.success;
			} else {
				
				responseStr =errorJson( RestApiConstants.BAD_REQUEST_MSG);
				status=RestApiConstants.fail;
				logger.error("SongApi : ERROR :getSongsByCodeTitleAndComposer() : message: "+responseStr);
			}

		}catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByCodeTitleAndComposer() : message: "+ex.getMessage());
			status=RestApiConstants.fail;
			new LinkedHashMap<String, String>();
			responseStr = errorJson( ex.getMessage());
		}
		//logger.info("SongApi : End :getSongsByCodeTitleAndComposer() : responseStr: "+responseStr);
		return Response.status(status).type("application/json").entity(responseStr).build();
	}

	/*
	 * This method is used to get Json songs data based on date range - in between start and end dates
	 */

	@GET
	@Path("/date")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getSongsByDateRange(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getSongsByDateRange()");
		String responseStr=null;
		SongApiBO songBo=null;
		Map<String, String> map = null;
		int status=0;
		try{
			List<String> jsonStr=null;

			String  start=info.getQueryParameters().getFirst("startdate");
			String  end=info.getQueryParameters().getFirst("enddate");
			String  withIP=info.getQueryParameters(). getFirst("withIP");
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");

			logger.info("SongApi : Start :getSongsByDateRange() : Inuputs: start :"+": end :"+end+" : page :"+page+" : per_page :"+per_page+": withIP:"+withIP);
			songBo=new SongApiBO();
			if((start!=null && !start.equals("")) &&  (end!=null && !end.equals(""))){

				String[] perpage=getPageAndPerPageVals(page,per_page);
			
				logger.info("SongApi : Start in IF :getSongsByDateRange() : Inuputs: start :"+": end :"+end+" : page :"+page+" : per_page :"+per_page);
				jsonStr=songBo.getSongsJsonByDateRange(start,end,perpage[0],perpage[1],getWithIPStrVal(withIP),apiDaoImpl);
				responseStr=jsonStr.toString();
				status=RestApiConstants.success;
			} else {
				
				responseStr =errorJson( RestApiConstants.BAD_REQUEST_MSG);
				status=RestApiConstants.fail;
				logger.error("SongApi : ERROR :getSongsByDateRange() : message: "+responseStr);
			}

		} catch(Exception ex){
			logger.info("SongApi : ERROR :getSongsByDateRange() : message: "+ex.getMessage());
			status=RestApiConstants.fail;
			new LinkedHashMap<String, String>();
			responseStr = errorJson( ex.getMessage());

		}
		//logger.info("SongApi : End :getSongsByDateRange() : responseStr: "+responseStr);
		return Response.status(status).type("application/json").entity(responseStr).build();
	}


	/*
	 * This method is used to get all Json songs data 
	 */

	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)

	public Response getAllSongs(@Context  UriInfo info) throws Exception{
		logger.info("SongApi : Start :getAllSongs()");
		String responseStr=null;
		SongApiBO songBo=null;
		int status=0;
		Map<String, String> map = null;
		try{
			List<String> jsonStr=null;
			songBo=new SongApiBO();
			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");
			String  withIP=info.getQueryParameters(). getFirst("withIP");

			
			logger.info("SongApi : Start :getSongsByDateRange() : Inuputs:page :"+page+" : per_page :"+per_page+": withIP:"+withIP);
			
			
			if( (page!=null && !page.equals("0")) && (per_page!=null && !per_page.equals("0"))){

				jsonStr=songBo.getAllSongsJson(page,per_page,getWithIPStrVal(withIP),apiDaoImpl);
			} else {
				page=RestApiConstants.page;
				per_page=RestApiConstants.per_page;
				jsonStr=songBo.getAllSongsJson(page,per_page,getWithIPStrVal(withIP),apiDaoImpl);
			} 
				responseStr=jsonStr.toString();
				status=RestApiConstants.success;

		} catch(Exception ex){
			logger.info("SongApi : ERROR :getAllSongs() : message: "+ex.getMessage());
			status=RestApiConstants.fail;
			responseStr = errorJson( ex.getMessage());

		}
		//logger.info("SongApi : End :getAllSongs() : responseStr: "+responseStr);
		return Response.status(status).type("application/json").entity(responseStr).build();
	}

	public String getWithIPStrVal(String withIP){
		if(withIP!=null && !withIP.equals("") && withIP.equalsIgnoreCase("Y")){

		}else {
			withIP="N";
		}
		return withIP;
	}
	public String[] getPageAndPerPageVals(String page,String per_page ){
		String[] strArr=new String[2];
		if((page!=null && !page.equals("0")) && (per_page!=null && !per_page.equals("0"))){
		} else{
			page=RestApiConstants.page;
			per_page=RestApiConstants.per_page;
		}
		strArr[0]=page;
		strArr[1]=per_page;
		return strArr;
	}
	public String errorJson(String erroStr){
		Map<String, String> mapp =new LinkedHashMap<String, String>();
		Gson gson = new Gson();
		mapp.put("errors", erroStr);
		return gson.toJson(mapp);
	}
	public ApiDaoImpl apiDaoImpl;

	public ApiDaoImpl getApiDaoImpl() {
		return apiDaoImpl;
	}

	public void setApiDaoImpl(ApiDaoImpl apiDaoImpl) {
		this.apiDaoImpl = apiDaoImpl;
	}

}
