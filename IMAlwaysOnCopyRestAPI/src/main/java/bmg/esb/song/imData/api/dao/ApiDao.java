package bmg.esb.song.imData.api.dao;

import java.util.List;
import java.util.Map;



public interface ApiDao {
	
	public abstract List<Map<String, Object>> getSongsById(String songId,String withIP,int[] startEndRecNumbers) throws Exception;
	public abstract List<Map<String, Object>> getSongsByTitle(String songTitle,String withIP,int[] startEndRecNumbers) throws Exception;
	public abstract List<Map<String, Object>> getSongsByComposer(String songComposer,String withIP,int[] startEndRecNumbers) throws Exception;
	public abstract List<Map<String, Object>> getSongsByIdAndTitle(String songId,String title,String withIP,int[] startEndRecNumbers,String sqlFilterForAndOr) throws Exception;
	public abstract List<Map<String, Object>> getSongsByIdAndComposer(String songId,String composer,String withIP,int[] startEndRecNumbers,String sqlFilterForAndOr) throws Exception;
	public abstract List<Map<String, Object>> getSongsByTitleAndComposer(String title,String composer,String withIP,int[] startEndRecNumbers,String sqlFilterForAndOr) throws Exception;
	public abstract List<Map<String, Object>> getSongsByCodeTitleAndComposer(String songId,String title,String composer,String withIP,int[] startEndRecNumbers,String sqlFilterForAndOr) throws Exception;
	public abstract List<Map<String, Object>> getSongByDateRange(String amendedDate1,String amendedDate2,String withIP,int[] startEndRecNumbers) throws Exception;
	public abstract List<Map<String, Object>> getAllSongs(String withIP,int[] startEndRecNumbers) throws Exception;


}
