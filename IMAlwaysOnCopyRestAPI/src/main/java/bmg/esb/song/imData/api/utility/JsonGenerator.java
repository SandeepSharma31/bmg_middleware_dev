package bmg.esb.song.imData.api.utility;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import bmg.esb.song.imData.api.beans.IPData;
import bmg.esb.song.imData.api.beans.SongData;
import bmg.esb.song.imData.api.bo.SongApiBO;


public class JsonGenerator {

	public static Logger logger = Logger.getLogger(JsonGenerator.class.getName());
	
	public List<String> generateJsonObject(List<Map<String, Object>> inputList,String withIP)
	{


		ArrayList<String> lRet = new ArrayList<String>();

		try{
			List<SongData> songData=CreateSongDatawithHierarchy(inputList,withIP);

			logger.info("songData size::"+songData.size());
			
			for(SongData songdata:songData){
				lRet.add(convertDbToJSon(songdata,withIP));
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return lRet;

	}

	public List<SongData> CreateSongDatawithHierarchy(List<Map<String, Object>> inputList,String withIP)
	{


		ArrayList<SongData> alRet = new ArrayList<SongData>();
		try{

			ArrayList<IPData> alIPs = new ArrayList<IPData>();
			for (Map<String, Object> inputMap : inputList)
			{
				IPData ipData = null;
				SongData sd = null;
				if ((inputMap != null))
				{
					DecimalFormat format=new DecimalFormat(); 
					format.setParseBigDecimal(true);

					//System.out.println("---inputMap.get(DG_SONG_CODE)--->"+inputMap.get("DG_SONG_CODE"));
					//int iSongCode = ((BigDecimal) inputMap.get("DG_SONG_CODE")).intValue();
					BigDecimal bigDecmal = (BigDecimal)format.parseObject((String)inputMap.get("DG_SONG_CODE"));
					int iSongCode =bigDecmal.intValue();
					sd = getSongByCode(iSongCode, alRet, inputMap,withIP);
					//one songcode, new list, one row

					if(withIP.equalsIgnoreCase("Y")){
						ipData = getIPData(inputMap, alIPs);
					}else {
						ipData =null;
					}
					
					
				}
			}
			//10 song ojbects+ip objects
			createIPHierarchy(alIPs);
			
			if(withIP.equalsIgnoreCase("Y")){
				//eliminate duplicate songs
				assignIPDataToSong(alRet, alIPs);
			}else {
				assignIPToSong(alRet, alIPs);
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return alRet;
	}

	private SongData getSongByCode(int iSongCode, ArrayList<SongData> alRet, Map<String, Object> inputMap,String withIP) throws ParseException
	{
		boolean bFound = false;
		SongData sdNew=null;
		SongData sdRet = null;

		try {
			/*for (SongData sd : alRet)
		{
			System.out.println("iSongCode-->"+iSongCode);
			System.out.println("sd.getCode()-->"+sd.getCode());
			if (sd.getCode() == iSongCode)
			{
				sdRet = sd;
				bFound = true;
				break;
			}
		}*/
			//if (!bFound)
			//{
			String str = "";
			sdNew = new SongData();
			/*Integer i = (Integer) inputMap.get("SNGA_ID");
   				if (i != null) sdNew.setSongId(i);*/
			
			
			DecimalFormat format=new DecimalFormat(); 
			format.setParseBigDecimal(true);

			BigDecimal bd = (BigDecimal)format.parseObject((String)inputMap.get("DG_SONG_CODE"));


			//BigDecimal bd = (BigDecimal) inputMap.get("DG_SONG_CODE");
			if (bd != null) sdNew.setCode(bd.intValue());
			str = (String) inputMap.get("SNGA_OWNING_SYS_TERR");
			if (str != null)
				sdNew.setSite(str);
			else
				sdNew.setSite("");
			str = (String) inputMap.get("SNGA_SONG_TITLE");
			if (str != null)
				sdNew.setTitle(str);
			else
				sdNew.setTitle("");
			str = (String) inputMap.get("SNGA_ISWC_CODE");
			if (str != null)
				sdNew.setIswc((String) inputMap.get("SNGA_ISWC_CODE"));
			else
				sdNew.setIswc("");
			
			
			if(withIP.equalsIgnoreCase("Y")){
				str = (String) inputMap.get("DG_TERRITORYCONTROLLEDNAME");
			}else if(withIP.equalsIgnoreCase("N")){	
				str = (String) inputMap.get("TERS_TERRI_DESC");
			}
			
			if (str != null)
				sdNew.setTerritoryName(str);
			else
				sdNew.setTerritoryName("");
			
			str = (String) inputMap.get("SNGA_TERRITORY_CONTD");
			if (str != null)
				sdNew.setTerritoryCode(str);
			else
				sdNew.setTerritoryCode("");
			str = (String) inputMap.get("SNGA_COMPOSERS");
			if (str != null)
				sdNew.setComposers((str));
			else
				sdNew.setComposers("");
			str = (String) inputMap.get("SNGA_PRINCIPAL_ARTIST");
			if (str != null)
				sdNew.setPrincipalArtist(str);
			else
				sdNew.setPrincipalArtist("");
			str = (String) inputMap.get("SNGA_SNGL_ALBUM_TITLE");
			if (str != null)
				sdNew.setAlbumTitle(str);
			else
				sdNew.setAlbumTitle("");
			str = (String) inputMap.get("SNGA_LABEL_CODE");
			if (str != null)
				sdNew.setRecordLabel(str);
			else
				sdNew.setRecordLabel("");
			str = (String) inputMap.get("DG_RECORD_LABELNAME");
			if (str != null)
				sdNew.setRecordLabelName(str);
			else
				sdNew.setRecordLabelName("");
			str = (String) inputMap.get("SNGA_SONG_STATUS");
			if (str != null)
				sdNew.setStatus(str);
			else
				sdNew.setStatus("");

			//DecimalFormat formatDate=new DecimalFormat(); 
			//format.setParseBigDecimal(true);
			//System.out.println("date added-->"+(String)inputMap.get("SNGA_DATE_ADDED"));
			str = (String) inputMap.get("SNGA_DATE_ADDED");
			if (str != null)
				sdNew.setAdded(str);
			else
				sdNew.setAdded("");
			//BigDecimal b = (BigDecimal)format.parseObject((String)inputMap.get("SNGA_DATE_ADDED"));
			//BigDecimal b = (BigDecimal) inputMap.get("SNGA_DATE_ADDED");\
			//System.out.println("b-->"+b);
			/*if (b != null && !b.equals(0))
			{
				//Date dateAdded = new Date(b.longValue());
				System.out.println("dateAdded-->"+b);
				sdNew.setAdded(b);
			}*/
			str = (String) inputMap.get("SNGA_ADDED_BY");
			if (str != null)
				sdNew.setAddedby(str);
			else
				sdNew.setAddedby("");
			/*b=  (BigDecimal)format.parseObject((String)inputMap.get("SNGA_DATE_AMENDED"));
			//b = (BigDecimal) inputMap.get("SNGA_DATE_AMENDED");
			if (b != null && !b.equals(0))
			{
				Date dateAmended = new Date(b.longValue());
				sdNew.setAmended(dateAmended);
			}*/
			
			str=(String)inputMap.get("SNGA_DATE_AMENDED");
			if (str != null)
				sdNew.setAmended(str);
			else
				sdNew.setAmended("");
			
			str = (String) inputMap.get("SNGA_AMENDED_BY"); 
			if (str != null)
				sdNew.setAmendedby(str);
			else
				sdNew.setAmendedby("");
			str = (String) inputMap.get("SNGA_DELETE_IND");
			if (str != null)
			{
				if (str == "t")
					str = "true";
				else
					str = "false";
				sdNew.setDeleted(str);
			}
			else
				sdNew.setDeleted("");
			str = (String) inputMap.get("CHANGE_IND");
			if (str != null)
				sdNew.setChangeIndicator(str);
			else
				sdNew.setChangeIndicator("");
			sdRet = sdNew;
			alRet.add(sdNew);
			//}
		}catch(Exception ex){

		}
		return sdRet;
	}
	public String convertDbToJSon(SongData allData,String withIP)
	{
		ArrayList<String> lRet = new ArrayList<String>();
		Map<String, Object> songData = new HashMap<String, Object>();
		String json=null;
		try
		{
			//Create a new Map to form a JSon object. Using LinkedHashMap ensures that the objects are added as FIFO, so that type and changeIndicator appear data in the JSon output
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			map.put("type", "Song");
			map.put("changeindicator", "U");
			map.put("data", creatSongeDataMapping(allData,withIP));
			Gson gson = new Gson();
			json = gson.toJson(map);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		//System.out.println("-----end CreateSongDatawithHierarchy()-->");
		return json;

	}


	//This method will create a new Map which be passed to the GSon to form a JSon object
	//private Map<String, Object> creatSongeDataMapping(SongData allData)
	private Map<String, Object> creatSongeDataMapping(SongData songInfo,String withIP)
	{
		Map<String, Object> songData = new HashMap<String, Object>();
		
		songData.put("code", songInfo.getCode());
		//songData.put("site", songInfo.getSite());
		songData.put("originatingSystem", songInfo.getSite());
		songData.put("title", songInfo.getTitle());
		songData.put("iswc", songInfo.getIswc());
		//songData.put("territory", songInfo.getTerritory());
		songData.put("territoryControlledName", songInfo.getTerritoryName());
		songData.put("territoryControlledCode", songInfo.getTerritoryCode());
		songData.put("composers", songInfo.getComposers());
		songData.put("principalArtist", songInfo.getPrincipalArtist());
		songData.put("albumTitle", songInfo.getAlbumTitle());
		songData.put("recordLabelCode", songInfo.getRecordLabel());
		songData.put("recordLabelName", songInfo.getRecordLabelName());
		songData.put("status", songInfo.getStatus());
		songData.put("added", songInfo.getAdded());
		songData.put("addedby", songInfo.getAddedby());
		songData.put("amended", songInfo.getAmended());
		songData.put("amendedby", songInfo.getAmendedby());
		if(withIP.equalsIgnoreCase("Y")){
			songData.put("ipData", songInfo.getIpData());
		}else {
			songData.put("ipData", "");
		}
		



		return songData;
	}
	//Fetch data from db and create a hash map with column names and column data


	private void assignIPToSong(ArrayList<SongData> alRet, ArrayList<IPData> alIPs)
	{
		try
		{
			for (SongData sd : alRet)
			{
				ArrayList<IPData> ipData = new ArrayList<IPData>();
				for (IPData ip : alIPs)
				{
					
					if (sd.getCode() == ip.getCode() && !ip.isChild() && ip.getParentIpLink().equals("0"))
					{
						ipData.add(ip);
						//sd.setIp(ip);
					}
					
				}
				
				sd.setIpData(ipData);
			}
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());

		}
	}
	
	
	private void assignIPDataToSong(ArrayList<SongData> alRet, ArrayList<IPData> alIPs)
	{
		try
		{	
			
			boolean ipCheck=false;
			//ArrayList<SongData> songwithIpList=new ArrayList<SongData>();
			logger.info("start:alRet: size:: "+alRet.size());
			for (SongData sd : alRet)
			{
				
				ArrayList<IPData> ipData = new ArrayList<IPData>();
				for (IPData ip : alIPs)
				{
					
					if (sd.getCode() == ip.getCode() && !ip.isChild() && ip.getParentIpLink().equals("0"))
					{
						ipData.add(ip);
						//sd.setIp(ip);
					}
					ipCheck=true;
				}
				
				if(ipCheck){
					sd.setIpData(ipData);
					alRet.clear();
					alRet.add(sd);
					break;
				}
					
				
			}
			logger.info("end:alRet: size:: "+alRet.size());
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());

		}
	}
	private void createIPHierarchy(ArrayList<IPData> alIPs)
	{
		try
		{
			for (IPData ip : alIPs)
			{
				int songCode = ip.getCode();
				String strIPLink = ip.getIpLink();
				if (strIPLink != null) ip.setChildren(getIPDataByParentIPLink(strIPLink, alIPs, songCode));
			}
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
		}
	}
	private IPData[] getIPDataByParentIPLink(String strParentIPLink, ArrayList<IPData> alIPs, int songCode)
	{
		ArrayList<IPData> alRetIPs = new ArrayList<IPData>();
		IPData[] aIPs = new IPData[1];
		try
		{
			for (IPData ip : alIPs)
			{
				if ((songCode == ip.getCode()) && strParentIPLink.equals(ip.getParentIpLink()))
				{
					ip.setChild(true);
					alRetIPs.add(ip);
				}
			}
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());
		}
		return alRetIPs.toArray(aIPs);
	}
	private IPData getIPData(Map<String, Object> inputMap, ArrayList<IPData> alIPs)
	{
		BigDecimal bd = null;
		String str = "";
		IPData ip = new IPData();
		try
		{
			DecimalFormat format=new DecimalFormat(); 
			format.setParseBigDecimal(true);

			bd = (BigDecimal)format.parseObject((String)inputMap.get("DG_SONG_CODE"));
			//bd = (BigDecimal) inputMap.get("DG_SONG_CODE");
			if (bd != null)
				ip.setCode(bd.intValue());
			else
				ip.setCode(0);
			/*
			bd = (BigDecimal)format.parseObject((String)inputMap.get("DG_COMPOSER_CODE"));
			//bd = (BigDecimal) inputMap.get("DG_SONG_CODE");
			if (bd != null)
				ip.setComposerCode(bd.intValue());
			else
				ip.setComposerCode(0);*/
			bd = (BigDecimal)format.parseObject((String)inputMap.get("DG_IP_CODE_1"));
			//bd = (BigDecimal) inputMap.get("DG_SONG_CODE");
			if (bd != null)
				ip.setIpCode(bd.intValue());
			else
				ip.setIpCode(0);
			
			bd = (BigDecimal)format.parseObject((String)inputMap.get("DG_SEQUENCE_NO"));
			//bd = (BigDecimal) inputMap.get("DG_SEQUENCE_NO");
			if (bd != null)
				ip.setSequence(bd.longValue());
			else
				ip.setSequence(0);
			str = (String) inputMap.get("DG_IPS_NAME");
			if (str != null)
				ip.setName(str);
			else
				ip.setName("");
			if (bd != null) ip.setChainId(bd.longValue());
			str = (String) inputMap.get("DG_TERR_CODE");
			if (str != null)
				ip.setTerritory(str);
			else
				ip.setTerritory("");
			
			str = (String) inputMap.get("DG_TERS_TERRI_DESC");
			if (str != null)
				ip.setTerritoryName(str);
			else
				ip.setTerritoryName("");
			/*str = (String) inputMap.get("_TERR_NAME");
   if (str != null)
    ip.setTerritoryName(str);
   else
    ip.setTerritoryName("");*/
			str = (String) inputMap.get("DG_ORIG_TERRITORY");
			if (str != null)
				ip.setOriginTerritory(str);
			else
				ip.setOriginTerritory("");
			str = (String) inputMap.get("DG_CAPACITY_CODE");
			if (str != null)
				ip.setCapacityCode(str);
			else
				ip.setCapacityCode("");
			str = (String) inputMap.get("DG_CONTROLLED");
			if (str != null)
				ip.setControlled(str);
			else
				ip.setControlled("");
			bd = (BigDecimal)format.parseObject((String)inputMap.get("DG_MECH_OWNED"));
			//bd = (BigDecimal) inputMap.get("DG_MECH_OWNED");
			if (bd != null) ip.setMechOwned(bd.floatValue());
			bd = (BigDecimal)format.parseObject((String)inputMap.get("DG_PERF_OWNED"));
			//bd = (BigDecimal) inputMap.get("DG_PERF_OWNED");
			if (bd != null) ip.setPerfOwned(bd.floatValue());
			bd = (BigDecimal)format.parseObject((String)inputMap.get("DG_SYNC_OWNED"));
			//bd = (BigDecimal) inputMap.get("DG_SYNC_OWNED");
			if (bd != null) ip.setSyncOwned(bd.floatValue());
			bd = (BigDecimal)format.parseObject((String)inputMap.get("DG_OTHER_OWNED"));
			//bd = (BigDecimal) inputMap.get("DG_OTHER_OWNED");
			if (bd != null) ip.setOtherOwned(bd.floatValue());
			str = (String) inputMap.get("DG_IP_TYPE_1");
			if (str != null)
				ip.setIpType(str);
			else
				ip.setIpType("");
			//str = (String) inputMap.get("SIPS_IPS_SURNAME");
			str = (String) inputMap.get("DG_IPS_SURNAME");
			if (str != null)
				ip.setSurname(str);
			else
				ip.setSurname("");
			
			str = (String) inputMap.get("DG_COMA_CNAME1");
			if (str != null)
				ip.setFirstName(str);
			else
				ip.setFirstName("");
			
			str = (String) inputMap.get("DG_COMA_CNAME2");
			if (str != null)
				ip.setMiddleName(str);
			else
				ip.setMiddleName("");
			
			bd = (BigDecimal)format.parseObject((String)inputMap.get("DG_IPLINK_ID"));
			//bd = (BigDecimal) inputMap.get("DG_IPLINK_ID");
			if (bd != null) str = bd.toString();
			if (str != null)
				ip.setIpLink(str);
			else
				ip.setIpLink("");
			bd = (BigDecimal)format.parseObject((String)inputMap.get("DG_PARENT_IPLINK_ID"));
			//bd = (BigDecimal) inputMap.get("DG_PARENT_IPLINK_ID");
			if (bd != null) str = bd.toString();
			if (str != null)
				ip.setParentIpLink(str);
			else
				ip.setParentIpLink("");
			
			
			alIPs.add(ip);
			
		}
		catch (Exception e)
		{
			logger.error(e.getMessage());

		}
		return ip;
	}


	
}
