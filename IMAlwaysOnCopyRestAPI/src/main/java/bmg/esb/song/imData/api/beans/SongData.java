package bmg.esb.song.imData.api.beans;

import java.util.ArrayList;
import java.util.Date;

/**
 * This is a bean class used for the Song data coming from the database
 * 
 */
public class SongData {

	private String type;
	private String changeIndicator;
	private String site;
	private String title;
	private String iswc;
	private String territoryName;
	private String territoryCode;
	private String composers;
	private String principalArtist;
	private String albumTitle;
	private String RecordLabel;
	private String RecordLabelName;
	private String status;
	//private Date added;
	private String added;
	private String addedby;
	//private Date amended;
	private String amended;
	private String amendedby;
	private String deleted;
	private int code;
	private IPData ip;
	private ArrayList<IPData> ipData;
	private int songId;
	private int ipId;
	
	public ArrayList<IPData> getIpData() {
		return ipData;
	}

	public void setIpData(ArrayList<IPData> ipData) {
		this.ipData = ipData;
	}

	public IPData getIp() {
		return ip;
	}

	public void setIp(IPData ip) {
		this.ip = ip;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIswc() {
		return iswc;
	}

	public void setIswc(String iswc) {
		this.iswc = iswc;
	}

	public String getTerritoryCode() {
		return territoryCode;
	}

	public void setTerritoryCode(String territoryCode) {
		this.territoryCode = territoryCode;
	}

	public String getComposers() {
		return composers;
	}

	public void setComposers(String composers) {
		this.composers = composers;
	}

	public String getPrincipalArtist() {
		return principalArtist;
	}

	public void setPrincipalArtist(String principalArtist) {
		this.principalArtist = principalArtist;
	}

	public String getAlbumTitle() {
		return albumTitle;
	}

	public void setAlbumTitle(String albumTitle) {
		this.albumTitle = albumTitle;
	}

	public String getRecordLabel() {
		return RecordLabel;
	}

	public void setRecordLabel(String recordLabel) {
		RecordLabel = recordLabel;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAdded() {
		return added;
	}

	public void setAdded(String added) {
		this.added = added;
	}

	public String getAddedby() {
		return addedby;
	}

	public void setAddedby(String addedby) {
		this.addedby = addedby;
	}

	public String getAmended() {
		return amended;
	}

	public void setAmended(String amended) {
		this.amended = amended;
	}

	public String getAmendedby() {
		return amendedby;
	}

	public void setAmendedby(String amendedby) {
		this.amendedby = amendedby;
	}

	public String getDeleted() {
		return deleted;
	}

	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	public int getSongId() {
		return songId;
	}

	public void setSongId(int songId) {
		this.songId = songId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getChangeIndicator() {
		return changeIndicator;
	}

	public void setChangeIndicator(String changeIndicator) {
		this.changeIndicator = changeIndicator;
	}

	public int getIpId()
	{
		return ipId;
	}

	public void setIpId(int ipId)
	{
		this.ipId = ipId;
	}
	public String getRecordLabelName() {
		return RecordLabelName;
	}

	public void setRecordLabelName(String recordLabelName) {
		RecordLabelName = recordLabelName;
	}

	public String getTerritoryName() {
		return territoryName;
	}

	public void setTerritoryName(String territoryName) {
		this.territoryName = territoryName;
	}
}
