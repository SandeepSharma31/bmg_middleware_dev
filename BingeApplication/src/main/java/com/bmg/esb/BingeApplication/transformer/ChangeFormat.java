package com.bmg.esb.BingeApplication.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bmg.esb.BingeApplication.Constants.BMGSongsConstant;
import com.bmg.esb.BingeApplication.beans.BingeSong;

public class ChangeFormat {

	private static final Logger ChangeFormatLogger = LoggerFactory
			.getLogger(ChangeFormat.class);

	public void generateJSONEntityWise(List<Map<String, Object>> inputList,
			Exchange ex) {
		if (inputList.size()>0) {
			
			if (ex.getIn().getHeader("entityType")
					.equals((BMGSongsConstant.ENTITY_SONG))) {
				//List<Map<String, Object>> deltaSongList = (List<Map<String, Object>>) ex.getIn().getHeader("songCodedata"); commented for sort purpose
				ArrayList<BingeSong> alRet = new ArrayList<BingeSong>();
				
				for (Map<String, Object> inputMap : inputList) {
					BingeSong sd = null;

					if (inputMap.size()>0) {
					
						sd = getBingeSongByID(alRet, inputMap);
					}
				}
			
				Collections.sort(alRet);
				ChangeFormatLogger.info("generateJSONEntityWise: Binge Song Sorting$$$$$ min --"+alRet.get(0).getId_md()+"--max--"+alRet.get(alRet.size() - 1).getId_md());
				
				if (alRet.size()>0) {
					//int deltaSize = alRet.size();
					
					ex.getIn().setHeader("songMinCount",alRet.get(0).getId_md());
					ChangeFormatLogger.debug("generateJSONEntityWise:ex.getIn(),getHeader(songMinCount):"
									+ ex.getIn().getHeader("songMinCount"));
					if(alRet.size()==1){
						ex.getIn().setHeader("songMaxCount",alRet.get(0).getId_md());
						ChangeFormatLogger.debug("generateJSONEntityWise:ex.getIn(),getHeader(songMaxCount):"
										+ ex.getIn().getHeader("songMaxCount"));
					} else{
						ex.getIn().setHeader("songMaxCount",alRet.get(alRet.size() - 1).getId_md());
						ChangeFormatLogger.debug("generateJSONEntityWise:ex.getIn(),getHeader(songMaxCount):"
										+ ex.getIn().getHeader("songMaxCount"));
					}
				} 
				else {

					ex.getIn().setHeader("songMinCount", "MIN");
					ChangeFormatLogger.info("generateJSONEntityWise: Binge Song Entity ::If list null and list empty then we are setting flag and not processing further");

				}

				ChangeFormatLogger.info("generateJSONEntityWise: Binge Song Entity Size of the list before conversion to JSon::"+alRet.size() );
			
				ex.getIn().setBody(alRet);
				alRet=null;
				//deltaSongList=null;

			} 
			
			
		} else {
			ex.getIn().setHeader("startRange", "FLAG");
		}

	}
	

	private BingeSong getBingeSongByID(ArrayList<BingeSong> alRet,Map<String, Object> inputMap) {
		BingeSong sdNew;
		BingeSong sdRet = null;
		ChangeFormatLogger.info("getBingeSongByID");

		ChangeFormatLogger.debug("getBingeSongByID:getting values from map and setting to");
		sdNew = new BingeSong();
		
		long id =  (Long) inputMap.get("id_md");
		sdNew.setId_md(id);
		
		
		BigDecimal songCode =  (BigDecimal) inputMap.get("DG_Song_Code");
		if (songCode != null && !inputMap.get("DG_Song_Code").equals("null")){
			sdNew.setDG_Song_Code(songCode.longValue());
		}
		
		if (inputMap.get("DG_SONM_ID") != null)
			sdNew.setDG_SONM_ID((String)inputMap.get("DG_SONM_ID"));
		else
			sdNew.setDG_SONM_ID("");
		
		if (inputMap.get("DG_SONM_ORIG_COMPANY") != null)
			sdNew.setDG_SONM_ORIG_COMPANY((String)inputMap.get("DG_SONM_ORIG_COMPANY"));
		else
			sdNew.setDG_SONM_ORIG_COMPANY("");
		
		if (inputMap.get("DG_SONM_TITLE") != null)
			sdNew.setDG_SONM_TITLE((String)inputMap.get("DG_SONM_TITLE"));
		else
			sdNew.setDG_SONM_TITLE("");
		
		if (inputMap.get("DG_SONM_ISWC") != null)
			sdNew.setDG_SONM_ISWC((String)inputMap.get("DG_SONM_ISWC"));
		else
			sdNew.setDG_SONM_ISWC("");
		
		if (inputMap.get("DG_SONM_COMPOSERS") != null)
			sdNew.setDG_SONM_COMPOSERS((String)inputMap.get("DG_SONM_COMPOSERS"));
		else
			sdNew.setDG_SONM_COMPOSERS("");
		
		if (inputMap.get("CHANGE_ID") != null)
			sdNew.setCHANGE_ID((String)inputMap.get("CHANGE_ID"));
		else
			sdNew.setCHANGE_ID("");
		

		sdRet = sdNew;
		alRet.add(sdNew);

		ChangeFormatLogger.debug("getBingeSongByID:After setting all values to new map");
		return sdRet;
	}

	
}
