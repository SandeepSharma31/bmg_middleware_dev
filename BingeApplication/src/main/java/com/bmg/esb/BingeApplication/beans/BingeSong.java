package com.bmg.esb.BingeApplication.beans;


public class BingeSong  implements Comparable<BingeSong>
{
	
	private long DG_Song_Code;
	private String DG_SONM_ID;
	private String DG_SONM_ORIG_COMPANY;
	private String DG_SONM_TITLE;
	private String DG_SONM_ISWC;
	private String DG_SONM_COMPOSERS;
	private long id;
	private long id_md;
	private String CHANGE_ID;
	
	
	public long getDG_Song_Code() {
		return DG_Song_Code;
	}
	public void setDG_Song_Code(long dG_Song_Code) {
		DG_Song_Code = dG_Song_Code;
	}
	public String getDG_SONM_ID() {
		return DG_SONM_ID;
	}
	public void setDG_SONM_ID(String dG_SONM_ID) {
		DG_SONM_ID = dG_SONM_ID;
	}
	public String getDG_SONM_ORIG_COMPANY() {
		return DG_SONM_ORIG_COMPANY;
	}
	public void setDG_SONM_ORIG_COMPANY(String dG_SONM_ORIG_COMPANY) {
		DG_SONM_ORIG_COMPANY = dG_SONM_ORIG_COMPANY;
	}
	public String getDG_SONM_TITLE() {
		return DG_SONM_TITLE;
	}
	public void setDG_SONM_TITLE(String dG_SONM_TITLE) {
		DG_SONM_TITLE = dG_SONM_TITLE;
	}
	public String getDG_SONM_ISWC() {
		return DG_SONM_ISWC;
	}
	public void setDG_SONM_ISWC(String dG_SONM_ISWC) {
		DG_SONM_ISWC = dG_SONM_ISWC;
	}
	public String getDG_SONM_COMPOSERS() {
		return DG_SONM_COMPOSERS;
	}
	public void setDG_SONM_COMPOSERS(String dG_SONM_COMPOSERS) {
		DG_SONM_COMPOSERS = dG_SONM_COMPOSERS;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCHANGE_ID() {
		return CHANGE_ID;
	}
	public void setCHANGE_ID(String cHANGE_ID) {
		CHANGE_ID = cHANGE_ID;
	}
	
	@Override
	public int compareTo(BingeSong sng) {
		return (int) (this.DG_Song_Code - sng.DG_Song_Code);
	}
	public long getId_md() {
		return id_md;
	}
	public void setId_md(long id_md) {
		this.id_md = id_md;
	}
}
