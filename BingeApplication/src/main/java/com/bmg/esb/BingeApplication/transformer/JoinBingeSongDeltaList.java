package com.bmg.esb.BingeApplication.transformer;
import com.bmg.esb.BingeApplication.Constants.BMGSongsConstant;

import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JoinBingeSongDeltaList {
	
	private static final Logger joinBingeSongDeltaList = LoggerFactory
			.getLogger(JoinBingeSongDeltaList.class);
	
	
	public void createRangeValue(List<Map<String, Object>> countList,Exchange ex)
	{
		joinBingeSongDeltaList.info("in createRangeValue");
		
		
		if (countList.size()>0)
		{
			
			if(ex.getIn().getHeader("entityType").equals((BMGSongsConstant.ENTITY_SONG)))
			{
				joinBingeSongDeltaList.info("ENTITY_BINGESONG and value for start range is "+countList.get(0).get("startCount"));
				ex.getIn().setHeader("songStartRange", countList.get(0).get("startCount"));
			}
		}
		
		else{
		ex.getIn().setHeader("startRange","FLAG");
		}
		
		
	}
	
	
		
	
	
}
