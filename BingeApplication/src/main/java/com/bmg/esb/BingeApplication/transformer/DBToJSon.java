package com.bmg.esb.BingeApplication.transformer;



import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bmg.esb.BingeApplication.beans.BingeSong;
import com.google.gson.Gson;

public class DBToJSon {
	
	
	

	private static final Logger dbToJSonLogger = LoggerFactory
			.getLogger(DBToJSon.class);
	
	
	public String convertBingeSongToJSon(BingeSong allData)
	{
		String json="";
		dbToJSonLogger.debug("in convertBingeSongToJSon");
	
		Map<String, Object> bingeSongData = new HashMap<String, Object>();
		try
		{
			//Create a new Map to form a JSon object. Using LinkedHashMap ensures that the objects are added as FIFO, so that type and changeIndicator appear data in the JSon output
			Map<String, Object> map = new LinkedHashMap<String, Object>();
			map.put("type", "BingeLinkSongIdSONMId");
			//map.put("changeIndicator", allData.getChangeIndicator());	
			dbToJSonLogger.debug("Message before createBingeSongDataMapping conversion to JSon data{}");
			bingeSongData = createBingeSongDataMapping(allData);
			map.put("data", bingeSongData);
			Gson gson = new Gson();
			json = gson.toJson(map);
			map=null;
			dbToJSonLogger.debug("Message after conversion to JSon::" + json);
			

		}
		catch (Exception e)
		{
			dbToJSonLogger.error("In Exception block of convertBingeSongToJSon while creating json message"+e.getMessage());
		}
		return json;

	}

	//This method will create a new Map which be passed to the GSon to form a JSon object
	private Map<String, Object> createBingeSongDataMapping(BingeSong allData)
	{
		Map<String, Object> bingeSongData = new HashMap<String, Object>();
		dbToJSonLogger.debug("in creatSongDataMapping");
		bingeSongData.put("songId", allData.getDG_Song_Code());
		bingeSongData.put("sonmId", allData.getDG_SONM_ID());
		bingeSongData.put("sonmOrigCompany", allData.getDG_SONM_ORIG_COMPANY());
		bingeSongData.put("sonmTitle", allData.getDG_SONM_TITLE());
		bingeSongData.put("sonmISWC", allData.getDG_SONM_ISWC());
		bingeSongData.put("sonmComposers", allData.getDG_SONM_COMPOSERS());
		dbToJSonLogger.debug("creatBingeSongDataMapping:After putting into Map of Binge Song");
		return bingeSongData;
	}
	
}
