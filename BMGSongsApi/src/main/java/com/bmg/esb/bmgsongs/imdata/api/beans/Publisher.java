package com.bmg.esb.bmgsongs.imdata.api.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Publisher {
	private Integer id;
	private String name;
	private String ipi;
	private String mechanicalSocietyId;
	private String performanceSocietyId;
	private String isControlled;
	@JsonIgnore
	private Integer row;
	public Integer getRow() {
		return row;
	}
	public void setRow(Integer row) {
		this.row = row;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIpi() {
		return ipi;
	}
	public void setIpi(String ipi) {
		this.ipi = ipi;
	}
	public String getMechanicalSocietyId() {
		return mechanicalSocietyId;
	}
	public void setMechanicalSocietyId(String mechanicalSocietyId) {
		this.mechanicalSocietyId = mechanicalSocietyId;
	}
	public String getPerformanceSocietyId() {
		return performanceSocietyId;
	}
	public void setPerformanceSocietyId(String performanceSocietyId) {
		this.performanceSocietyId = performanceSocietyId;
	}
	public String getIsControlled() {
		return isControlled;
	}
	public void setIsControlled(String isControlled) {
		this.isControlled = isControlled;
	}
}
