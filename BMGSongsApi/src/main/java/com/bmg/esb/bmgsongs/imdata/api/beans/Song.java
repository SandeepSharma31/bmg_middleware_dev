package com.bmg.esb.bmgsongs.imdata.api.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Song 
{
	private int songCode;
	private String title;
	private String territory;
	private String composers;
	private String iswc;
	private String isDeleted;
	private String status;
	@JsonIgnore
	private Integer row;
	public Integer getRow() {
		return row;
	}
	public void setRow(Integer row) {
		this.row = row;
	}
	public int getSongCode() {
		return songCode;
	}
	public void setSongCode(int songCode) {
		this.songCode = songCode;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTerritory() {
		return territory;
	}
	public void setTerritory(String territory) {
		this.territory = territory;
	}
	public String getComposers() {
		return composers;
	}
	public void setComposers(String composers) {
		this.composers = composers;
	}
	public String getIswc() {
		return iswc;
	}
	public void setIswc(String iswc) {
		this.iswc = iswc;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
