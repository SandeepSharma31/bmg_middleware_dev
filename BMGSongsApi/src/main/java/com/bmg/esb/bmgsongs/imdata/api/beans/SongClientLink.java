package com.bmg.esb.bmgsongs.imdata.api.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class SongClientLink 
{
	private Integer clientCode;
	private Integer songCode;
	private Integer subCode;
	private String sysTerr;
	@JsonIgnore
	private Integer row;
	public Integer getRow() {
		return row;
	}
	public void setRow(Integer row) {
		this.row = row;
	}
	public Integer getClientCode() {
		return clientCode;
	}
	public void setClientCode(Integer clientCode) {
		this.clientCode = clientCode;
	}
	public Integer getSongCode() {
		return songCode;
	}
	public void setSongCode(Integer songCode) {
		this.songCode = songCode;
	}
	public Integer getSubCode() {
		return subCode;
	}
	public void setSubCode(Integer subCode) {
		this.subCode = subCode;
	}
	public String getSysTerr() {
		return sysTerr;
	}
	public void setSysTerr(String sysTerr) {
		this.sysTerr = sysTerr;
	}

}
