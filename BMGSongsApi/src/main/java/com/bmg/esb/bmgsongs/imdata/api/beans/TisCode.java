package com.bmg.esb.bmgsongs.imdata.api.beans;

public class TisCode {

	private String tisCode;
	private String tisDesc;
	public String getTisCode() {
		return tisCode;
	}
	public void setTisCode(String tisCode) {
		this.tisCode = tisCode;
	}
	public String getTisDesc() {
		return tisDesc;
	}
	public void setTisDesc(String tisDesc) {
		this.tisDesc = tisDesc;
	}
	
	
}
