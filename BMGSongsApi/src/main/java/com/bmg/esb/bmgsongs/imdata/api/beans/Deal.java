package com.bmg.esb.bmgsongs.imdata.api.beans;

public class Deal {
	
	private String sysTerr;
	private String dealNo;
	private String dealName;
	public String getSysTerr() {
		return sysTerr;
	}
	public void setSysTerr(String sysTerr) {
		this.sysTerr = sysTerr;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getDealName() {
		return dealName;
	}
	public void setDealName(String dealName) {
		this.dealName = dealName;
	}
	
	
	
	

}
