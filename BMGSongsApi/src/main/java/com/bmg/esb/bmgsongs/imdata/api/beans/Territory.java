package com.bmg.esb.bmgsongs.imdata.api.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Territory 
{
	private String id;
	private String name;
	private String incCodes;
	private String excCodes;
	@JsonIgnore
	private Integer row;
	public Integer getRow() {
		return row;
	}
	public void setRow(Integer row) {
		this.row = row;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIncCodes() {
		return incCodes;
	}
	public void setIncCodes(String incCodes) {
		this.incCodes = incCodes;
	}
	public String getExcCodes() {
		return excCodes;
	}
	public void setExcCodes(String excCodes) {
		this.excCodes = excCodes;
	}


}
