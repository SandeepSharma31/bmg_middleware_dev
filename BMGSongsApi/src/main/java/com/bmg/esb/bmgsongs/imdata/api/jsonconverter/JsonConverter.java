package com.bmg.esb.bmgsongs.imdata.api.jsonconverter;


import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import javax.ws.rs.core.Response;
import org.apache.camel.Exchange;
import org.apache.log4j.Logger;
import com.bmg.esb.bmgsongs.imdata.api.beans.Client;
import com.bmg.esb.bmgsongs.imdata.api.beans.ClientDealLink;
import com.bmg.esb.bmgsongs.imdata.api.beans.Composer;
import com.bmg.esb.bmgsongs.imdata.api.beans.Deal;
import com.bmg.esb.bmgsongs.imdata.api.beans.Publisher;
import com.bmg.esb.bmgsongs.imdata.api.beans.Society;
import com.bmg.esb.bmgsongs.imdata.api.beans.Song;
import com.bmg.esb.bmgsongs.imdata.api.beans.SongAka;
import com.bmg.esb.bmgsongs.imdata.api.beans.SongClientLink;
import com.bmg.esb.bmgsongs.imdata.api.beans.SongGroup;
import com.bmg.esb.bmgsongs.imdata.api.beans.SongGroupLink;
import com.bmg.esb.bmgsongs.imdata.api.beans.SongInterestedParty;
import com.bmg.esb.bmgsongs.imdata.api.beans.SongNotes;
import com.bmg.esb.bmgsongs.imdata.api.beans.SongShare;
import com.bmg.esb.bmgsongs.imdata.api.beans.SongStatus;
import com.bmg.esb.bmgsongs.imdata.api.beans.SongWarning;
import com.bmg.esb.bmgsongs.imdata.api.beans.Territory;
import com.bmg.esb.bmgsongs.imdata.api.beans.TisCode;
import com.bmg.esb.bmgsongs.imdata.api.beans.TisHierarchyLink;
import com.bmg.esb.bmgsongs.imdata.api.constants.BmgSongsApiConstants;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
/**
 * 
 * @author rajesh_thattikonda
 *	This class converts the data obtained from database to required key value pairs.
 *This class also is responsible for giving the bad request message if not data found with requested parameter values.
 */
public class JsonConverter 
{
	private static Logger logger = Logger.getLogger(JsonConverter.class.getName());
	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * This method creates key value pairs required for SONG ENTITY Json data using the data obtained from database.
	 */
	public void processSongData(Exchange exchange) throws Exception
	{ 
		logger.info("In song converter");
		try {

			logger.debug("in json class about to convert body");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<Song> songList = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<Song>>() {});
			logger.debug("body converted into list of song objects");
			int len = songList.size();
			/*List<LinkedHashMap<String, Object>> songsDataList = new ArrayList<LinkedHashMap<String,Object>>();*/
			List<Object> songsDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(Song song:songList)
				{
					LinkedHashMap<String, Object> songMap = new LinkedHashMap<String, Object>();
					songMap.put("changeIndicator","U" );
					songMap.put("type","Song" );
					logger.debug("before setting data");
					songMap.put("data", getSongData(song));
					logger.debug("after setting data in map");
					songsDataList.add(songMap);
				}*/
				for(Song song:songList)
				{
					songsDataList.add(getSongData(song));
				}
				/* 1st way setting body*/
				/*exchange.getOut().setBody(songsDataList);*/
				/* 2nd way creating response*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				String songEntityWithParam = exchange.getIn().getHeader("songEntityWithParam").toString();
				if(songEntityWithParam.equals("SONGCODE"))
				{
					json=gson.toJson(songsDataList.get(0));
					responseStr=json.toString();
				}
				else
				{
					for(Object obj:songsDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}

					responseStr=jsonList.toString();
				}
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();

				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_SONGDATA+" "+exchange.getIn().getHeader("songcode")+" and "+exchange.getIn().getHeader("deleteind"));*/
				/* 2nd way of else*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String songEntityWithParam = exchange.getIn().getHeader("songEntityWithParam").toString();
				if(songEntityWithParam.equals("ALLSONGS"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGDATA_WITHOUTPARAM;
				}
				else if(songEntityWithParam.equals("SONGCODE"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGDATA_WITHSONGCODE+" "+exchange.getIn().getHeader("songcode");
				}
				else if(songEntityWithParam.equals("DELETEIND"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGDATA_WITHDELETEIND+" "+exchange.getIn().getHeader("deleteind");
				}
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			songList=null;
			songsDataList=null;
			logger.debug("Out of json class");
		} catch (Exception e) {
			throw new Exception("Exception while converting song into proper json structure. Error is"+e.getMessage());
		}

	}
	private Object getSongData(Song song) {
		logger.debug("entered getSongData method");
		LinkedHashMap<String, Object> dataMap = new LinkedHashMap<String, Object>();
		dataMap.put("songCode", song.getSongCode());
		dataMap.put("title", song.getTitle());
		dataMap.put("territory", song.getTerritory());
		dataMap.put("composers", song.getComposers());
		dataMap.put("iswc", song.getIswc());
		dataMap.put("isDeleted", song.getIsDeleted());
		dataMap.put("status", song.getStatus());
		logger.debug("exited getSongData method");
		return dataMap;
	}
	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * This method creates key value pairs required for COMPOSER ENTITY Json data using the data obtained from database.
	 */
	public void processComposerData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for COmposer data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<Composer> composerList = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<Composer>>() {});
			logger.debug("body converted into list of composer objects");
			int len = composerList.size();
			/*List<LinkedHashMap<String, Object>> composerDataList = new ArrayList<LinkedHashMap<String,Object>>();*/
			List<Object> composerDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(Composer comp:composerList)
				{
					LinkedHashMap<String, Object> composerMap = new LinkedHashMap<String, Object>();
					composerMap.put("changeIndicator","U" );
					composerMap.put("type","Composer" );
					logger.debug("before setting data");
					composerMap.put("data", getComposerData(comp));
					logger.debug("after setting data in map");
					composerDataList.add(composerMap);
				}*/
				for(Composer comp:composerList)
				{
					composerDataList.add(getComposerData(comp));
				}
				/*exchange.getOut().setBody(composerDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				String compEntityWithParam = exchange.getIn().getHeader("composerEntityWithParam").toString();
				if(compEntityWithParam.equals("COMPPUBCODE"))
				{
					json=gson.toJson(composerDataList.get(0));
					responseStr=json.toString();
				}
				else
				{
					/*for(Map<String, Object> dataMap:composerDataList)
					{
						json = gson.toJson(dataMap);
						jsonList.add(json);
					}*/
					for(Object obj:composerDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				}
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_COMPOSERDATA+" "+exchange.getIn().getHeader("pubcode"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String compEntityWithParam = exchange.getIn().getHeader("composerEntityWithParam").toString();
				if(compEntityWithParam.equals("ALLCOMPOSERS"))
				{
					responseStr = BmgSongsApiConstants.NO_COMPDATA_WITHOUTPARAM;
				}
				else if(compEntityWithParam.equals("COMPPUBCODE"))
				{
					responseStr = BmgSongsApiConstants.NO_COMPDATA_WITHPUBCODE+" "+exchange.getIn().getHeader("pubcode");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			composerList=null;
			composerDataList=null;
			logger.debug("Out of json class and method processComposerData");
		} catch (Exception e) {
			throw new Exception("Exception while converting composer into proper json structure. Error is"+e.getMessage());
		}

	}
	private Object getComposerData(Composer comp) {
		logger.debug("entered getComposerData  method");

		LinkedHashMap<String, Object> composerDataMap = new LinkedHashMap<String, Object>();
		composerDataMap.put("id", comp.getId());
		composerDataMap.put("firstName", comp.getFirstName());
		composerDataMap.put("middleName", comp.getMiddleName());
		composerDataMap.put("lastName", comp.getLastName());
		composerDataMap.put("name", comp.getName());
		composerDataMap.put("ipi", comp.getIpi());
		composerDataMap.put("mechanicalSocietyId", comp.getMechanicalSocietyId());
		composerDataMap.put("performanceSocietyId", comp.getPerformanceSocietyId());
		composerDataMap.put("isControlled", comp.getIsControlled());

		logger.debug("exited getComposerData method");
		return composerDataMap;
	}
	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * This method creates key value pairs required for PUBLISHER ENTITY Json data using the data obtained from database.
	 */
	public void processPublisherData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for Publisher data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<Publisher> publisherListFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<Publisher>>() {});
			logger.debug("body converted into list of publisher objects");
			int len = publisherListFromDb.size();
			/*List<LinkedHashMap<String, Object>> publisherDataList = new ArrayList<LinkedHashMap<String,Object>>();*/
			List<Object> publisherDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(Publisher pub:publisherListFromDb)
				{
					LinkedHashMap<String, Object> publisherMap = new LinkedHashMap<String, Object>();
					publisherMap.put("changeIndicator","U" );
					publisherMap.put("type","Publisher" );
					logger.debug("before setting data");
					publisherMap.put("data", getPublisherData(pub));
					logger.debug("after setting data in map");
					publisherDataList.add(publisherMap);
				}*/
				for(Publisher pub:publisherListFromDb)
				{
					publisherDataList.add( getPublisherData(pub));
				}

				/*exchange.getOut().setBody(publisherDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				String publisherEntityWithParam = exchange.getIn().getHeader("publisherEntityWithParam").toString();
				if(publisherEntityWithParam.equals("PUBLPUBCODE"))
				{
					json=gson.toJson(publisherDataList.get(0));
					responseStr=json.toString();
				}
				else
				{
					/*for(Map<String, Object> dataMap:publisherDataList)
					{
						json = gson.toJson(dataMap);
						jsonList.add(json);
					}*/
					for(Object obj:publisherDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				}
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_PUBLISHERDATA+" "+exchange.getIn().getHeader("pubcode"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String publisherEntityWithParam = exchange.getIn().getHeader("publisherEntityWithParam").toString();
				if(publisherEntityWithParam.equals("ALLPUBLISHERS"))
				{
					responseStr = BmgSongsApiConstants.NO_PUBLDATA_WITHOUTPARAM;
				}
				else if(publisherEntityWithParam.equals("PUBLPUBCODE"))
				{
					responseStr = BmgSongsApiConstants.NO_PUBLDATA_WITHPUBCODE+" "+exchange.getIn().getHeader("pubcode");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			publisherListFromDb=null;
			publisherDataList=null;
			logger.debug("Out of json class and method processPublisherData");
		} catch (Exception e) {
			throw new Exception("Exception while converting Publisher into proper json structure. Error is"+e.getMessage());
		}

	}
	private Object getPublisherData(Publisher pub) {
		logger.debug("entered getPublisherData  method");

		LinkedHashMap<String, Object> publisherDataMap = new LinkedHashMap<String, Object>();
		publisherDataMap.put("id", pub.getId());
		publisherDataMap.put("name", pub.getName());
		publisherDataMap.put("ipi", pub.getIpi());
		publisherDataMap.put("mechanicalSocietyId", pub.getMechanicalSocietyId());
		publisherDataMap.put("performanceSocietyId", pub.getPerformanceSocietyId());
		publisherDataMap.put("isControlled", pub.getIsControlled());

		logger.debug("exited getPublisherData method");
		return publisherDataMap;
	}
	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * This method creates key value pairs required for SOCIETY ENTITY Json data using the data obtained from database.
	 */
	public void processSocietyData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for Society data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<Society> societyListFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<Society>>() {});
			logger.debug("body converted into list of society objects");
			int len = societyListFromDb.size();
			/*List<LinkedHashMap<String, Object>> societyDataList = new ArrayList<LinkedHashMap<String,Object>>();*/
			List<Object> societyDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(Society soc:societyListFromDb)
				{
					LinkedHashMap<String, Object> societyMap = new LinkedHashMap<String, Object>();
					societyMap.put("changeIndicator","U" );
					societyMap.put("type","Society" );
					logger.debug("before setting data");
					societyMap.put("data", getSocietyData(soc));
					logger.debug("after setting data in map");
					societyDataList.add(societyMap);
				}*/
				for(Society soc:societyListFromDb)
				{
					societyDataList.add(getSocietyData(soc));
				}
				/*exchange.getOut().setBody(societyDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				String societyEntityWithParam = exchange.getIn().getHeader("societyEntityWithParam").toString();
				if(societyEntityWithParam.equals("SOCIETYSOCCODE"))
				{
					json=gson.toJson(societyDataList.get(0));
					responseStr=json.toString();
				}
				else
				{
					/*for(Map<String, Object> dataMap:societyDataList)
					{
						json = gson.toJson(dataMap);
						jsonList.add(json);
					}*/
					for(Object obj:societyDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				}
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_SOCIETYDATA+" "+exchange.getIn().getHeader("soccode"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String societyEntityWithParam = exchange.getIn().getHeader("societyEntityWithParam").toString();
				if(societyEntityWithParam.equals("ALLSOCIETY"))
				{
					responseStr = BmgSongsApiConstants.NO_SOCDATA_WITHOUTPARAM;
				}
				else if(societyEntityWithParam.equals("SOCIETYSOCCODE"))
				{
					responseStr = BmgSongsApiConstants.NO_SOCDATA_WITHSOCCODE+" "+exchange.getIn().getHeader("soccode");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}

			societyListFromDb=null;
			societyDataList=null;
			logger.debug("Out of json class and method processSocietyData");
		} catch (Exception e) {
			throw new Exception("Exception while converting Society into proper json structure. Error is"+e.getMessage());
		}

	}
	private Object getSocietyData(Society soc) {
		logger.debug("entered getSocietyData  method");

		LinkedHashMap<String, Object> societyDataMap = new LinkedHashMap<String, Object>();
		societyDataMap.put("id", soc.getId());
		societyDataMap.put("name", soc.getName());
		societyDataMap.put("CISACCode", soc.getCisacCode());
		societyDataMap.put("abbreviation", soc.getAbbreviation());
		societyDataMap.put("territoryName", soc.getTerritoryName());
		societyDataMap.put("countryName", soc.getCountryName());

		logger.debug("exited getSocietyData method");
		return societyDataMap;
	}

	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * This method creates key value pairs required for TERRITORY ENTITY Json data using the data obtained from database.
	 */
	public void processTerritoryData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for Territory data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<Territory> territoryListFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<Territory>>() {});
			logger.debug("body converted into list of Territory objects");
			int len = territoryListFromDb.size();
			/*List<LinkedHashMap<String, Object>> territoryDataList = new ArrayList<LinkedHashMap<String,Object>>();*/
			List<Object> territoryDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(Territory terr:territoryListFromDb)
				{
					LinkedHashMap<String, Object> territoryMap = new LinkedHashMap<String, Object>();
					territoryMap.put("changeIndicator","U" );
					territoryMap.put("type","Territory" );
					logger.debug("before setting data");
					territoryMap.put("data", getTerritoryData(terr));
					logger.debug("after setting data in map");
					territoryDataList.add(territoryMap);
				}*/
				for(Territory terr:territoryListFromDb)
				{
					territoryDataList.add(getTerritoryData(terr));
				}
				/*exchange.getOut().setBody(territoryDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				String territoryEntityWithParam = exchange.getIn().getHeader("territoryEntityWithParam").toString();
				if(territoryEntityWithParam.equals("TERRCODE"))
				{
					json=gson.toJson(territoryDataList.get(0));
					responseStr=json.toString();
				}
				else
				{
					/*for(Map<String, Object> dataMap:territoryDataList)
					{
						json = gson.toJson(dataMap);
						jsonList.add(json);
					}*/
					for(Object obj:territoryDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				}
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_TERRITORYDATA+" "+exchange.getIn().getHeader("terrcode"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String territoryEntityWithParam = exchange.getIn().getHeader("territoryEntityWithParam").toString();
				if(territoryEntityWithParam.equals("ALLTERRITORY"))
				{
					responseStr = BmgSongsApiConstants.NO_TERRDATA_WITHOUTPARAM;
				}
				else if(territoryEntityWithParam.equals("TERRCODE"))
				{
					responseStr = BmgSongsApiConstants.NO_TERRDATA_WITHTERRCODE+" "+exchange.getIn().getHeader("terrcode");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			territoryListFromDb=null;
			territoryListFromDb=null;
			logger.debug("Out of json class and method processTerritoryData");
		} catch (Exception e) {
			throw new Exception("Exception while converting territory into proper json structure. Error is"+e.getMessage());
		}

	}
	private Object getTerritoryData(Territory terr) {
		logger.debug("entered getTerritoryData  method");

		LinkedHashMap<String, Object> territoryDataMap = new LinkedHashMap<String, Object>();
		territoryDataMap.put("id", terr.getId());
		territoryDataMap.put("name", terr.getName());
		territoryDataMap.put("incCodes", terr.getIncCodes());
		territoryDataMap.put("excCodes", terr.getExcCodes());

		logger.debug("exited getTerritoryData method");
		return territoryDataMap;
	}

	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * This method creates key value pairs required for TISHIERARCHYLINK ENTITY Json data using the data obtained from database.
	 */
	public void processTisHierarchyLinkData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for TisHierarchyLink data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<TisHierarchyLink> tisHierarchyListFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<TisHierarchyLink>>() {});
			logger.debug("body converted into list of TisHierarchyLink objects");
			int len = tisHierarchyListFromDb.size();
			List<Object> tisHierarchyDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(TisHierarchyLink tisHieLink:tisHierarchyListFromDb)
				{
					LinkedHashMap<String, Object> tisHierarchyMap = new LinkedHashMap<String, Object>();
					tisHierarchyMap.put("changeIndicator","U" );
					tisHierarchyMap.put("type","TisHierarchyLink" );
					logger.debug("before setting data");
					tisHierarchyMap.put("data", getTisHierarchyData(tisHieLink));
					logger.debug("after setting data in map");
					tisHierarchyDataList.add(tisHierarchyMap);
				}*/
				for(TisHierarchyLink tisHieLink:tisHierarchyListFromDb)
				{
					tisHierarchyDataList.add(getTisHierarchyData(tisHieLink));
				}
				/*exchange.getOut().setBody(tisHierarchyDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				String tisHierarchyEntityWithParam = exchange.getIn().getHeader("tisHierarchyEntityWithParam").toString();
				if(tisHierarchyEntityWithParam.equals("TISLINKID"))
				{
					json=gson.toJson(tisHierarchyDataList.get(0));
					responseStr=json.toString();
				}
				else
				{
					/*for(Map<String, Object> dataMap:tisHierarchyDataList)
					{
						json = gson.toJson(dataMap);
						jsonList.add(json);
					}*/
					for(Object obj:tisHierarchyDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				}
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_TISHIERARCHYDATA+" "+exchange.getIn().getHeader("tislinkid"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String tisHierarchyEntityWithParam = exchange.getIn().getHeader("tisHierarchyEntityWithParam").toString();
				if(tisHierarchyEntityWithParam.equals("ALLTISHIERARCHY"))
				{
					responseStr = BmgSongsApiConstants.NO_TISDATA_WITHOUTPARAM;
				}
				else if(tisHierarchyEntityWithParam.equals("TISLINKID"))
				{
					responseStr = BmgSongsApiConstants.NO_TISDATA_WITHTISLINKID+" "+exchange.getIn().getHeader("tislinkid");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			tisHierarchyListFromDb=null;
			tisHierarchyDataList=null;
			logger.debug("Out of json class and method processTisHierarchyLinkData");
		} catch (Exception e) {
			throw new Exception("Exception while converting tisHierarchyLink into proper json structure. Error is"+e.getMessage());
		}

	}

	private Object getTisHierarchyData(TisHierarchyLink tisHieLink) {
		logger.debug("entered getTisHierarchyData  method");

		LinkedHashMap<String, Object> tisHierarchyDataMap = new LinkedHashMap<String, Object>();
		tisHierarchyDataMap.put("linkId", tisHieLink.getLinkId());
		tisHierarchyDataMap.put("tisCode", tisHieLink.getTisCode());
		tisHierarchyDataMap.put("parent", tisHieLink.getParent());

		logger.debug("exited getTisHierarchyData method");
		return tisHierarchyDataMap;
	}

	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * This method creates key value pairs required for SONGWARNING ENTITY Json data using the data obtained from database.
	 */
	public void processSongWarningData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for SongWarning data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<SongWarning> songWarningDataFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<SongWarning>>() {});
			logger.debug("body converted into list of SongWarning objects");
			int len = songWarningDataFromDb.size();
			/*List<LinkedHashMap<String, Object>> songWarningDataList = new ArrayList<LinkedHashMap<String,Object>>();*/
			List<Object> songWarningDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(SongWarning sngWrn:songWarningDataFromDb)
				{
					LinkedHashMap<String, Object> songWarningMap = new LinkedHashMap<String, Object>();
					songWarningMap.put("changeIndicator","U" );
					songWarningMap.put("type","SongWarning" );
					logger.debug("before setting data");
					songWarningMap.put("data", getSongWarningData(sngWrn));
					logger.debug("after setting data in map");
					songWarningDataList.add(songWarningMap);
				}*/
				for(SongWarning sngWrn:songWarningDataFromDb)
				{
					songWarningDataList.add(getSongWarningData(sngWrn));
				}
				/*exchange.getOut().setBody(songWarningDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				
				/*if(songWarningDataList.size()==1)
				{
					json=gson.toJson(songWarningDataList.get(0));
					responseStr=json.toString();
				}
				else
				{*/
					for(Object obj:songWarningDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				/*}*/
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_SONGWARNINGDATA+" "+exchange.getIn().getHeader("warnSongCode"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String songWarnEntityWithParam = exchange.getIn().getHeader("songWarnEntityWithParam").toString();
				if(songWarnEntityWithParam.equals("ALLSONGWARN"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGWARNDATA_WITHOUTPARAM;
				}
				else if(songWarnEntityWithParam.equals("WARNSONGCODE"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGWARNDATA_WITHWARNSONGCODE+" "+exchange.getIn().getHeader("warnSongCode");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			songWarningDataFromDb=null;
			songWarningDataList=null;
			logger.debug("Out of json class and method processSongWarningData");
		} catch (Exception e) {
			throw new Exception("Exception while converting songWarning into proper json structure. Error is"+e.getMessage());
		}

	}

	private Object getSongWarningData(SongWarning sngWrn) {
		logger.debug("entered getSongWarningData  method");

		LinkedHashMap<String, Object> songWarningDataMap = new LinkedHashMap<String, Object>();
		songWarningDataMap.put("songCode", sngWrn.getSongCode());
		songWarningDataMap.put("severity", sngWrn.getSeverity());
		songWarningDataMap.put("summary", sngWrn.getSummary());
		songWarningDataMap.put("note1", sngWrn.getNote1());
		songWarningDataMap.put("note2", sngWrn.getNote2());
		songWarningDataMap.put("note3", sngWrn.getNote3());
		songWarningDataMap.put("note4", sngWrn.getNote4());
		songWarningDataMap.put("note5", sngWrn.getNote5());
		songWarningDataMap.put("note6", sngWrn.getNote6());
		songWarningDataMap.put("sequence", sngWrn.getSequence());
		songWarningDataMap.put("displayFrom", sngWrn.getDisplayFrom());
		songWarningDataMap.put("displayTo", sngWrn.getDisplayTo());
		songWarningDataMap.put("status", sngWrn.getStatus());

		logger.debug("exited getSongWarningData method");
		return songWarningDataMap;
	}
	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * This method creates key value pairs required for SONGAKA ENTITY Json data using the data obtained from database.
	 */
	public void processSongAkaData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for SongAka data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<SongAka> songAkaDataFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<SongAka>>() {});
			logger.debug("body converted into list of SongAka objects");
			int len = songAkaDataFromDb.size();
			/*List<LinkedHashMap<String, Object>> songAkaDataList = new ArrayList<LinkedHashMap<String,Object>>();*/
			List<Object> songAkaDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(SongAka sngAka:songAkaDataFromDb)
				{
					LinkedHashMap<String, Object> songAkaMap = new LinkedHashMap<String, Object>();
					songAkaMap.put("changeIndicator","U" );
					songAkaMap.put("type","SongAka" );
					logger.debug("before setting data");
					songAkaMap.put("data", getSongAkaData(sngAka));
					logger.debug("after setting data in map");
					songAkaDataList.add(songAkaMap);
				}*/
				for(SongAka sngAka:songAkaDataFromDb)
				{
					songAkaDataList.add(getSongAkaData(sngAka));
				}
				/*exchange.getOut().setBody(songAkaDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				/*if(songAkaDataList.size()==1)
				{
					json=gson.toJson(songAkaDataList.get(0));
					responseStr=json.toString();
				}
				else
				{*/
					for(Object obj:songAkaDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				/*}*/
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_SONGAKADATA+" "+exchange.getIn().getHeader("akaSongCode"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String songAkaEntityWithParam = exchange.getIn().getHeader("songAkaEntityWithParam").toString();
				if(songAkaEntityWithParam.equals("ALLSONGAKA"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGAKADATA_WITHOUTPARAM;
				}
				else if(songAkaEntityWithParam.equals("AKASONGCODE"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGAKADATA_WITHAKASONGCODE+" "+exchange.getIn().getHeader("akaSongCode");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			songAkaDataFromDb=null;
			songAkaDataList=null;
			logger.debug("Out of json class and method processSongAkaData");
		} catch (Exception e) {
			throw new Exception("Exception while converting songAka into proper json structure. Error is"+e.getMessage());
		}

	}



	private Object getSongAkaData(SongAka sngAka) {
		logger.debug("entered getSongAkaData  method");

		LinkedHashMap<String, Object> songAkaDataMap = new LinkedHashMap<String, Object>();
		songAkaDataMap.put("name", sngAka.getName());
		songAkaDataMap.put("songCode", sngAka.getSongCode());
		songAkaDataMap.put("aliasSongCode", sngAka.getAliasSongCode());

		logger.debug("exited getSongAkaData method");
		return songAkaDataMap;
	}
	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * This method creates key value pairs required for SONGINTERESTEDPARTY ENTITY Json data using the data obtained from database.
	 */
	public void processSongIpData(Exchange exchange) throws Exception
	{ 
		logger.info("In SONGIP converter");
		try {

			logger.debug("in json class about to convert body for SongIp data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<SongInterestedParty> songIpListFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<SongInterestedParty>>() {});
			logger.debug("body converted into list of SongIP objects");
			int len = songIpListFromDb.size();
			/*List<LinkedHashMap<String, Object>> songIpDataList = new ArrayList<LinkedHashMap<String,Object>>();*/
			List<Object> songIpDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(SongInterestedParty songIp:songIpListFromDb)
				{
					LinkedHashMap<String, Object> songIpMap = new LinkedHashMap<String, Object>();
					songIpMap.put("changeIndicator","U" );
					songIpMap.put("type","SongIP" );
					logger.debug("before setting data");
					songIpMap.put("data", getSongIpData(songIp));
					logger.debug("after setting data in map");
					songIpDataList.add(songIpMap);
				}*/
				for(SongInterestedParty songIp:songIpListFromDb)
				{
					songIpDataList.add(getSongIpData(songIp));
				}
				/*exchange.getOut().setBody(songIpDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				String songIpEntityWithParam = exchange.getIn().getHeader("songIpEntityWithParam").toString();
				if(songIpEntityWithParam.equals("SONGCODEANDIPLINK"))
				{
					json=gson.toJson(songIpDataList.get(0));
					responseStr=json.toString();
				}
				else
				{
					for(Object obj:songIpDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				}
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_SONGIPDATA+" "+exchange.getIn().getHeader("songcode")+" and "+exchange.getIn().getHeader("iplinkId"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String songIpEntityWithParam = exchange.getIn().getHeader("songIpEntityWithParam").toString();
				if(songIpEntityWithParam.equals("ALLSONGIPS"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGIPDATA_WITHOUTPARAM;
				}
				else if(songIpEntityWithParam.equals("SONGCODE"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGIPDATA_WITHSONGCODE+" "+exchange.getIn().getHeader("songcode");
				}
				else if(songIpEntityWithParam.equals("SONGCODEANDIPLINK"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGIPDATA_WITHSONGCODEANDIPLINK+" "+exchange.getIn().getHeader("songcode")+" and "+exchange.getIn().getHeader("iplinkId");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			songIpDataList=null;
			songIpListFromDb=null;
			logger.debug("Out of json class and method processSongIpData");
		} catch (Exception e) {
			throw new Exception("Exception while converting songIp into proper json structure. Error is"+e.getMessage());
		}

	}
	private Object getSongIpData(SongInterestedParty songIp) {
		logger.debug("entered getSongIpData  method");

		LinkedHashMap<String, Object> songIpDataMap = new LinkedHashMap<String, Object>();
		songIpDataMap.put("songCode",songIp.getSongCode());
		songIpDataMap.put("ipLinkId",songIp.getIpLinkId());
		songIpDataMap.put("sequenceNumber",songIp.getSequenceNumber());
		songIpDataMap.put("capacityTypeCode",songIp.getCapacityTypeCode());
		songIpDataMap.put("isControlled",songIp.getIsControlled());
		songIpDataMap.put("ipCode1",songIp.getIpCode1());
		songIpDataMap.put("ipType1",songIp.getIpType1());
		songIpDataMap.put("territoryId",songIp.getTerritoryId());
		songIpDataMap.put("isOriginatingTerritory",songIp.getIsOriginatingTerritory());
		songIpDataMap.put("authorContributionPercentage",songIp.getAuthorContributionPercentage());
		songIpDataMap.put("composerContributionPercentage",songIp.getComposerContributionPercentage());
		songIpDataMap.put("generalContributionPercentage",songIp.getGeneralContributionPercentage());
		songIpDataMap.put("mechControlledShare",songIp.getMechControlledShare());
		songIpDataMap.put("perfControlledShare",songIp.getPerfControlledShare());
		songIpDataMap.put("synchControlledShare",songIp.getSynchControlledShare());
		songIpDataMap.put("mechOwnershipShare",songIp.getMechOwnershipShare());
		songIpDataMap.put("perfOwnershipShare",songIp.getPerfOwnershipShare());
		songIpDataMap.put("synchOwnershipShare",songIp.getSynchOwnershipShare());
		songIpDataMap.put("parent",songIp.getParent());

		logger.debug("exited getSongIpData method");
		return songIpDataMap;
	}

	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * This method creates key value pairs required for SONGNOTES ENTITY Json data using the data obtained from database.
	 */
	public void processSongNotesData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for SongNotes data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<SongNotes> songNotesDataFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<SongNotes>>() {});
			logger.debug("body converted into list of SongNotes objects");
			int len = songNotesDataFromDb.size();
			/*List<LinkedHashMap<String, Object>> songNotesDataList = new ArrayList<LinkedHashMap<String,Object>>();*/
			List<Object> songNotesDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(SongNotes sngNotes:songNotesDataFromDb)
				{
					LinkedHashMap<String, Object> songNotesMap = new LinkedHashMap<String, Object>();
					songNotesMap.put("changeIndicator","U" );
					songNotesMap.put("type","SongNotes" );
					logger.debug("before setting data");
					songNotesMap.put("data", getSongNotesData(sngNotes));
					logger.debug("after setting data in map");
					songNotesDataList.add(songNotesMap);
				}*/
				for(SongNotes sngNotes:songNotesDataFromDb)
				{
					songNotesDataList.add(getSongNotesData(sngNotes));
				}
				/*exchange.getOut().setBody(songNotesDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				/*if(songNotesDataList.size()==1)
				{
					json=gson.toJson(songNotesDataList.get(0));
					responseStr=json.toString();
				}
				else
				{*/
					for(Object obj:songNotesDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
					logger.info("data after conversion :"+jsonList.toString());
				/*}*/
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_SONGNOTESDATA+" "+exchange.getIn().getHeader("songCode"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String songNotesEntityWithParam = exchange.getIn().getHeader("songNotesEntityWithParam").toString();
				if(songNotesEntityWithParam.equals("ALLSONGNOTES"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGNOTESDATA_WITHOUTPARAM;
				}
				else if(songNotesEntityWithParam.equals("SONGNOTESSONGCODE"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGNOTESDATA_WITHSONGCODE+" "+exchange.getIn().getHeader("songCode");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			songNotesDataFromDb=null;
			songNotesDataList=null;
			logger.debug("Out of json class and method processSongNotesData");
		} catch (Exception e) {
			throw new Exception("Exception while converting songNotes into proper json structure. Error is"+e.getMessage());
		}

	}
	private Object getSongNotesData(SongNotes sngNotes) {
		logger.debug("entered getSongNotesData  method");

		LinkedHashMap<String, Object> songNotesDataMap = new LinkedHashMap<String, Object>();
		songNotesDataMap.put("songCode", sngNotes.getSongCode());
		songNotesDataMap.put("type", sngNotes.getType());
		songNotesDataMap.put("sequence", sngNotes.getSequence());
		songNotesDataMap.put("note", sngNotes.getNote());
		logger.info("Note after extracting from body is :"+sngNotes.getNote());
		songNotesDataMap.put("system", sngNotes.getSystem());

		logger.debug("exited getSongNotesData method");
		return songNotesDataMap;
	}
	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * This method creates key value pairs required for SONGSHARE ENTITY Json data using the data obtained from database.
	 */
	public void processSongShareData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for SongShare data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<SongShare> songShareDataFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<SongShare>>() {});
			logger.debug("body converted into list of SongShare objects");
			int len = songShareDataFromDb.size();
			/*List<LinkedHashMap<String, Object>> songShareDataList = new ArrayList<LinkedHashMap<String,Object>>();*/
			List<Object> songShareDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(SongShare sngShr:songShareDataFromDb)
				{
					LinkedHashMap<String, Object> songShareMap = new LinkedHashMap<String, Object>();
					songShareMap.put("changeIndicator","U" );
					songShareMap.put("type","SongShare" );
					logger.debug("before setting data");
					songShareMap.put("data", getSongShareData(sngShr));
					logger.debug("after setting data in map");
					songShareDataList.add(songShareMap);
				}*/
				for(SongShare sngShr:songShareDataFromDb)
				{
					songShareDataList.add(getSongShareData(sngShr));
				}
				/*exchange.getOut().setBody(songShareDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				String songShareEntityWithParam = exchange.getIn().getHeader("songShareEntityWithParam").toString();
				if(songShareEntityWithParam.equals("SONGCODEANDTISCODE"))
				{
					json=gson.toJson(songShareDataList.get(0));
					responseStr=json.toString();
				}
				else
				{
					for(Object obj:songShareDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				}
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_SONGSHAREDATA+" "+exchange.getIn().getHeader("songCode")+" and "+exchange.getIn().getHeader("tisCode"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String songShareEntityWithParam = exchange.getIn().getHeader("songShareEntityWithParam").toString();
				if(songShareEntityWithParam.equals("ALLSONGSHARE"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGSHAREDATA_WITHOUTPARAM;
				}
				else if(songShareEntityWithParam.equals("SONGCODEANDTISCODE"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGSHAREDATA_WITHSONGCODEANDTISCODE+" "+exchange.getIn().getHeader("songCode")+" and "+exchange.getIn().getHeader("tisCode");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			songShareDataFromDb=null;
			songShareDataList=null;
			logger.debug("Out of json class and method processSongShareData");
		} catch (Exception e) {
			throw new Exception("Exception while converting songShare into proper json structure. Error is"+e.getMessage());
		}

	}
	private Object getSongShareData(SongShare sngShr) {
		logger.debug("entered getSongShareData  method");

		LinkedHashMap<String, Object> songShareDataMap = new LinkedHashMap<String, Object>();
		songShareDataMap.put("songCode", sngShr.getSongCode());
		songShareDataMap.put("tisCode", sngShr.getTisCode());
		songShareDataMap.put("mechOwnershipShare", sngShr.getMechOwnershipShare());
		songShareDataMap.put("perfOwnershipShare", sngShr.getPerfOwnershipShare());
		songShareDataMap.put("synchOwnershipShare", sngShr.getSynchOwnershipShare());
		songShareDataMap.put("mechControlledShare", sngShr.getMechControlledShare());
		songShareDataMap.put("perfControlledShare", sngShr.getPerfControlledShare());
		songShareDataMap.put("synchControlledShare", sngShr.getSynchControlledShare());
		songShareDataMap.put("isControlled", sngShr.getIsControlled());

		logger.debug("exited getSongShareData method");
		return songShareDataMap;
	}
	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * This method creates key value pairs required for SONGSTATUS ENTITY Json data using the data obtained from database.
	 */
	public void processSongStatusData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for SongStatus data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<SongStatus> songStatusDataFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<SongStatus>>() {});
			logger.debug("body converted into list of SongStatus objects");
			int len = songStatusDataFromDb.size();
			/*List<LinkedHashMap<String, Object>> songStatusDataList = new ArrayList<LinkedHashMap<String,Object>>();*/
			List<Object> songStatusDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(SongStatus sngStatus:songStatusDataFromDb)
				{
					LinkedHashMap<String, Object> songStatusMap = new LinkedHashMap<String, Object>();
					songStatusMap.put("changeIndicator","U" );
					songStatusMap.put("type","SongStatus" );
					logger.debug("before setting data");
					songStatusMap.put("data", getSongStatusData(sngStatus));
					logger.debug("after setting data in map");
					songStatusDataList.add(songStatusMap);
				}*/
				for(SongStatus sngStatus:songStatusDataFromDb)
				{
					songStatusDataList.add(getSongStatusData(sngStatus));
				}
				/*exchange.getOut().setBody(songStatusDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				String songStatusEntityWithParam = exchange.getIn().getHeader("songStatusEntityWithParam").toString();
				if(songStatusEntityWithParam.equals("SONGSTATUSCODE"))
				{
					json=gson.toJson(songStatusDataList.get(0));
					responseStr=json.toString();
				}
				else
				{
					for(Object obj:songStatusDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				}
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_SONGSTATUSDATA+" "+exchange.getIn().getHeader("songStatusCode"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String songStatusEntityWithParam = exchange.getIn().getHeader("songStatusEntityWithParam").toString();
				if(songStatusEntityWithParam.equals("ALLSONGSTATUS"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGSTATUSDATA_WITHOUTPARAM;
				}
				else if(songStatusEntityWithParam.equals("SONGSTATUSCODE"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGSTATUSDATA_WITHSONGCODE+" "+exchange.getIn().getHeader("songStatusCode");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			songStatusDataFromDb=null;
			songStatusDataList=null;
			logger.debug("Out of json class and method processSongStatusData");
		} catch (Exception e) {
			throw new Exception("Exception while converting songStatus into proper json structure. Error is"+e.getMessage());
		}

	}
	private Object getSongStatusData(SongStatus sngStatus) {
		logger.debug("entered getSongStatusData  method");

		LinkedHashMap<String, Object> songStatusDataMap = new LinkedHashMap<String, Object>();
		songStatusDataMap.put("songStatusCode", sngStatus.getSongStatusCode());
		songStatusDataMap.put("description", sngStatus.getDescription());

		logger.debug("exited getSongStatusData method");
		return songStatusDataMap;
	}

	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * This method creates key value pairs required for SONGCLIENTLINK ENTITY Json data using the data obtained from database.
	 */
	public void processSongClientLinkData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for SongClientLink data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<SongClientLink> songClientLinkDataFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<SongClientLink>>() {});
			logger.debug("body converted into list of SongClientLink objects");
			int len = songClientLinkDataFromDb.size();
			/*List<LinkedHashMap<String, Object>> songClientLinkDataList = new ArrayList<LinkedHashMap<String,Object>>();*/
			List<Object> songClientLinkDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(SongClientLink sngClnLink:songClientLinkDataFromDb)
				{
					LinkedHashMap<String, Object> songClientLinkMap = new LinkedHashMap<String, Object>();
					songClientLinkMap.put("changeIndicator","U" );
					songClientLinkMap.put("type","SongClientLink" );
					logger.debug("before setting data");
					songClientLinkMap.put("data", getSongClientLinkData(sngClnLink));
					logger.debug("after setting data in map");
					songClientLinkDataList.add(songClientLinkMap);
				}*/
				for(SongClientLink sngClnLink:songClientLinkDataFromDb)
				{
					songClientLinkDataList.add(getSongClientLinkData(sngClnLink));
				}
				/*exchange.getOut().setBody(songClientLinkDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				/*if(songClientLinkDataList.size()==1)
				{
					json=gson.toJson(songClientLinkDataList.get(0));
					responseStr=json.toString();
				}
				else
				{*/
					for(Object obj:songClientLinkDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				/*}*/
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_SONGCLIENTLINKDATA+exchange.getIn().getHeader("songCode")+" and "+exchange.getIn().getHeader("sysTerr"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String songClientLinkEntityWithParam = exchange.getIn().getHeader("songClientLinkEntityWithParam").toString();
				if(songClientLinkEntityWithParam.equals("ALLSONGCLIENTLINK"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGCLIENTLINKDATA_WITHOUTPARAM;
				}
				else if(songClientLinkEntityWithParam.equals("SONGCODEANDSYSTERR"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGCLIENTLINKDATA_WITHSONGCODEANDSYSTERR+" "+exchange.getIn().getHeader("songCode")+" and "+exchange.getIn().getHeader("sysTerr");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			songClientLinkDataFromDb=null;
			songClientLinkDataList=null;
			logger.debug("Out of json class and method processSongClientLinkData");
		} catch (Exception e) {
			throw new Exception("Exception while converting songClietnLink into proper json structure. Error is"+e.getMessage());
		}

	}
	private Object getSongClientLinkData(SongClientLink sngClnLink) {
		logger.debug("entered getSongClientLinkData  method");

		LinkedHashMap<String, Object> songClientLinkDataMap = new LinkedHashMap<String, Object>();
		songClientLinkDataMap.put("clientCode", sngClnLink.getClientCode());
		songClientLinkDataMap.put("songCode", sngClnLink.getSongCode());
		songClientLinkDataMap.put("subCode", sngClnLink.getSubCode());
		songClientLinkDataMap.put("sysTerr", sngClnLink.getSysTerr());

		logger.debug("exited getSongClientLinkData method");
		return songClientLinkDataMap;
	}
	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * This method creates key value pairs required for CLIENT ENTITY Json data using the data obtained from database.
	 */
	public void processClientData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for Client data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<Client> clientDataFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<Client>>() {});
			logger.debug("body converted into list of Client objects");
			int len = clientDataFromDb.size();
			/*List<LinkedHashMap<String, Object>> clientDataList = new ArrayList<LinkedHashMap<String,Object>>();*/
			List<Object> clientDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(Client client:clientDataFromDb)
				{
					LinkedHashMap<String, Object> clientMap = new LinkedHashMap<String, Object>();
					clientMap.put("changeIndicator","U" );
					clientMap.put("type","Client" );
					logger.debug("before setting data");
					clientMap.put("data", getClientData(client));
					logger.debug("after setting data in map");
					clientDataList.add(clientMap);
				}*/
				for(Client client:clientDataFromDb)
				{
					clientDataList.add(getClientData(client));
				}
				/*exchange.getOut().setBody(clientDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				String clientEntityWithParam = exchange.getIn().getHeader("clientEntityWithParam").toString();
				if(clientEntityWithParam.equals("CLIENTCODE"))
				{
					json=gson.toJson(clientDataList.get(0));
					responseStr=json.toString();
				}
				else
				{
					for(Object obj:clientDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				}
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_CLIENTDATA+exchange.getIn().getHeader("clientCode"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String clientEntityWithParam = exchange.getIn().getHeader("clientEntityWithParam").toString();
				if(clientEntityWithParam.equals("ALLCLIENT"))
				{
					responseStr = BmgSongsApiConstants.NO_CLIENTDATA_WITHOUTPARAM;
				}
				else if(clientEntityWithParam.equals("CLIENTCODE"))
				{
					responseStr = BmgSongsApiConstants.NO_CLIENTDATA_WITHCLIENTCODE+" "+exchange.getIn().getHeader("clientCode");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			clientDataFromDb=null;
			clientDataList=null;
			logger.debug("Out of json class and method processClientData");
		} catch (Exception e) {
			throw new Exception("Exception while converting client into proper json structure. Error is"+e.getMessage());
		}

	}
	private Object getClientData(Client client) {
		logger.debug("entered getClientData  method");

		LinkedHashMap<String, Object> clientDataMap = new LinkedHashMap<String, Object>();
		clientDataMap.put("clientCode", client.getClientCode());
		clientDataMap.put("clientName", client.getClientName());

		logger.debug("exited getClientData method");
		return clientDataMap;
	}
	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * This method creates key value pairs required for CLIENTDEALLINK ENTITY Json data using the data obtained from database.
	 */
	public void processClientDealLinkData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for ClientDealLink data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<ClientDealLink> clientDealLinkDataFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<ClientDealLink>>() {});
			logger.debug("body converted into list of ClientDealLink objects");
			int len = clientDealLinkDataFromDb.size();
			/*List<LinkedHashMap<String, Object>> clientDealLinkDataList = new ArrayList<LinkedHashMap<String,Object>>();*/
			List<Object> clientDealLinkDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(ClientDealLink clnDealLink:clientDealLinkDataFromDb)
				{
					LinkedHashMap<String, Object> clientDealLinkMap = new LinkedHashMap<String, Object>();
					clientDealLinkMap.put("changeIndicator","U" );
					clientDealLinkMap.put("type","ClientDealLink" );
					logger.debug("before setting data");
					clientDealLinkMap.put("data", getClientDealLinkData(clnDealLink));
					logger.debug("after setting data in map");
					clientDealLinkDataList.add(clientDealLinkMap);
				}
				exchange.getOut().setBody(clientDealLinkDataList);*/
				for(ClientDealLink dealLink:clientDealLinkDataFromDb)
				{
					clientDealLinkDataList.add(getClientDealLinkData(dealLink));
				}
				/*exchange.getOut().setBody(clientDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				String clientDealLinkEntityWithParam = exchange.getIn().getHeader("clientDealLinkEntityWithParam").toString();
				if(clientDealLinkEntityWithParam.equals("CLIENTCODEANDSYSTERR"))
				{
					json=gson.toJson(clientDealLinkDataList.get(0));
					responseStr=json.toString();
				}
				else
				{
					for(Object obj:clientDealLinkDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				}
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_CLIENTDEALLINKDATA+exchange.getIn().getHeader("clientCode")+" and "+exchange.getIn().getHeader("sysTerr"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String clientDealLinkEntityWithParam = exchange.getIn().getHeader("clientDealLinkEntityWithParam").toString();
				if(clientDealLinkEntityWithParam.equals("ALLCLIENTDEALLINKS"))
				{
					responseStr = BmgSongsApiConstants.NO_CLIENTDEALLINKDATA_WITHOUTPARAM;
				}
				else if(clientDealLinkEntityWithParam.equals("CLIENTCODEANDSYSTERR"))
				{
					responseStr = BmgSongsApiConstants.NO_CLIENTDEALLINKDATA_WITHCLIENTCODEANDSYSTERR+" "+exchange.getIn().getHeader("clientCode")+" and "+exchange.getIn().getHeader("sysTerr");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			clientDealLinkDataFromDb=null;
			clientDealLinkDataList=null;
			logger.debug("Out of json class and method processClientDealLinkData");
		} catch (Exception e) {
			throw new Exception("Exception while converting clientDealLink into proper json structure. Error is"+e.getMessage());
		}

	}
	private Object getClientDealLinkData(ClientDealLink clnDealLink) {
		logger.debug("entered getClientDealLinkData  method");

		LinkedHashMap<String, Object> clientDealLinkDataMap = new LinkedHashMap<String, Object>();
		clientDealLinkDataMap.put("clientCode", clnDealLink.getClientCode());
		clientDealLinkDataMap.put("sysTerr", clnDealLink.getSysTerr());
		clientDealLinkDataMap.put("dealNo", clnDealLink.getDealNo());
		logger.debug("exited getClientDealLinkData method");
		return clientDealLinkDataMap;
	}
	
	public void processTisCodeData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for TisCode data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<TisCode> tisCodeListFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<TisCode>>() {});
			logger.debug("body converted into list of TisCode objects");
			int len = tisCodeListFromDb.size();
			List<Object> tisCodeDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(TisHierarchyLink tisHieLink:tisHierarchyListFromDb)
				{
					LinkedHashMap<String, Object> tisHierarchyMap = new LinkedHashMap<String, Object>();
					tisHierarchyMap.put("changeIndicator","U" );
					tisHierarchyMap.put("type","TisHierarchyLink" );
					logger.debug("before setting data");
					tisHierarchyMap.put("data", getTisHierarchyData(tisHieLink));
					logger.debug("after setting data in map");
					tisHierarchyDataList.add(tisHierarchyMap);
				}*/
				for(TisCode tisCode:tisCodeListFromDb)
				{
					tisCodeDataList.add(getTisCodeData(tisCode));
				}
				/*exchange.getOut().setBody(tisHierarchyDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				String tisCodeEntityWithParam = exchange.getIn().getHeader("tisCodeEntityWithParam").toString();
				if(tisCodeEntityWithParam.equals("TISCODE"))
				{
					json=gson.toJson(tisCodeDataList.get(0));
					responseStr=json.toString();
				}
				else
				{
					
					for(Object obj:tisCodeDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				}
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_TISHIERARCHYDATA+" "+exchange.getIn().getHeader("tislinkid"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String tisCodeEntityWithParam = exchange.getIn().getHeader("tisCodeEntityWithParam").toString();
				if(tisCodeEntityWithParam.equals("ALLTISCODE"))
				{
					responseStr = BmgSongsApiConstants.NO_TISCODEDATA_WITHOUTPARAM;
				}
				else if(tisCodeEntityWithParam.equals("TISCODE"))
				{
					responseStr = BmgSongsApiConstants.NO_TISCODEDATA_WITHTISCODE+" "+exchange.getIn().getHeader("tisCode");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			tisCodeListFromDb=null;
			tisCodeDataList=null;
			logger.debug("Out of json class and method processTisCodeData");
		} catch (Exception e) {
			throw new Exception("Exception while converting tisCode into proper json structure. Error is"+e.getMessage());
		}

	}
	
	private Object getTisCodeData(TisCode tisCode) {
		logger.debug("entered getTisCodeData  method");

		LinkedHashMap<String, Object> tisCodeDataMap = new LinkedHashMap<String, Object>();
		tisCodeDataMap.put("tisCode", tisCode.getTisCode());
		tisCodeDataMap.put("tisDesc", tisCode.getTisDesc());
	
		logger.debug("exited getTisCodeData method");
		return tisCodeDataMap;
	}
	
	public void processDealData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for Deal data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<Deal> dealDataFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<Deal>>() {});
			logger.debug("body converted into list of deal objects");
			int len = dealDataFromDb.size();
			
			List<Object> dealDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(ClientDealLink clnDealLink:clientDealLinkDataFromDb)
				{
					LinkedHashMap<String, Object> clientDealLinkMap = new LinkedHashMap<String, Object>();
					clientDealLinkMap.put("changeIndicator","U" );
					clientDealLinkMap.put("type","ClientDealLink" );
					logger.debug("before setting data");
					clientDealLinkMap.put("data", getClientDealLinkData(clnDealLink));
					logger.debug("after setting data in map");
					clientDealLinkDataList.add(clientDealLinkMap);
				}
				exchange.getOut().setBody(clientDealLinkDataList);*/
				for(Deal deal:dealDataFromDb)
				{
					dealDataList.add(getDealData(deal));
				}
				/*exchange.getOut().setBody(clientDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				String dealEntityWithParam = exchange.getIn().getHeader("dealEntityWithParam").toString();
				if(dealEntityWithParam.equals("SYSTERRANDDEALNO"))
				{
					json=gson.toJson(dealDataList.get(0));
					responseStr=json.toString();
				}
				else
				{
					for(Object obj:dealDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				}
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_CLIENTDEALLINKDATA+exchange.getIn().getHeader("clientCode")+" and "+exchange.getIn().getHeader("sysTerr"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String dealEntityWithParam = exchange.getIn().getHeader("dealEntityWithParam").toString();
				if(dealEntityWithParam.equals("ALLDEALS"))
				{
					responseStr = BmgSongsApiConstants.NO_DEALDATA_WITHOUTPARAM;
				}
				else if(dealEntityWithParam.equals("SYSTERRANDDEALNO"))
				{
					responseStr = BmgSongsApiConstants.NO_DEALDATA_WITHSYSTERRANDDEALNO+exchange.getIn().getHeader("sysTerr")+" and "+exchange.getIn().getHeader("dealNo");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			dealDataFromDb=null;
			dealDataList=null;
			logger.debug("Out of json class and method processDealData");
		} catch (Exception e) {
			throw new Exception("Exception while converting deal into proper json structure. Error is"+e.getMessage());
		}

	}
	private Object getDealData(Deal deal) {
		logger.debug("entered getDealData  method");

		LinkedHashMap<String, Object> dealDataMap = new LinkedHashMap<String, Object>();
		dealDataMap.put("sysTerr", deal.getSysTerr());
		dealDataMap.put("dealNo", deal.getDealNo());
		dealDataMap.put("dealName", deal.getDealName());
	
		logger.debug("exited getDealData method");
		return dealDataMap;
	}
	
	public void processSongGroupData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for SongGroup data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<SongGroup> songGroupListFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<SongGroup>>() {});
			logger.debug("body converted into list of SongGroup objects");
			int len = songGroupListFromDb.size();
			List<Object> songGroupDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(TisHierarchyLink tisHieLink:tisHierarchyListFromDb)
				{
					LinkedHashMap<String, Object> tisHierarchyMap = new LinkedHashMap<String, Object>();
					tisHierarchyMap.put("changeIndicator","U" );
					tisHierarchyMap.put("type","TisHierarchyLink" );
					logger.debug("before setting data");
					tisHierarchyMap.put("data", getTisHierarchyData(tisHieLink));
					logger.debug("after setting data in map");
					tisHierarchyDataList.add(tisHierarchyMap);
				}*/
				for(SongGroup songGroup:songGroupListFromDb)
				{
					songGroupDataList.add(getSongGroupData(songGroup));
				}
				/*exchange.getOut().setBody(tisHierarchyDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				String songGroupEntityWithParam = exchange.getIn().getHeader("songGroupEntityWithParam").toString();
				if(songGroupEntityWithParam.equals("GROUPCODE"))
				{
					json=gson.toJson(songGroupDataList.get(0));
					responseStr=json.toString();
				}
				else
				{
					for(Object obj:songGroupDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				}
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_TISHIERARCHYDATA+" "+exchange.getIn().getHeader("tislinkid"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String songGroupEntityWithParam = exchange.getIn().getHeader("songGroupEntityWithParam").toString();
				if(songGroupEntityWithParam.equals("ALLSONGGROUP"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGGROUPDATA_WITHOUTPARAM;
				}
				else if(songGroupEntityWithParam.equals("GROUPCODE"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGGROUPDATA_WITHGROUPCODE+" "+exchange.getIn().getHeader("groupCode");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			songGroupListFromDb=null;
			songGroupDataList=null;
			logger.debug("Out of json class and method processSongGroupData");
		} catch (Exception e) {
			throw new Exception("Exception while converting songGroup into proper json structure. Error is"+e.getMessage());
		}

	}
	private Object getSongGroupData(SongGroup songGroup) {
		logger.debug("entered getSongGroupData  method");

		LinkedHashMap<String, Object> songGroupDataMap = new LinkedHashMap<String, Object>();
		songGroupDataMap.put("groupCode", songGroup.getGroupCode());
		songGroupDataMap.put("groupName", songGroup.getGroupName());
		songGroupDataMap.put("groupShortName", songGroup.getGroupShortName());
	
		logger.debug("exited getSongGroupData method");
		return songGroupDataMap;
	}
	
	public void processSongGroupLinkData(Exchange exchange) throws Exception
	{ 
		try {

			logger.debug("in json class about to convert body for SongGroupLink data");
			Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());
			ObjectMapper mapper = new ObjectMapper();
			List<SongGroupLink> songGroupLinkListFromDb = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<SongGroupLink>>() {});
			logger.debug("body converted into list of SongGroupLink objects");
			int len = songGroupLinkListFromDb.size();
			List<Object> songGroupLinkDataList = new ArrayList<Object>();
			if(len>0)
			{
				/*for(TisHierarchyLink tisHieLink:tisHierarchyListFromDb)
				{
					LinkedHashMap<String, Object> tisHierarchyMap = new LinkedHashMap<String, Object>();
					tisHierarchyMap.put("changeIndicator","U" );
					tisHierarchyMap.put("type","TisHierarchyLink" );
					logger.debug("before setting data");
					tisHierarchyMap.put("data", getTisHierarchyData(tisHieLink));
					logger.debug("after setting data in map");
					tisHierarchyDataList.add(tisHierarchyMap);
				}*/
				for(SongGroupLink groupLink:songGroupLinkListFromDb)
				{
					songGroupLinkDataList.add(getSongGroupLinkData(groupLink));
				}
				/*exchange.getOut().setBody(tisHierarchyDataList);*/
				Response response = null;
				String responseStr=null;
				int status=0;
				List<String> jsonList = new ArrayList<String>();
				String json=null;
				Gson gson = new Gson();
				/*if(songGroupLinkDataList.size()==1)
				{
					json=gson.toJson(songGroupLinkDataList.get(0));
					responseStr=json.toString();
				}
				else
				{*/
					for(Object obj:songGroupLinkDataList)
					{
						json = gson.toJson(obj);
						jsonList.add(json);
					}
					responseStr=jsonList.toString();
				/*}*/
				status=BmgSongsApiConstants.success_Status;
				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			else
			{
				/*exchange.getOut().setBody(BmgSongsApiConstants.NO_TISHIERARCHYDATA+" "+exchange.getIn().getHeader("tislinkid"));*/
				Response response = null;
				String responseStr=null;
				int status=BmgSongsApiConstants.fail_NotFound_Status;

				String songGroupLinkEntityWithParam = exchange.getIn().getHeader("songGroupLinkEntityWithParam").toString();
				if(songGroupLinkEntityWithParam.equals("ALLSONGGROUPLINK"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGGROUPLINKDATA_WITHOUTPARAM;
				}
				else if(songGroupLinkEntityWithParam.equals("GRPACODE"))
				{
					responseStr = BmgSongsApiConstants.NO_SONGGROUPLINKDATA_WITHGRPACODE+" "+exchange.getIn().getHeader("grpaCode");
				}

				response = Response.status(status).type("application/json").entity(responseStr).header("page",page).header("per_page", per_page).build();
				exchange.getOut().setBody(response);
			}
			songGroupLinkListFromDb=null;
			songGroupLinkDataList=null;
			logger.debug("Out of json class and method processSongGroupLinkData");
		} catch (Exception e) {
			throw new Exception("Exception while converting songGroupLink into proper json structure. Error is"+e.getMessage());
		}

	}
	private Object getSongGroupLinkData(SongGroupLink groupLink) {
		logger.debug("entered getSongGroupLinkData  method");

		LinkedHashMap<String, Object> songGroupDataMap = new LinkedHashMap<String, Object>();
		songGroupDataMap.put("songCode", groupLink.getSongCode());
		songGroupDataMap.put("groupCode", groupLink.getGroupCode());
		songGroupDataMap.put("type", groupLink.getType());
	
		logger.debug("exited getSongGroupLinkData method");
		return songGroupDataMap;
	}
	
}
