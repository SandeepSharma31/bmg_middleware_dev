package com.bmg.esb.bmgsongs.imdata.api.processor;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import org.apache.camel.Exchange;
import org.apache.log4j.Logger;

import com.bmg.esb.bmgsongs.imdata.api.constants.BmgSongsApiConstants;

/**
 * 
 * @author rajesh_thattikonda
 * This Class has different methods for different entities to retrieve parameter values from the URI and helps to call the respective route for each entity.
 * This Class will also handle exceptions like if no values are passed or if mandatory parameters are not passed in the URI.
 */
public class BmgSongsServiceProcessor {
	private static Logger logger = Logger.getLogger(BmgSongsServiceProcessor.class.getName());
	public String process(Exchange ex, @Context  UriInfo info) throws Exception
	{ 
		try
		{
			logger.info("info.getBaseUri().getPath() is "+info.getBaseUri().getPath());
			logger.info("info.getAbsolutePath().getPath() is "+info.getAbsolutePath().getPath());

			String  page=info.getQueryParameters().getFirst("page");
			String  per_page=info.getQueryParameters().getFirst("per_page");
			String[] perpage=getPageAndPerPageVals(page,per_page);
			logger.info("page value is "+perpage[0]+" and per_page value is "+perpage[1]);
			ex.getIn().setHeader("page", perpage[0]);
			ex.getIn().setHeader("per_page", perpage[1]);
			Integer[] startEndRecNumbers=getStartAndEndRecordNumbers(perpage[0],perpage[1]);
			logger.info("start and end numbers after method are "+startEndRecNumbers[0] +" and "+startEndRecNumbers[1]);
			ex.getIn().setHeader("startRecord", startEndRecNumbers[0]);
			ex.getIn().setHeader("endRecord", startEndRecNumbers[1]);
			/*ex.getIn().setHeader("startRecord", 0);
			ex.getIn().setHeader("endRecord", 5);*/
			logger.info("start header is " + ex.getIn().getHeader("startRecord"));
			logger.info("end header is "+ex.getIn().getHeader("endRecord"));
			/*if(Integer.parseInt(perpage[1])<=300)
			{*/
			/*if(info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.SONG_URI_ENDSWITH))
			{
				String sngCodeFromUri=info.getQueryParameters(). getFirst("songcode");
				String delIndFromUri=info.getQueryParameters(). getFirst("deleteind");
				logger.info("sngCode and dlete values are" + sngCodeFromUri +" and " +delIndFromUri);
				if(sngCodeFromUri==null || delIndFromUri==null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_SONGMSG);
				}
				else
				{
					if((!(sngCodeFromUri.equals("null")) && !sngCodeFromUri.isEmpty()) && (!(delIndFromUri.equals("null")) && !delIndFromUri.isEmpty()))
					{
						logger.info("sngCode and dlete values are" + sngCodeFromUri +" and " +delIndFromUri);
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONG);
						Integer  songCode=Integer.parseInt(info.getQueryParameters(). getFirst("songcode"));
						String  deleteInd=info.getQueryParameters(). getFirst("deleteind");
						ex.getIn().setHeader("songcode", songCode);
						ex.getIn().setHeader("deleteind", deleteInd);
					}
					else
					{
						if(sngCodeFromUri.isEmpty())
						{
							sngCodeFromUri="empty";
						}
						if(delIndFromUri.isEmpty())
						{
							delIndFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGPARAM_MSG+sngCodeFromUri+" and "+delIndFromUri);

					}
				}
			}*/
			if(info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.SONG_URI_ENDSWITH))
			{
				
				
				logger.info("In Song PRocessor");
				String sngCodeFromUri=info.getQueryParameters(). getFirst("songcode");
				String delIndFromUri=info.getQueryParameters(). getFirst("deleteind");
				if(sngCodeFromUri==null && delIndFromUri==null)
				{
					ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONG);
					ex.getIn().setHeader("songEntityWithParam", BmgSongsApiConstants.SONG_PARAMS_ALL);
				}
				else if(sngCodeFromUri!=null && delIndFromUri==null)
				{
					if(!(sngCodeFromUri.equals("null")) && !sngCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONG);
						ex.getIn().setHeader("songEntityWithParam", BmgSongsApiConstants.SONG_PARAMS_SONGCODE);
						Integer  songCode=Integer.parseInt(info.getQueryParameters(). getFirst("songcode"));
						ex.getIn().setHeader("songcode", songCode);
					}
					else
					{
						if(sngCodeFromUri.isEmpty())
						{
							sngCodeFromUri="empty";
						}

						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGPARAM_SONGCODE_MSG+sngCodeFromUri);

					}
				}
				else if(sngCodeFromUri==null && delIndFromUri!=null)
				{
					if((!(delIndFromUri.equals("null")) && !delIndFromUri.isEmpty()))
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONG);
						ex.getIn().setHeader("songEntityWithParam", BmgSongsApiConstants.SONG_PARAMS_DELETEIND);
						String  deleteInd=info.getQueryParameters(). getFirst("deleteind");
						ex.getIn().setHeader("deleteind", deleteInd);
					}
					else
					{
						if(delIndFromUri.isEmpty())
						{
							delIndFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGPARAM_DELETEIND_MSG+delIndFromUri);
					}
				}
				else
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_SONGMSG);
				}

			}
			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.COMPOSER_URI_ENDSWITH)) 
			{
				/*String pubCodeFromUri = info.getQueryParameters(). getFirst("pubcode");
				if(pubCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_COMPMSG);
				}
				else
				{
					if(!(pubCodeFromUri.equals("null")) && !pubCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_COMPOSER);
						Integer pubCode = Integer.parseInt(info.getQueryParameters(). getFirst("pubcode"));
						ex.getIn().setHeader("pubcode", pubCode);
						logger.debug(ex.getIn().getHeader("pubcode"));
					}
					else
					{
						if(pubCodeFromUri.isEmpty())
						{
							pubCodeFromUri="empty";	
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_COMPPARAM_MSG+pubCodeFromUri);
					}
				}*/
				String pubCodeFromUri = info.getQueryParameters(). getFirst("pubcode");
				if(pubCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_COMPOSER);
					ex.getIn().setHeader("composerEntityWithParam", BmgSongsApiConstants.COMP_PARAMS_ALL);
				}
				else if(pubCodeFromUri != null)
				{
					if(!(pubCodeFromUri.equals("null")) && !pubCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_COMPOSER);
						ex.getIn().setHeader("composerEntityWithParam", BmgSongsApiConstants.COMP_PARAMS_PUBCODE);
						Integer pubCode = Integer.parseInt(info.getQueryParameters(). getFirst("pubcode"));
						ex.getIn().setHeader("pubcode", pubCode);
					}
					else
					{
						if(pubCodeFromUri.isEmpty())
						{
							pubCodeFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_COMPPARAM_PUBCODE_MSG+pubCodeFromUri);
					}
				}
			}
			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.PUBLISHER_URI_ENDSWITH)) 
			{
				/*String pubCodeFromUri = info.getQueryParameters(). getFirst("pubcode");
				if(pubCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_PUBLMSG);
				}
				else
				{
					if(!(pubCodeFromUri.equals("null")) && !pubCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_PUBLISHER);
						Integer pubCode = Integer.parseInt(info.getQueryParameters(). getFirst("pubcode"));
						ex.getIn().setHeader("pubcode", pubCode);
						logger.debug(ex.getIn().getHeader("pubcode"));
					}
					else
					{
						if(pubCodeFromUri.isEmpty())
						{
							pubCodeFromUri="empty";	
						}

						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_PUBLPARAM_MSG+pubCodeFromUri);
					}
				}*/
				String pubCodeFromUri = info.getQueryParameters(). getFirst("pubcode");
				if(pubCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_PUBLISHER);
					ex.getIn().setHeader("publisherEntityWithParam", BmgSongsApiConstants.PUB_PARAMS_ALL);
				}
				else if(pubCodeFromUri != null)
				{
					if(!(pubCodeFromUri.equals("null")) && !pubCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_PUBLISHER);
						ex.getIn().setHeader("publisherEntityWithParam", BmgSongsApiConstants.PUB_PARAMS_PUBCODE);
						Integer pubCode = Integer.parseInt(info.getQueryParameters(). getFirst("pubcode"));
						ex.getIn().setHeader("pubcode", pubCode);
					}
					else
					{
						if(pubCodeFromUri.isEmpty())
						{
							pubCodeFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_PUBLPARAM_PUBCODE_MSG+pubCodeFromUri);
					}
				}
			}
			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.SOCIETY_URI_ENDSWITH)) 
			{
				/*String socCodeFromUri = info.getQueryParameters(). getFirst("soccode");
				if(socCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_SOCIETYMSG);
				}
				else
				{
					if(!(socCodeFromUri.equals("null")) && !socCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SOCIETY);
						String socCode = (info.getQueryParameters(). getFirst("soccode"));
						ex.getIn().setHeader("soccode", socCode);
						logger.debug(ex.getIn().getHeader("soccode"));
					}
					else
					{
						if(socCodeFromUri.isEmpty())
						{
							socCodeFromUri="empty";	
						}

						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SOCIETYPARAM_MSG+socCodeFromUri);
					}
				}*/
				String socCodeFromUri = info.getQueryParameters(). getFirst("soccode");
				if(socCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SOCIETY);
					ex.getIn().setHeader("societyEntityWithParam", BmgSongsApiConstants.SOC_PARAMS_ALL);
				}
				else if(socCodeFromUri != null)
				{
					if(!(socCodeFromUri.equals("null")) && !socCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("societyEntityWithParam", BmgSongsApiConstants.SOC_PARAMS_SOCCODE);
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SOCIETY);
						String socCode = (info.getQueryParameters(). getFirst("soccode"));
						ex.getIn().setHeader("soccode", socCode);
					}
					else
					{
						if(socCodeFromUri.isEmpty())
						{
							socCodeFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SOCIPARAM_SOCCODE_MSG+socCodeFromUri);
					}
				}
			}


			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.TERRITORY_URI_ENDSWITH)) 
			{
				/*String terrCodeFromUri = info.getQueryParameters(). getFirst("terrCode");
				if(terrCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_TERRMSG);
				}
				else
				{
					if(!(terrCodeFromUri.equals("null")) && !terrCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_TERRITORY);
						String terrCode = (info.getQueryParameters(). getFirst("terrCode"));
						ex.getIn().setHeader("terrcode", terrCode);
						logger.debug(ex.getIn().getHeader("terrcode"));
					}
					else
					{
						if(terrCodeFromUri.isEmpty())
						{
							terrCodeFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_TERRPARAM_MSG+terrCodeFromUri);
					}
				}*/
				String terrCodeFromUri = info.getQueryParameters(). getFirst("terrCode");
				if(terrCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_TERRITORY);
					ex.getIn().setHeader("territoryEntityWithParam", BmgSongsApiConstants.TERR_PARAMS_ALL);
				}
				else if(terrCodeFromUri != null)
				{
					if(!(terrCodeFromUri.equals("null")) && !terrCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_TERRITORY);
						ex.getIn().setHeader("territoryEntityWithParam", BmgSongsApiConstants.TERR_PARAMS_TERRCODE);
						String terrCode = (info.getQueryParameters(). getFirst("terrCode"));
						ex.getIn().setHeader("terrcode", terrCode);
					}
					else
					{
						if(terrCodeFromUri.isEmpty())
						{
							terrCodeFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_TERRPARAM_TERRCODE_MSG+terrCodeFromUri);
					}
				}
			}
			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.TISHIERARCHYLINK_URI_ENDSWITH)) 
			{
				/*String tisLinkIdFromUri = info.getQueryParameters(). getFirst("tisLinkId");
				if(tisLinkIdFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_TISHIERARCHYMSG);
				}
				else
				{
					if(!(tisLinkIdFromUri.equals("null")) && !tisLinkIdFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_TISHIERARCHY);
						Integer tisLinkId = (Integer.parseInt(info.getQueryParameters(). getFirst("tisLinkId")));
						ex.getIn().setHeader("tislinkid", tisLinkId);
					}
					else
					{
						if(tisLinkIdFromUri.isEmpty())
						{
							tisLinkIdFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_TISHIERARCHYPARAM_MSG+tisLinkIdFromUri);
					}
				}*/
				String tisLinkIdFromUri = info.getQueryParameters(). getFirst("tisLinkId");
				if(tisLinkIdFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_TISHIERARCHY);
					ex.getIn().setHeader("tisHierarchyEntityWithParam", BmgSongsApiConstants.TISHIERARCHY_PARAMS_ALL);
				}
				else if(tisLinkIdFromUri != null)
				{
					if(!(tisLinkIdFromUri.equals("null")) && !tisLinkIdFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_TISHIERARCHY);
						ex.getIn().setHeader("tisHierarchyEntityWithParam", BmgSongsApiConstants.TIS_PARAMS_TISLINKID);
						Integer tisLinkId = (Integer.parseInt(info.getQueryParameters(). getFirst("tisLinkId")));
						ex.getIn().setHeader("tislinkid", tisLinkId);
					}
					else
					{
						if(tisLinkIdFromUri.isEmpty())
						{
							tisLinkIdFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_TISPARAM_TISLINK_MSG+tisLinkIdFromUri);
					}
				}
			}
			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.SONGWARNING_URI_ENDSWITH)) 
			{
				/*String warnSongCodeFromUri = info.getQueryParameters(). getFirst("warnSongCode");
				if(warnSongCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_SNGWARNMSG);
				}
				else
				{
					if(!(warnSongCodeFromUri.equals("null")) && !warnSongCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGWARNING);
						Integer warnSongCode = (Integer.parseInt(info.getQueryParameters(). getFirst("warnSongCode")));
						ex.getIn().setHeader("warnSongCode", warnSongCode);
					}
					else
					{
						if(warnSongCodeFromUri.isEmpty())
						{
							warnSongCodeFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGWARNPARAM_MSG+warnSongCodeFromUri);
					}
				}*/
				String warnSongCodeFromUri = info.getQueryParameters(). getFirst("warnSongCode");
				if(warnSongCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGWARNING);
					ex.getIn().setHeader("songWarnEntityWithParam", BmgSongsApiConstants.SONGWARN_PARAMS_ALL);
				}
				else if(warnSongCodeFromUri!=null)
				{
					if(!(warnSongCodeFromUri.equals("null")) && !warnSongCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGWARNING);
						ex.getIn().setHeader("songWarnEntityWithParam", BmgSongsApiConstants.SONGWARN_PARAMS_SONGCODE);
						Integer warnSongCode = (Integer.parseInt(info.getQueryParameters(). getFirst("warnSongCode")));
						ex.getIn().setHeader("warnSongCode", warnSongCode);
					}
					else
					{
						if(warnSongCodeFromUri.isEmpty())
						{
							warnSongCodeFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGWARNPARAM_SONGCODE_MSG+warnSongCodeFromUri);
					}
				}
			}
			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.SONGAKA_URI_ENDSWITH)) 
			{
				/*String akaSongCodeFromUri = info.getQueryParameters(). getFirst("akaSongCode");
				if(akaSongCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_SNGAKAMSG);
				}
				else
				{
					if(!(akaSongCodeFromUri.equals("null")) && !akaSongCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGAKA);
						Integer akaSongCode = (Integer.parseInt(info.getQueryParameters(). getFirst("akaSongCode")));
						ex.getIn().setHeader("akaSongCode", akaSongCode);
					}
					else
					{
						if(akaSongCodeFromUri.isEmpty())
						{
							akaSongCodeFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGAKAPARAM_MSG+akaSongCodeFromUri);
					}
				}*/
				String akaSongCodeFromUri = info.getQueryParameters(). getFirst("akaSongCode");
				if(akaSongCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.ENTITY_SONGAKA);
					ex.getIn().setHeader("songAkaEntityWithParam", BmgSongsApiConstants.SONGAKA_PARAMS_ALL);
				}
				else if(akaSongCodeFromUri!=null)
				{
					if(!(akaSongCodeFromUri.equals("null")) && !akaSongCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGAKA);
						ex.getIn().setHeader("songAkaEntityWithParam", BmgSongsApiConstants.SONGAKA_PARAMS_SONGCODE);
						Integer akaSongCode = (Integer.parseInt(info.getQueryParameters(). getFirst("akaSongCode")));
						ex.getIn().setHeader("akaSongCode", akaSongCode);
					}
					else
					{
						if(akaSongCodeFromUri.isEmpty())
						{
							akaSongCodeFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGAKAPARAM_SONGCODE_MSG+akaSongCodeFromUri);
					}
				}

			}



			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.SONGIP_URI_ENDSWITH)) 
			{

				String sngCodeFromUri = info.getQueryParameters(). getFirst("songcode");
				String ipLinkIdFromUri = info.getQueryParameters(). getFirst("iplinkId");
				/*if(sngCodeFromUri == null || ipLinkIdFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_SONGIPMSG);
				}
				else
				{
					if((!(sngCodeFromUri.equals("null")) && !sngCodeFromUri.isEmpty()) && (!(ipLinkIdFromUri.equals("null")) && !ipLinkIdFromUri.isEmpty()))
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGIP);
						Integer  songCode=Integer.parseInt(info.getQueryParameters(). getFirst("songcode"));
						Integer  ipLinkId=Integer.parseInt(info.getQueryParameters(). getFirst("iplinkId"));
						ex.getIn().setHeader("songcode", songCode);
						ex.getIn().setHeader("iplinkId", ipLinkId);
					}
					else
					{
						if(sngCodeFromUri.isEmpty())
						{
							sngCodeFromUri="empty";
						}
						if(ipLinkIdFromUri.isEmpty())
						{
							ipLinkIdFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGIPPARAM_MSG+sngCodeFromUri+" and "+ipLinkIdFromUri);
					}
				}*/
				logger.info("In SONGIP processor");
				if(sngCodeFromUri==null && ipLinkIdFromUri==null)
				{
					ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGIP);
					ex.getIn().setHeader("songIpEntityWithParam", BmgSongsApiConstants.SONGIP_PARAMS_ALL);

				}
				else if(sngCodeFromUri!=null && ipLinkIdFromUri==null)
				{
					if(!(sngCodeFromUri.equals("null")) && !sngCodeFromUri.isEmpty())
					{

						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGIP);
						ex.getIn().setHeader("songIpEntityWithParam", BmgSongsApiConstants.SONGIP_PARAMS_SONGCODE);
						Integer  songCode=Integer.parseInt(info.getQueryParameters(). getFirst("songcode"));
						ex.getIn().setHeader("songcode", songCode);
					}
					else
					{
						if(sngCodeFromUri.isEmpty())
						{
							sngCodeFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGIPPARAM_SONGCODE_MSG+sngCodeFromUri);
					}
				}
				else if(sngCodeFromUri!=null && ipLinkIdFromUri!=null)
				{
					if(!(sngCodeFromUri.equals("null")) && !sngCodeFromUri.isEmpty() && !(ipLinkIdFromUri.equals("null")) && !ipLinkIdFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGIP);
						ex.getIn().setHeader("songIpEntityWithParam", BmgSongsApiConstants.SONGIP_PARAMS_SONGCODEANDIPLINK);
						Integer  songCode=Integer.parseInt(info.getQueryParameters(). getFirst("songcode"));
						Integer  ipLinkId=Integer.parseInt(info.getQueryParameters(). getFirst("iplinkId"));
						ex.getIn().setHeader("songcode", songCode);
						ex.getIn().setHeader("iplinkId", ipLinkId);
					}
					else
					{
						if(sngCodeFromUri.isEmpty())
						{
							sngCodeFromUri="empty";
						}
						if(ipLinkIdFromUri.isEmpty())
						{
							ipLinkIdFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGIPPARAM_SONGCODEANDIPLINK_MSG+sngCodeFromUri+" and "+ipLinkIdFromUri);
					}
				}
				else if(sngCodeFromUri==null && ipLinkIdFromUri!=null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_SONGIPMSG);
				}
			}
			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.SONGNOTES_URI_ENDSWITH)) 
			{
				/*String songCodeFromUri = info.getQueryParameters(). getFirst("songCode");
				if(songCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_SNGNOTESMSG);
				}
				else
				{
					if(!(songCodeFromUri.equals("null")) && !songCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGNOTES);
						Integer songCode = Integer.parseInt(info.getQueryParameters(). getFirst("songCode"));
						ex.getIn().setHeader("songCode", songCode);
					}
					else
					{
						if(songCodeFromUri.isEmpty())
						{
							songCodeFromUri="empty";	
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGNOTESPARAM_MSG+songCodeFromUri);
					}
				}*/
				String songCodeFromUri = info.getQueryParameters(). getFirst("songCode");
				if(songCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.ENTITY_SONGNOTES);
					ex.getIn().setHeader("songNotesEntityWithParam", BmgSongsApiConstants.SONGNOTES_PARAMS_ALL);
				}
				else if(songCodeFromUri !=null)
				{
					if(!(songCodeFromUri.equals("null")) && !songCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGNOTES);
						ex.getIn().setHeader("songNotesEntityWithParam", BmgSongsApiConstants.SONGNOTES_PARAMS_SONGCODE);
						Integer songCode = Integer.parseInt(info.getQueryParameters(). getFirst("songCode"));
						ex.getIn().setHeader("songCode", songCode);
					}
					else
					{
						if(songCodeFromUri.isEmpty())
						{
							songCodeFromUri="empty";	
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGNOTESPARAM_SONGCODE_MSG+songCodeFromUri);
					}
				}

			}



			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.SONGSHARE_URI_ENDSWITH)) 
			{
				/*String songCodeFromUri = info.getQueryParameters(). getFirst("songCode");
				String tisCodeFromUri = info.getQueryParameters(). getFirst("tisCode");
				if(songCodeFromUri == null || tisCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_SNGSHAREMSG);
				}
				else
				{
					if(!(songCodeFromUri.equals("null")) && !songCodeFromUri.isEmpty() && !(tisCodeFromUri.equals("null")) && !tisCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGSHARE);
						Integer songCode = Integer.parseInt(info.getQueryParameters(). getFirst("songCode"));
						String tisCode = info.getQueryParameters(). getFirst("tisCode");
						ex.getIn().setHeader("songCode", songCode);
						ex.getIn().setHeader("tisCode", tisCode);
					}
					else
					{
						if(songCodeFromUri.isEmpty())
						{
							songCodeFromUri="empty";	
						}
						if(tisCodeFromUri.isEmpty())
						{
							tisCodeFromUri = "empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGSHAREPARAM_MSG+songCodeFromUri+" and "+tisCodeFromUri);
					}
				}*/
				String songCodeFromUri = info.getQueryParameters(). getFirst("songCode");
				String tisCodeFromUri = info.getQueryParameters(). getFirst("tisCode");
				if(songCodeFromUri == null && tisCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.ENTITY_SONGSHARE);
					ex.getIn().setHeader("songShareEntityWithParam", BmgSongsApiConstants.SONGSHARE_PARAMS_ALL);
				}
				else if(songCodeFromUri!=null && tisCodeFromUri!=null)
				{
					if(!(songCodeFromUri.equals("null")) && !songCodeFromUri.isEmpty() && !(tisCodeFromUri.equals("null")) && !tisCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGSHARE);
						ex.getIn().setHeader("songShareEntityWithParam", BmgSongsApiConstants.SONGSHARE_PARAMS_SONGCODEANDTISCODE);
						Integer songCode = Integer.parseInt(info.getQueryParameters(). getFirst("songCode"));
						String tisCode = info.getQueryParameters(). getFirst("tisCode");
						ex.getIn().setHeader("songCode", songCode);
						ex.getIn().setHeader("tisCode", tisCode);
					}
					else
					{
						if(songCodeFromUri.isEmpty())
						{
							songCodeFromUri="empty";	
						}
						if(tisCodeFromUri.isEmpty())
						{
							tisCodeFromUri = "empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGSHAREPARAM_SONGCODEANDTISCODE_MSG+songCodeFromUri+" and "+tisCodeFromUri);
					}
				}
				else
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_SNGSHAREMSG);
				}
			}

			else if(info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.SONGSTATUS_URI_ENDSWITH))
			{
				/*String songStatusCodeFromUri = info.getQueryParameters(). getFirst("songStatusCode");
				if(songStatusCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_SNGSTATUSMSG);
				}
				else
				{
					if(!(songStatusCodeFromUri.equals("null")) && !songStatusCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGSTATUS);
						String songStatusCode = info.getQueryParameters(). getFirst("songStatusCode");
						ex.getIn().setHeader("songStatusCode", songStatusCode);
					}
					else
					{
						if(songStatusCodeFromUri.isEmpty())
						{
							songStatusCodeFromUri="empty";	
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGSTATUSPARAM_MSG+songStatusCodeFromUri);
					}
				}*/
				String songStatusCodeFromUri = info.getQueryParameters(). getFirst("songStatusCode");
				if(songStatusCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.ENTITY_SONGSTATUS);
					ex.getIn().setHeader("songStatusEntityWithParam", BmgSongsApiConstants.SONGSTATUS_PARAMS_ALL);
				}
				else if(songStatusCodeFromUri!=null)
				{
					if(!(songStatusCodeFromUri.equals("null")) && !songStatusCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGSTATUS);
						ex.getIn().setHeader("songStatusEntityWithParam", BmgSongsApiConstants.SONGSTATUS_PARAMS_SONGSTATUSCODE);
						String songStatusCode = info.getQueryParameters(). getFirst("songStatusCode");
						ex.getIn().setHeader("songStatusCode", songStatusCode);
					}
					else
					{
						if(songStatusCodeFromUri.isEmpty())
						{
							songStatusCodeFromUri="empty";	
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGSTATUSPARAM_SONGCODE_MSG+songStatusCodeFromUri);
					}
				}
			}

			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.SONGCLIENTLINK_URI_ENDSWITH)) 
			{
				/*String songCodeFromUri = info.getQueryParameters(). getFirst("songCode");
				String sysTerrFromUri = info.getQueryParameters(). getFirst("sysTerr");
				if(songCodeFromUri == null || sysTerrFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_SNGCLIENTLINKMSG);
				}
				else
				{
					if(!(songCodeFromUri.equals("null")) && !songCodeFromUri.isEmpty() && !(sysTerrFromUri.equals("null")) && !sysTerrFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGCLIENTLINK);
						Integer songCode = Integer.parseInt(info.getQueryParameters(). getFirst("songCode"));
						String sysTerr = info.getQueryParameters(). getFirst("sysTerr");
						ex.getIn().setHeader("songCode", songCode);
						ex.getIn().setHeader("sysTerr", sysTerr);
					}
					else
					{
						if(songCodeFromUri.isEmpty())
						{
							songCodeFromUri="empty";	
						}
						if(sysTerrFromUri.isEmpty())
						{
							sysTerrFromUri = "empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGCLIENTLINKPARAM_MSG+songCodeFromUri+" and "+sysTerrFromUri);
					}
				}*/
				String songCodeFromUri = info.getQueryParameters(). getFirst("songCode");
				String sysTerrFromUri = info.getQueryParameters(). getFirst("sysTerr");
				if(songCodeFromUri == null && sysTerrFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.ENTITY_SONGCLIENTLINK);
					ex.getIn().setHeader("songClientLinkEntityWithParam", BmgSongsApiConstants.SONGCLIENTLINK_PARAMS_ALL);
				}
				else if(songCodeFromUri != null && sysTerrFromUri != null)
				{
					if(!(songCodeFromUri.equals("null")) && !songCodeFromUri.isEmpty() && !(sysTerrFromUri.equals("null")) && !sysTerrFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGCLIENTLINK);
						ex.getIn().setHeader("songClientLinkEntityWithParam", BmgSongsApiConstants.SONGCLIENTLINK_PARAMS_SONGCODEANDSYSTERR);
						Integer songCode = Integer.parseInt(info.getQueryParameters(). getFirst("songCode"));
						String sysTerr = info.getQueryParameters(). getFirst("sysTerr");
						ex.getIn().setHeader("songCode", songCode);
						ex.getIn().setHeader("sysTerr", sysTerr);
					}
					else
					{
						if(songCodeFromUri.isEmpty())
						{
							songCodeFromUri="empty";	
						}
						if(sysTerrFromUri.isEmpty())
						{
							sysTerrFromUri = "empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGCLIENTLINKPARAM_SONGCODEANDSYSTERR_MSG+songCodeFromUri+" and "+sysTerrFromUri);
					}
				}
				else
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_SNGCLIENTLINKMSG);
				}

			}
			else if(info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.CLIENT_URI_ENDSWITH))
			{
				/*String clientCodeFromUri = info.getQueryParameters(). getFirst("clientCode");
				if(clientCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_CLIENTMSG);
				}
				else
				{
					if(!(clientCodeFromUri.equals("null")) && !clientCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_CLIENT);
						String clientCode = info.getQueryParameters(). getFirst("clientCode");
						ex.getIn().setHeader("clientCode", clientCode);
					}
					else
					{
						if(clientCodeFromUri.isEmpty())
						{
							clientCodeFromUri="empty";	
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_CLIENTPARAM_MSG+clientCodeFromUri);
					}
				}*/
				String clientCodeFromUri = info.getQueryParameters(). getFirst("clientCode");
				if(clientCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.ENTITY_CLIENT);
					ex.getIn().setHeader("clientEntityWithParam", BmgSongsApiConstants.CLIENT_PARAMS_ALL);
				}
				else if(clientCodeFromUri!=null)
				{
					if(!(clientCodeFromUri.equals("null")) && !clientCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_CLIENT);
						ex.getIn().setHeader("clientEntityWithParam", BmgSongsApiConstants.CLIENT_PARAMS_CLIENTCODE);
						Integer clientCode = Integer.parseInt(info.getQueryParameters(). getFirst("clientCode"));
						ex.getIn().setHeader("clientCode", clientCode);
					}
					else
					{
						if(clientCodeFromUri.isEmpty())
						{
							clientCodeFromUri="empty";	
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_CLIENTPARAM_SONGCODE_MSG+clientCodeFromUri);
					}
				}
			}
			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.CLIENTDEALLINK_URI_ENDSWITH)) 
			{
				String clientCodeFromUri = info.getQueryParameters(). getFirst("clientCode");
				String sysTerrFromUri = info.getQueryParameters(). getFirst("sysTerr");
				if(clientCodeFromUri == null || sysTerrFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_CLIENTDEALLINK);
					ex.getIn().setHeader("clientDealLinkEntityWithParam",BmgSongsApiConstants.CLIENTDEALLINK_PARAMS_ALL);
				}
				else if (clientCodeFromUri!=null && sysTerrFromUri!=null)
				{
					if(!(clientCodeFromUri.equals("null")) && !clientCodeFromUri.isEmpty() && !(sysTerrFromUri.equals("null")) && !sysTerrFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_CLIENTDEALLINK);
						ex.getIn().setHeader("clientDealLinkEntityWithParam",BmgSongsApiConstants.CLIENTDEALLINK_PARAMS_CLIENTCODEANDSYSTERR);
						Integer clientCode = Integer.parseInt(info.getQueryParameters(). getFirst("clientCode"));
						String sysTerr = info.getQueryParameters(). getFirst("sysTerr");
						ex.getIn().setHeader("clientCode", clientCode);
						ex.getIn().setHeader("sysTerr", sysTerr);
					}
					else
					{
						if(clientCodeFromUri.isEmpty())
						{
							clientCodeFromUri="empty";	
						}
						if(sysTerrFromUri.isEmpty())
						{
							sysTerrFromUri = "empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_CLIENTDEALLINKPARAM_MSG+clientCodeFromUri+" and "+sysTerrFromUri);
					}
				}
				else
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_CLIENTDEALLINKMSG);
				}
			}
			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.TISCODE_URI_ENDSWITH)) 
			{

				String tisCodeFromUri = info.getQueryParameters(). getFirst("tisCode");
				if(tisCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_TISCODE);
					ex.getIn().setHeader("tisCodeEntityWithParam", BmgSongsApiConstants.TISCODE_PARAMS_ALL);
				}
				else if(tisCodeFromUri != null)
				{
					if(!(tisCodeFromUri.equals("null")) && !tisCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_TISCODE);
						ex.getIn().setHeader("tisCodeEntityWithParam", BmgSongsApiConstants.TISCODE_PARAMS_TISCODE);
						String tisCode = info.getQueryParameters(). getFirst("tisCode");
						ex.getIn().setHeader("tisCode", tisCode);
					}
					else
					{
						if(tisCodeFromUri.isEmpty())
						{
							tisCodeFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_TISPARAM_TISCODE_MSG+tisCodeFromUri);
					}
				}
			}
			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.DEAL_URI_ENDSWITH)) 
			{
				String sysTerrFromUri = info.getQueryParameters(). getFirst("sysTerr");
				String dealNoFromUri = info.getQueryParameters(). getFirst("dealNo");
				if(sysTerrFromUri == null || dealNoFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_DEAL);
					ex.getIn().setHeader("dealEntityWithParam",BmgSongsApiConstants.DEAL_PARAMS_ALL);
				}
				else if (sysTerrFromUri!=null && dealNoFromUri!=null)
				{
					if(!(sysTerrFromUri.equals("null")) && !sysTerrFromUri.isEmpty() && !(dealNoFromUri.equals("null")) && !dealNoFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_DEAL);
						ex.getIn().setHeader("dealEntityWithParam",BmgSongsApiConstants.DEAL_PARAMS_SYSTERRANDDEALNO);

						String sysTerr = info.getQueryParameters(). getFirst("sysTerr");
						String dealNo= info.getQueryParameters(). getFirst("dealNo");
						ex.getIn().setHeader("sysTerr", sysTerr);
						ex.getIn().setHeader("dealNo", dealNo);
					}
					else
					{
						if(sysTerrFromUri.isEmpty())
						{
							sysTerrFromUri="empty";	
						}
						if(dealNoFromUri.isEmpty())
						{
							dealNoFromUri = "empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_DEALPARAM_MSG+sysTerrFromUri+" and "+dealNoFromUri);
					}
				}
				else
				{
					ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.MANDATORY_PARAM_DEALMSG);
				}
			}
			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.SONGGROUP_URI_ENDSWITH)) 
			{

				String groupCodeFromUri = info.getQueryParameters(). getFirst("groupCode");
				logger.info("GroupCode From URI is :" + groupCodeFromUri);
				if(groupCodeFromUri == null)
				{
					logger.info("GroupCode From URI is :" + groupCodeFromUri);
					logger.info("GroupCodeFromURI is null");
					ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGGROUP);
					ex.getIn().setHeader("songGroupEntityWithParam", BmgSongsApiConstants.SONGGROUP_PARAMS_ALL);
				}
				else if(groupCodeFromUri != null)
				{
					logger.info("GroupCode From URI is :" + groupCodeFromUri);
					logger.info("GroupCodeFromURI is not null");
					if(!(groupCodeFromUri.equals("null")) && !groupCodeFromUri.isEmpty())
					{
						logger.info("GroupCode From URI is :" + groupCodeFromUri);
						logger.info("GroupCodeFromURI is not null and not empty cond");
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGGROUP);
						ex.getIn().setHeader("songGroupEntityWithParam", BmgSongsApiConstants.SONGGROUP_PARAMS_GROUPCODE);
						String groupCode = info.getQueryParameters(). getFirst("groupCode");
						ex.getIn().setHeader("groupCode", groupCode);
					}
					else
					{
						logger.info("GroupCode From URI is :" + groupCodeFromUri);
						logger.info("GroupCodeFromURI is not null but emtpy");
						if(groupCodeFromUri.isEmpty())
						{
							groupCodeFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_GROUPCODEPARAM_SONGGROUP_MSG+groupCodeFromUri);
					}
				}
			}
			else if (info.getAbsolutePath().getPath().endsWith(BmgSongsApiConstants.SONGGROUPLINK_URI_ENDSWITH)) 
			{

				String grpaCodeFromUri = info.getQueryParameters(). getFirst("grpaCode");
				if(grpaCodeFromUri == null)
				{
					ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGGROUPLINK);
					ex.getIn().setHeader("songGroupLinkEntityWithParam", BmgSongsApiConstants.SONGGROUPLINK_PARAMS_ALL);
				}
				else if(grpaCodeFromUri != null)
				{
					if(!(grpaCodeFromUri.equals("null")) && !grpaCodeFromUri.isEmpty())
					{
						ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.ENTITY_SONGGROUPLINK);
						ex.getIn().setHeader("songGroupLinkEntityWithParam", BmgSongsApiConstants.SONGGROUPLINK_PARAMS_GRPACODE);
						Integer grpaCode = Integer.parseInt(info.getQueryParameters(). getFirst("grpaCode"));
						ex.getIn().setHeader("grpaCode", grpaCode);
					}
					else
					{
						if(grpaCodeFromUri.isEmpty())
						{
							grpaCodeFromUri="empty";
						}
						ex.getIn().setHeader("entitybmgsongs",BmgSongsApiConstants.INCORRECT_SONGGROUPLINKPARAM_GRPACODE_MSG+grpaCodeFromUri);
					}
				}
			}
			/*}
		else
		{
			Integer perPage = Integer.parseInt(perpage[1]);
			ex.getIn().setHeader("entitybmgsongs", BmgSongsApiConstants.Bad_Request_Exceeded_PerpageValue+perPage);
		}*/
		}
		catch(Exception excep)
		{
			throw new Exception("Error in BmgSongsServicePRocessor" + excep.getMessage());
		}

		return "";
	}

	private Integer[] getStartAndEndRecordNumbers(String pageno, String perpage) throws Exception {
		Integer[] startEndRecNum=null;
		try{

			Integer pageNo=Integer.parseInt(pageno);
			Integer perPage=Integer.parseInt(perpage);
			/*Integer startRecordNumber =(pageNo-1)*perPage+1;
			Integer endRecordNumber= startRecordNumber+(perPage-1);*/
			Integer startRecordNumber =(pageNo-1)*perPage;
			Integer endRecordNumber= perPage;

			logger.info("BmgSongsServiceProcessor : Start :getStartAndEndRecordNumbers() :   startRecordNumber :"+startRecordNumber+" : endRecordNumber :"+endRecordNumber);
			startEndRecNum= new Integer[2];
			startEndRecNum[0]=startRecordNumber;
			startEndRecNum[1]=endRecordNumber;

		}catch(Exception ex){
			logger.info("BmgSongServiceProcessor : ERROR :getStartAndEndRecordNumbers() :   message: "+ex.getMessage());
			throw new Exception(ex.getMessage()) ;
		}
		return startEndRecNum;
	}
	private String[] getPageAndPerPageVals(String page, String per_page) {
		String[] strArr=new String[2];
		if((page!=null && !page.equals("0")) && (per_page!=null && !per_page.equals("0"))){
		} else{
			page=BmgSongsApiConstants.page;
			per_page=BmgSongsApiConstants.per_page;
		}
		strArr[0]=page;
		strArr[1]=per_page;
		return strArr;
	}
}