package com.bmg.esb.bmgsongs.imdata.api.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class SongWarning 
{
	private Integer songCode;
	private String severity;
	private String summary;
	private String note1;
	private String note2;
	private String note3;
	private String note4;
	private String note5;
	private String note6;
	private Integer sequence;
	private Integer displayFrom;
	private Integer displayTo;
	private String status;
	@JsonIgnore
	private Integer row;
	public Integer getRow() {
		return row;
	}
	public void setRow(Integer row) {
		this.row = row;
	}
	public Integer getSongCode() {
		return songCode;
	}
	public void setSongCode(Integer songCode) {
		this.songCode = songCode;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getNote1() {
		return note1;
	}
	public void setNote1(String note1) {
		this.note1 = note1;
	}
	public String getNote2() {
		return note2;
	}
	public void setNote2(String note2) {
		this.note2 = note2;
	}
	public String getNote3() {
		return note3;
	}
	public void setNote3(String note3) {
		this.note3 = note3;
	}
	public String getNote4() {
		return note4;
	}
	public void setNote4(String note4) {
		this.note4 = note4;
	}
	public String getNote5() {
		return note5;
	}
	public void setNote5(String note5) {
		this.note5 = note5;
	}
	public String getNote6() {
		return note6;
	}
	public void setNote6(String note6) {
		this.note6 = note6;
	}
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	public Integer getDisplayFrom() {
		return displayFrom;
	}
	public void setDisplayFrom(Integer displayFrom) {
		this.displayFrom = displayFrom;
	}
	public Integer getDisplayTo() {
		return displayTo;
	}
	public void setDisplayTo(Integer displayTo) {
		this.displayTo = displayTo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
