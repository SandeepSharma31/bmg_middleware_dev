package com.bmg.esb.bmgsongs.imdata.api.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class SongShare 
{
	private Integer songCode;
	private String tisCode;
	private Double mechOwnershipShare;
	private Double perfOwnershipShare;
	private Double synchOwnershipShare;
	private Double mechControlledShare;
	private Double perfControlledShare;
	private Double synchControlledShare;
	private String isControlled;
	@JsonIgnore
	private Integer row;
	public Integer getRow() {
		return row;
	}
	public void setRow(Integer row) {
		this.row = row;
	}
	public Integer getSongCode() {
		return songCode;
	}
	public void setSongCode(Integer songCode) {
		this.songCode = songCode;
	}
	public String getTisCode() {
		return tisCode;
	}
	public void setTisCode(String tisCode) {
		this.tisCode = tisCode;
	}
	public Double getMechOwnershipShare() {
		return mechOwnershipShare;
	}
	public void setMechOwnershipShare(Double mechOwnershipShare) {
		this.mechOwnershipShare = mechOwnershipShare;
	}
	public Double getPerfOwnershipShare() {
		return perfOwnershipShare;
	}
	public void setPerfOwnershipShare(Double perfOwnershipShare) {
		this.perfOwnershipShare = perfOwnershipShare;
	}
	public Double getSynchOwnershipShare() {
		return synchOwnershipShare;
	}
	public void setSynchOwnershipShare(Double synchOwnershipShare) {
		this.synchOwnershipShare = synchOwnershipShare;
	}
	public Double getMechControlledShare() {
		return mechControlledShare;
	}
	public void setMechControlledShare(Double mechControlledShare) {
		this.mechControlledShare = mechControlledShare;
	}
	public Double getPerfControlledShare() {
		return perfControlledShare;
	}
	public void setPerfControlledShare(Double perfControlledShare) {
		this.perfControlledShare = perfControlledShare;
	}
	public Double getSynchControlledShare() {
		return synchControlledShare;
	}
	public void setSynchControlledShare(Double synchControlledShare) {
		this.synchControlledShare = synchControlledShare;
	}
	public String getIsControlled() {
		return isControlled;
	}
	public void setIsControlled(String isControlled) {
		this.isControlled = isControlled;
	}
}
