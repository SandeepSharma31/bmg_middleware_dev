package com.bmg.esb.bmgsongs.imdata.api.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientDealLink {

	private Integer clientCode;
	private String sysTerr;
	private String dealNo;
	@JsonIgnore
	private Integer row;
	public Integer getRow() {
		return row;
	}
	public void setRow(Integer row) {
		this.row = row;
	}
	public Integer getClientCode() {
		return clientCode;
	}
	public void setClientCode(Integer clientCode) {
		this.clientCode = clientCode;
	}
	public String getSysTerr() {
		return sysTerr;
	}
	public void setSysTerr(String sysTerr) {
		this.sysTerr = sysTerr;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}


}
