package com.bmg.esb.bmgsongs.imdata.api.jsonconverter;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;

import com.bmg.esb.bmgsongs.imdata.api.constants.BmgSongsApiConstants;

public class BadRequestJsonConverter {

	public void incorrectParameters(Exchange exchange) throws Exception
	{
		try 
		{
			Response response = null;
			String responseStr=null;
			/*Integer page = Integer.parseInt(exchange.getIn().getHeader("page").toString());
			Integer per_page = Integer.parseInt(exchange.getIn().getHeader("per_page").toString());*/
			int status=BmgSongsApiConstants.fail_BadRequest_Status;
			responseStr = exchange.getIn().getHeader("entitybmgsongs").toString();
			response = Response.status(status).type("application/json").entity(responseStr).build();
			exchange.getOut().setBody(response);
		} 
		catch (Exception e)
		{
			throw new Exception("Exception in bad request Json Converter Class " + e.getMessage());
		}
	}
}
