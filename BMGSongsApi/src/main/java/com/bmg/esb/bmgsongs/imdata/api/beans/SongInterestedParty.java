package com.bmg.esb.bmgsongs.imdata.api.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class SongInterestedParty 
{
	private Integer songCode;
	private Integer ipLinkId;
	private Integer sequenceNumber;
	private String capacityTypeCode;
	private String isControlled;
	private Integer ipCode1;
	private String ipType1;
	private String territoryId;
	private String isOriginatingTerritory;
	private Double authorContributionPercentage;
	private Double composerContributionPercentage;
	private Double generalContributionPercentage;
	private Double mechControlledShare;
	private Double perfControlledShare;
	private Double synchControlledShare;
	private Double mechOwnershipShare;
	private Double perfOwnershipShare;
	private Double synchOwnershipShare;
	private Integer parent;
	@JsonIgnore
	private Integer row;
	public Integer getRow() {
		return row;
	}
	public void setRow(Integer row) {
		this.row = row;
	}
	public Integer getSongCode() {
		return songCode;
	}
	public void setSongCode(Integer songCode) {
		this.songCode = songCode;
	}
	public Integer getIpLinkId() {
		return ipLinkId;
	}
	public void setIpLinkId(Integer ipLinkId) {
		this.ipLinkId = ipLinkId;
	}
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getCapacityTypeCode() {
		return capacityTypeCode;
	}
	public void setCapacityTypeCode(String capacityTypeCode) {
		this.capacityTypeCode = capacityTypeCode;
	}
	public String getIsControlled() {
		return isControlled;
	}
	public void setIsControlled(String isControlled) {
		this.isControlled = isControlled;
	}
	public Integer getIpCode1() {
		return ipCode1;
	}
	public void setIpCode1(Integer ipCode1) {
		this.ipCode1 = ipCode1;
	}
	public String getIpType1() {
		return ipType1;
	}
	public void setIpType1(String ipType1) {
		this.ipType1 = ipType1;
	}
	public String getTerritoryId() {
		return territoryId;
	}
	public void setTerritoryId(String territoryId) {
		this.territoryId = territoryId;
	}
	public String getIsOriginatingTerritory() {
		return isOriginatingTerritory;
	}
	public void setIsOriginatingTerritory(String isOriginatingTerritory) {
		this.isOriginatingTerritory = isOriginatingTerritory;
	}
	public Double getAuthorContributionPercentage() {
		return authorContributionPercentage;
	}
	public void setAuthorContributionPercentage(Double authorContributionPercentage) {
		this.authorContributionPercentage = authorContributionPercentage;
	}
	public Double getComposerContributionPercentage() {
		return composerContributionPercentage;
	}
	public void setComposerContributionPercentage(
			Double composerContributionPercentage) {
		this.composerContributionPercentage = composerContributionPercentage;
	}
	public Double getGeneralContributionPercentage() {
		return generalContributionPercentage;
	}
	public void setGeneralContributionPercentage(
			Double generalContributionPercentage) {
		this.generalContributionPercentage = generalContributionPercentage;
	}
	public Double getMechControlledShare() {
		return mechControlledShare;
	}
	public void setMechControlledShare(Double mechControlledShare) {
		this.mechControlledShare = mechControlledShare;
	}
	public Double getPerfControlledShare() {
		return perfControlledShare;
	}
	public void setPerfControlledShare(Double perfControlledShare) {
		this.perfControlledShare = perfControlledShare;
	}
	public Double getSynchControlledShare() {
		return synchControlledShare;
	}
	public void setSynchControlledShare(Double synchControlledShare) {
		this.synchControlledShare = synchControlledShare;
	}
	public Double getMechOwnershipShare() {
		return mechOwnershipShare;
	}
	public void setMechOwnershipShare(Double mechOwnershipShare) {
		this.mechOwnershipShare = mechOwnershipShare;
	}
	public Double getPerfOwnershipShare() {
		return perfOwnershipShare;
	}
	public void setPerfOwnershipShare(Double perfOwnershipShare) {
		this.perfOwnershipShare = perfOwnershipShare;
	}
	public Double getSynchOwnershipShare() {
		return synchOwnershipShare;
	}
	public void setSynchOwnershipShare(Double synchOwnershipShare) {
		this.synchOwnershipShare = synchOwnershipShare;
	}
	public Integer getParent() {
		return parent;
	}
	public void setParent(Integer parent) {
		this.parent = parent;
	}
	
}
