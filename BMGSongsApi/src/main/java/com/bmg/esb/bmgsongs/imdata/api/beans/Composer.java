package com.bmg.esb.bmgsongs.imdata.api.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Composer {
	private Integer id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String name;
	private String ipi;
	private String mechanicalSocietyId;
	private String performanceSocietyId;
	private String isControlled;
	@JsonIgnore
	private Integer row;
	public Integer getRow() {
		return row;
	}
	public void setRow(Integer row) {
		this.row = row;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIpi() {
		return ipi;
	}
	public void setIpi(String ipi) {
		this.ipi = ipi;
	}
	public String getMechanicalSocietyId() {
		return mechanicalSocietyId;
	}
	public void setMechanicalSocietyId(String mechanicalSocietyId) {
		this.mechanicalSocietyId = mechanicalSocietyId;
	}
	public String getPerformanceSocietyId() {
		return performanceSocietyId;
	}
	public void setPerformanceSocietyId(String performanceSocietyId) {
		this.performanceSocietyId = performanceSocietyId;
	}
	public String getIsControlled() {
		return isControlled;
	}
	public void setIsControlled(String isControlled) {
		this.isControlled = isControlled;
	}

}
