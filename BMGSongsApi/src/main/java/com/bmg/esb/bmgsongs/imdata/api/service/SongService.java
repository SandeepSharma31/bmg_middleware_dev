package com.bmg.esb.bmgsongs.imdata.api.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;



/**
 * 
 * @author rajesh_thattikonda
 * This Class is responsible for calling the data route if any of the paths for BMGSongsApi is found in the URI.
 */

@Path("/bmgsongs/data")
public class SongService 
{
	private static Logger logger = Logger.getLogger(SongService.class.getName());
	/**
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for song data
	 */
	@GET
	@Path("/song")
	@Produces(MediaType.APPLICATION_JSON)
	public Response songEntityRequest(@Context  UriInfo info)
	{
		logger.info("In Song Service method");
		return null;
	}
	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for composer data
	 */
	@GET
	@Path("/composer")
	@Produces(MediaType.APPLICATION_JSON)
	public Response composerEntityRequest(@Context  UriInfo info)
	{
		return null;
	}
	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for publisher data
	 */
	@GET
	@Path("/publisher")
	@Produces(MediaType.APPLICATION_JSON)
	public Response publisherEntityRequest(@Context  UriInfo info)
	{
		return null;
	}
	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for society data
	 */
	@GET
	@Path("/society")
	@Produces(MediaType.APPLICATION_JSON)
	public Response societyEntityRequest(@Context  UriInfo info)
	{
		return null;
	}
	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for territory data
	 */
	@GET
	@Path("/territory")
	@Produces(MediaType.APPLICATION_JSON)
	public Response territoryEntityRequest(@Context  UriInfo info)
	{
		return null;
	}
	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for tisHierarchyLink data
	 */	
	@GET
	@Path("/tisHierarchyLink")
	@Produces(MediaType.APPLICATION_JSON)
	public Response tisHierarchyLinkEntityRequest(@Context  UriInfo info)
	{
		return null;
	}
	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for songWarning data
	 */	
	@GET
	@Path("/songWarning")
	@Produces(MediaType.APPLICATION_JSON)
	public Response songWarningEntityRequest(@Context  UriInfo info)
	{
		return null;
	}
	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for songAka data
	 */
	@GET
	@Path("/songAka")
	@Produces(MediaType.APPLICATION_JSON)
	public Response songAkaEntityRequest(@Context  UriInfo info)
	{
		return null;
	}


	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for songInterestedParty data
	 */
	@GET
	@Path("/songInterestedParty")
	@Produces(MediaType.APPLICATION_JSON)
	public Response songIpEntityRequest(@Context  UriInfo info)
	{
		logger.info("In SongIP Service method");
		return null;
	}
	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for songNotes data
	 */
	@GET
	@Path("/songNotes")
	@Produces(MediaType.APPLICATION_JSON)
	public Response songNotesEntityRequest(@Context  UriInfo info)
	{
		return null;
	}
	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for songShare data
	 */
	@GET
	@Path("/songShare")
	@Produces(MediaType.APPLICATION_JSON)
	public Response songShareEntityRequest(@Context  UriInfo info)
	{
		return null;
	}

	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for songStatus data
	 */
	@GET
	@Path("/songStatus")
	@Produces(MediaType.APPLICATION_JSON)
	public Response songStatusEntityRequest(@Context  UriInfo info)
	{
		return null;
	}

	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for SongCLientLink data
	 */
	@GET
	@Path("/songClientLink")
	@Produces(MediaType.APPLICATION_JSON)
	public Response songClientLinkEntityRequest(@Context  UriInfo info)
	{
		return null;
	}

	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for client data
	 */
	@GET
	@Path("/client")
	@Produces(MediaType.APPLICATION_JSON)
	public Response clientEntityRequest(@Context  UriInfo info)
	{
		return null;
	}

	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for ClientDealLink data
	 */
	@GET
	@Path("/clientDealLink")
	@Produces(MediaType.APPLICATION_JSON)
	public Response clientDealLinkEntityRequest(@Context  UriInfo info)
	{
		return null;
	}
	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for TisCode data
	 */
	@GET
	@Path("/tisCode")
	@Produces(MediaType.APPLICATION_JSON)
	public Response tisCodeEntityRequest(@Context  UriInfo info)
	{
		return null;
	}
	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for Deal data
	 */
	@GET
	@Path("/deal")
	@Produces(MediaType.APPLICATION_JSON)
	public Response dealEntityRequest(@Context  UriInfo info)
	{
		return null;
	}
	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for Deal data
	 */
	@GET
	@Path("/songGroup")
	@Produces(MediaType.APPLICATION_JSON)
	public Response songGroupEntityRequest(@Context  UriInfo info)
	{
		return null;
	}
	/**
	 * 
	 * @author rajesh_thattikonda
	 * Method to invoke route if  the request is for Deal data
	 */
	@GET
	@Path("/songGroupLink")
	@Produces(MediaType.APPLICATION_JSON)
	public Response songGroupLinkEntityRequest(@Context  UriInfo info)
	{
		return null;
	}
	
}
