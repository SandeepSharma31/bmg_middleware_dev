package com.bmg.esb.bmgsongs.imdata.api.constants;
/**
 * 
 * @author rajesh_thattikonda
 *This class contains all the constants which are needed and used by the API.
 */
public class BmgSongsApiConstants 
{
	/*public static final String NO_SONGDATA = "No Song Data found for the given SongCode and deleteIndicator values. Given values respectively are ";
	public static final String NO_COMPOSERDATA = "No Composer Data found for the given Pub_Code ";
	public static final String NO_PUBLISHERDATA = "No Publisher Data found for the given Pub_Code ";
	public static final String NO_SOCIETYDATA = "No Society Data found for the given Soc_Code ";
	public static final String NO_TERRITORYDATA = "No Territory Data found for the given Terr_Code ";
	public static final String NO_TISHIERARCHYDATA = "No TISHierarchyLink Data found for the given Tisl_Link_Id ";
	public static final String NO_SONGWARNINGDATA = "No SongWarning Data found for the given SngWarn_Song_Code ";
	public static final String NO_SONGAKADATA = "No SongAka Data found for the given Aka_Song_Code ";
	public static final String NO_SONGIPDATA = "No SongInterestedParty Data found for the given SongCode and IpLinkId values. Given values respectively are ";
	public static final String NO_SONGNOTESDATA = "No SongNotes Data found for the given Song_Code ";
	public static final String NO_SONGSHAREDATA = "No SongShare Data found for the given SongCode and tisCode values. Given values respectively are ";
	public static final String NO_SONGSTATUSDATA = "No SongStatus Data found for the given Song_Status_Code ";
	public static final String NO_SONGCLIENTLINKDATA = "No SongClientLink Data found for the given SongCode and SysTerr values. Given values respectively are ";
	public static final String NO_CLIENTDATA = "No Client Data found for the given Client_Code ";
	*/public static final String NO_CLIENTDEALLINKDATA = "No ClientDealLink Data found for the given ClientCode and SysTerr values. Given values respectively are ";
	
	public static final String ENTITY_SONG = "SONGENTITY";
	public static final String ENTITY_COMPOSER = "COMPOSERENTITY";
	public static final String ENTITY_PUBLISHER = "PUBLISHERENTITY";
	public static final String ENTITY_SOCIETY = "SOCIETYENTITY";
	public static final String ENTITY_TERRITORY = "TERRITORYENTITY";
	public static final String ENTITY_TISHIERARCHY = "TISHIERARCHYENTITY";
	public static final String ENTITY_SONGWARNING = "SONGWARNINGENTITY";
	public static final String ENTITY_SONGAKA = "SONGAKAENTITY";
	public static final Object ENTITY_SONGNOTES = "SONGNOTESENTITY";
	public static final String ENTITY_SONGIP = "SONGIPENTITY";
	public static final Object ENTITY_SONGSHARE = "SONGSHAREENTITY";
	public static final Object ENTITY_SONGSTATUS = "SONGSTATUSENTITY";
	public static final Object ENTITY_SONGCLIENTLINK = "SONGCLIENTLINKENTITY";
	public static final Object ENTITY_CLIENT = "CLIENTENTITY";
	public static final Object ENTITY_CLIENTDEALLINK = "CLIENTDEALLINKENTITY";
	public static final Object ENTITY_TISCODE = "TISCODEENTITY";
	public static final Object ENTITY_DEAL = "DEALENTITY";
	public static final Object ENTITY_SONGGROUP = "SONGGROUPENTITY";
	public static final Object ENTITY_SONGGROUPLINK = "SONGGROUPLINKENTITY";
	
	public static final String SONG_URI_ENDSWITH = "song";
	public static final String COMPOSER_URI_ENDSWITH = "composer";
	public static final String PUBLISHER_URI_ENDSWITH = "publisher";
	public static final String SOCIETY_URI_ENDSWITH = "society";
	public static final String TERRITORY_URI_ENDSWITH = "territory";
	public static final String TISHIERARCHYLINK_URI_ENDSWITH = "tisHierarchyLink";
	public static final String SONGWARNING_URI_ENDSWITH = "songWarning";
	public static final String SONGAKA_URI_ENDSWITH = "songAka";
	public static final String SONGNOTES_URI_ENDSWITH = "songNotes";
	public static final String SONGIP_URI_ENDSWITH = "songInterestedParty";
	public static final String SONGSHARE_URI_ENDSWITH = "songShare";
	public static final String SONGSTATUS_URI_ENDSWITH = "songStatus";
	public static final String SONGCLIENTLINK_URI_ENDSWITH = "songClientLink";
	public static final String CLIENT_URI_ENDSWITH = "client";
	public static final String CLIENTDEALLINK_URI_ENDSWITH = "clientDealLink";
	public static final String TISCODE_URI_ENDSWITH = "tisCode";
	public static final String DEAL_URI_ENDSWITH = "deal";
	public static final String SONGGROUP_URI_ENDSWITH = "songGroup";
	public static final String SONGGROUPLINK_URI_ENDSWITH = "songGroupLink";
	/*public static final String INCORRECT_SONGPARAM_MSG = "Wrong Parameters passed for SONG Entity. SongCode and DeleteInd may be passed as null or empty. Both values respectively are ";
	public static final String INCORRECT_COMPPARAM_MSG = "Wrong Parameters passed for COMPOSER Entity. PubCode may be passed as null or empty. Value passed is ";
	public static final String INCORRECT_PUBLPARAM_MSG = "Wrong Parameters passed for PUBLISHER Entity. PubCode may be passed as null or empty. Value passed is ";
	public static final String INCORRECT_SOCIETYPARAM_MSG = "Wrong Parameters passed for SOCIETY Entity. SocCode may be passed as null or empty. Value passed is ";
	public static final String INCORRECT_TERRPARAM_MSG = "Wrong Parameters passed for TERRITORY Entity. TerrCode may be passed as null or empty. Value passed is ";
	public static final String INCORRECT_TISHIERARCHYPARAM_MSG = "Wrong Parameters passed for TISHIERARCHYLINK Entity. TisLinkId may be passed as null or empty. Value passed is ";
	public static final String INCORRECT_SONGWARNPARAM_MSG = "Wrong Parameters passed for SONGWARNING Entity. WarnSongCode may be passed as null or empty. Value passed is ";
	public static final String INCORRECT_SONGAKAPARAM_MSG = "Wrong Parameters passed for SONGAKA Entity. AkaSongCode may be passed as null or empty. Value passed is ";
	public static final String INCORRECT_SONGIPPARAM_MSG = "Wrong Parameters passed for SONGIP Entity. SongCode and IpLinkId may be passed as null or empty. Values passed respectively are ";
	public static final String INCORRECT_SONGNOTESPARAM_MSG = "Wrong Parameter passed for SONGNOTES Entity. SongCode may be passed as null or empty. Value passed is ";
	public static final String INCORRECT_SONGSHAREPARAM_MSG = "Wrong Parameters passed for SONGSHARE Entity. SongCode and tisCode may be passed as null or empty. Values passed respectively are ";
	public static final String INCORRECT_SONGSTATUSPARAM_MSG = "Wrong Parameter passed for SONGSTATUS Entity. SongStatusCode may be passed as null or empty. Value passed is ";
	public static final String INCORRECT_SONGCLIENTLINKPARAM_MSG = "Wrong Parameters passed for SONGCLIENTLINK Entity. SongCode and sysTerr may be passed as null or empty. Values passed respectively are ";
	public static final String INCORRECT_CLIENTPARAM_MSG = "Wrong Parameter passed for CLIENT Entity. ClientCode may be passed as null or empty. Value passed is ";
	*/
	
	public static final Object MANDATORY_PARAM_SONGMSG = "MANDATORY: Either of songcode and deleteind parameters have to be passed in the URI to return song data.";
	/*public static final Object MANDATORY_PARAM_COMPMSG = "MANDATORY: pubcode parameter have to be passed in the URI to return composer data.";
	public static final Object MANDATORY_PARAM_PUBLMSG = "MANDATORY: pubcode parameter have to be passed in the URI to return publisher data.";
	public static final Object MANDATORY_PARAM_SOCIETYMSG = "MANDATORY: soccode parameter have to be passed in the URI to return society data.";
	public static final Object MANDATORY_PARAM_TERRMSG = "MANDATORY: terrCode parameter have to be passed in the URI to return territory data.";
	public static final Object MANDATORY_PARAM_TISHIERARCHYMSG = "MANDATORY: tisLinkId parameter have to be passed in the URI to return tishierarchylink data.";
	public static final Object MANDATORY_PARAM_SNGWARNMSG = "MANDATORY: warnSongCode parameter have to be passed in the URI to return songwarning data.";
	public static final Object MANDATORY_PARAM_SNGAKAMSG = "MANDATORY: akaSongCode parameter have to be passed in the URI to return songaka data.";*/
	public static final Object MANDATORY_PARAM_SONGIPMSG = "MANDATORY: songcode parameter has to be passed with ipLinkId in the URI to return songinterestedparty data.";
	/*public static final Object MANDATORY_PARAM_SNGNOTESMSG = "MANDATORY: songCode parameter has to be passed in the URI to return songNotes data.";*/
	public static final Object MANDATORY_PARAM_SNGSHAREMSG = "MANDATORY: songCode and tisCode parameters have to be passed in the URI to return songShare data.";
	/*public static final Object MANDATORY_PARAM_SNGSTATUSMSG = "MANDATORY: songStatusCode parameter has to be passed in the URI to return songStatus data.";*/
	public static final Object MANDATORY_PARAM_SNGCLIENTLINKMSG = "MANDATORY: songCode and sysTerr parameters have to be passed in the URI to return songClientLink data.";
	/*public static final Object MANDATORY_PARAM_CLIENTMSG = "MANDATORY: clientCode parameter has to be passed in the URI to return client data.";*/
	public static final Object MANDATORY_PARAM_CLIENTDEALLINKMSG = "MANDATORY: clientCode and sysTerr parameters have to be passed in the URI to return clientDealLink data.";
	public static final Object MANDATORY_PARAM_DEALMSG = "MANDATORY: sysTerr and dealNo parameters have to be passed in the URI to return deal data.";
	
	public static final int success_Status =200;
	public static final int fail_BadRequest_Status =400;
	public static final int fail_NotFound_Status = 404;
	
	public static final String INCORRECT_SONGPARAM_SONGCODE_MSG = "Bad Request. Incorrect Parameter passed for SONG Entity. SongCode parameter is passed as ";
	public static final String INCORRECT_SONGPARAM_DELETEIND_MSG = "Bad Request. Incorrect Parameter passed for SONG Entity. DeleteInd parameter is passed as ";
	public static final Object SONG_PARAMS_ALL = "ALLSONGS";
	public static final Object SONG_PARAMS_SONGCODE = "SONGCODE";
	public static final Object SONG_PARAMS_DELETEIND = "DELETEIND";
	public static final String NO_SONGDATA_WITHOUTPARAM = "No Data Found for Song Entity.";
	public static final String NO_SONGDATA_WITHSONGCODE = "No Song Data Found for given songCode. SongCode value passed is ";
	public static final Object NO_SONGDATA_WITHDELETEIND = "No Song Data Found for given deleteind. DeleteInd value passed is ";
	
	public static final Object SONGIP_PARAMS_ALL = "ALLSONGIPS";
	public static final Object SONGIP_PARAMS_SONGCODE = "SONGCODE";
	public static final Object SONGIP_PARAMS_SONGCODEANDIPLINK = "SONGCODEANDIPLINK";
	public static final String INCORRECT_SONGIPPARAM_SONGCODE_MSG = "Bad Request. Incorrect Parameter passed for SONGIP Entity. SongCode parameter is passed as ";
	public static final String INCORRECT_SONGIPPARAM_SONGCODEANDIPLINK_MSG = "Bad Request. Incorrect Parameters passed for SONGIP Entity. Respective values passed for songcode and iplinkid parameters are ";
	public static final String NO_SONGIPDATA_WITHOUTPARAM = "No Data Found for SongInterestedParty Entity";
	public static final String NO_SONGIPDATA_WITHSONGCODE = "No SongInterestedParty Data Found for given songCode. SongCode value passed is ";
	public static final String NO_SONGIPDATA_WITHSONGCODEANDIPLINK = "No SongInterestedParty Data Found for given songCode and ipLinkId. SongCode and IpLinkId values passed respectively are";
	
	
	
	
	public static final Object COMP_PARAMS_ALL = "ALLCOMPOSERS";
	public static final Object COMP_PARAMS_PUBCODE = "COMPPUBCODE";
	public static final String INCORRECT_COMPPARAM_PUBCODE_MSG = "Bad Request. Incorrect parameter passed for Composer Entity. PubCode parameter is passed as ";
	public static final String NO_COMPDATA_WITHOUTPARAM = "No Data Found for Composer Entity";
	public static final String NO_COMPDATA_WITHPUBCODE = "No Composer Data Found for given PubCode. PubCode value passed is";
	
	public static final Object PUB_PARAMS_ALL = "ALLPUBLISHERS";
	public static final Object PUB_PARAMS_PUBCODE = "PUBLPUBCODE";
	public static final String INCORRECT_PUBLPARAM_PUBCODE_MSG = "Bad Request. Incorrect parameter passed for Publisher Entity. PubCode parameter is passed as ";
	public static final String NO_PUBLDATA_WITHOUTPARAM = "No Data Found for Publisher Entity";
	public static final String NO_PUBLDATA_WITHPUBCODE = "No Publisher Data Found for given PubCode. PubCode value passed is";
	
	
	public static final Object SOC_PARAMS_ALL = "ALLSOCIETY";
	public static final Object SOC_PARAMS_SOCCODE = "SOCIETYSOCCODE";
	public static final String INCORRECT_SOCIPARAM_SOCCODE_MSG = "Bad Request. Incorrect parameter passed for Society Entity. SocCode parameter is passed as ";
	public static final String NO_SOCDATA_WITHOUTPARAM = "No Data Found for Society Entity";
	public static final String NO_SOCDATA_WITHSOCCODE = "No Society Data Found for given SocCode. SocCode value passed is";
	
	public static final Object TERR_PARAMS_ALL = "ALLTERRITORY";
	public static final Object TERR_PARAMS_TERRCODE = "TERRCODE";
	public static final String INCORRECT_TERRPARAM_TERRCODE_MSG = "Bad Request. Incorrect parameter passed for Territory Entity. TerrCode parameter is passed as ";
	public static final String NO_TERRDATA_WITHOUTPARAM = "No Data Found for Territory Entity";
	public static final String NO_TERRDATA_WITHTERRCODE = "No Territory Data Found for given TerrCode. TerrCode value passed is";
	
	
	public static final Object TISHIERARCHY_PARAMS_ALL = "ALLTISHIERARCHY";
	public static final Object TIS_PARAMS_TISLINKID = "TISLINKID";
	public static final String INCORRECT_TISPARAM_TISLINK_MSG = "Bad Request. Incorrect parameter passed for TisHierarchyLink Entity. TisLinkId parameter is passed as ";
	public static final String NO_TISDATA_WITHOUTPARAM = "No Data Found for TisHierarchyLink Entity";
	public static final String NO_TISDATA_WITHTISLINKID = "No TisHierarchy Data Found for given tisLinkId. TisLinkId value passed is";
	
	
	public static final Object SONGWARN_PARAMS_ALL = "ALLSONGWARN";
	public static final Object SONGWARN_PARAMS_SONGCODE = "WARNSONGCODE";
	public static final String INCORRECT_SONGWARNPARAM_SONGCODE_MSG = "Bad Request. Incorrect parameter passed for SongWarning Entity. WarnSongCode parameter is passed as ";
	public static final String NO_SONGWARNDATA_WITHOUTPARAM = "No Data Found for SongWarning Entity";
	public static final String NO_SONGWARNDATA_WITHWARNSONGCODE = "No SongWarning Data Found for given WarnSongCode. WarnSongCode value passed is";
	
	
	public static final Object SONGAKA_PARAMS_ALL = "ALLSONGAKA";
	public static final Object SONGAKA_PARAMS_SONGCODE = "AKASONGCODE";
	public static final String INCORRECT_SONGAKAPARAM_SONGCODE_MSG = "Bad Request. Incorrect parameter passed for SongAka Entity. AkaSongCode parameter is passed as ";
	public static final String NO_SONGAKADATA_WITHOUTPARAM = "No Data Found for SongAka Entity";
	public static final String NO_SONGAKADATA_WITHAKASONGCODE = "No SongAka Data Found for given AkaSongCode. AkaSongCode value passed is";
	
	
	public static final Object SONGNOTES_PARAMS_ALL = "ALLSONGNOTES";
	public static final Object SONGNOTES_PARAMS_SONGCODE = "SONGNOTESSONGCODE";
	public static final String INCORRECT_SONGNOTESPARAM_SONGCODE_MSG = "Bad Request. Incorrect parameter passed for SongNotes Entity. SongCode parameter is passed as ";
	public static final String NO_SONGNOTESDATA_WITHOUTPARAM = "No Data Found for SongNotes Entity";
	public static final String NO_SONGNOTESDATA_WITHSONGCODE = "No SongNotes Data Found for given SongCode. SongCode value passed is";
	
	public static final Object SONGSHARE_PARAMS_ALL = "ALLSONGSHARE";
	public static final Object SONGSHARE_PARAMS_SONGCODEANDTISCODE = "SONGCODEANDTISCODE";
	public static final String INCORRECT_SONGSHAREPARAM_SONGCODEANDTISCODE_MSG = "Bad Request. Incorrect parameters passed for SongShare Entity. Respective values passed for SongCode and TisCode parameters are ";
	public static final String NO_SONGSHAREDATA_WITHOUTPARAM = "No SongShare Data Found.";
	public static final String NO_SONGSHAREDATA_WITHSONGCODEANDTISCODE = "No SongShare Data Found for given songCode and TisCode. Parameters passed respectively are ";
	
	public static final Object SONGSTATUS_PARAMS_ALL = "ALLSONGSTATUS";
	public static final Object SONGSTATUS_PARAMS_SONGSTATUSCODE = "SONGSTATUSCODE";
	public static final String INCORRECT_SONGSTATUSPARAM_SONGCODE_MSG = "Bad Request. Incorrect parameter passed for SongStatus Entity. SongStatusCode parameter is passed as ";
	public static final String NO_SONGSTATUSDATA_WITHOUTPARAM = "No Data Found for SongStatus Entity";
	public static final String NO_SONGSTATUSDATA_WITHSONGCODE = "No SongStatus Data Found for given SongStatusCode. SongStatusCode value passed is";
	
	public static final Object SONGCLIENTLINK_PARAMS_ALL = "ALLSONGCLIENTLINK";
	public static final Object SONGCLIENTLINK_PARAMS_SONGCODEANDSYSTERR = "SONGCODEANDSYSTERR";
	public static final String INCORRECT_SONGCLIENTLINKPARAM_SONGCODEANDSYSTERR_MSG = "Bad Request. Incorrect parameters passed for SongClientLink Entity. Respective values passed for SongCode and SysTerr parameters are ";
	public static final String NO_SONGCLIENTLINKDATA_WITHOUTPARAM = "No SongClientLink Data Found.";
	public static final String NO_SONGCLIENTLINKDATA_WITHSONGCODEANDSYSTERR = "No SongClientLink Data Found for given songCode and SysTerr. Parameters passed respectively are ";
	
	public static final Object CLIENT_PARAMS_ALL = "ALLCLIENT";
	public static final Object CLIENT_PARAMS_CLIENTCODE = "CLIENTCODE";
	public static final String INCORRECT_CLIENTPARAM_SONGCODE_MSG = "Bad Request. Incorrect parameter passed for Client Entity. ClientCode parameter is passed as ";
	public static final String NO_CLIENTDATA_WITHOUTPARAM = "No Data Found for Client Entity";
	public static final String NO_CLIENTDATA_WITHCLIENTCODE = "No Client Data Found for given ClientCode. ClientCode value passed is";

	public static final Object TISCODE_PARAMS_ALL = "ALLTISCODE";
	public static final Object TISCODE_PARAMS_TISCODE = "TISCODE";
	public static final String INCORRECT_TISPARAM_TISCODE_MSG = "Bad Request. Incorrect parameter passed for TisCode Entity. TisCode parameter is passed as ";
	public static final String NO_TISCODEDATA_WITHOUTPARAM = "No Data Found for TisCode Entity";
	public static final String NO_TISCODEDATA_WITHTISCODE = "No TisCode Data Found for given tisCode. TisCode value passed is";
	
	public static final Object SONGGROUP_PARAMS_ALL = "ALLSONGGROUP";
	public static final Object SONGGROUP_PARAMS_GROUPCODE = "GROUPCODE";
	public static final String INCORRECT_GROUPCODEPARAM_SONGGROUP_MSG = "Bad Request. Incorrect parameter passed for SongGroup Entity. GroupCode parameter is passed as ";
	public static final String NO_SONGGROUPDATA_WITHOUTPARAM = "No Data Found for SongGroup Entity";
	public static final String NO_SONGGROUPDATA_WITHGROUPCODE = "No SongGroup Data Found for given GroupCode. GroupCode value passed is";
	
	public static final Object SONGGROUPLINK_PARAMS_ALL = "ALLSONGGROUPLINK";
	public static final Object SONGGROUPLINK_PARAMS_GRPACODE = "GRPACODE";
	public static final String INCORRECT_SONGGROUPLINKPARAM_GRPACODE_MSG = "Bad Request. Incorrect parameter passed for SongGroupLink Entity. grpaCode parameter is passed as ";
	public static final String NO_SONGGROUPLINKDATA_WITHOUTPARAM = "No Data Found for SongGroupLink Entity";
	public static final String NO_SONGGROUPLINKDATA_WITHGRPACODE = "No SongGroupLink Data Found for given GrpaCode. GrpaCode value passed is";
	
	public static final String page="1";
	public static final String per_page="1000";

	public static final String SQL_SONG_PAGINATION = "select SNGDTL_SONG_CODE as songCode,SNGDTL_SONG_TITLE as title,SNGDTL_TERRITORY_CONTD as territory,SNGDTL_COMPOSERS as composers,SNGDTL_ISWC_CODE as iswc,SNGDTL_DELETE_IND as isDeleted,SNGDTL_SONG_STATUS as status from bmg_dataintegration_datahub.dbo.vw_Datahub_DGSNGDTLVW ORDER BY SNGDTL_SONG_CODE OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";

	public static final Object CLIENTDEALLINK_PARAMS_ALL = "ALLCLIENTDEALLINKS";
	public static final Object CLIENTDEALLINK_PARAMS_CLIENTCODEANDSYSTERR = "CLIENTCODEANDSYSTERR";
	public static final String INCORRECT_CLIENTDEALLINKPARAM_MSG = "Bad Request. Incorrect parameter passed for ClientDealLink Entity. Respective values passed for clientCode and SysTerr are ";

	public static final String NO_CLIENTDEALLINKDATA_WITHOUTPARAM = "No Data Found For ClientDealLink Entity.";

	public static final String NO_CLIENTDEALLINKDATA_WITHCLIENTCODEANDSYSTERR = "No ClientDealLink data Found for give clientCode and SysTerr values. Parameters passed respectively are ";

	
	public static final Object DEAL_PARAMS_ALL = "ALLDEALS";
	public static final Object DEAL_PARAMS_SYSTERRANDDEALNO = "SYSTERRANDDEALNO";
	public static final String INCORRECT_DEALPARAM_MSG = "Bad Request. Incorrect parameters passed for Deal Entity. Respective values passed for SysTerr and DealNo are ";

	public static final String NO_DEALDATA_WITHOUTPARAM = "No Data Found For Deal Entity.";

	public static final String NO_DEALDATA_WITHSYSTERRANDDEALNO = "No Deal data Found for give SysTerr and DealNo values. Parameters passed respectively are ";

	public static final String Bad_Request_Exceeded_PerpageValue = "Bad Request. Per_Page value cannot be greater than 300. Value passed is ";
	

	

	


	
	
	
	
	
	

	
	
	
	
	
	
	
	

	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
