Camel Router Project for Blueprint (OSGi)
=========================================

To build this project use

    mvn install

To run the project you can execute the following Maven goal

    mvn camel:run

To deploy the project in OSGi. For example using Apache ServiceMix
or Apache Karaf. You can run the following command from its shell:

    osgi:install -s mvn:com.bmg.esb.subscribeToBmgMechLicensing/subscribeToBmgMechLicensing/1.0.0-SNAPSHOT

For more help see the Apache Camel documentation

    http://camel.apache.org/
    
1.0.0- With internal queue and external topic name
1.0.3 External topic is used different for testing
1.0.4 External topic name is changed
1.0.5 added new entity for composer
1.0.6 added new entities client and linksongclient