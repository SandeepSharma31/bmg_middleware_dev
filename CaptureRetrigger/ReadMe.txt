Camel Router Project for Blueprint (OSGi)
=========================================

To build this project use

    mvn install

To run the project you can execute the following Maven goal

    mvn camel:run

To deploy the project in OSGi. For example using Apache ServiceMix
or Apache Karaf. You can run the following command from its shell:

    osgi:install -s mvn:com.bmg.esb/CaptureRetrigger/1.0.0-SNAPSHOT

For more help see the Apache Camel documentation

    http://camel.apache.org/
    
    1.0.6- Single route
    4.0.3- Final Code
    4.0.4- size removed from logger. as we are getting exception.
    4.0.5->Don't log "body.size" as it will fail for 1 record also added Id and type headers in exception block content.
    4.0.6- removing headers for the Recordings
    4.0.7- removing headers for the Product,Song,Publisher
    4.0.8- song related id changes
    5.0.4- handled for no response scenario for all entities
	6.0.0 -Merged code.
	6.0.1- changed filepath property name
	6.0.6- removed specific headers
	6.0.7- removed setting body in exception and removed file componenet. As duplicate entries were coming for Exception in the file.
	6.0.8- added user defined exceptions for No Response from capture API
