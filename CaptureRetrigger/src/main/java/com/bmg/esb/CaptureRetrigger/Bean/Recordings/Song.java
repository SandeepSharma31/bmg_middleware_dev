/*Addded by Amruta */
package com.bmg.esb.CaptureRetrigger.Bean.Recordings;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)

public class Song {

    @JsonProperty("site")
    private Site site;
    @JsonProperty("composerName")
    private String composerName;
    @JsonProperty("isIMaestro")
    private Boolean isIMaestro;
    @JsonProperty("updatedBy")
    private String updatedBy;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("isPublicDomain")
    private Boolean isPublicDomain;
    @JsonProperty("title")
    private String title;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("created")
    private String created;
    @JsonProperty("updated")
    private String updated;
    @JsonProperty("publisherName")
    private String publisherName;
    @JsonProperty("entityStatus")
    private String entityStatus;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The site
     */
    @JsonProperty("site")
    public Site getSite() {
        return site;
    }

    /**
     * 
     * @param site
     *     The site
     */
    @JsonProperty("site")
    public void setSite(Site site) {
        this.site = site;
    }

    /**
     * 
     * @return
     *     The composerName
     */
    @JsonProperty("composerName")
    public String getComposerName() {
        return composerName;
    }

    /**
     * 
     * @param composerName
     *     The composerName
     */
    @JsonProperty("composerName")
    public void setComposerName(String composerName) {
        this.composerName = composerName;
    }

    /**
     * 
     * @return
     *     The isIMaestro
     */
    @JsonProperty("isIMaestro")
    public Boolean getIsIMaestro() {
        return isIMaestro;
    }

    /**
     * 
     * @param isIMaestro
     *     The isIMaestro
     */
    @JsonProperty("isIMaestro")
    public void setIsIMaestro(Boolean isIMaestro) {
        this.isIMaestro = isIMaestro;
    }

    /**
     * 
     * @return
     *     The updatedBy
     */
    @JsonProperty("updatedBy")
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 
     * @param updatedBy
     *     The updatedBy
     */
    @JsonProperty("updatedBy")
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The isPublicDomain
     */
    @JsonProperty("isPublicDomain")
    public Boolean getIsPublicDomain() {
        return isPublicDomain;
    }

    /**
     * 
     * @param isPublicDomain
     *     The isPublicDomain
     */
    @JsonProperty("isPublicDomain")
    public void setIsPublicDomain(Boolean isPublicDomain) {
        this.isPublicDomain = isPublicDomain;
    }

    /**
     * 
     * @return
     *     The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The createdBy
     */
    @JsonProperty("createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * 
     * @param createdBy
     *     The createdBy
     */
    @JsonProperty("createdBy")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * 
     * @return
     *     The created
     */
    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    /**
     * 
     * @param created
     *     The created
     */
    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * 
     * @return
     *     The updated
     */
    @JsonProperty("updated")
    public String getUpdated() {
        return updated;
    }

    /**
     * 
     * @param updated
     *     The updated
     */
    @JsonProperty("updated")
    public void setUpdated(String updated) {
        this.updated = updated;
    }

    /**
     * 
     * @return
     *     The publisherName
     */
    @JsonProperty("publisherName")
    public String getPublisherName() {
        return publisherName;
    }

    /**
     * 
     * @param publisherName
     *     The publisherName
     */
    @JsonProperty("publisherName")
    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    /**
     * 
     * @return
     *     The entityStatus
     */
    @JsonProperty("entityStatus")
    public String getEntityStatus() {
        return entityStatus;
    }

    /**
     * 
     * @param entityStatus
     *     The entityStatus
     */
    @JsonProperty("entityStatus")
    public void setEntityStatus(String entityStatus) {
        this.entityStatus = entityStatus;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
