package com.bmg.esb.CaptureRetrigger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;




public class CsvHeaders {
	public String outputFilePath;

	public String getOutputFilePath() {
		return outputFilePath;
	}

	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}


	public void writeHeader() throws IOException {
		
				

		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
	
		String[] header = { "Date Time", "Entity Type", "External Code",
				"Process Status", "Error message"};
		String outputDir = getOutputFilePath();

		System.out.println("write headers");
		
		System.out.println("filepath:"+outputDir);
		String outputFile = outputDir +"CaptureRetrigger" +"_"+dateFormat.format(date)+"_" + "Errorlog" + ".csv";

		File file = new File(outputFile);
		File folder = new File(outputDir);

		if (!file.exists()) {

			if (!folder.exists()) {
				folder.mkdirs();
			}
			FileWriter writer = new FileWriter(file, false);

			List<String> list = Arrays.asList(header);
			String str = "";
			for (String s : list) {
				str = str + s + ",";

			}
			String strNew = null;
			strNew = str.substring(0, str.length() - 1);
			writer.append(strNew);
			writer.append('\n');
			writer.flush();
			writer.close();

		}

	}
}
