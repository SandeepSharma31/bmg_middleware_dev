package com.bmg.esb.CaptureRetrigger;



import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;






public class SetValuestoBean {
	private static final Logger setValuestoBean = LoggerFactory
			.getLogger(SetValuestoBean.class);
	
	
	
	public void setTypeAndData(Exchange exe)
	{
		String dataVal=(String) exe.getIn().getHeader("datavalue");
		setValuestoBean.info("setValuestoBean:datavalue from header is "+dataVal);
		String typeVal=(String) exe.getIn().getHeader("typevalue");
		setValuestoBean.info("setValuestoBean:Typevalue from header is "+typeVal);
		typeVal ="models."+typeVal;
		StringBuilder sb = new StringBuilder();
		exe.getIn().setHeader("ResponseRcd","true" );
		/*if(!dataVal.getClass().equals(Type.class))
		{*/
		if(dataVal.contains("DataBean"))
		{
			setValuestoBean.info("setValuestoBean: 1##For DataBean check");
			exe.getIn().setHeader("ResponseRcd","false" );
			//exe.getIn().setBody("null");
		}
		else{
			
			setValuestoBean.info("setValuestoBean: 2##For DataBean check else");
			sb.append("{").append("\"type\"").append(":").append("\""+typeVal+ "\"").append(",")
			.append("\"data\"").append(":").append(dataVal).append("}") ;
			exe.getIn().setBody(sb.toString());
		}
		
		/*else
		{
			setValuestoBean.info("setValuestoBean: 0##Type case check else ");
			exe.getIn().setHeader("ResponseRcd","false" );
			
		}*/
	/*	if(dataVal.contains("DataBean"))
		{
			setValuestoBean.info("setValuestoBean: 1##For DataBean check");
		}
		else{
			
			setValuestoBean.info("setValuestoBean: 2##For DataBean check else");
		}*/
	
	}
	
	

}
