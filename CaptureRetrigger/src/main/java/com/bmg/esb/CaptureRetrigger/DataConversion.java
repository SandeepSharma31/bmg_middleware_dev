package com.bmg.esb.CaptureRetrigger;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.bmg.esb.CaptureRetrigger.Bean.Recordings.RecordingSongInfo;
import com.bmg.esb.CaptureRetrigger.Bean.Recordings.Song;

public class DataConversion {
	
	private static final Logger dataConversion = LoggerFactory
			.getLogger(DataConversion.class);
	public void recordingSongList(Exchange exchange) throws JsonParseException,
			JsonMappingException, IOException, ParseException {
		
		dataConversion.info("dataConversion:in RecorsingSongList");
		ObjectMapper mapper = new ObjectMapper();

		List<RecordingSongInfo> recSongList = mapper.readValue(exchange.getIn()
				.getBody(String.class),
				new TypeReference<List<RecordingSongInfo>>() {
				});
		int len = recSongList.size();
		dataConversion.info("dataConversion:in RecorsingSongList and Songs present for the recordings are :"+len);
		ArrayList<Song> listsong = new ArrayList<Song>();
		
		for (int i = 0; i < len; i++) {		
			
			dataConversion.info("dataConversion:in for loop");
			listsong.add(recSongList.get(i).getSong());
			dataConversion.info("dataConversion:All Songs are added to list");
		}
		if (len > 0) {
			dataConversion.info("dataConversion:Song List set as body to camel"+listsong.toString());
			exchange.getIn().setBody(listsong);
		}

		else {
			dataConversion.info("dataConversion:Song List is null so null set as body to camel");
			exchange.getIn().setBody(null);

		}
	}

}
