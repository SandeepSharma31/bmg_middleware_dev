/*Addded by Amruta */
package com.bmg.esb.CaptureRetrigger.Bean.Recordings;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)


public class RecordingSongInfo {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("idx")
    private Integer idx;
    @JsonProperty("song")
    private Song song;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The idx
     */
    @JsonProperty("idx")
    public Integer getIdx() {
        return idx;
    }

    /**
     * 
     * @param idx
     *     The idx
     */
    @JsonProperty("idx")
    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    /**
     * 
     * @return
     *     The song
     */
    @JsonProperty("song")
    public Song getSong() {
        return song;
    }

    /**
     * 
     * @param song
     *     The song
     */
    @JsonProperty("song")
    public void setSong(Song song) {
        this.song = song;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
