package com.bmg.esb.CaptureRetrigger;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import org.apache.camel.Exchange;
import org.apache.camel.Message;

public class SplitTokenizer {
	
	 StringTokenizer st;
	 StringTokenizer st1;
	 DataBean databean=new DataBean();
	 public  ArrayList<DataBean> tokenizer(Exchange ex)
	 {
		 Message message = ex.getIn();
		 String str=message.getBody(String.class);
		 
		 System.out.println(str);
		 /*ArrayList<String> arr = new ArrayList<String>();*/
		/* Array arr[]=new Arrays();*/
		 ArrayList<DataBean> listDatabean=new ArrayList<DataBean>();
		 DataBean databean=null;
		 st = new StringTokenizer(str,"\n");
		    while(st.hasMoreTokens()){
		    	databean=new DataBean();
		       databean.setExternalCode(st.nextToken());
		       System.out.println("externalcode"+" "+databean.getExternalCode());
		       	       
		       st1 = new StringTokenizer(databean.getExternalCode(),"_");
		       if(st1.hasMoreTokens())
		       {
		    	   databean.setExType(st1.nextToken());
		       }
		       if(st1.hasMoreTokens())
		       databean.setId(Integer.parseInt(st1.nextToken().trim()));
		       
		       /*System.out.println("Type "+databean.getExType());
		       System.out.println("Id "+databean.getId());*/
		       if(databean.getExType().equals("BMGPB"))
		       	   databean.setEntityType("Publisher");
		       else if(databean.getExType().equals("BMGS"))
		    	   databean.setEntityType("Song");
		       else if(databean.getExType().equals("BMGP"))
	    	   databean.setEntityType("Product");
		       else if(databean.getExType().equals("BMGR"))
		    	   databean.setEntityType("Recording");
		       else 
		    	   {
		    	   databean.setErrorMessage("invalidExternalCode");
		           databean.setEntityType("invalidExternalCode");
		           databean.setProcessStatus("Failed");
		    	   }   

		    	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		    	Date date=new Date();
		    	
		    	databean.setDateTime(dateFormat.format(date)+"");
	  		    listDatabean.add(databean);
		      
		      
		    }
		   
			return listDatabean;
	 }
	 
	 
	 
	 		   
}