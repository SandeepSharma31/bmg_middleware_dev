package com.bmg.esb.CaptureRetrigger;


import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;



@CsvRecord(separator = ",", skipFirstLine = true)
public class DataBean {

	@DataField(pos = 1,trim=true)
	private String dateTime;

	@DataField(pos = 2,trim=true)
	private String entityType;

	@DataField(pos = 3,trim=true)
	private String externalCode;

	@DataField(pos = 4,trim=true)
	private String ProcessStatus="Processed";

	@DataField(pos = 5,trim=true)
	private String errorMessage=" ";
	private String exType;
	private int id;

	public int getId() {
		return id;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getExType() {
		return exType;
	}

	public void setExType(String exType) {
		this.exType = exType;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public String getExternalCode() {
		return externalCode;
	}

	public void setExternalCode(String externalCode) {
		this.externalCode = externalCode;
	}

	public String getProcessStatus() {
		return ProcessStatus;
	}

	public void setProcessStatus(String processStatus) {
		ProcessStatus = processStatus;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
