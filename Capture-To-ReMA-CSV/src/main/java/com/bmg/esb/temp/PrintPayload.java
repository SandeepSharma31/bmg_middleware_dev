package com.bmg.esb.temp;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class PrintPayload implements Processor
{

	@Override
	public void process(Exchange arg0) throws Exception 
	{
		System.out.println("Payload Details:");
		System.out.println("================");
		System.out.println(arg0.getIn().getBody().getClass());

		System.out.println("songbody header Details:");
		System.out.println("========================");
		if(arg0.getProperty("songbody")!=null)
		{
			System.out.println(arg0.getProperty("songbody").getClass());
		}
		else
		{
			System.out.println("songsbody is null!");
		}
	}

}
