package com.bmg.esb.capture2rm.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bmg.esb.capture2rm.beans.products.ProductsInfo;
import com.bmg.esb.capture2rm.beans.products.child.ProductChild;
import com.bmg.esb.capture2rm.transformer.JsonToFieldValues;

public class CalcTrackandSideSum implements Processor
{

	public static String SIDE_COUNT = "sideCount";
	public static String TRACK_COUNT = "trackCount";
	
	private static final Logger CalcTrackandSideSum = LoggerFactory
			.getLogger(CalcTrackandSideSum.class);

	@Override
	public void process(Exchange arg0) throws Exception 
	{

		//Object objTemp = arg0.getIn().getHeader("childProductBody");
		Object objTemp = arg0.getProperty("childProductBody");
		
		ProductChild pc = null;
		if(objTemp instanceof ProductChild)
		{
			pc = (ProductChild) objTemp;
		}
		else
		{
			pc = ProductChild.fromString((String) objTemp);
		}

		if(arg0.getProperty(SIDE_COUNT)!=null)
		{
			int iNewSideSum = Integer.parseInt((String) arg0.getProperty(SIDE_COUNT)) + pc.getChild().getFormat().getNumberOfSides();
			arg0.setProperty(SIDE_COUNT, ""+iNewSideSum);
			CalcTrackandSideSum.info("CalcTrackandSideSum for property  "
					+ iNewSideSum);
		}
		else
		{
			arg0.setProperty(SIDE_COUNT, ""+pc.getChild().getFormat().getNumberOfSides());
		}

		if(arg0.getProperty(TRACK_COUNT)!=null)
		{
			int iNewTrackSum = Integer.parseInt((String) arg0.getProperty(TRACK_COUNT)) + pc.getIdx();
			arg0.setProperty(TRACK_COUNT, ""+iNewTrackSum);
			CalcTrackandSideSum.info("CalcTrackandSideSum for property  "
					+ iNewTrackSum);
		}
		else
		{
			arg0.setProperty(TRACK_COUNT, ""+pc.getIdx());
		}
		System.out.println("Side Count: "+arg0.getProperty(SIDE_COUNT));
		System.out.println("Track Count: "+arg0.getProperty(TRACK_COUNT));
	}

}
