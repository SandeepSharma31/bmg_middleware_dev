
package com.bmg.esb.capture2rm.beans.track.packagetrue;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)


public class Recording {

    @JsonProperty("genre")
    private Genre genre;
    @JsonProperty("copyrightYear")
    private Integer copyrightYear;
    @JsonProperty("isLive")
    private Boolean isLive;
    @JsonProperty("isrc")
    private String isrc;
    @JsonProperty("hasStreamingRights")
    private Boolean hasStreamingRights;
    @JsonProperty("hasPrimaryArtistAgreement")
    private Boolean hasPrimaryArtistAgreement;
    @JsonProperty("isMedley")
    private Boolean isMedley;
   // @JsonProperty("rightsHolder")
   // private String rightsHolder;
    @JsonProperty("isBMG_UK")
    private Boolean isBMGUK;
    @JsonProperty("recordingType")
    private RecordingType recordingType;
    @JsonProperty("repertoireOwnerCompany")
    private RepertoireOwnerCompany repertoireOwnerCompany;
    @JsonProperty("created")
    private String created;
    @JsonProperty("updated")
    private String updated;
    @JsonProperty("copyrightNotice")
    private String copyrightNotice;
    @JsonProperty("recordingDate")
    private String recordingDate;
    @JsonProperty("label")
    private Label label;
    @JsonProperty("hasUGCRights")
    private Boolean hasUGCRights;
    @JsonProperty("category")
    private String category;
    @JsonProperty("isMasteredITunes")
    private Boolean isMasteredITunes;
    @JsonProperty("metadataLanguage")
    private MetadataLanguage metadataLanguage;
    @JsonProperty("sendIndicator")
    private Boolean sendIndicator;
    @JsonProperty("artistName")
    private String artistName;
    @JsonProperty("recordingTitle")
    private String recordingTitle;
    @JsonProperty("ownershipType")
    private String ownershipType;
    @JsonProperty("versionTitle")
    private String versionTitle;
    @JsonProperty("recordingLanguage")
    private RecordingLanguage recordingLanguage;
    @JsonProperty("copyrightOwner")
    private String copyrightOwner;
    @JsonProperty("mixCountry")
    private MixCountry mixCountry;
    @JsonProperty("hasSyncRights")
    private Boolean hasSyncRights;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("entityStatus")
    private String entityStatus;
    @JsonProperty("site")
    private Site site;
    @JsonProperty("recordingCountry")
    private RecordingCountry recordingCountry;
    @JsonProperty("isRemaster")
    private Boolean isRemaster;
    @JsonProperty("duration")
    private Integer duration;
    @JsonProperty("isExplicit")
    private Boolean isExplicit;
    @JsonProperty("rightsOwnerPercentage")
    private Double rightsOwnerPercentage;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The genre
     */
    @JsonProperty("genre")
    public Genre getGenre() {
        return genre;
    }

    /**
     * 
     * @param genre
     *     The genre
     */
    @JsonProperty("genre")
    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    /**
     * 
     * @return
     *     The copyrightYear
     */
    @JsonProperty("copyrightYear")
    public Integer getCopyrightYear() {
        return copyrightYear;
    }

    /**
     * 
     * @param copyrightYear
     *     The copyrightYear
     */
    @JsonProperty("copyrightYear")
    public void setCopyrightYear(Integer copyrightYear) {
        this.copyrightYear = copyrightYear;
    }

    /**
     * 
     * @return
     *     The isLive
     */
    @JsonProperty("isLive")
    public Boolean getIsLive() {
        return isLive;
    }

    /**
     * 
     * @param isLive
     *     The isLive
     */
    @JsonProperty("isLive")
    public void setIsLive(Boolean isLive) {
        this.isLive = isLive;
    }

    /**
     * 
     * @return
     *     The isrc
     */
    @JsonProperty("isrc")
    public String getIsrc() {
        return isrc;
    }

    /**
     * 
     * @param isrc
     *     The isrc
     */
    @JsonProperty("isrc")
    public void setIsrc(String isrc) {
        this.isrc = isrc;
    }

    /**
     * 
     * @return
     *     The hasStreamingRights
     */
    @JsonProperty("hasStreamingRights")
    public Boolean getHasStreamingRights() {
        return hasStreamingRights;
    }

    /**
     * 
     * @param hasStreamingRights
     *     The hasStreamingRights
     */
    @JsonProperty("hasStreamingRights")
    public void setHasStreamingRights(Boolean hasStreamingRights) {
        this.hasStreamingRights = hasStreamingRights;
    }

    /**
     * 
     * @return
     *     The hasPrimaryArtistAgreement
     */
    @JsonProperty("hasPrimaryArtistAgreement")
    public Boolean getHasPrimaryArtistAgreement() {
        return hasPrimaryArtistAgreement;
    }

    /**
     * 
     * @param hasPrimaryArtistAgreement
     *     The hasPrimaryArtistAgreement
     */
    @JsonProperty("hasPrimaryArtistAgreement")
    public void setHasPrimaryArtistAgreement(Boolean hasPrimaryArtistAgreement) {
        this.hasPrimaryArtistAgreement = hasPrimaryArtistAgreement;
    }

    /**
     * 
     * @return
     *     The isMedley
     */
    @JsonProperty("isMedley")
    public Boolean getIsMedley() {
        return isMedley;
    }

    /**
     * 
     * @param isMedley
     *     The isMedley
     */
    @JsonProperty("isMedley")
    public void setIsMedley(Boolean isMedley) {
        this.isMedley = isMedley;
    }

   

    /**
     * 
     * @return
     *     The isBMGUK
     */
    @JsonProperty("isBMG_UK")
    public Boolean getIsBMGUK() {
        return isBMGUK;
    }

    /**
     * 
     * @param isBMGUK
     *     The isBMG_UK
     */
    @JsonProperty("isBMG_UK")
    public void setIsBMGUK(Boolean isBMGUK) {
        this.isBMGUK = isBMGUK;
    }

    /**
     * 
     * @return
     *     The recordingType
     */
    @JsonProperty("recordingType")
    public RecordingType getRecordingType() {
        return recordingType;
    }

    /**
     * 
     * @param recordingType
     *     The recordingType
     */
    @JsonProperty("recordingType")
    public void setRecordingType(RecordingType recordingType) {
        this.recordingType = recordingType;
    }

    /**
     * 
     * @return
     *     The repertoireOwnerCompany
     */
    @JsonProperty("repertoireOwnerCompany")
    public RepertoireOwnerCompany getRepertoireOwnerCompany() {
        return repertoireOwnerCompany;
    }

    /**
     * 
     * @param repertoireOwnerCompany
     *     The repertoireOwnerCompany
     */
    @JsonProperty("repertoireOwnerCompany")
    public void setRepertoireOwnerCompany(RepertoireOwnerCompany repertoireOwnerCompany) {
        this.repertoireOwnerCompany = repertoireOwnerCompany;
    }

    /**
     * 
     * @return
     *     The created
     */
    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    /**
     * 
     * @param created
     *     The created
     */
    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * 
     * @return
     *     The updated
     */
    @JsonProperty("updated")
    public String getUpdated() {
        return updated;
    }

    /**
     * 
     * @param updated
     *     The updated
     */
    @JsonProperty("updated")
    public void setUpdated(String updated) {
        this.updated = updated;
    }

    /**
     * 
     * @return
     *     The copyrightNotice
     */
    @JsonProperty("copyrightNotice")
    public String getCopyrightNotice() {
        return copyrightNotice;
    }

    /**
     * 
     * @param copyrightNotice
     *     The copyrightNotice
     */
    @JsonProperty("copyrightNotice")
    public void setCopyrightNotice(String copyrightNotice) {
        this.copyrightNotice = copyrightNotice;
    }

    /**
     * 
     * @return
     *     The recordingDate
     */
    @JsonProperty("recordingDate")
    public String getRecordingDate() {
        return recordingDate;
    }

    /**
     * 
     * @param recordingDate
     *     The recordingDate
     */
    @JsonProperty("recordingDate")
    public void setRecordingDate(String recordingDate) {
        this.recordingDate = recordingDate;
    }

    /**
     * 
     * @return
     *     The label
     */
    @JsonProperty("label")
    public Label getLabel() {
        return label;
    }

    /**
     * 
     * @param label
     *     The label
     */
    @JsonProperty("label")
    public void setLabel(Label label) {
        this.label = label;
    }

    /**
     * 
     * @return
     *     The hasUGCRights
     */
    @JsonProperty("hasUGCRights")
    public Boolean getHasUGCRights() {
        return hasUGCRights;
    }

    /**
     * 
     * @param hasUGCRights
     *     The hasUGCRights
     */
    @JsonProperty("hasUGCRights")
    public void setHasUGCRights(Boolean hasUGCRights) {
        this.hasUGCRights = hasUGCRights;
    }

    /**
     * 
     * @return
     *     The category
     */
    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    /**
     * 
     * @param category
     *     The category
     */
    @JsonProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * 
     * @return
     *     The isMasteredITunes
     */
    @JsonProperty("isMasteredITunes")
    public Boolean getIsMasteredITunes() {
        return isMasteredITunes;
    }

    /**
     * 
     * @param isMasteredITunes
     *     The isMasteredITunes
     */
    @JsonProperty("isMasteredITunes")
    public void setIsMasteredITunes(Boolean isMasteredITunes) {
        this.isMasteredITunes = isMasteredITunes;
    }

    /**
     * 
     * @return
     *     The metadataLanguage
     */
    @JsonProperty("metadataLanguage")
    public MetadataLanguage getMetadataLanguage() {
        return metadataLanguage;
    }

    /**
     * 
     * @param metadataLanguage
     *     The metadataLanguage
     */
    @JsonProperty("metadataLanguage")
    public void setMetadataLanguage(MetadataLanguage metadataLanguage) {
        this.metadataLanguage = metadataLanguage;
    }

    /**
     * 
     * @return
     *     The sendIndicator
     */
    @JsonProperty("sendIndicator")
    public Boolean getSendIndicator() {
        return sendIndicator;
    }

    /**
     * 
     * @param sendIndicator
     *     The sendIndicator
     */
    @JsonProperty("sendIndicator")
    public void setSendIndicator(Boolean sendIndicator) {
        this.sendIndicator = sendIndicator;
    }

    /**
     * 
     * @return
     *     The artistName
     */
    @JsonProperty("artistName")
    public String getArtistName() {
        return artistName;
    }

    /**
     * 
     * @param artistName
     *     The artistName
     */
    @JsonProperty("artistName")
    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    /**
     * 
     * @return
     *     The recordingTitle
     */
    @JsonProperty("recordingTitle")
    public String getRecordingTitle() {
        return recordingTitle;
    }

    /**
     * 
     * @param recordingTitle
     *     The recordingTitle
     */
    @JsonProperty("recordingTitle")
    public void setRecordingTitle(String recordingTitle) {
        this.recordingTitle = recordingTitle;
    }

    /**
     * 
     * @return
     *     The ownershipType
     */
    @JsonProperty("ownershipType")
    public String getOwnershipType() {
        return ownershipType;
    }

    /**
     * 
     * @param ownershipType
     *     The ownershipType
     */
    @JsonProperty("ownershipType")
    public void setOwnershipType(String ownershipType) {
        this.ownershipType = ownershipType;
    }

    /**
     * 
     * @return
     *     The versionTitle
     */
    @JsonProperty("versionTitle")
    public String getVersionTitle() {
        return versionTitle;
    }

    /**
     * 
     * @param versionTitle
     *     The versionTitle
     */
    @JsonProperty("versionTitle")
    public void setVersionTitle(String versionTitle) {
        this.versionTitle = versionTitle;
    }

    /**
     * 
     * @return
     *     The recordingLanguage
     */
    @JsonProperty("recordingLanguage")
    public RecordingLanguage getRecordingLanguage() {
        return recordingLanguage;
    }

    /**
     * 
     * @param recordingLanguage
     *     The recordingLanguage
     */
    @JsonProperty("recordingLanguage")
    public void setRecordingLanguage(RecordingLanguage recordingLanguage) {
        this.recordingLanguage = recordingLanguage;
    }

    /**
     * 
     * @return
     *     The copyrightOwner
     */
    @JsonProperty("copyrightOwner")
    public String getCopyrightOwner() {
        return copyrightOwner;
    }

    /**
     * 
     * @param copyrightOwner
     *     The copyrightOwner
     */
    @JsonProperty("copyrightOwner")
    public void setCopyrightOwner(String copyrightOwner) {
        this.copyrightOwner = copyrightOwner;
    }

    /**
     * 
     * @return
     *     The mixCountry
     */
    @JsonProperty("mixCountry")
    public MixCountry getMixCountry() {
        return mixCountry;
    }

    /**
     * 
     * @param mixCountry
     *     The mixCountry
     */
    @JsonProperty("mixCountry")
    public void setMixCountry(MixCountry mixCountry) {
        this.mixCountry = mixCountry;
    }

    /**
     * 
     * @return
     *     The hasSyncRights
     */
    @JsonProperty("hasSyncRights")
    public Boolean getHasSyncRights() {
        return hasSyncRights;
    }

    /**
     * 
     * @param hasSyncRights
     *     The hasSyncRights
     */
    @JsonProperty("hasSyncRights")
    public void setHasSyncRights(Boolean hasSyncRights) {
        this.hasSyncRights = hasSyncRights;
    }

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The entityStatus
     */
    @JsonProperty("entityStatus")
    public String getEntityStatus() {
        return entityStatus;
    }

    /**
     * 
     * @param entityStatus
     *     The entityStatus
     */
    @JsonProperty("entityStatus")
    public void setEntityStatus(String entityStatus) {
        this.entityStatus = entityStatus;
    }

    /**
     * 
     * @return
     *     The site
     */
    @JsonProperty("site")
    public Site getSite() {
        return site;
    }

    /**
     * 
     * @param site
     *     The site
     */
    @JsonProperty("site")
    public void setSite(Site site) {
        this.site = site;
    }

    /**
     * 
     * @return
     *     The recordingCountry
     */
    @JsonProperty("recordingCountry")
    public RecordingCountry getRecordingCountry() {
        return recordingCountry;
    }

    /**
     * 
     * @param recordingCountry
     *     The recordingCountry
     */
    @JsonProperty("recordingCountry")
    public void setRecordingCountry(RecordingCountry recordingCountry) {
        this.recordingCountry = recordingCountry;
    }

    /**
     * 
     * @return
     *     The isRemaster
     */
    @JsonProperty("isRemaster")
    public Boolean getIsRemaster() {
        return isRemaster;
    }

    /**
     * 
     * @param isRemaster
     *     The isRemaster
     */
    @JsonProperty("isRemaster")
    public void setIsRemaster(Boolean isRemaster) {
        this.isRemaster = isRemaster;
    }

    /**
     * 
     * @return
     *     The duration
     */
    @JsonProperty("duration")
    public Integer getDuration() {
        return duration;
    }

    /**
     * 
     * @param duration
     *     The duration
     */
    @JsonProperty("duration")
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     * 
     * @return
     *     The isExplicit
     */
    @JsonProperty("isExplicit")
    public Boolean getIsExplicit() {
        return isExplicit;
    }

    /**
     * 
     * @param isExplicit
     *     The isExplicit
     */
    @JsonProperty("isExplicit")
    public void setIsExplicit(Boolean isExplicit) {
        this.isExplicit = isExplicit;
    }

    /**
     * 
     * @return
     *     The rightsOwnerPercentage
     */
    @JsonProperty("rightsOwnerPercentage")
    public Double getRightsOwnerPercentage() {
        return rightsOwnerPercentage;
    }

    /**
     * 
     * @param rightsOwnerPercentage
     *     The rightsOwnerPercentage
     */
    @JsonProperty("rightsOwnerPercentage")
    public void setRightsOwnerPercentage(Double rightsOwnerPercentage) {
        this.rightsOwnerPercentage = rightsOwnerPercentage;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
