package com.bmg.esb.capture2rm.beans;

public class TrackInfo {
	
	
	
	private int side=0;
	private int duration=0;
	private String trackNumber="";
	private int idx=0;
	
	private String parentLink="N";
	private boolean isHidden=false;
	private boolean isGapless=false;
	private boolean isBonus=false;
	private boolean isUnbundle=false;
	private String overrideRecordingTitle="";
	private boolean displayVersionTitle=false;
	private Product product;
	private int id=0;

	// Recordings
	private Genre genre;
	private String ownershipType="";// : "EXCLUSIVE_LICENSE",
	private boolean isLive=false;// : false,
	private String isrc="";// : "GBXYZ1400001",
	private boolean hasStreamingRights=false;// : false,
	private String versionTitle="";// : "",
	private String complianceStatus="";// : "NON_COMPLIANT",
	private boolean isMedley=false;// : false,
	private String rightsHolder="";
	private boolean isBMG_UK=false;
	private RecordingType recordingType;
	private boolean hasSyncRights=false;// : false,
	
	private String title="";// : "My First Recording",
	private String supplyChainStatus="";// : "UNAVAILABLE",
	private Site site;
	private boolean isRemaster=false;// : false,
	private String recordingDate="";// : "2014-03-23T00:00:00",
	private boolean hasUGCRights=false;// : false,
	private String category="";// : "VOCAL",
	
	private boolean isMasteredITunes=false;// : false,
	private boolean isExplicit=false;// : false,
	private String artistName="";// : "Iron Maiden",
	
	private String recordingTitle="";// : "My First Recording"
	
	private boolean isImportBlanks=false;
	private String style="P";
	private String rType="S";
	private String copyrightYear="";
	private String copyrightNotice="";
	private String created="";
	private String updated="";
	private String updateProducttracks="N";
	private String updateProductTime="N";
	private String tech="D";
	    
    private String repertoireOwnerType="";
    private String rightsOwnerType="";

    
    private RecordingLanguage recordingLanguage;
    
    private RecordingCountry recordingCountry;
    private String DDenabled="Y";
    

    private boolean hasPrimaryArtistAgreement=false;

    private RightsOwnerCompany rightsOwnerCompany;
    private ContractualRightsCode contractualRightsCode;

    private MetadataLanguage metadataLanguage;
    private boolean sendIndicator=false;
    
    private CommissioningCountry commissioningCountry;

    private FirstProductionCountry firstProductionCountry;

    private String copyrightOwner="";
    private MixCountry mixCountry; 
    private FirstReleaseCountry firstReleaseCountry;

    private String rightsOwnerBeginDate="";
    private String repertoireOwnerBeginDate="";
    private RightsClaimType rightsClaimType;
    private ProducerCountry producerCountry;
    
	private double repertoireOwnerPercentage=0.0;
	private String deliveredSocieties="";
	private double rightsOwnerPercentage=0.0;
	private String entityStatus;;


	// }

	public String getOwnershipType() {
		return ownershipType;
	}

	public void setOwnershipType(String ownershipType) {
		this.ownershipType = ownershipType;
	}

	public boolean getIsLive() {
		return isLive;
	}

	public void setIsLive(boolean isLive) {
		this.isLive = isLive;
	}

	public String getIsrc() {
		return isrc;
	}

	public void setIsrc(String isrc) {
		this.isrc = isrc;
	}

	public boolean getHasStreamingRights() {
		return hasStreamingRights;
	}

	public void setHasStreamingRights(boolean hasStreamingRights) {
		this.hasStreamingRights = hasStreamingRights;
	}

	public String getVersionTitle() {
		return versionTitle;
	}

	public void setVersionTitle(String versionTitle) {
		this.versionTitle = versionTitle;
	}

	public String getComplianceStatus() {
		return complianceStatus;
	}

	public void setComplianceStatus(String complianceStatus) {
		this.complianceStatus = complianceStatus;
	}

	public boolean getIsMedley() {
		return isMedley;
	}

	public void setIsMedley(boolean isMedley) {
		this.isMedley = isMedley;
	}

	public boolean getHasSyncRights() {
		return hasSyncRights;
	}

	public void setHasSyncRights(boolean hasSyncRights) {
		this.hasSyncRights = hasSyncRights;
	}

	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSupplyChainStatus() {
		return supplyChainStatus;
	}

	public void setSupplyChainStatus(String supplyChainStatus) {
		this.supplyChainStatus = supplyChainStatus;
	}

	public boolean getIsRemaster() {
		return isRemaster;
	}

	public void setIsRemaster(boolean isRemaster) {
		this.isRemaster = isRemaster;
	}

	public String getRecordingDate() {
		return recordingDate;
	}

	public void setRecordingDate(String recordingDate) {
		this.recordingDate = recordingDate;
	}

	public boolean getHasUGCRights() {
		return hasUGCRights;
	}

	public void setHasUGCRights(boolean hasUGCRights) {
		this.hasUGCRights = hasUGCRights;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	
	public boolean getIsMasteredITunes() {
		return isMasteredITunes;
	}

	public void setIsMasteredITunes(boolean isMasteredITunes) {
		this.isMasteredITunes = isMasteredITunes;
	}

	public boolean getIsExplicit() {
		return isExplicit;
	}

	public void setIsExplicit(boolean isExplicit) {
		this.isExplicit = isExplicit;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getRecordingTitle() {
		return recordingTitle;
	}

	public void setRecordingTitle(String recordingTitle) {
		this.recordingTitle = recordingTitle;
	}

	public RecordingType getRecordingType() {
		return recordingType;
	}

	public void setRecordingType(RecordingType recordingType) {
		this.recordingType = recordingType;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public class RecordingType// : {
	{
		private String name;// : "Audio",
		private String recordingGroup;// : "AUDIO",
		private int id;// : 1
		private String created="";
		private String modified="";

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getRecordingGroup() {
			return recordingGroup;
		}

		public void setRecordingGroup(String recordingGroup) {
			this.recordingGroup = recordingGroup;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getCreated()
		{
			return created;
		}

		public void setCreated(String created)
		{
			this.created = created;
		}

		public String getModified()
		{
			return modified;
		}

		public void setModified(String modified)
		{
			this.modified = modified;
		}

	}
	
	public class RecordingCountry
	{
		
		private String name;
		private String code;
		private int id;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
	}

	public class Site// : {
	{
		private String id;// : "ba7e81f5-79cd-4ccd-a040-cb60ecf3ad02",
		private String name;// : "BMG Rights Management UK",
		private String code;// : "BGUK",
		private String country;// : "GB"

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

	}

	
	public class RecordingLanguage
	{
		
		private String name;
		private String code2;
		private String code3;
		private int id;
		private String created;
		private String modified;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCode2() {
			return code2;
		}
		public void setCode2(String code2) {
			this.code2 = code2;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getCode3() {
			return code3;
		}
		public void setCode3(String code3) {
			this.code3 = code3;
		}
		public String getCreated()
		{
			return created;
		}
		public void setCreated(String created)
		{
			this.created = created;
		}
		public String getModified()
		{
			return modified;
		}
		public void setModified(String modified)
		{
			this.modified = modified;
		}
		
		
	}
	
	public String getCopyrightYear() {
		return copyrightYear;
	}

	public void setCopyrightYear(String copyrightYear) {
		this.copyrightYear = copyrightYear;
	}

	public String getCopyrightNotice() {
		return copyrightNotice;
	}

	public void setCopyrightNotice(String copyrightNotice) {
		this.copyrightNotice = copyrightNotice;
	}

	public RecordingLanguage getRecordingLanguage() {
		return recordingLanguage;
	}

	public void setRecordingLanguage(RecordingLanguage recordingLanguage) {
		this.recordingLanguage = recordingLanguage;
	}

	public String getRepertoireOwnerType() {
		return repertoireOwnerType;
	}

	public void setRepertoireOwnerType(String repertoireOwnerType) {
		this.repertoireOwnerType = repertoireOwnerType;
	}

	public String getDDenabled() {
		return DDenabled;
	}

	public void setDDenabled(String dDenabled) {
		DDenabled = dDenabled;
	}

	public String getRightsOwnerType() {
		return rightsOwnerType;
	}

	public void setRightsOwnerType(String rightsOwnerType) {
		this.rightsOwnerType = rightsOwnerType;
	}

	public boolean getHasPrimaryArtistAgreement() {
		return hasPrimaryArtistAgreement;
	}

	public void setHasPrimaryArtistAgreement(boolean hasPrimaryArtistAgreement) {
		this.hasPrimaryArtistAgreement = hasPrimaryArtistAgreement;
	}
	
	public class RightsOwnerCompany
	{
		
		private String name="";
		private String code="";
		private boolean isRepertoireOwner=false;
		private boolean isContracting=false;
		private int id=0;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public boolean getIsRepertoireOwner() {
			return isRepertoireOwner;
		}
		public void setIsRepertoireOwner(boolean isRepertoireOwner) {
			this.isRepertoireOwner = isRepertoireOwner;
		}
		public boolean getIsContracting() {
			return isContracting;
		}
		public void setIsContracting(boolean isContracting) {
			this.isContracting = isContracting;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
		
	}
	
	
	public class ContractualRightsCode
	{
		private String name="";
		private String code="";
		private int id=0;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
		
	}
	
	
	public class MetadataLanguage
	{
		
		private String name="";
		private String code="";
		private int id=0;
		private String created="";
		private String modified="";
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getCreated()
		{
			return created;
		}
		public void setCreated(String created)
		{
			this.created = created;
		}
		public String getModified()
		{
			return modified;
		}
		public void setModified(String modified)
		{
			this.modified = modified;
		}
	}
	
	public class CommissioningCountry
	{
		
		private String name="";
		private String code="";
		private int id=0;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
		
	}
	
	public class FirstProductionCountry
	{
		
		private String name="";
		private String code="";
		private int id=0;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
	}
	
	public class FirstReleaseCountry
	{
		
		private String name="";
		private String code="";
		private int id=0;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
	}
	
	
	public class RightsClaimType
	{
		private String name="";
	
		private int id=0;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
		
	}
	
	public class ProducerCountry
	{
		private String name="";
		private String code="";
		private int id=0;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		
	}
	
	public class Genre
	{
		private String name="";
		private int id=0;
		private String created;
		private String modified;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getCreated()
		{
			return created;
		}
		public void setCreated(String created)
		{
			this.created = created;
		}
		public String getModified()
		{
			return modified;
		}
		public void setModified(String modified)
		{
			this.modified = modified;
		}
		
	}
	public RightsOwnerCompany getRightsOwnerCompany() {
		return rightsOwnerCompany;
	}

	public void setRightsOwnerCompany(RightsOwnerCompany rightsOwnerCompany) {
		this.rightsOwnerCompany = rightsOwnerCompany;
	}

	public ContractualRightsCode getContractualRightsCode() {
		return contractualRightsCode;
	}

	public void setContractualRightsCode(ContractualRightsCode contractualRightsCode) {
		this.contractualRightsCode = contractualRightsCode;
	}

	public boolean getSendIndicator() {
		return sendIndicator;
	}

	public void setSendIndicator(boolean sendIndicator) {
		this.sendIndicator = sendIndicator;
	}

	public CommissioningCountry getCommissioningCountry() {
		return commissioningCountry;
	}

	public void setCommissioningCountry(CommissioningCountry commissioningCountry) {
		this.commissioningCountry = commissioningCountry;
	}

	public FirstProductionCountry getFirstProductionCountry() {
		return firstProductionCountry;
	}

	public void setFirstProductionCountry(
			FirstProductionCountry firstProductionCountry) {
		this.firstProductionCountry = firstProductionCountry;
	}

	public String getCopyrightOwner() {
		return copyrightOwner;
	}

	public void setCopyrightOwner(String copyrightOwner) {
		this.copyrightOwner = copyrightOwner;
	}

	public MetadataLanguage getMetadataLanguage() {
		return metadataLanguage;
	}

	public void setMetadataLanguage(MetadataLanguage metadataLanguage) {
		this.metadataLanguage = metadataLanguage;
	}

	public FirstReleaseCountry getFirstReleaseCountry() {
		return firstReleaseCountry;
	}

	public void setFirstReleaseCountry(FirstReleaseCountry firstReleaseCountry) {
		this.firstReleaseCountry = firstReleaseCountry;
	}

	public String getRightsOwnerBeginDate() {
		return rightsOwnerBeginDate;
	}

	public void setRightsOwnerBeginDate(String rightsOwnerBeginDate) {
		this.rightsOwnerBeginDate = rightsOwnerBeginDate;
	}

	public String getRepertoireOwnerBeginDate() {
		return repertoireOwnerBeginDate;
	}

	public void setRepertoireOwnerBeginDate(String repertoireOwnerBeginDate) {
		this.repertoireOwnerBeginDate = repertoireOwnerBeginDate;
	}

	public RightsClaimType getRightsClaimType() {
		return rightsClaimType;
	}

	public void setRightsClaimType(RightsClaimType rightsClaimType) {
		this.rightsClaimType = rightsClaimType;
	}

	public ProducerCountry getProducerCountry() {
		return producerCountry;
	}

	public void setProducerCountry(ProducerCountry producerCountry) {
		this.producerCountry = producerCountry;
	}

	public double getRepertoireOwnerPercentage() {
		return repertoireOwnerPercentage;
	}

	public void setRepertoireOwnerPercentage(double repertoireOwnerPercentage) {
		this.repertoireOwnerPercentage = repertoireOwnerPercentage;
	}

	public String getDeliveredSocieties() {
		return deliveredSocieties;
	}

	public void setDeliveredSocieties(String deliveredSocieties) {
		this.deliveredSocieties = deliveredSocieties;
	}

	public double getRightsOwnerPercentage() {
		return rightsOwnerPercentage;
	}

	public void setRightsOwnerPercentage(double rightsOwnerPercentage) {
		this.rightsOwnerPercentage = rightsOwnerPercentage;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getrType() {
		return rType;
	}

	public void setrType(String rType) {
		this.rType = rType;
	}

	public String getUpdateProducttracks() {
		return updateProducttracks;
	}

	public void setUpdateProducttracks(String updateProducttracks) {
		this.updateProducttracks = updateProducttracks;
	}

	public String getUpdateProductTime() {
		return updateProductTime;
	}

	public void setUpdateProductTime(String updateProductTime) {
		this.updateProductTime = updateProductTime;
	}

	public String getTech() {
		return tech;
	}

	public void setTech(String tech) {
		this.tech = tech;
	}
	
	

	public RecordingCountry getRecordingCountry() {
		return recordingCountry;
	}

	public void setRecordingCountry(RecordingCountry recordingCountry) {
		this.recordingCountry = recordingCountry;
	}
	
	

	public int getSide() {
		return side;
	}
	public void setSide(int side) {
		this.side = side;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public String getTrackNumber() {
		return trackNumber;
	}
	public void setTrackNumber(String trackNumber) {
		this.trackNumber = trackNumber;
	}
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getParentLink() {
		return parentLink;
	}
	public void setParentLink(String parentLink) {
		this.parentLink = parentLink;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public class Product
	{
		private int id=0;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}
		
	}

	public boolean getIsHidden() {
		return isHidden;
	}
	public void setIsHidden(boolean isHidden) {
		this.isHidden = isHidden;
	}
	public boolean getIsGapless() {
		return isGapless;
	}
	public void setIsGapless(boolean isGapless) {
		this.isGapless = isGapless;
	}
	public boolean getIsBonus() {
		return isBonus;
	}
	public void setIsBonus(boolean isBonus) {
		this.isBonus = isBonus;
	}
	public boolean getIsUnbundle() {
		return isUnbundle;
	}
	public void setIsUnbundle(boolean isUnbundle) {
		this.isUnbundle = isUnbundle;
	}
	public String getOverrideRecordingTitle() {
		return overrideRecordingTitle;
	}
	public void setOverrideRecordingTitle(String overrideRecordingTitle) {
		this.overrideRecordingTitle = overrideRecordingTitle;
	}
	public boolean getDisplayVersionTitle() {
		return displayVersionTitle;
	}
	public void setDisplayVersionTitle(boolean displayVersionTitle) {
		this.displayVersionTitle = displayVersionTitle;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
		
	public TrackInfo acceptTrack(TrackInfo track) {
		return track;
	}

	public String getRightsHolder()
	{
		return rightsHolder;
	}

	public void setRightsHolder(String rightsHolder)
	{
		this.rightsHolder = rightsHolder;
	}

	public boolean getIsBMG_UK()
	{
		return isBMG_UK;
	}

	public void setIsBMG_UK(boolean isBMG_UK)
	{
		this.isBMG_UK = isBMG_UK;
	}

	public String getCreated()
	{
		return created;
	}

	public void setCreated(String created)
	{
		this.created = created;
	}

	public String getUpdated()
	{
		return updated;
	}

	public void setUpdated(String updated)
	{
		this.updated = updated;
	}

 public String getEntityStatus()
	{
		return entityStatus;
	}

	public void setEntityStatus(String entityStatus)
	{
		this.entityStatus = entityStatus;
	}

public class MixCountry
 {
	 
	 private String name;
	 private String code;
	 private boolean availableToRightsCodes;
	 private int id;
	 private String created;
	 private String modified;
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getCode()
	{
		return code;
	}
	public void setCode(String code)
	{
		this.code = code;
	}
	public boolean isAvailableToRightsCodes()
	{
		return availableToRightsCodes;
	}
	public void setAvailableToRightsCodes(boolean availableToRightsCodes)
	{
		this.availableToRightsCodes = availableToRightsCodes;
	}
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getCreated()
	{
		return created;
	}
	public void setCreated(String created)
	{
		this.created = created;
	}
	public String getModified()
	{
		return modified;
	}
	public void setModified(String modified)
	{
		this.modified = modified;
	}
	 
 
 
  }

}
