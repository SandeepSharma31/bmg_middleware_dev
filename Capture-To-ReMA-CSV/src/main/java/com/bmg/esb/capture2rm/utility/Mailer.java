package com.bmg.esb.capture2rm.utility;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

public class Mailer {

	public static Logger logger = Logger.getLogger(Mailer.class.getName());
	
	String FROM_EMAIL="amruta_patange@bmg.com";
	String TO_EMAIL="Amruta_patange@infosys.com";
	String EMAIL_SERVER_HOST="cmail.servicemail24.de";
			//cmailint.servicemail24.de
			//192.168.105.74


	public boolean sendEmailToSupportTeam(String mailSubject,String soundRecCode,int sidenum,int trackNum) throws Exception {

		
		logger.info("Inside sendEmailToSupportTeam");

		String to = TO_EMAIL;
		String from = FROM_EMAIL;
		String host = EMAIL_SERVER_HOST;
		System.out.println("to-->"+to);
		System.out.println("from-->"+from);
		System.out.println("host-->"+host);
		// Get the session object
		Properties properties = System.getProperties();
		logger.info("Properties: " +properties);
		properties.setProperty("mail.smtp.host", host);
		logger.info("SMTP Host: " +host);
		Session session = Session.getDefaultInstance(properties);

		// compose the message
		try {
			logger.info("Inside try block in sendEmailToSupportTeam");
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			message.setSubject(mailSubject);
			String mailbody="Hi,";
			String oneLine="\n Below mentioned track data has been removed from Capture system. \n";
			String secondLine=" Below mentioned track data has been removed from Capture system.\n";
					String thirdLine=" Sound Recording Code :{"+soundRecCode+"} Side Number: {"+sidenum+"} Track Number {"+trackNum+"}  \n";
			String lastLine="Regards, BMG Support team"; 
			
			message.setText(mailbody+oneLine+secondLine+thirdLine+lastLine);
			// Send message
			Transport.send(message);
			System.out.println("Testing Mail Functionality");

		} catch (MessagingException mex) {
			logger.info("Mail Exception: "+mex);
			throw new Exception("Error while sending email to  notification to support team. Please check smtp host is available");
		}

		return true;
	}

}
