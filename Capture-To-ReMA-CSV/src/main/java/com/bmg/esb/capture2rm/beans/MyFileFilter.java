package com.bmg.esb.capture2rm.beans;


import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.camel.component.file.GenericFile;
import org.apache.camel.component.file.GenericFileFilter;

public class MyFileFilter<T> implements GenericFileFilter<T> {

	@Override
	public boolean accept(GenericFile<T> file) {
		// we want all directories
        if (file.isDirectory()) {
            return true;
        }
        
        Calendar calendar = Calendar.getInstance();
        // we dont accept any files starting with skip in the name
       
        return !file.getFileName().contains(new SimpleDateFormat("yyyyMMddHH").format(calendar.getTime()).toString());
        //return !file.getFileName().startsWith(new SimpleDateFormat("yyyyMMddHH").format(calendar.getTime()).toString());
	}	
}
/*public static void main(String args[]){
	Calendar calendar = Calendar.getInstance();
	 String currentdate = new SimpleDateFormat("yyyyMMddHH").format(calendar.getTime()).toString();
	int date = Integer.parseInt(currentdate);
	calendar.add(Calendar.HOUR, -1);
	   System.out.println("Subtract one day from current date : " + new SimpleDateFormat("yyyyMMddHH").format(calendar.getTime()));

	System.out.println("Date::::"+new SimpleDateFormat("yyyyMMddHH").format(calendar.getTime()).toString());
	System.out.println("int date:::"+date);
	
}
}*/