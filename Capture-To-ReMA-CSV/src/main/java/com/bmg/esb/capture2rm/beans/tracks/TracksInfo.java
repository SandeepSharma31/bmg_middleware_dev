
package com.bmg.esb.capture2rm.beans.tracks;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import com.bmg.esb.capture2rm.beans.songs.SongsInfo;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


@JsonIgnoreProperties(ignoreUnknown = true)
public class TracksInfo {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("product")
    private Product product;
    @JsonProperty("recording")
    private Recording recording;
    @JsonProperty("side")
    private Integer side;
    @JsonProperty("idx")
    private Integer idx;
    @JsonProperty("isHidden")
    private Boolean isHidden;
    @JsonProperty("isGapless")
    private Boolean isGapless;
    @JsonProperty("isBonus")
    private Boolean isBonus;
    @JsonProperty("isUnbundle")
    private Boolean isUnbundle;
    @JsonProperty("overrideRecordingTitle")
    private String overrideRecordingTitle;
    @JsonProperty("displayVersionTitle")
    private Boolean displayVersionTitle;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The product
     */
    @JsonProperty("product")
    public Product getProduct() {
        return product;
    }

    /**
     * 
     * @param product
     *     The product
     */
    @JsonProperty("product")
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * 
     * @return
     *     The recording
     */
    @JsonProperty("recording")
    public Recording getRecording() {
        return recording;
    }

    /**
     * 
     * @param recording
     *     The recording
     */
    @JsonProperty("recording")
    public void setRecording(Recording recording) {
        this.recording = recording;
    }

    /**
     * 
     * @return
     *     The side
     */
    @JsonProperty("side")
    public Integer getSide() {
        return side;
    }

    /**
     * 
     * @param side
     *     The side
     */
    @JsonProperty("side")
    public void setSide(Integer side) {
        this.side = side;
    }

    /**
     * 
     * @return
     *     The idx
     */
    @JsonProperty("idx")
    public Integer getIdx() {
        return idx;
    }

    /**
     * 
     * @param idx
     *     The idx
     */
    @JsonProperty("idx")
    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    /**
     * 
     * @return
     *     The isHidden
     */
    @JsonProperty("isHidden")
    public Boolean getIsHidden() {
        return isHidden;
    }

    /**
     * 
     * @param isHidden
     *     The isHidden
     */
    @JsonProperty("isHidden")
    public void setIsHidden(Boolean isHidden) {
        this.isHidden = isHidden;
    }

    /**
     * 
     * @return
     *     The isGapless
     */
    @JsonProperty("isGapless")
    public Boolean getIsGapless() {
        return isGapless;
    }

    /**
     * 
     * @param isGapless
     *     The isGapless
     */
    @JsonProperty("isGapless")
    public void setIsGapless(Boolean isGapless) {
        this.isGapless = isGapless;
    }

    /**
     * 
     * @return
     *     The isBonus
     */
    @JsonProperty("isBonus")
    public Boolean getIsBonus() {
        return isBonus;
    }

    /**
     * 
     * @param isBonus
     *     The isBonus
     */
    @JsonProperty("isBonus")
    public void setIsBonus(Boolean isBonus) {
        this.isBonus = isBonus;
    }

    /**
     * 
     * @return
     *     The isUnbundle
     */
    @JsonProperty("isUnbundle")
    public Boolean getIsUnbundle() {
        return isUnbundle;
    }

    /**
     * 
     * @param isUnbundle
     *     The isUnbundle
     */
    @JsonProperty("isUnbundle")
    public void setIsUnbundle(Boolean isUnbundle) {
        this.isUnbundle = isUnbundle;
    }

    /**
     * 
     * @return
     *     The overrideRecordingTitle
     */
    @JsonProperty("overrideRecordingTitle")
    public String getOverrideRecordingTitle() {
        return overrideRecordingTitle;
    }

    /**
     * 
     * @param overrideRecordingTitle
     *     The overrideRecordingTitle
     */
    @JsonProperty("overrideRecordingTitle")
    public void setOverrideRecordingTitle(String overrideRecordingTitle) {
        this.overrideRecordingTitle = overrideRecordingTitle;
    }

    /**
     * 
     * @return
     *     The displayVersionTitle
     */
    @JsonProperty("displayVersionTitle")
    public Boolean getDisplayVersionTitle() {
        return displayVersionTitle;
    }

    /**
     * 
     * @param displayVersionTitle
     *     The displayVersionTitle
     */
    @JsonProperty("displayVersionTitle")
    public void setDisplayVersionTitle(Boolean displayVersionTitle) {
        this.displayVersionTitle = displayVersionTitle;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    
    public String toString()
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectMapper mapper = new ObjectMapper();

		try 
		{
			mapper.writeValue(baos, this);
		} 
		catch (Exception e) 
		{
			throw new RuntimeException("Unable to serialize TracksInfo object to a Json-"+e.getMessage());
		}
		return baos.toString();
	}
	
	public static TracksInfo fromString(String strTrackData) throws Exception
	{
		
		ObjectMapper mapper = new ObjectMapper();
		TracksInfo objTracks = mapper.readValue(strTrackData,new TypeReference<TracksInfo>() {});
		return objTracks;
	}
    
    
    
    
}
