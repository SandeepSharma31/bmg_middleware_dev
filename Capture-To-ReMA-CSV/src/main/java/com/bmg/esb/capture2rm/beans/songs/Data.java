
package com.bmg.esb.capture2rm.beans.songs;

import java.util.HashMap;
import java.util.Map;

//import org.codehaus.jackson.annotate.JsonAnyGetter;
/*import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;*/
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {

    @JsonProperty("site")
    private Site site;
    @JsonProperty("composerName")
    private String composerName;
    @JsonProperty("isIMaestro")
    private Boolean isIMaestro;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("isPublicDomain")
    private Boolean isPublicDomain;
    @JsonProperty("title")
    private String title;
    @JsonProperty("created")
    private String created;
    @JsonProperty("updated")
    private String updated;
    @JsonProperty("entityStatus")
    private String entityStatus;
    @JsonProperty("generateSoundRec")
    private boolean generateSoundRec;
    @JsonProperty("publisherName")
    private String publisherName;
    @JsonProperty("iswc")
    private String iswc;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("iMaestroCode")
    private String iMaestroCode;
    public String getiMaestroCode() {
		return iMaestroCode;
	}

	public void setiMaestroCode(String iMaestroCode) {
		this.iMaestroCode = iMaestroCode;
	}

	@JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The site
     */
    @JsonProperty("site")
    public Site getSite() {
        return site;
    }

    /**
     * 
     * @param site
     *     The site
     */
    @JsonProperty("site")
    public void setSite(Site site) {
        this.site = site;
    }

    /**
     * 
     * @return
     *     The composerName
     */
    @JsonProperty("composerName")
    public String getComposerName() {
        return composerName;
    }

    /**
     * 
     * @param composerName
     *     The composerName
     */
    @JsonProperty("composerName")
    public void setComposerName(String composerName) {
        this.composerName = composerName;
    }

    /**
     * 
     * @return
     *     The isIMaestro
     */
    @JsonProperty("isIMaestro")
    public Boolean getIsIMaestro() {
        return isIMaestro;
    }

    /**
     * 
     * @param isIMaestro
     *     The isIMaestro
     */
    @JsonProperty("isIMaestro")
    public void setIsIMaestro(Boolean isIMaestro) {
        this.isIMaestro = isIMaestro;
    }

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The isPublicDomain
     */
    @JsonProperty("isPublicDomain")
    public Boolean getIsPublicDomain() {
        return isPublicDomain;
    }

    /**
     * 
     * @param isPublicDomain
     *     The isPublicDomain
     */
    @JsonProperty("isPublicDomain")
    public void setIsPublicDomain(Boolean isPublicDomain) {
        this.isPublicDomain = isPublicDomain;
    }

    /**
     * 
     * @return
     *     The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The created
     */
    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    /**
     * 
     * @param created
     *     The created
     */
    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * 
     * @return
     *     The updated
     */
    @JsonProperty("updated")
    public String getUpdated() {
        return updated;
    }

    /**
     * 
     * @param updated
     *     The updated
     */
    @JsonProperty("updated")
    public void setUpdated(String updated) {
        this.updated = updated;
    }

    /**
     * 
     * @return
     *     The entityStatus
     */
    @JsonProperty("entityStatus")
    public String getEntityStatus() {
        return entityStatus;
    }

    /**
     * 
     * @param entityStatus
     *     The entityStatus
     */
    @JsonProperty("entityStatus")
    public void setEntityStatus(String entityStatus) {
        this.entityStatus = entityStatus;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

	/**
	 * @return the generateSoundRec
	 */
	public boolean isGenerateSoundRec()
	{
		return generateSoundRec;
	}

	/**
	 * @param generateSoundRec the generateSoundRec to set
	 */
	public void setGenerateSoundRec(boolean generateSoundRec)
	{
		this.generateSoundRec = generateSoundRec;
	}

	/**
	 * @return the publisherName
	 */
	public String getPublisherName()
	{
		return publisherName;
	}

	/**
	 * @param publisherName the publisherName to set
	 */
	public void setPublisherName(String publisherName)
	{
		this.publisherName = publisherName;
	}

	/**
	 * @return the iswc
	 */
	public String getIswc()
	{
		return iswc;
	}

	/**
	 * @param iswc the iswc to set
	 */
	public void setIswc(String iswc)
	{
		this.iswc = iswc;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy()
	{
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}

}
