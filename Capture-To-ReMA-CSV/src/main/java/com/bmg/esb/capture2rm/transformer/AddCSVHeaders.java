package com.bmg.esb.capture2rm.transformer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.camel.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.bmg.esb.capture2rm.model.SongsBindyInfo;

public class AddCSVHeaders {

	private static final Logger AddCSVHeaders = LoggerFactory
			.getLogger(AddCSVHeaders.class);

	public String captureoutputFilePath;

	/*public String getOutputFilePath() {
		return captureoutputFilePath;
	}*/
	

	/*public void setOutputFilePath(String captureoutputFilePath) {
		this.captureoutputFilePath = captureoutputFilePath;
	}*/

	public String getCaptureoutputFilePath() {
		return captureoutputFilePath;
	}

	public void setCaptureoutputFilePath(String captureoutputFilePath) {
		this.captureoutputFilePath = captureoutputFilePath;
	}

	public void writeCSVHeader(@Header("batchId") String batchId,
			@Header("entitytype") String entitytype) throws IOException {

		if (entitytype.equalsIgnoreCase("SONG")) {

			String[] header = { "batch_id", "ADOADD", "IM_SONG_SEQ",
					"ERRORCODE", "ERRORCODEI", "AIMPORTBLANKS", "aSite",
					"aCode", "aTitle", "aComposer", "aISWC", "aPublicDomain",
					"aSoundrecISRC", "aCMRRASongCode", "aGenerateSoundRec",
					"aArtist", "aExternalCode", "aLyricist" };

			writeHeader(batchId, header, entitytype);

		}

		/*if (entitytype.equalsIgnoreCase("SONGPUBLISHER")) {

			String[] header = { "batch_id", "ADOADD", "IM_SONGPUB_SEQ",
					"ERRORCODE", "ERRORCODEI", "AIMPORTBLANKS", "aSongSite",
					"aSongCode", "aTerr", "aPubSite", "aPubCode", "aShare",
					"aUpdateLicenseShare", "aExternalSongCode",
					"aExternalPubCode", "aExternalSongPubCode" };

			writeHeader(batchId, header, entitytype);

		}

		if (entitytype.equalsIgnoreCase("PUBLISHER")) {

			String[] header = { "batch_id", "ADOADD", "IM_PUBLISHER_SEQ",
					"ERRORCODE", "ERRORCODEI", "AIMPORTBLANKS", "aSite",
					"aCode", "aName", "aCompany", "aCurrency", "aExRate",
					"aTaxID", "aControlled", "aAgency", "aCrossMechCode",
					"aCrossPrintCode", "aNegCycle", "aPriceType", "aUnitType",
					"aMinCheque", "aHoldRes", "aHoldPay", "aPayVAT",
					"aFreeGoods", "aPaymentFreq", "aContactName", "aStreet2",
					"aTown", "aCounty", "aPostcode", "aCountry", "aPhone",
					"aFax", "aEmail", "aEmailCC", "aAddrTypeCode",
					"aAddrNotes", "aSendStatement", "aAccountingSystem",
					"aCMRRAPublCode", "aExternalCode"

			};

			writeHeader(batchId, header, entitytype);

		}*/

		if (entitytype.equalsIgnoreCase("PRODUCT")) {

			String[] header = { "batch_id", "ADOADD", "IM_PRODUCT_SEQ",
					"ERRORCODE", "ERRORCODEI", "AIMPORTBLANKS", "aSite",
					"aCode", "aBarCode", "aTitle", "aArtist", "aCompany",
					"aComponents", "aSides", "aTracks", "aPriceCategory",
					"aConfiguration", "aTotalPlayingTime", "aReleaseDate",
					"aItReleaseDate", "aRereleaseDate", "aDeletionDate",
					"aTVAdvertised", "aPayCopyright", "aCopyrightProductType",
					"aCopyrightCapType", "aCopyrightCapSongs",
					"aCopyrightCapValue", "aCopyrightCapPerc",
					"aCopyrightCapFreezeDate", "aSociety", "aSocietyNumber",
					"aTVAdFrom", "aTVAdTo", "aInternal", "aPriceLevel",
					"aTerritory", "aLabel", "aHoldReserves",
					"aCopyrightTracks", "aCompilation", "aDigitalDownload",
					"aAllowFree", "aRoyaltyInfoComplete", "aPromo",
					"aLanguage", "aType", "aRecType", "aTech", "aStyle",
					"aVocal", "aPayCSI", "aPaySOCAN", "aProjectCode",
					"aGridCode", "aExplicitContent", "aProductClearedTerrName",
					"aProductExcludedTerrName", "aPYear", "aPNote", "aCYear",
					"aCNote", "aCatCoEnabled", "aCatCoResend",
					"aAutoCreateAKACodes", "aNotes", "aExternalCode",
					"aUpdateUserCode", "aUpdateTerrBarcodes",
					"aExternalLabelCode", "aGenreGroup1", "aGenre1",
					"aGenreGroup2", "aGenre2" };

			writeHeader(batchId, header, entitytype);

		}

		if (entitytype.equalsIgnoreCase("PRODUCTAPPROVAL")) {

			String[] header = { "batch_id", "ADOADD",
					"IM_PRODUCTAPPROVALS_SEQ", "ERRORCODE", "ERRORCODEI",
					"AIMPORTBLANKS", "aProductSite", "aProductCode",
					"aApproved", "aUpdateICR" };

			writeHeader(batchId, header, entitytype);

		}

		if (entitytype.equalsIgnoreCase("PRODUCTAKA")) {

			String[] header = { "batch_id", "ADOADD", "IM_PRODAKA_SEQ",
					"ERRORCODE", "ERRORCODEI", "AIMPORTBLANKS", "aProdSite",
					"aProdCode", "aAKASite", "aAKACode", "aDeleteAKA",
					"aPrimaryAKA", "aAKATypeCode" };

			writeHeader(batchId, header, entitytype);

		}

		if (entitytype.equalsIgnoreCase("TRACK")) {

			String[] header = { "batch_id", "ADOADD", "IM_TRACK_SEQ",
					"ERRORCODE", "ERRORCODEI", "AIMPORTBLANKS", "aProductSite",
					"aProductCode", "aSoundrecSite", "aSoundrecCode", "aSide",
					"aTrack", "aSub", "aTime", "aSoundrecISRC", "aParentLink",
					"aMedleyTitle", "aExProductCode", "aExSoundRecCode" };

			writeHeader(batchId, header, entitytype);

		}

		if (entitytype.equalsIgnoreCase("SOUNDRECORDINGS")) {

			String[] header = { "batch_id", "ADOADD", "IM_SOUNDREC_SEQ",
					"ERRORCODE", "ERRORCODEI", "AIMPORTBLANKS", "aSite",
					"aCode", "aSongSite", "aSongCode", "aArtist", "aSubmix",
					"aISRC", "aType", "aStyle", "aRType", "aVocal", "aPYear",
					"aPNote", "aTime", "aUpdateProductTracks",
					"aUpdateProductTime", "aTech", "aRecordingDate",
					"aDeliveredDate", "aReleaseDate", "aTerr", "aLang",
					"aOrigin", "aLabel", "aCompany", "aDDEnabled", "aDDCode",
					"aSociety", "aCopyrightProductType", "aPayCopyright",
					"aSoleRecTerr", "aFinalMixTerr", "aCommissionTerr",
					"aPerformanceTerr", "aPayCSI", "aPaySOCAN",
					"aLevelOfInterest", "aOwnershipDate", "aOwnershipType",
					"aRightsHolderCode", "aRightsHoldingType",
					"aRightsHoldingPerc", "aRightsHoldingEndDate", "aCYear",
					"aCNote", "aGridCode", "aExplicitContent", "aMedley",
					"aMedleyType", "aMedleyTitle", "aLabelCatNo",
					"aProjectCode", "aExternalCode", "aExternalSongCode",
					"aExternalLabelCode" };

			writeHeader(batchId, header, entitytype);
			// changes done for SoundRec digitization enhancement 22/4/2016
			writeHeader(batchId, header, "SOUNDRECORDINGSIVE");
			

		}
		
		// MFD - 104 changes - Adding new csv's for licensing starts here
		
		if (entitytype.equalsIgnoreCase("LICENCINGPRODUCT")) {

			String[] header = { "Import Blanks", "Site Code",
					"Product Code", "Barcode", "Product Title",
					"Product Artist", "Company Code", "Components",
					"Sides", "Tracks","Price Category Code", "Configuration Code", "Total playing Time",
					"Actual Release Date", "International Release Date", "Re-Release Date",
					"Deletion Date", "TV Advertised", "Pay Copyright", "Copyright Product Type",
					"Copyright Cap Type", "Copyright Cap Songs", "Copyright Cap Value (Cents)", "Copyright Cap % of cap"
					, "Copyright Cap Freeze Date", "Society Code", "Society Number/ID", "TV Advertised From Date", "TV Advertised To Date", "Internal", "Price Level"
					, "Territory Code", "Label Code", "Hold Reserves", "Copyright Tracks", "Compilation", "Digital Download", "Allow Free"
					, "Royalty Complete", "Promotional", "Barcode", "Language", "Type", "Rec Type", "Technique"
					, "Style", "Vocal", "Pay CSI", "Pay SOCAN", "Project Code", "Grid Code", "Explicit Content"
					, "Included Territory Names", "Excluded Territory Names", "P Year", "P Note", "C Year", "C Note", "CATCO Enabled"
					, "CATCO Resend", "Auto-Create AKA Codes", "Notes", "External Code", "Update User Code", "Update Territory Level Barcodes", "External Label Code"
					, "Genre Group 1", "Genre 1", "Genre Group 2", "Genre 2"};

			writeHeader(batchId, header, entitytype);

		}
		if (entitytype.equalsIgnoreCase("LICENCINGTRACK")) {

			String[] header = { "Product Site", "Product Code",
					"Sound Recording Site", "Sound Recording Code", "Side Number",
					"Track Number", "Subtrack Code", "Track Time",
					"Sound Recording ISRC", "Link Parent Product"
					, "Medley Title", "External Product Code", "External Sound Recording Code"};

			writeHeader(batchId, header, entitytype);

		}
		if (entitytype.equalsIgnoreCase("LICENCINGPRODUCTAKA")) {

			String[] header = { "Product Site", "Product Code",
					"AKA Site", "AKA Code", "Delete AKA",
					"Primary AKA", "AKA Type Code"};

			writeHeader(batchId, header, entitytype);

		}
		// MFD - 104 changes - Adding new csv's for licensing ends here.

	}

	public void writeHeader(String batchId, String[] header, String entitytype)
			throws IOException {

		String outputDir = getCaptureoutputFilePath();
		String outputFile = outputDir + entitytype + "_" + batchId + ".csv";

		File file = new File(outputFile);
		File folder = new File(outputDir);

		if (!file.exists()) {

			if (!folder.exists()) {
				folder.mkdirs();
			}
			FileWriter writer = new FileWriter(file, false);

			List<String> list = Arrays.asList(header);
			String str = "";
			for (String s : list) {
				str = str + s + ",";

			}
			String strNew = null;
			strNew = str.substring(0, str.length() - 1);
			writer.append(strNew);
			writer.append('\n');
			writer.flush();
			writer.close();

			/*
			 * if (!file.exists()) {
			 * 
			 * if(!folder.exists()) { folder.mkdirs(); } FileWriter writer = new
			 * FileWriter(file, false);
			 * 
			 * List<String> list = Arrays.asList(header);
			 * 
			 * for (String s : list) { writer.append(s); writer.append(',');
			 * 
			 * 
			 * }
			 */

			
		}

	}
}
