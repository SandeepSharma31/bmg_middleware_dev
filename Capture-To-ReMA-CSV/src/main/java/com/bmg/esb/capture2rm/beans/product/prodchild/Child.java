
package com.bmg.esb.capture2rm.beans.product.prodchild;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Child {

    @JsonProperty("genre")
    private Genre genre;
    @JsonProperty("copyrightYear")
    private Integer copyrightYear;
    @JsonProperty("pricePointMidEditable")
    private Boolean pricePointMidEditable;
    @JsonProperty("hasContractualDigitalRights")
    private Boolean hasContractualDigitalRights;
    @JsonProperty("hasStreamingRights")
    private Boolean hasStreamingRights;
    @JsonProperty("isPackage")
    private Boolean isPackage;
    @JsonProperty("hasPrimaryArtistAgreement")
    private Boolean hasPrimaryArtistAgreement;
    @JsonProperty("isSoundtrack")
    private Boolean isSoundtrack;
    @JsonProperty("isPartial")
    private Boolean isPartial;
    @JsonProperty("numberOfTracks")
    private Integer numberOfTracks;
    @JsonProperty("pricePointBudgetEditable")
    private Boolean pricePointBudgetEditable;
    @JsonProperty("isCompilation")
    private Boolean isCompilation;
    @JsonProperty("hasManufacturingRights")
    private Boolean hasManufacturingRights;
    @JsonProperty("repertoireOwnerCompany")
    private RepertoireOwnerCompany repertoireOwnerCompany;
    @JsonProperty("created")
    private String created;
    @JsonProperty("updated")
    private String updated;
    @JsonProperty("artworkYear")
    private Integer artworkYear;
    @JsonProperty("hasPricePointsFull")
    private Boolean hasPricePointsFull;
    @JsonProperty("altProductCodes")
    private List<Object> altProductCodes = new ArrayList<Object>();
    @JsonProperty("copyrightNotice")
    private String copyrightNotice;
    @JsonProperty("isLicenced")
    private Boolean isLicenced;
    @JsonProperty("barcode")
    private String barcode;
    @JsonProperty("label")
    private Label label;
    @JsonProperty("format")
    private Format format;
    @JsonProperty("metadataLanguage")
    private MetadataLanguage metadataLanguage;
    @JsonProperty("hasContractualPhysicalRights")
    private Boolean hasContractualPhysicalRights;
    @JsonProperty("artistName")
    private String artistName;
    @JsonProperty("artworkNotice")
    private String artworkNotice;
    @JsonProperty("ownershipType")
    private String ownershipType;
    @JsonProperty("numberOfTracksByBMG")
    private Integer numberOfTracksByBMG;
    @JsonProperty("versionTitle")
    private String versionTitle;
    @JsonProperty("parentalAdvisoryLyricsIndicator")
    private String parentalAdvisoryLyricsIndicator;
    @JsonProperty("copyrightOwner")
    private String copyrightOwner;
    @JsonProperty("artworkOwner")
    private String artworkOwner;
    @JsonProperty("isPackageOnly")
    private Boolean isPackageOnly;
    @JsonProperty("hasSyncRights")
    private Boolean hasSyncRights;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("releaseDate")
    private String releaseDate;
    @JsonProperty("entityStatus")
    private String entityStatus;
    @JsonProperty("hasPricePointsBudget")
    private Boolean hasPricePointsBudget;
    @JsonProperty("site")
    private Site site;
    @JsonProperty("isRemaster")
    private Boolean isRemaster;
    @JsonProperty("productCode")
    private String productCode;
    @JsonProperty("hasPricePointsMid")
    private Boolean hasPricePointsMid;
    @JsonProperty("hasBooklet")
    private Boolean hasBooklet;
    @JsonProperty("isPromo")
    private Boolean isPromo;
    @JsonProperty("duration")
    private Integer duration;
    @JsonProperty("productTitle")
    private String productTitle;
    @JsonProperty("parentalAdvisoryArtworkIndicator")
    private String parentalAdvisoryArtworkIndicator;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The genre
     */
    @JsonProperty("genre")
    public Genre getGenre() {
        return genre;
    }

    /**
     * 
     * @param genre
     *     The genre
     */
    @JsonProperty("genre")
    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    /**
     * 
     * @return
     *     The copyrightYear
     */
    @JsonProperty("copyrightYear")
    public Integer getCopyrightYear() {
        return copyrightYear;
    }

    /**
     * 
     * @param copyrightYear
     *     The copyrightYear
     */
    @JsonProperty("copyrightYear")
    public void setCopyrightYear(Integer copyrightYear) {
        this.copyrightYear = copyrightYear;
    }

    /**
     * 
     * @return
     *     The pricePointMidEditable
     */
    @JsonProperty("pricePointMidEditable")
    public Boolean getPricePointMidEditable() {
        return pricePointMidEditable;
    }

    /**
     * 
     * @param pricePointMidEditable
     *     The pricePointMidEditable
     */
    @JsonProperty("pricePointMidEditable")
    public void setPricePointMidEditable(Boolean pricePointMidEditable) {
        this.pricePointMidEditable = pricePointMidEditable;
    }

    /**
     * 
     * @return
     *     The hasContractualDigitalRights
     */
    @JsonProperty("hasContractualDigitalRights")
    public Boolean getHasContractualDigitalRights() {
        return hasContractualDigitalRights;
    }

    /**
     * 
     * @param hasContractualDigitalRights
     *     The hasContractualDigitalRights
     */
    @JsonProperty("hasContractualDigitalRights")
    public void setHasContractualDigitalRights(Boolean hasContractualDigitalRights) {
        this.hasContractualDigitalRights = hasContractualDigitalRights;
    }

    /**
     * 
     * @return
     *     The hasStreamingRights
     */
    @JsonProperty("hasStreamingRights")
    public Boolean getHasStreamingRights() {
        return hasStreamingRights;
    }

    /**
     * 
     * @param hasStreamingRights
     *     The hasStreamingRights
     */
    @JsonProperty("hasStreamingRights")
    public void setHasStreamingRights(Boolean hasStreamingRights) {
        this.hasStreamingRights = hasStreamingRights;
    }

    /**
     * 
     * @return
     *     The isPackage
     */
    @JsonProperty("isPackage")
    public Boolean getIsPackage() {
        return isPackage;
    }

    /**
     * 
     * @param isPackage
     *     The isPackage
     */
    @JsonProperty("isPackage")
    public void setIsPackage(Boolean isPackage) {
        this.isPackage = isPackage;
    }

    /**
     * 
     * @return
     *     The hasPrimaryArtistAgreement
     */
    @JsonProperty("hasPrimaryArtistAgreement")
    public Boolean getHasPrimaryArtistAgreement() {
        return hasPrimaryArtistAgreement;
    }

    /**
     * 
     * @param hasPrimaryArtistAgreement
     *     The hasPrimaryArtistAgreement
     */
    @JsonProperty("hasPrimaryArtistAgreement")
    public void setHasPrimaryArtistAgreement(Boolean hasPrimaryArtistAgreement) {
        this.hasPrimaryArtistAgreement = hasPrimaryArtistAgreement;
    }

    /**
     * 
     * @return
     *     The isSoundtrack
     */
    @JsonProperty("isSoundtrack")
    public Boolean getIsSoundtrack() {
        return isSoundtrack;
    }

    /**
     * 
     * @param isSoundtrack
     *     The isSoundtrack
     */
    @JsonProperty("isSoundtrack")
    public void setIsSoundtrack(Boolean isSoundtrack) {
        this.isSoundtrack = isSoundtrack;
    }

    /**
     * 
     * @return
     *     The isPartial
     */
    @JsonProperty("isPartial")
    public Boolean getIsPartial() {
        return isPartial;
    }

    /**
     * 
     * @param isPartial
     *     The isPartial
     */
    @JsonProperty("isPartial")
    public void setIsPartial(Boolean isPartial) {
        this.isPartial = isPartial;
    }

    /**
     * 
     * @return
     *     The numberOfTracks
     */
    @JsonProperty("numberOfTracks")
    public Integer getNumberOfTracks() {
        return numberOfTracks;
    }

    /**
     * 
     * @param numberOfTracks
     *     The numberOfTracks
     */
    @JsonProperty("numberOfTracks")
    public void setNumberOfTracks(Integer numberOfTracks) {
        this.numberOfTracks = numberOfTracks;
    }

    /**
     * 
     * @return
     *     The pricePointBudgetEditable
     */
    @JsonProperty("pricePointBudgetEditable")
    public Boolean getPricePointBudgetEditable() {
        return pricePointBudgetEditable;
    }

    /**
     * 
     * @param pricePointBudgetEditable
     *     The pricePointBudgetEditable
     */
    @JsonProperty("pricePointBudgetEditable")
    public void setPricePointBudgetEditable(Boolean pricePointBudgetEditable) {
        this.pricePointBudgetEditable = pricePointBudgetEditable;
    }

    /**
     * 
     * @return
     *     The isCompilation
     */
    @JsonProperty("isCompilation")
    public Boolean getIsCompilation() {
        return isCompilation;
    }

    /**
     * 
     * @param isCompilation
     *     The isCompilation
     */
    @JsonProperty("isCompilation")
    public void setIsCompilation(Boolean isCompilation) {
        this.isCompilation = isCompilation;
    }

    /**
     * 
     * @return
     *     The hasManufacturingRights
     */
    @JsonProperty("hasManufacturingRights")
    public Boolean getHasManufacturingRights() {
        return hasManufacturingRights;
    }

    /**
     * 
     * @param hasManufacturingRights
     *     The hasManufacturingRights
     */
    @JsonProperty("hasManufacturingRights")
    public void setHasManufacturingRights(Boolean hasManufacturingRights) {
        this.hasManufacturingRights = hasManufacturingRights;
    }

    /**
     * 
     * @return
     *     The repertoireOwnerCompany
     */
    @JsonProperty("repertoireOwnerCompany")
    public RepertoireOwnerCompany getRepertoireOwnerCompany() {
        return repertoireOwnerCompany;
    }

    /**
     * 
     * @param repertoireOwnerCompany
     *     The repertoireOwnerCompany
     */
    @JsonProperty("repertoireOwnerCompany")
    public void setRepertoireOwnerCompany(RepertoireOwnerCompany repertoireOwnerCompany) {
        this.repertoireOwnerCompany = repertoireOwnerCompany;
    }

    /**
     * 
     * @return
     *     The created
     */
    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    /**
     * 
     * @param created
     *     The created
     */
    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * 
     * @return
     *     The updated
     */
    @JsonProperty("updated")
    public String getUpdated() {
        return updated;
    }

    /**
     * 
     * @param updated
     *     The updated
     */
    @JsonProperty("updated")
    public void setUpdated(String updated) {
        this.updated = updated;
    }

    /**
     * 
     * @return
     *     The artworkYear
     */
    @JsonProperty("artworkYear")
    public Integer getArtworkYear() {
        return artworkYear;
    }

    /**
     * 
     * @param artworkYear
     *     The artworkYear
     */
    @JsonProperty("artworkYear")
    public void setArtworkYear(Integer artworkYear) {
        this.artworkYear = artworkYear;
    }

    /**
     * 
     * @return
     *     The hasPricePointsFull
     */
    @JsonProperty("hasPricePointsFull")
    public Boolean getHasPricePointsFull() {
        return hasPricePointsFull;
    }

    /**
     * 
     * @param hasPricePointsFull
     *     The hasPricePointsFull
     */
    @JsonProperty("hasPricePointsFull")
    public void setHasPricePointsFull(Boolean hasPricePointsFull) {
        this.hasPricePointsFull = hasPricePointsFull;
    }

    /**
     * 
     * @return
     *     The altProductCodes
     */
    @JsonProperty("altProductCodes")
    public List<Object> getAltProductCodes() {
        return altProductCodes;
    }

    /**
     * 
     * @param altProductCodes
     *     The altProductCodes
     */
    @JsonProperty("altProductCodes")
    public void setAltProductCodes(List<Object> altProductCodes) {
        this.altProductCodes = altProductCodes;
    }

    /**
     * 
     * @return
     *     The copyrightNotice
     */
    @JsonProperty("copyrightNotice")
    public String getCopyrightNotice() {
        return copyrightNotice;
    }

    /**
     * 
     * @param copyrightNotice
     *     The copyrightNotice
     */
    @JsonProperty("copyrightNotice")
    public void setCopyrightNotice(String copyrightNotice) {
        this.copyrightNotice = copyrightNotice;
    }

    /**
     * 
     * @return
     *     The isLicenced
     */
    @JsonProperty("isLicenced")
    public Boolean getIsLicenced() {
        return isLicenced;
    }

    /**
     * 
     * @param isLicenced
     *     The isLicenced
     */
    @JsonProperty("isLicenced")
    public void setIsLicenced(Boolean isLicenced) {
        this.isLicenced = isLicenced;
    }

    /**
     * 
     * @return
     *     The barcode
     */
    @JsonProperty("barcode")
    public String getBarcode() {
        return barcode;
    }

    /**
     * 
     * @param barcode
     *     The barcode
     */
    @JsonProperty("barcode")
    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    /**
     * 
     * @return
     *     The label
     */
    @JsonProperty("label")
    public Label getLabel() {
        return label;
    }

    /**
     * 
     * @param label
     *     The label
     */
    @JsonProperty("label")
    public void setLabel(Label label) {
        this.label = label;
    }

    /**
     * 
     * @return
     *     The format
     */
    @JsonProperty("format")
    public Format getFormat() {
        return format;
    }

    /**
     * 
     * @param format
     *     The format
     */
    @JsonProperty("format")
    public void setFormat(Format format) {
        this.format = format;
    }

    /**
     * 
     * @return
     *     The metadataLanguage
     */
    @JsonProperty("metadataLanguage")
    public MetadataLanguage getMetadataLanguage() {
        return metadataLanguage;
    }

    /**
     * 
     * @param metadataLanguage
     *     The metadataLanguage
     */
    @JsonProperty("metadataLanguage")
    public void setMetadataLanguage(MetadataLanguage metadataLanguage) {
        this.metadataLanguage = metadataLanguage;
    }

    /**
     * 
     * @return
     *     The hasContractualPhysicalRights
     */
    @JsonProperty("hasContractualPhysicalRights")
    public Boolean getHasContractualPhysicalRights() {
        return hasContractualPhysicalRights;
    }

    /**
     * 
     * @param hasContractualPhysicalRights
     *     The hasContractualPhysicalRights
     */
    @JsonProperty("hasContractualPhysicalRights")
    public void setHasContractualPhysicalRights(Boolean hasContractualPhysicalRights) {
        this.hasContractualPhysicalRights = hasContractualPhysicalRights;
    }

    /**
     * 
     * @return
     *     The artistName
     */
    @JsonProperty("artistName")
    public String getArtistName() {
        return artistName;
    }

    /**
     * 
     * @param artistName
     *     The artistName
     */
    @JsonProperty("artistName")
    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    /**
     * 
     * @return
     *     The artworkNotice
     */
    @JsonProperty("artworkNotice")
    public String getArtworkNotice() {
        return artworkNotice;
    }

    /**
     * 
     * @param artworkNotice
     *     The artworkNotice
     */
    @JsonProperty("artworkNotice")
    public void setArtworkNotice(String artworkNotice) {
        this.artworkNotice = artworkNotice;
    }

    /**
     * 
     * @return
     *     The ownershipType
     */
    @JsonProperty("ownershipType")
    public String getOwnershipType() {
        return ownershipType;
    }

    /**
     * 
     * @param ownershipType
     *     The ownershipType
     */
    @JsonProperty("ownershipType")
    public void setOwnershipType(String ownershipType) {
        this.ownershipType = ownershipType;
    }

    /**
     * 
     * @return
     *     The numberOfTracksByBMG
     */
    @JsonProperty("numberOfTracksByBMG")
    public Integer getNumberOfTracksByBMG() {
        return numberOfTracksByBMG;
    }

    /**
     * 
     * @param numberOfTracksByBMG
     *     The numberOfTracksByBMG
     */
    @JsonProperty("numberOfTracksByBMG")
    public void setNumberOfTracksByBMG(Integer numberOfTracksByBMG) {
        this.numberOfTracksByBMG = numberOfTracksByBMG;
    }

    /**
     * 
     * @return
     *     The versionTitle
     */
    @JsonProperty("versionTitle")
    public String getVersionTitle() {
        return versionTitle;
    }

    /**
     * 
     * @param versionTitle
     *     The versionTitle
     */
    @JsonProperty("versionTitle")
    public void setVersionTitle(String versionTitle) {
        this.versionTitle = versionTitle;
    }

    /**
     * 
     * @return
     *     The parentalAdvisoryLyricsIndicator
     */
    @JsonProperty("parentalAdvisoryLyricsIndicator")
    public String getParentalAdvisoryLyricsIndicator() {
        return parentalAdvisoryLyricsIndicator;
    }

    /**
     * 
     * @param parentalAdvisoryLyricsIndicator
     *     The parentalAdvisoryLyricsIndicator
     */
    @JsonProperty("parentalAdvisoryLyricsIndicator")
    public void setParentalAdvisoryLyricsIndicator(String parentalAdvisoryLyricsIndicator) {
        this.parentalAdvisoryLyricsIndicator = parentalAdvisoryLyricsIndicator;
    }

    /**
     * 
     * @return
     *     The copyrightOwner
     */
    @JsonProperty("copyrightOwner")
    public String getCopyrightOwner() {
        return copyrightOwner;
    }

    /**
     * 
     * @param copyrightOwner
     *     The copyrightOwner
     */
    @JsonProperty("copyrightOwner")
    public void setCopyrightOwner(String copyrightOwner) {
        this.copyrightOwner = copyrightOwner;
    }

    /**
     * 
     * @return
     *     The artworkOwner
     */
    @JsonProperty("artworkOwner")
    public String getArtworkOwner() {
        return artworkOwner;
    }

    /**
     * 
     * @param artworkOwner
     *     The artworkOwner
     */
    @JsonProperty("artworkOwner")
    public void setArtworkOwner(String artworkOwner) {
        this.artworkOwner = artworkOwner;
    }

    /**
     * 
     * @return
     *     The isPackageOnly
     */
    @JsonProperty("isPackageOnly")
    public Boolean getIsPackageOnly() {
        return isPackageOnly;
    }

    /**
     * 
     * @param isPackageOnly
     *     The isPackageOnly
     */
    @JsonProperty("isPackageOnly")
    public void setIsPackageOnly(Boolean isPackageOnly) {
        this.isPackageOnly = isPackageOnly;
    }

    /**
     * 
     * @return
     *     The hasSyncRights
     */
    @JsonProperty("hasSyncRights")
    public Boolean getHasSyncRights() {
        return hasSyncRights;
    }

    /**
     * 
     * @param hasSyncRights
     *     The hasSyncRights
     */
    @JsonProperty("hasSyncRights")
    public void setHasSyncRights(Boolean hasSyncRights) {
        this.hasSyncRights = hasSyncRights;
    }

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * 
     * @param title
     *     The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 
     * @return
     *     The releaseDate
     */
    @JsonProperty("releaseDate")
    public String getReleaseDate() {
        return releaseDate;
    }

    /**
     * 
     * @param releaseDate
     *     The releaseDate
     */
    @JsonProperty("releaseDate")
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     * 
     * @return
     *     The entityStatus
     */
    @JsonProperty("entityStatus")
    public String getEntityStatus() {
        return entityStatus;
    }

    /**
     * 
     * @param entityStatus
     *     The entityStatus
     */
    @JsonProperty("entityStatus")
    public void setEntityStatus(String entityStatus) {
        this.entityStatus = entityStatus;
    }

    /**
     * 
     * @return
     *     The hasPricePointsBudget
     */
    @JsonProperty("hasPricePointsBudget")
    public Boolean getHasPricePointsBudget() {
        return hasPricePointsBudget;
    }

    /**
     * 
     * @param hasPricePointsBudget
     *     The hasPricePointsBudget
     */
    @JsonProperty("hasPricePointsBudget")
    public void setHasPricePointsBudget(Boolean hasPricePointsBudget) {
        this.hasPricePointsBudget = hasPricePointsBudget;
    }

    /**
     * 
     * @return
     *     The site
     */
    @JsonProperty("site")
    public Site getSite() {
        return site;
    }

    /**
     * 
     * @param site
     *     The site
     */
    @JsonProperty("site")
    public void setSite(Site site) {
        this.site = site;
    }

    /**
     * 
     * @return
     *     The isRemaster
     */
    @JsonProperty("isRemaster")
    public Boolean getIsRemaster() {
        return isRemaster;
    }

    /**
     * 
     * @param isRemaster
     *     The isRemaster
     */
    @JsonProperty("isRemaster")
    public void setIsRemaster(Boolean isRemaster) {
        this.isRemaster = isRemaster;
    }

    /**
     * 
     * @return
     *     The productCode
     */
    @JsonProperty("productCode")
    public String getProductCode() {
        return productCode;
    }

    /**
     * 
     * @param productCode
     *     The productCode
     */
    @JsonProperty("productCode")
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * 
     * @return
     *     The hasPricePointsMid
     */
    @JsonProperty("hasPricePointsMid")
    public Boolean getHasPricePointsMid() {
        return hasPricePointsMid;
    }

    /**
     * 
     * @param hasPricePointsMid
     *     The hasPricePointsMid
     */
    @JsonProperty("hasPricePointsMid")
    public void setHasPricePointsMid(Boolean hasPricePointsMid) {
        this.hasPricePointsMid = hasPricePointsMid;
    }

    /**
     * 
     * @return
     *     The hasBooklet
     */
    @JsonProperty("hasBooklet")
    public Boolean getHasBooklet() {
        return hasBooklet;
    }

    /**
     * 
     * @param hasBooklet
     *     The hasBooklet
     */
    @JsonProperty("hasBooklet")
    public void setHasBooklet(Boolean hasBooklet) {
        this.hasBooklet = hasBooklet;
    }

    /**
     * 
     * @return
     *     The isPromo
     */
    @JsonProperty("isPromo")
    public Boolean getIsPromo() {
        return isPromo;
    }

    /**
     * 
     * @param isPromo
     *     The isPromo
     */
    @JsonProperty("isPromo")
    public void setIsPromo(Boolean isPromo) {
        this.isPromo = isPromo;
    }

    /**
     * 
     * @return
     *     The duration
     */
    @JsonProperty("duration")
    public Integer getDuration() {
        return duration;
    }

    /**
     * 
     * @param duration
     *     The duration
     */
    @JsonProperty("duration")
    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    /**
     * 
     * @return
     *     The productTitle
     */
    @JsonProperty("productTitle")
    public String getProductTitle() {
        return productTitle;
    }

    /**
     * 
     * @param productTitle
     *     The productTitle
     */
    @JsonProperty("productTitle")
    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    /**
     * 
     * @return
     *     The parentalAdvisoryArtworkIndicator
     */
    @JsonProperty("parentalAdvisoryArtworkIndicator")
    public String getParentalAdvisoryArtworkIndicator() {
        return parentalAdvisoryArtworkIndicator;
    }

    /**
     * 
     * @param parentalAdvisoryArtworkIndicator
     *     The parentalAdvisoryArtworkIndicator
     */
    @JsonProperty("parentalAdvisoryArtworkIndicator")
    public void setParentalAdvisoryArtworkIndicator(String parentalAdvisoryArtworkIndicator) {
        this.parentalAdvisoryArtworkIndicator = parentalAdvisoryArtworkIndicator;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
