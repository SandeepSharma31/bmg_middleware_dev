package com.bmg.esb.capture2rm.beans;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReadProductApprovalCSV {
	
	
	private static final Logger ReadProductApprovalCSV = LoggerFactory
			.getLogger(ReadProductApprovalCSV.class);
	
	public void readApprovalCSV(Exchange exchange){
		
		try {
			
			  List<ProductApprovalDataBean> prodapprovalDatalist = new ArrayList<ProductApprovalDataBean>();
			  
				String productfileName = (String) exchange.getIn().getHeader("ProductApprovalfileName");
				String productfilepath = (String) exchange.getIn().getHeader("ProductfilePath");
				ReadProductApprovalCSV.info("In the readCSVFile try $$$$$$$ productfileName"+productfileName);
				String filepath=productfilepath+productfileName;
				ProductApprovalDataBean approvaldataBean = new ProductApprovalDataBean();
				BufferedReader br = new BufferedReader(new FileReader(
						filepath));
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

				String line = br.readLine();
				
				while ((line = br.readLine()) != null && !line.isEmpty()) {
					ReadProductApprovalCSV.info("Inside while ********");
				String[] values = line.split(",");
				
				approvaldataBean = new ProductApprovalDataBean();
				approvaldataBean.setBatch_id(values[0]);
				approvaldataBean.setADOADD(values[1]);
				approvaldataBean.setIM_PRODUCTAPPROVALS_SEQ(values[2]);
				approvaldataBean.setERRORCODE(values[3]);
				approvaldataBean.setERRORCODEI(values[4]);
				approvaldataBean.setAIMPORTBLANKS(values[5]);
				approvaldataBean.setaProductSite(values[6]);
				approvaldataBean.setaProductCode(values[7]);
				approvaldataBean.setaApproved(values[8]);
				approvaldataBean.setaUpdateICR(values[9]);
				
				prodapprovalDatalist.add(approvaldataBean);
				
				ReadProductApprovalCSV.info("In the readCSVFile******** dataBean"+approvaldataBean);
				
				/*exchange.setProperty("firstTimeflag", "N");*/
				
				ReadProductApprovalCSV.info("whileloop filepth"+exchange.getProperty("filepath"));
				ReadProductApprovalCSV.info("whileloop flag set insideloop"+exchange.getProperty("firstTimeflag"));
				}
				exchange.setProperty("ProductapprovalDatalist", prodapprovalDatalist);
				ReadProductApprovalCSV.info("whileloop flag set"+exchange.getProperty("firstTimeflag"));
				prodapprovalDatalist=null;

			} catch (Exception e) {
				e.printStackTrace();
				ReadProductApprovalCSV.info("inside catch block of readCSv::::"+e.toString());
			}
			
		}
	
	public void checkForDuplicateAcodeValues(Exchange exchange){
		try
		{
		List<ProductApprovalDataBean> prodapprovalDatalist = (List<ProductApprovalDataBean>) exchange.getProperty("ProductapprovalDatalist");
		List<DataBean> prodDatalist = (List<DataBean>) exchange.getProperty("ProductdataList");
		List<ProductApprovalDataBean> newapprovalDatalist = new ArrayList<ProductApprovalDataBean>();
		//String prod_acode = ProductdataList.get(i).getaCode();
		String prod_acode = prodDatalist.get(0).getaCode();
		for(int i=0;i<prodapprovalDatalist.size();i++){
		   if(!prod_acode.equals(prodapprovalDatalist.get(i).getaProductCode())){
			   
			   newapprovalDatalist.add(prodapprovalDatalist.get(i));
		   }
			
			
		}
		
		exchange.getIn().setBody(newapprovalDatalist);
		}catch(Exception e){
			ReadProductApprovalCSV.info("catch block inside checkForDuplicateAcodeValues::::"+e.toString());
		}
	}

}
