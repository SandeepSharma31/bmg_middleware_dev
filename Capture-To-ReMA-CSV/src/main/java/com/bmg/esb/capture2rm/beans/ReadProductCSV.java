package com.bmg.esb.capture2rm.beans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReadProductCSV {

	private static final Logger ReadProductCSV = LoggerFactory
			.getLogger(ReadProductCSV.class);

	public void readCSVFile(Exchange exchange) {

		ReadProductCSV.info("In the readCSVFile&&&&&&&&&&&&&&&&&&&&&&&");

		// List<DataBean> prodDatalist = new ArrayList<DataBean>();
		try {

			List<DataBean> prodDatalist = new ArrayList<DataBean>();

			String productfileName = (String) exchange.getIn().getHeader(
					"ProductfileName");
			String productfilepath = (String) exchange.getIn().getHeader(
					"ProductfilePath");
			ReadProductCSV
					.info("In the readCSVFile try $$$$$$$ productfileName"
							+ productfileName);
			String filepath = productfilepath + productfileName;
			DataBean dataBean = new DataBean();
			BufferedReader br = new BufferedReader(new FileReader(filepath));
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

			String line = br.readLine();

			while ((line = br.readLine()) != null && !line.isEmpty()) {
				ReadProductCSV.info("Inside while ********");
				String[] values = line.split(",");

				dataBean = new DataBean();
				dataBean.setBatch_id(values[0]);
				dataBean.setADOADD(values[1]);
				dataBean.setIM_PRODUCT_SEQ(values[2]);
				dataBean.setERRORCODE(values[3]);
				dataBean.setERRORCODEI(values[4]);
				dataBean.setAIMPORTBLANKS(values[5]);
				dataBean.setaSite(values[6]);
				dataBean.setaCode(values[7]);
				dataBean.setaBarCode(values[8]);
				dataBean.setaTitle(values[9]);
				dataBean.setaArtist(values[10]);
				dataBean.setaCompany(values[11]);
				if (values[12] != null && !values[12].equals("")) {
					dataBean.setaComponents(Integer.parseInt(values[12]));
				}
				if (values[13] != null && !values[13].equals("")) {
					dataBean.setaSides(Integer.parseInt(values[13]));
				}
				dataBean.setaTracks(values[14]);
				dataBean.setaPriceCategory(values[15]);
				dataBean.setaConfiguration(values[16]);
				if (values[17] != null && !values[17].equals("")) {
					dataBean.setaTotalPlayingTime(Integer.parseInt(values[17]));
				}
				if (values[18] != null && !values[18].equals("")) {
					dataBean.setaReleaseDate(df.parse(values[18]));
				}
				if (values[19] != null && !values[19].equals("")) {
					dataBean.setaItReleaseDate(df.parse(values[19]));
				}
				if (values[20] != null && !values[20].equals("")) {
					dataBean.setaRereleaseDate(df.parse(values[20]));
				}
				if (values[21] != null && !values[21].equals("")) {
					dataBean.setaDeletionDate(df.parse(values[21]));
				}
				dataBean.setaTVAdvertised(values[22]);
				dataBean.setaPayCopyright(values[23]);

				if (values[24] != null && !values[24].equals("")) {
					dataBean.setaCopyrightProductType(Integer
							.parseInt(values[24]));
				}
				dataBean.setaCopyrightCapType(values[25]);
				if (values[26] != null && !values[26].equals("")) {
					dataBean.setaCopyrightCapSongs(Integer.parseInt(values[26]));
				}
				if (values[27] != null && !values[27].equals("")) {
					dataBean.setaCopyrightCapValue(Float.parseFloat(values[27]));
				}
				if (values[28] != null && !values[28].equals("")) {
					dataBean.setaCopyrightCapPerc(Float.parseFloat(values[28]));
				}
				if (values[29] != null && !values[29].equals("")) {
					dataBean.setaCopyrightCapFreezeDate(df.parse(values[29]));
				}
				dataBean.setaSociety(values[30]);
				dataBean.setaSocietyNumber(values[31]);
				if (values[32] != null && !values[32].equals("")) {
					dataBean.setaTVAdFrom(df.parse(values[32]));
				}
				if (values[33] != null && !values[33].equals("")) {
					dataBean.setaTVAdTo(df.parse(values[33]));
				}
				dataBean.setaInternal(values[34]);
				if (values[35] != null && !values[35].equals("")) {
					dataBean.setaPriceLevel(Integer.parseInt(values[35]));
				}
				dataBean.setaTerritory(values[36]);
				dataBean.setaLabel(values[37]);
				dataBean.setaHoldReserves(values[38]);
				if (values[39] != null && !values[39].equals("")) {
					dataBean.setaCopyrightTracks(Integer.parseInt(values[39]));
				}
				dataBean.setaCompilation(values[40]);
				dataBean.setaDigitalDownload(values[41]);
				dataBean.setaAllowFree(values[42]);
				dataBean.setaRoyaltyInfoComplete(values[43]);
				dataBean.setaPromo(values[44]);
				dataBean.setaLanguage(values[45]);
				dataBean.setaType(values[46]);
				dataBean.setaRecType(values[47]);
				dataBean.setaTech(values[48]);
				dataBean.setaStyle(values[49]);
				dataBean.setaVocal(values[50]);
				dataBean.setaPayCSI(values[51]);
				dataBean.setaPaySOCAN(values[52]);
				dataBean.setaProjectCode(values[53]);
				dataBean.setaGridCode(values[54]);
				dataBean.setaExplicitContent(values[55]);
				dataBean.setaProductClearedTerrName(values[56]);
				dataBean.setaProductExcludedTerrName(values[57]);
				dataBean.setaPYear(values[58]);
				dataBean.setaPNote(values[59]);
				dataBean.setaCYear(values[60]);
				dataBean.setaCNote(values[61]);
				dataBean.setaCatCoEnabled(values[62]);
				dataBean.setaCatCoResend(values[63]);
				dataBean.setaAutoCreateAKACodes(values[64]);
				dataBean.setaNotes(values[65]);
				dataBean.setaExternalCode(values[66]);
				dataBean.setaUpdateUserCode(values[67]);
				dataBean.setaUpdateTerrBarcodes(values[68]);
				dataBean.setaExternalLabelCode(values[69]);
				dataBean.setaGenreGroup1(values[70]);
				dataBean.setaGenre1(values[71]);
				dataBean.setaGenreGroup2(values[72]);
				dataBean.setaGenre2(values[73]);
				prodDatalist.add(dataBean);

				ReadProductCSV.debug("In the readCSVFile******** dataBean"
						+ dataBean);
				exchange.setProperty("firstTimeflag", "N");

				ReadProductCSV.debug("whileloop filepth"
						+ exchange.getProperty("filepath"));
				ReadProductCSV.debug("whileloop flag set insideloop"
						+ exchange.getProperty("firstTimeflag"));
			}
			exchange.setProperty("ProductdataList", prodDatalist);

			ReadProductCSV.info("whileloop flag set"
					+ exchange.getProperty("firstTimeflag"));
			prodDatalist = null;
			br.close();

		} catch (Exception e) {
			e.printStackTrace();
			ReadProductCSV.debug("inside catch block of readCSv::::"
					+ e.toString());
		}

	}

	public void readProductApprovalFile(Exchange exchange) {

		try {
			ReadProductCSV
			.info("readProductApprovalFile ==0==>");
			List<ProductApprovalDataBean> prodapprovalDatalist = new ArrayList<ProductApprovalDataBean>();

			String productApprovalfileName = (String) exchange.getIn()
					.getHeader("ProductApprovalfileName");
			String productfilepath = (String) exchange.getIn().getHeader(
					"ProductfilePath");
			ReadProductCSV
					.info("readProductApprovalFile ==1==>"
							+ productApprovalfileName + productfilepath);
			String filepath = productfilepath + productApprovalfileName;
			ReadProductCSV
					.info("readProductApprovalFile ==2==>"
							+ filepath);
			ProductApprovalDataBean approvaldataBean = new ProductApprovalDataBean();
			File f = new File(filepath);
			if(f.exists()) { 
			BufferedReader br = new BufferedReader(new FileReader(filepath));
			String line = br.readLine();
			ReadProductCSV
			.info("readProductApprovalFile ==3==>");
			while ((line = br.readLine()) != null && !line.isEmpty()) {
				ReadProductCSV.info("readProductApprovalFile ==4==>");
				String[] values = line.split(",");

				approvaldataBean = new ProductApprovalDataBean();
				approvaldataBean.setBatch_id(values[0]);
				approvaldataBean.setADOADD(values[1]);
				approvaldataBean.setIM_PRODUCTAPPROVALS_SEQ(values[2]);
				approvaldataBean.setERRORCODE(values[3]);
				approvaldataBean.setERRORCODEI(values[4]);
				approvaldataBean.setAIMPORTBLANKS(values[5]);
				approvaldataBean.setaProductSite(values[6]);
				approvaldataBean.setaProductCode(values[7]);
				approvaldataBean.setaApproved(values[8]);
				approvaldataBean.setaUpdateICR(values[9]);

				prodapprovalDatalist.add(approvaldataBean);

				ReadProductCSV.info("readProductApprovalFile ==5==>"
						+ approvaldataBean);

				/* exchange.setProperty("firstTimeflag", "N"); */

				/*
				 * ReadProductCSV.info("whileloop filepth" +
				 * exchange.getProperty("filepath"));
				 */
				/*
				 * ReadProductCSV.info("whileloop flag set insideloop" +
				 * exchange.getProperty("firstTimeflag"));
				 */
			}
			br.close();
		}
			exchange.setProperty("ProductapprovalDatalist",
					prodapprovalDatalist);
			ReadProductCSV.info("readProductApprovalFile ==6==>prodapprovalDatalist"
					+ exchange.getProperty("ProductapprovalDatalist"));
			prodapprovalDatalist = null;
			

		} catch (Exception e) {
			e.printStackTrace();
			ReadProductCSV.info("inside catch block of readProductApprovalFile "
					+ e.toString());
		}

	}

	public void checkDuplicateForProduct(Exchange exchange) {
		ReadProductCSV.info("In the checkForDuplicate &&&&&&&&&&&&&&&&&&&&&&&");

		try {

			List<DataBean> prodDatalist = (List<DataBean>) exchange
					.getProperty("ProductdataList");// file data
			List<DataBean> newProdDatalist = new ArrayList<DataBean>();// new
																		// list
			String productfileName = (String) exchange.getIn().getHeader(
					"ProductfileName");
			String productfilepath = (String) exchange.getIn().getHeader(
					"ProductfilePath");
			String filepath = productfilepath + productfileName;
			ReadProductCSV.debug("file contents in check for duplicate::::"
					+ prodDatalist);
			Map<String, Object> productData = new LinkedHashMap<String, Object>();
			ReadProductCSV.debug("queuebody returned from route:::"
					+ exchange.getProperty("NewMessagebody"));// new message
			productData = (Map<String, Object>) exchange
					.getProperty("NewMessagebody");
			ReadProductCSV.info("productData &&&&&&&&&&&&&&&&&&&&&&&"
					+ productData);

			ReadProductCSV
					.debug("Message in checkforDuplicate productData============"
							+ productData + "size" + prodDatalist.size());

			String externalProdID = (String) productData.get("aExternalCode");
			ReadProductCSV.info("external code from queue body :::::"
					+ externalProdID);
			String prod_acode = null;

			for (int i = 0; i < prodDatalist.size(); i++) {
				ReadProductCSV
						.info("Message in checkforDuplicate for loop============"
								+ prodDatalist.size() + prodDatalist.get(i).getaExternalCode());
				if (!externalProdID.equals(prodDatalist.get(i)
						.getaExternalCode())) {
					newProdDatalist.add(prodDatalist.get(i));

					ReadProductCSV
							.info("Message in checkforDuplicate if=== condition============"
									+ newProdDatalist);

				} else {

					ReadProductCSV.info("==else ===" + externalProdID
							+ "======from file==="
							+ prodDatalist.get(i).getaExternalCode()+"======"+prodDatalist.get(i)
							.getaCode());

					checkDuplicateForAcodeValues(exchange, prodDatalist.get(i)
							.getaCode());

					/*
					 * List<ProductApprovalDataBean> prodapprovalDatalist =
					 * (List<ProductApprovalDataBean>) exchange
					 * .getProperty("ProductapprovalDatalist");
					 * List<ProductApprovalDataBean> newapprovalDatalist = new
					 * ArrayList<ProductApprovalDataBean>(); for (int j = 0; j <
					 * prodDatalist.size(); j++) { prod_acode =
					 * prodDatalist.get(i).getaCode(); for (int k = 0; k <
					 * prodapprovalDatalist.size(); k++) { if
					 * (!prod_acode.equals(prodapprovalDatalist
					 * .get(k).getaProductCode())) {
					 * 
					 * newapprovalDatalist .add(prodapprovalDatalist.get(k)); }
					 * 
					 * }
					 * 
					 * 
					 * } exchange.getIn().setBody(newapprovalDatalist);
					 */

				}

			}
			exchange.getIn().setBody(newProdDatalist);
			ReadProductCSV.info("Message in checkforDuplicate newProdDatalist"
					+ newProdDatalist);

			/*
			 * Path
			 * destPath=Paths.get((String)exchange.getIn().getHeader("destFilePath"
			 * ));
			 * ReadProductCSV.debug("Message in checkforDuplicate destPath "+
			 * destPath);
			 */
			Path sourcePath = Paths.get(
					(String) exchange.getIn().getHeader("ProductfilePath"),
					(String) exchange.getIn().getHeader("ProductfileName"));
			ReadProductCSV
					.debug("Message in checkforDuplicate after sourcePath "
							+ sourcePath);
			// Files.copy(Paths.get(sourcePath, destPath));

			// Files.copy(sourcePath, destPath);
			ReadProductCSV
					.debug("inside Checkduplicate Method before delete fileName::::"
							+ sourcePath);
			Files.deleteIfExists(sourcePath);

			ReadProductCSV.debug("Message in checkforDuplicate deletefile"
					+ sourcePath);

		} catch (Exception e) {
			e.printStackTrace();

			ReadProductCSV.info("inside catch block of duplicateMetod::::"
					+ e.toString());
		}

	}

	public void checkDuplicateForAcodeValues(Exchange exchange,
			String acodeValue) {
		ReadProductCSV.info("Inside checkDuplicateForAcodeValues::1:"+acodeValue);
		try {
			List<ProductApprovalDataBean> prodapprovalDatalist = (List<ProductApprovalDataBean>) exchange
					.getProperty("ProductapprovalDatalist");
			ReadProductCSV.info("Inside checkDuplicateForAcodeValues::2:");
			
			List<ProductApprovalDataBean> newapprovalDatalist = new ArrayList<ProductApprovalDataBean>();
			ReadProductCSV.info("Inside checkDuplicateForAcodeValues::3:");
			
			for (int j = 0; j < prodapprovalDatalist.size(); j++) {
				ReadProductCSV.info("inside for loop::::");

				if (!acodeValue.equals(prodapprovalDatalist.get(j).getaProductCode())) {
					ReadProductCSV.info("Inside if checkDuplicateForAcodeValues:::");
					ReadProductCSV.info("value of aProductCode:::"
							+ prodapprovalDatalist.get(j).getaProductCode());

					newapprovalDatalist.add(prodapprovalDatalist.get(j));
					ReadProductCSV
							.info("Inside checkDuplicateForAcodeValues newapprovalDatalist:::"
									+ newapprovalDatalist);

				}

				else {
					ReadProductCSV
							.info("==checkDuplicateForAcodeValues else ==="
									+ acodeValue
									+ "======from file==="
									+ prodapprovalDatalist.get(j)
											.getaProductCode());
				}

			}

			/*exchange.getIn().setBody(newapprovalDatalist);*/
			exchange.setProperty("newProductapprovalData",newapprovalDatalist);
			ReadProductCSV
					.info("checkDuplicateForAcodeValues after seeting into property:::"
							+ exchange.getProperty("newProductapprovalData"));

			Path sourcePath = Paths.get(
					(String) exchange.getIn().getHeader(
							"ProductfilePath"), (String) exchange
							.getIn().getHeader("ProductApprovalfileName"));
			ReadProductCSV
					.info("Message in checkDuplicateForAcodeValues after sourcePath "
							+ sourcePath);
			
			Files.deleteIfExists(sourcePath);

			ReadProductCSV.info("Message in checkDuplicateForAcodeValues deletefile"
					+ sourcePath);
		} catch (Exception e) {
			ReadProductCSV
					.info("catch block inside checkDuplicateForAcodeValues::::"
							+ e.toString());
		}
	}

}
