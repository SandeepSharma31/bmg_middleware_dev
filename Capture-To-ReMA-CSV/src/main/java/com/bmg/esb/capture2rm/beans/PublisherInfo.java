package com.bmg.esb.capture2rm.beans;

//import org.codehaus.jackson.map.annotate.JsonSerialize;

//@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)


public class PublisherInfo {

	
	private String type = "";
	private String changeindicator = "";
	private Data data;
	private String batchId="";

	public class Data {
	private String complianceStatus="";
	private boolean isImportBlanks = false;
	private int id=0;
	private String code="";
	private String name="";
	private String currency = "";
	private String exDate = "";
	private String taxId = "";
	private String controlled = "";
	private String agency = "";
	private String crossMechCode = "";
	private String crossPrintCode = "";
	private String negCycle = "";
	private String priceType = "";
	private String unitType = "";
	private String minCheque = "";
	private String holdRes = "";
	private String holdPay = "";
	private String payVAT = "";
	private String freeGoods = "";
	private String paymentFreq = "";
	private String contactName = "";
	private String street1 = "";
	private String street2 = "";
	private String town = "";
	private String county = "";
	private String postcode = "";
	private String country = "";
	private String phone = "";
	private String fax = "";
	private String email = "";
	private String emailCC = "";
	private String addrTypeCode = "";
	private String addrNotes = "";
	private String sendStatement = "";
	private String accountingSystem = "";
	private String cMRRAPublCode = "";
	private Site site;
	private String created="";
	private String modified="";
	private String entityStatus;

		

	public boolean getIsImportBlanks() {
		return isImportBlanks;
	}

	public void setIsImportBlanks(boolean isImportBlanks) {
		this.isImportBlanks = isImportBlanks;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getExDate() {
		return exDate;
	}

	public void setExDate(String exDate) {
		this.exDate = exDate;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getControlled() {
		return controlled;
	}

	public void setControlled(String controlled) {
		this.controlled = controlled;
	}

	public String getAgency() {
		return agency;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}

	public String getCrossMechCode() {
		return crossMechCode;
	}

	public void setCrossMechCode(String crossMechCode) {
		this.crossMechCode = crossMechCode;
	}

	public String getCrossPrintCode() {
		return crossPrintCode;
	}

	public void setCrossPrintCode(String crossPrintCode) {
		this.crossPrintCode = crossPrintCode;
	}

	public String getNegCycle() {
		return negCycle;
	}

	public void setNegCycle(String negCycle) {
		this.negCycle = negCycle;
	}

	public String getPriceType() {
		return priceType;
	}

	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	public String getUnitType() {
		return unitType;
	}

	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	public String getMinCheque() {
		return minCheque;
	}

	public void setMinCheque(String minCheque) {
		this.minCheque = minCheque;
	}

	public String getHoldRes() {
		return holdRes;
	}

	public void setHoldRes(String holdRes) {
		this.holdRes = holdRes;
	}

	public String getHoldPay() {
		return holdPay;
	}

	public void setHoldPay(String holdPay) {
		this.holdPay = holdPay;
	}

	public String getPayVAT() {
		return payVAT;
	}

	public void setPayVAT(String payVAT) {
		this.payVAT = payVAT;
	}

	public String getFreeGoods() {
		return freeGoods;
	}

	public void setFreeGoods(String freeGoods) {
		this.freeGoods = freeGoods;
	}

	public String getPaymentFreq() {
		return paymentFreq;
	}

	public void setPaymentFreq(String paymentFreq) {
		this.paymentFreq = paymentFreq;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getStreet1() {
		return street1;
	}

	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	public String getStreet2() {
		return street2;
	}

	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailCC() {
		return emailCC;
	}

	public void setEmailCC(String emailCC) {
		this.emailCC = emailCC;
	}

	public String getAddrTypeCode() {
		return addrTypeCode;
	}

	public void setAddrTypeCode(String addrTypeCode) {
		this.addrTypeCode = addrTypeCode;
	}

	public String getAddrNotes() {
		return addrNotes;
	}

	public void setAddrNotes(String addrNotes) {
		this.addrNotes = addrNotes;
	}

	public String getSendStatement() {
		return sendStatement;
	}

	public void setSendStatement(String sendStatement) {
		this.sendStatement = sendStatement;
	}

	public String getAccountingSystem() {
		return accountingSystem;
	}

	public void setAccountingSystem(String accountingSystem) {
		this.accountingSystem = accountingSystem;
	}

	public String getcMRRAPublCode() {
		return cMRRAPublCode;
	}

	public void setcMRRAPublCode(String cMRRAPublCode) {
		this.cMRRAPublCode = cMRRAPublCode;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public class Site// : {
	{

		private String code="";

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

	}

	public String getComplianceStatus() {
		return complianceStatus;
	}

	public void setComplianceStatus(String complianceStatus) {
		this.complianceStatus = complianceStatus;
	}

	public String getCreated()
	{
		return created;
	}

	public void setCreated(String created)
	{
		this.created = created;
	}

	public String getModified()
	{
		return modified;
	}

	public void setModified(String modified)
	{
		this.modified = modified;
	}

	public String getEntityStatus()
	{
		return entityStatus;
	}

	public void setEntityStatus(String entityStatus)
	{
		this.entityStatus = entityStatus;
	}
	}
	
	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getChangeindicator() {
		return changeindicator;
	}


	public void setChangeindicator(String changeindicator) {
		this.changeindicator = changeindicator;
	}


	public Data getData() {
		return data;
	}


	public void setData(Data data) {
		this.data = data;
	}


	public PublisherInfo acceptPublishers(PublisherInfo publisher) {
		return publisher;
	}


	public String getBatchId()
	{
		return batchId;
	}


	public void setBatchId(String batchId)
	{
		this.batchId = batchId;
	}
	

}
