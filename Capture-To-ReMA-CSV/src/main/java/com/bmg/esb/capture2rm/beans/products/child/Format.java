
package com.bmg.esb.capture2rm.beans.products.child;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Format {

    @JsonProperty("name")
    private String name;
    @JsonProperty("productCarrierType")
    private ProductCarrierType productCarrierType;
    @JsonProperty("albumSingle")
    private String albumSingle;
    @JsonProperty("audioVisual")
    private String audioVisual;
    @JsonProperty("numberOfSides")
    private Integer numberOfSides;
    @JsonProperty("isPhysical")
    private Boolean isPhysical;
    @JsonProperty("configDigit")
    private Integer configDigit;
    @JsonProperty("isActive")
    private Boolean isActive;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("created")
    private String created;
    @JsonProperty("modified")
    private String modified;
    @JsonProperty("isPackage")
    private Boolean isPackage;
    @JsonProperty("isNonMusicalAsset")
    private Boolean isNonMusicalAsset;
    @JsonProperty("reMaConfigCode")
    private String reMaConfigCode;
    public String getReMaConfigCode() {
		return reMaConfigCode;
	}

	public void setReMaConfigCode(String reMaConfigCode) {
		this.reMaConfigCode = reMaConfigCode;
	}

	@JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The productCarrierType
     */
    @JsonProperty("productCarrierType")
    public ProductCarrierType getProductCarrierType() {
        return productCarrierType;
    }

    /**
     * 
     * @param productCarrierType
     *     The productCarrierType
     */
    @JsonProperty("productCarrierType")
    public void setProductCarrierType(ProductCarrierType productCarrierType) {
        this.productCarrierType = productCarrierType;
    }

    /**
     * 
     * @return
     *     The albumSingle
     */
    @JsonProperty("albumSingle")
    public String getAlbumSingle() {
        return albumSingle;
    }

    /**
     * 
     * @param albumSingle
     *     The albumSingle
     */
    @JsonProperty("albumSingle")
    public void setAlbumSingle(String albumSingle) {
        this.albumSingle = albumSingle;
    }

    /**
     * 
     * @return
     *     The audioVisual
     */
    @JsonProperty("audioVisual")
    public String getAudioVisual() {
        return audioVisual;
    }

    /**
     * 
     * @param audioVisual
     *     The audioVisual
     */
    @JsonProperty("audioVisual")
    public void setAudioVisual(String audioVisual) {
        this.audioVisual = audioVisual;
    }

    /**
     * 
     * @return
     *     The numberOfSides
     */
    @JsonProperty("numberOfSides")
    public Integer getNumberOfSides() {
        return numberOfSides;
    }

    /**
     * 
     * @param numberOfSides
     *     The numberOfSides
     */
    @JsonProperty("numberOfSides")
    public void setNumberOfSides(Integer numberOfSides) {
        this.numberOfSides = numberOfSides;
    }

    /**
     * 
     * @return
     *     The isPhysical
     */
    @JsonProperty("isPhysical")
    public Boolean getIsPhysical() {
        return isPhysical;
    }

    /**
     * 
     * @param isPhysical
     *     The isPhysical
     */
    @JsonProperty("isPhysical")
    public void setIsPhysical(Boolean isPhysical) {
        this.isPhysical = isPhysical;
    }

    /**
     * 
     * @return
     *     The configDigit
     */
    @JsonProperty("configDigit")
    public Integer getConfigDigit() {
        return configDigit;
    }

    /**
     * 
     * @param configDigit
     *     The configDigit
     */
    @JsonProperty("configDigit")
    public void setConfigDigit(Integer configDigit) {
        this.configDigit = configDigit;
    }

    /**
     * 
     * @return
     *     The isActive
     */
    @JsonProperty("isActive")
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * 
     * @param isActive
     *     The isActive
     */
    @JsonProperty("isActive")
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The created
     */
    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    /**
     * 
     * @param created
     *     The created
     */
    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * 
     * @return
     *     The modified
     */
    @JsonProperty("modified")
    public String getModified() {
        return modified;
    }

    /**
     * 
     * @param modified
     *     The modified
     */
    @JsonProperty("modified")
    public void setModified(String modified) {
        this.modified = modified;
    }

    /**
     * 
     * @return
     *     The isPackage
     */
    @JsonProperty("isPackage")
    public Boolean getIsPackage() {
        return isPackage;
    }

    /**
     * 
     * @param isPackage
     *     The isPackage
     */
    @JsonProperty("isPackage")
    public void setIsPackage(Boolean isPackage) {
        this.isPackage = isPackage;
    }

    /**
     * 
     * @return
     *     The isNonMusicalAsset
     */
    @JsonProperty("isNonMusicalAsset")
    public Boolean getIsNonMusicalAsset() {
        return isNonMusicalAsset;
    }

    /**
     * 
     * @param isNonMusicalAsset
     *     The isNonMusicalAsset
     */
    @JsonProperty("isNonMusicalAsset")
    public void setIsNonMusicalAsset(Boolean isNonMusicalAsset) {
        this.isNonMusicalAsset = isNonMusicalAsset;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
