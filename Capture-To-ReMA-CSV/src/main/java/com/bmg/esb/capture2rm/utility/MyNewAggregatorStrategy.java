package com.bmg.esb.capture2rm.utility;

import java.util.ArrayList;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class MyNewAggregatorStrategy implements AggregationStrategy{
	
	
		

        public MyNewAggregatorStrategy() {
                super();
        }

        public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
                Message newIn = newExchange.getIn();
                Object newBody = newIn.getBody();
                ArrayList list = null;
                if (oldExchange == null) {
                        list = new ArrayList();
                        list.add(newBody);
                        newIn.setBody(list);
                       // newIn.setHeader("aSideVal", 0); // added for track issue
                        return newExchange;
                } else {
                        Message in = oldExchange.getIn();
                        list = in.getBody(ArrayList.class);
                       // String straSideVal=(String) in.getHeader("aSideVal");//made changes for track issue
                        newIn.setHeader("aSideVal", in.getHeader("aSideVal")); // added for track issue
                        list.add(newBody);
                       
                        if((Boolean) newExchange.getProperty(Exchange.SPLIT_COMPLETE)){
                        	  
                        	in.setHeader("aSideVal", newIn.getHeader("aSideVal"));
                        
                        }
                        
                        return oldExchange;
                }
                
        }

}


