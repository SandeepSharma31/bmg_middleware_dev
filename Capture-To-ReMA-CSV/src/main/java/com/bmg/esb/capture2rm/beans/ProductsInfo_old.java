package com.bmg.esb.capture2rm.beans;

import java.lang.reflect.Array;


public class ProductsInfo_old {

	// Product
	// {
	private String type="";
	private String changeindicator="";
	private Data data;
	private String batchId="";
	
	public ProductsInfo_old()
	{
			
	}
	
	public ProductsInfo_old(String batchId)
	{
		
		 setBatchId(batchId);
		
	}
	
	public class Data
	{
	
	private Genre genre;
	private int copyrightYear=0;// : 2014,
	private boolean hasStreamingRights=false;// false,
	private String complianceStatus="";// : "NON_COMPLIANT",
	private boolean isPackage=false;// : false,
	private boolean isPartial=false;// : false,
	private int numberOfTracks=0;// : 0,
	private boolean isCompilation =false; // : false,
	private boolean hasManufacturingRights=false;// : false,
	private int artworkYear=0;// : 2014,
	private boolean hasPricePointsFull=false;// : true,
	private int numberOfParents=0;// : 0,
	private String copyrightNotice="";// :
									// "(P) 2014 The copyright in this sound recording is owned by Iron Maiden Holdings Limited",
	private ContractualRightCode contractualRightsCode;
	private boolean isLicenced=false;// : false,
	private String barcode="";// : "8765432987528",
	private Label label;
	private Format format;
	private MetadataLanguage metadataLanguage;
	private String artistName="";// : "Iron Maiden",
	private String artworkNotice="";// : "(C) 2014 Iron Maiden Holdings Limited",
	private String ownershipType="";// : "OWNED",
	private int numberOfTracksByBMG=0;// : 0,
	private String versionTitle="";// : "",
	private String parentalAdvisoryLyricsIndicator="";// : "NOT_APPLICABLE",
	private String copyrightOwner="";// : "Iron Maiden Holdings Limited",
	private String artworkOwner="";// : "Iron Maiden Holdings Limited",
	private boolean isPackageOnly=false;// : false,
	private int id=0;// : 1,
	private boolean hasSyncRights=false;// : false,
	private String title="";// : "First OK Product",
	private String releaseDate ="";// : "2014-07-01T00:00:00",
	private MarketingRightsCode marketingRightsCode;
	private String supplyChainStatus="";// "UNAVAILABLE",
	private Site site;
	private boolean hasPricePointsBudget=false;// : false,
	private boolean isRemaster=false;// : false,
	private String productCode="";// : "Test-12",
	private boolean hasPricePointsMid=false;// : false,
	private boolean hasBooklet=false;// : false,
	private boolean isPromo=false;// : false,
	private int duration=0;// : 0,
	private String productTitle="";// : "First OK Product",
	private String parentalAdvisoryArtworkIndicator="";// : "NOT_APPLICABLE"


	private String priceCategory="";
	private String actRealeaseDate="";
	private String reReleaseDate="";
	private String deletionDate="";
	private boolean isTVAdvertised=false;
	private boolean isPayCopyright=true;
	private int copyrightProductType=0;
	private String copyrightCapType="N";
	private int copyrightCapSongs=0;
	private int copyrightCapValue=0;
	private double copyrightCapPerc;
	private String copyrightCapFreezeDate="";
	private String society="";
	private String societyNumber="";
	private String tvAdFrom="";
	private String tvAdTo="";
	private String internal="";
	private int priceLevel=0;
	private String  territory="";
	private String holdReserves="N";
	private int copyrightTracks=0;
	private boolean isallowFree=false;
	private boolean isroyaltyInfoComplete=false;
	private String recType="S";
	private String tech="D";
	private String style="";
	private String vocal="";
	private boolean isPayCSI=false;
	private boolean isPaySOCAN=false;
	private String projectcode="";
	private String grId="";
	private String productClearedTerrName="";
	private String productExcludedTerrName="";
	private boolean isCatCoEnabled=false;
	private boolean isCatCoResend=false;
	private boolean aAutoCreateAKACodes=false;
	private String notes="";
	private boolean isUpdateUserCode=true;
	private boolean isUpdateTerrBarcodes=false;
	private boolean isImportBlanks=false;
	
	

	private boolean pricePointMidEditable=false;
	private String manager="";
	private boolean isSoundtrack=false;
	private boolean hasPrimaryArtistAgreement=false;
	private boolean pricePointBudgetEditable=false;
	private String coordinator="";
	private String[] altProductCodes;
	private int numberOfSides;
	private String created;
	private String updated;
	
	private Array assets[];
	private String createdBy="";
	private String updatedBy="";
	private String entityStatus;
	
	private boolean hasContractualDigitalRights;
	private String modified;
	private boolean hasContractualPhysicalRights;
	
	
	
	

// }
	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public int getCopyrightYear() {
		return copyrightYear;
	}

	public void setCopyrightYear(int copyrightYear) {
		this.copyrightYear = copyrightYear;
	}

	public boolean getHasStreamingRights() {
		return hasStreamingRights;
	}

	public void setHasStreamingRights(boolean hasStreamingRights) {
		this.hasStreamingRights = hasStreamingRights;
	}

	public String getComplianceStatus() {
		return complianceStatus;
	}

	public void setComplianceStatus(String complianceStatus) {
		this.complianceStatus = complianceStatus;
	}

	public boolean getIsPackage() {
		return isPackage;
	}

	public void setIsPackage(boolean isPackage) {
		this.isPackage = isPackage;
	}

	public boolean getIsPartial() {
		return isPartial;
	}

	public void setIsPartial(boolean isPartial) {
		this.isPartial = isPartial;
	}

	public int getNumberOfTracks() {
		return numberOfTracks;
	}

	public void setNumberOfTracks(int numberOfTracks) {
		this.numberOfTracks = numberOfTracks;
	}

	public boolean getIsCompilation() {
		return isCompilation;
	}

	public void setIsCompilation(boolean isCompilation) {
		this.isCompilation = isCompilation;
	}

	public boolean getHasManufacturingRights() {
		return hasManufacturingRights;
	}

	public void setHasManufacturingRights(boolean hasManufacturingRights) {
		this.hasManufacturingRights = hasManufacturingRights;
	}

	public int getArtworkYear() {
		return artworkYear;
	}

	public void setArtworkYear(int artworkYear) {
		this.artworkYear = artworkYear;
	}

	public boolean getHasPricePointsFull() {
		return hasPricePointsFull;
	}

	public void setHasPricePointsFull(boolean hasPricePointsFull) {
		this.hasPricePointsFull = hasPricePointsFull;
	}

	public int getNumberOfParents() {
		return numberOfParents;
	}

	public void setNumberOfParents(int numberOfParents) {
		this.numberOfParents = numberOfParents;
	}

	public String getCopyrightNotice() {
		return copyrightNotice;
	}

	public void setCopyrightNotice(String copyrightNotice) {
		this.copyrightNotice = copyrightNotice;
	}

	public ContractualRightCode getContractualRightsCode() {
		return contractualRightsCode;
	}

	public void setContractualRightsCode(
			ContractualRightCode contractualRightsCode) {
		this.contractualRightsCode = contractualRightsCode;
	}

	public boolean getIsLicenced() {
		return isLicenced;
	}

	public void setIsLicenced(boolean isLicenced) {
		this.isLicenced = isLicenced;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

	public Format getFormat() {
		return format;
	}

	public void setFormat(Format format) {
		this.format = format;
	}

	public MetadataLanguage getMetadataLanguage() {
		return metadataLanguage;
	}

	public void setMetadataLanguage(MetadataLanguage metadataLanguage) {
		this.metadataLanguage = metadataLanguage;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getArtworkNotice() {
		return artworkNotice;
	}

	public void setArtworkNotice(String artworkNotice) {
		this.artworkNotice = artworkNotice;
	}

	public String getOwnershipType() {
		return ownershipType;
	}

	public void setOwnershipType(String ownershipType) {
		this.ownershipType = ownershipType;
	}

	public int getNumberOfTracksByBMG() {
		return numberOfTracksByBMG;
	}

	public void setNumberOfTracksByBMG(int numberOfTracksByBMG) {
		this.numberOfTracksByBMG = numberOfTracksByBMG;
	}

	public String getVersionTitle() {
		return versionTitle;
	}

	public void setVersionTitle(String versionTitle) {
		this.versionTitle = versionTitle;
	}

	public String getParentalAdvisoryLyricsIndicator() {
		return parentalAdvisoryLyricsIndicator;
	}

	public void setParentalAdvisoryLyricsIndicator(
			String parentalAdvisoryLyricsIndicator) {
		this.parentalAdvisoryLyricsIndicator = parentalAdvisoryLyricsIndicator;
	}

	public String getCopyrightOwner() {
		return copyrightOwner;
	}

	public void setCopyrightOwner(String copyrightOwner) {
		this.copyrightOwner = copyrightOwner;
	}

	public String getArtworkOwner() {
		return artworkOwner;
	}

	public void setArtworkOwner(String artworkOwner) {
		this.artworkOwner = artworkOwner;
	}

	public boolean getIsPackageOnly() {
		return isPackageOnly;
	}

	public void setIsPackageOnly(boolean isPackageOnly) {
		this.isPackageOnly = isPackageOnly;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean getHasSyncRights() {
		return hasSyncRights;
	}

	public void setHasSyncRights(boolean hasSyncRights) {
		this.hasSyncRights = hasSyncRights;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}



	public MarketingRightsCode getMarketingRightsCode() {
		return marketingRightsCode;
	}

	public void setMarketingRightsCode(MarketingRightsCode marketingRightsCode) {
		this.marketingRightsCode = marketingRightsCode;
	}

	public String getSupplyChainStatus() {
		return supplyChainStatus;
	}

	public void setSupplyChainStatus(String supplyChainStatus) {
		this.supplyChainStatus = supplyChainStatus;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public boolean getHasPricePointsBudget() {
		return hasPricePointsBudget;
	}

	public void setHasPricePointsBudget(boolean hasPricePointsBudget) {
		this.hasPricePointsBudget = hasPricePointsBudget;
	}

	public boolean getIsRemaster() {
		return isRemaster;
	}

	public void setIsRemaster(boolean isRemaster) {
		this.isRemaster = isRemaster;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public boolean getHasPricePointsMid() {
		return hasPricePointsMid;
	}

	public void setHasPricePointsMid(boolean hasPricePointsMid) {
		this.hasPricePointsMid = hasPricePointsMid;
	}

	public boolean getHasBooklet() {
		return hasBooklet;
	}

	public void setHasBooklet(boolean hasBooklet) {
		this.hasBooklet = hasBooklet;
	}

	public boolean getIsPromo() {
		return isPromo;
	}

	public void setIsPromo(boolean isPromo) {
		this.isPromo = isPromo;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public String getParentalAdvisoryArtworkIndicator() {
		return parentalAdvisoryArtworkIndicator;
	}

	public void setParentalAdvisoryArtworkIndicator(
			String parentalAdvisoryArtworkIndicator) {
		this.parentalAdvisoryArtworkIndicator = parentalAdvisoryArtworkIndicator;
	}

	public class Genre {
		private String name;// : "Rock",
		private int id;// : 183
		private String created;
		private String modified;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getCreated()
		{
			return created;
		}

		public void setCreated(String created)
		{
			this.created = created;
		}

		public String getModified()
		{
			return modified;
		}

		public void setModified(String modified)
		{
			this.modified = modified;
		}

	}

	public class ContractualRightCode {
		private String name;// : "World",
		private String code;// : "AAC",
		private int id;// : 3
		private String created;
		private String modified;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getCreated()
		{
			return created;
		}

		public void setCreated(String created)
		{
			this.created = created;
		}

		public String getModified()
		{
			return modified;
		}

		public void setModified(String modified)
		{
			this.modified = modified;
		}
	}

	public class Label// : {
	{
		private String name;// : "BMG Rights Management (UK) Ltd.",
		private int id;// : 242
		
		private ParentCompany parentCompany;
		private String created;
		private String modified;
		
		public class ParentCompany
		{
			
			private String name;
			private String code;
			private String orderCodeSAP;
			private String orderNameSAP;
			private boolean isOwnedByBMG;
			private boolean isRepertoireOwner;
			private boolean isContracting;
			private int id;
			private String created;
			private String modified;
			public String getName()
			{
				return name;
			}
			public void setName(String name)
			{
				this.name = name;
			}
			public String getCode()
			{
				return code;
			}
			public void setCode(String code)
			{
				this.code = code;
			}
			public String getOrderCodeSAP()
			{
				return orderCodeSAP;
			}
			public void setOrderCodeSAP(String orderCodeSAP)
			{
				this.orderCodeSAP = orderCodeSAP;
			}
			public String getOrderNameSAP()
			{
				return orderNameSAP;
			}
			public void setOrderNameSAP(String orderNameSAP)
			{
				this.orderNameSAP = orderNameSAP;
			}
			public boolean getIsOwnedByBMG()
			{
				return isOwnedByBMG;
			}
			public void setIsOwnedByBMG(boolean isOwnedByBMG)
			{
				this.isOwnedByBMG = isOwnedByBMG;
			}
			public boolean getIsRepertoireOwner()
			{
				return isRepertoireOwner;
			}
			public void setIsRepertoireOwner(boolean isRepertoireOwner)
			{
				this.isRepertoireOwner = isRepertoireOwner;
			}
			public boolean getIsContracting()
			{
				return isContracting;
			}
			public void setIsContracting(boolean isContracting)
			{
				this.isContracting = isContracting;
			}
			public int getId()
			{
				return id;
			}
			public void setId(int id)
			{
				this.id = id;
			}
			public String getCreated()
			{
				return created;
			}
			public void setCreated(String created)
			{
				this.created = created;
			}
			public String getModified()
			{
				return modified;
			}
			public void setModified(String modified)
			{
				this.modified = modified;
			}
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		/**
		 * @return the parentCompany
		 */
		public ParentCompany getParentCompany()
		{
			return parentCompany;
		}

		/**
		 * @param parentCompany the parentCompany to set
		 */
		public void setParentCompany(ParentCompany parentCompany)
		{
			this.parentCompany = parentCompany;
		}

		public String getCreated()
		{
			return created;
		}

		public void setCreated(String created)
		{
			this.created = created;
		}

		public String getModified()
		{
			return modified;
		}

		public void setModified(String modified)
		{
			this.modified = modified;
		}
	}

	public class Format// : {
	{
		private String name;// : "CD Album",
		private ProductCarrierType productCarrierType;
		private String albumSingle;// : "ALBUM",
		private String audioVisual;// : "AUDIO",
		private int numberOfSides;// : 1,
		private boolean isPhysical;// : true,
		private int configDigit;// : 0,
		private boolean isActive;// : true,
		private int id =0;// : 2
		
		
		private String created;
		private String modified;
		private boolean isPackage=false;
		private boolean isNonMusicalAsset=false;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public ProductCarrierType getProductCarrierType() {
			return productCarrierType;
		}

		public void setProductCarrierType(ProductCarrierType productCarrierType) {
			this.productCarrierType = productCarrierType;
		}

		public String getAlbumSingle() {
			return albumSingle;
		}

		public void setAlbumSingle(String albumSingle) {
			this.albumSingle = albumSingle;
		}

		public String getAudioVisual() {
			return audioVisual;
		}

		public void setAudioVisual(String audioVisual) {
			this.audioVisual = audioVisual;
		}

		public int getNumberOfSides() {
			return numberOfSides;
		}

		public void setNumberOfSides(int numberOfSides) {
			this.numberOfSides = numberOfSides;
		}

		public boolean getIsPhysical() {
			return isPhysical;
		}

		public void setIsPhysical(boolean isPhysical) {
			this.isPhysical = isPhysical;
		}

		public int getConfigDigit() {
			return configDigit;
		}

		public void setConfigDigit(int configDigit) {
			this.configDigit = configDigit;
		}

		public boolean getIsActive() {
			return isActive;
		}

		public void setIsActive(boolean isActive) {
			this.isActive = isActive;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getCreated()
		{
			return created;
		}

		public void setCreated(String created)
		{
			this.created = created;
		}

		public String getModified()
		{
			return modified;
		}

		public void setModified(String modified)
		{
			this.modified = modified;
		}

		public class ProductCarrierType// : {
		{
			private String name;// : "Album",
			private String code;// : "ALB",
			private int carrierId;// : 23,
			private String carrierName;// : "CD",
			private int id;// : 6
			private String created;
			private String modified;

			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

			public String getCode() {
				return code;
			}

			public void setCode(String code) {
				this.code = code;
			}

			public int getCarrierId() {
				return carrierId;
			}

			public void setCarrierId(int carrierId) {
				this.carrierId = carrierId;
			}

			public String getCarrierName() {
				return carrierName;
			}

			public void setCarrierName(String carrierName) {
				this.carrierName = carrierName;
			}

			public int getId() {
				return id;
			}

			public void setId(int id) {
				this.id = id;
			}

			public String getCreated()
			{
				return created;
			}

			public void setCreated(String created)
			{
				this.created = created;
			}

			public String getModified()
			{
				return modified;
			}

			public void setModified(String modified)
			{
				this.modified = modified;
			}
		}

		public boolean getIsPackage()
		{
			return isPackage;
		}

		public void setIsPackage(boolean isPackage)
		{
			this.isPackage = isPackage;
		}

		public boolean getIsNonMusicalAsset()
		{
			return isNonMusicalAsset;
		}

		public void setIsNonMusicalAsset(boolean isNonMusicalAsset)
		{
			this.isNonMusicalAsset = isNonMusicalAsset;
		}
	}

	public class MarketingRightsCode {
		private String name;// : "World",
		private String code;// : "AAC",
		private int id;// : 3
		private String created="";
		private String modified="";

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getCreated()
		{
			return created;
		}

		public void setCreated(String created)
		{
			this.created = created;
		}

		public String getModified()
		{
			return modified;
		}

		public void setModified(String modified)
		{
			this.modified = modified;
		}
	}

	public class MetadataLanguage {
		private String name;// : "English",
		private String code;// : "eng",
		private int id;// : 2
		private String created;
		private String modified;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getCreated()
		{
			return created;
		}

		public void setCreated(String created)
		{
			this.created = created;
		}

		public String getModified()
		{
			return modified;
		}

		public void setModified(String modified)
		{
			this.modified = modified;
		}
	}

	public class Site {
		private String id;// "ba7e81f5-79cd-4ccd-a040-cb60ecf3ad02",
		private String name;// : "BMG Rights Management UK",
		private String code;// : "BGUK",
		private String country;// : "GB"

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}
	}



	public String getPriceCategory() {
		return priceCategory;
	}

	public void setPriceCategory(String priceCategory) {
		this.priceCategory = priceCategory;
	}

	public String getActRealeaseDate() {
		return actRealeaseDate;
	}

	public void setActRealeaseDate(String actRealeaseDate) {
		this.actRealeaseDate = actRealeaseDate;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getDeletionDate() {
		return deletionDate;
	}

	public void setDeletionDate(String deletionDate) {
		this.deletionDate = deletionDate;
	}

	public boolean getIsTVAdvertised() {
		return isTVAdvertised;
	}

	public void setIsTVAdvertised(boolean isTVAdvertised) {
		this.isTVAdvertised = isTVAdvertised;
	}

	public boolean getIsPayCopyright() {
		return isPayCopyright;
	}

	public void setIsPayCopyright(boolean isPayCopyright) {
		this.isPayCopyright = isPayCopyright;
	}

	public int getCopyrightProductType() {
		return copyrightProductType;
	}

	public void setCopyrightProductType(int copyrightProductType) {
		this.copyrightProductType = copyrightProductType;
	}

	public String getCopyrightCapType() {
		return copyrightCapType;
	}

	public void setCopyrightCapType(String copyrightCapType) {
		this.copyrightCapType = copyrightCapType;
	}

	public int getCopyrightCapSongs() {
		return copyrightCapSongs;
	}

	public void setCopyrightCapSongs(int copyrightCapSongs) {
		this.copyrightCapSongs = copyrightCapSongs;
	}

	public int getCopyrightCapValue() {
		return copyrightCapValue;
	}

	public void setCopyrightCapValue(int copyrightCapValue) {
		this.copyrightCapValue = copyrightCapValue;
	}

	public double getCopyrightCapPerc() {
		return copyrightCapPerc;
	}

	public void setCopyrightCapPerc(double copyrightCapPerc) {
		this.copyrightCapPerc = copyrightCapPerc;
	}

	public String getCopyrightCapFreezeDate() {
		return copyrightCapFreezeDate;
	}

	public void setCopyrightCapFreezeDate(String copyrightCapFreezeDate) {
		this.copyrightCapFreezeDate = copyrightCapFreezeDate;
	}

	public String getSociety() {
		return society;
	}

	public void setSociety(String society) {
		this.society = society;
	}

	public String getSocietyNumber() {
		return societyNumber;
	}

	public void setSocietyNumber(String societyNumber) {
		this.societyNumber = societyNumber;
	}

	public String getTvAdFrom() {
		return tvAdFrom;
	}

	public void setTvAdFrom(String tvAdFrom) {
		this.tvAdFrom = tvAdFrom;
	}

	public String getTvAdTo() {
		return tvAdTo;
	}

	public void setTvAdTo(String tvAdTo) {
		this.tvAdTo = tvAdTo;
	}

	public String getInternal() {
		return internal;
	}

	public void setInternal(String internal) {
		this.internal = internal;
	}

	public int getPriceLevel() {
		return priceLevel;
	}

	public void setPriceLevel(int priceLevel) {
		this.priceLevel = priceLevel;
	}

	public String getTerritory() {
		return territory;
	}

	public void setTerritory(String territory) {
		this.territory = territory;
	}

	public String getHoldReserves() {
		return holdReserves;
	}

	public void setHoldReserves(String holdReserves) {
		this.holdReserves = holdReserves;
	}

	public int getCopyrightTracks() {
		return copyrightTracks;
	}

	public void setCopyrightTracks(int copyrightTracks) {
		this.copyrightTracks = copyrightTracks;
	}

	public boolean getIsallowFree() {
		return isallowFree;
	}

	public void setIsallowFree(boolean isallowFree) {
		this.isallowFree = isallowFree;
	}

	public boolean getIsroyaltyInfoComplete() {
		return isroyaltyInfoComplete;
	}

	public void setIsroyaltyInfoComplete(boolean isroyaltyInfoComplete) {
		this.isroyaltyInfoComplete = isroyaltyInfoComplete;
	}

	public String getRecType() {
		return recType;
	}

	public void setRecType(String recType) {
		this.recType = recType;
	}

	public String getTech() {
		return tech;
	}

	public void setTech(String tech) {
		this.tech = tech;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getVocal() {
		return vocal;
	}

	public void setVocal(String vocal) {
		this.vocal = vocal;
	}

	public boolean getIsPayCSI() {
		return isPayCSI;
	}

	public void setIsPayCSI(boolean isPayCSI) {
		this.isPayCSI = isPayCSI;
	}

	public boolean getIsPaySOCAN() {
		return isPaySOCAN;
	}

	public void setIsPaySOCAN(boolean isPaySOCAN) {
		this.isPaySOCAN = isPaySOCAN;
	}

	public String getProjectcode() {
		return projectcode;
	}

	public void setProjectcode(String projectcode) {
		this.projectcode = projectcode;
	}

	
	public String getProductClearedTerrName() {
		return productClearedTerrName;
	}

	public void setProductClearedTerrName(String productClearedTerrName) {
		this.productClearedTerrName = productClearedTerrName;
	}

	public String getProductExcludedTerrName() {
		return productExcludedTerrName;
	}

	public void setProductExcludedTerrName(String productExcludedTerrName) {
		this.productExcludedTerrName = productExcludedTerrName;
	}

	public boolean getIsCatCoEnabled() {
		return isCatCoEnabled;
	}

	public void setIsCatCoEnabled(boolean isCatCoEnabled) {
		this.isCatCoEnabled = isCatCoEnabled;
	}

	public boolean getIsCatCoResend() {
		return isCatCoResend;
	}

	public void setIsCatCoResend(boolean isCatCoResend) {
		this.isCatCoResend = isCatCoResend;
	}

	public boolean getIsaAutoCreateAKACodes() {
		return aAutoCreateAKACodes;
	}

	public void setIsaAutoCreateAKACodes(boolean aAutoCreateAKACodes) {
		this.aAutoCreateAKACodes = aAutoCreateAKACodes;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean getIsUpdateUserCode() {
		return isUpdateUserCode;
	}

	public void setIsUpdateUserCode(boolean isUpdateUserCode) {
		this.isUpdateUserCode = isUpdateUserCode;
	}

	public boolean getIsUpdateTerrBarcodes() {
		return isUpdateTerrBarcodes;
	}

	public void setIsUpdateTerrBarcodes(boolean isUpdateTerrBarcodes) {
		this.isUpdateTerrBarcodes = isUpdateTerrBarcodes;
	}

	
	public void setPartial(boolean isPartial) {
		this.isPartial = isPartial;
	}

	public boolean getIsImportBlanks() {
		return isImportBlanks;
	}

	public void setIsImportBlanks(boolean isImportBlanks) {
		this.isImportBlanks = isImportBlanks;
	}

	public String getGrId() {
		return grId;
	}

	public void setGrId(String grId) {
		this.grId = grId;
	}

	public boolean getPricePointMidEditable() {
		return pricePointMidEditable;
	}

	public void setPricePointMidEditable(boolean pricePointMidEditable) {
		this.pricePointMidEditable = pricePointMidEditable;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public boolean getIsSoundtrack() {
		return isSoundtrack;
	}

	public void setIsSoundtrack(boolean isSoundtrack) {
		this.isSoundtrack = isSoundtrack;
	}

	public boolean getHasPrimaryArtistAgreement() {
		return hasPrimaryArtistAgreement;
	}

	public void setHasPrimaryArtistAgreement(boolean hasPrimaryArtistAgreement) {
		this.hasPrimaryArtistAgreement = hasPrimaryArtistAgreement;
	}

	public boolean getPricePointBudgetEditable() {
		return pricePointBudgetEditable;
	}

	public void setPricePointBudgetEditable(boolean pricePointBudgetEditable) {
		this.pricePointBudgetEditable = pricePointBudgetEditable;
	}

	public String getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}

	
	public int getNumberOfSides()
	{
		return numberOfSides;
	}

	public void setNumberOfSides(int numberOfSides)
	{
		this.numberOfSides = numberOfSides;
	}

	public String getCreated()
	{
		return created;
	}

	public void setCreated(String created)
	{
		this.created = created;
	}

	public String getUpdated()
	{
		return updated;
	}

	public void setUpdated(String updated)
	{
		this.updated = updated;
	}

	public Array[] getAssets()
	{
		return assets;
	}

	public void setAssets(Array assets[])
	{
		this.assets = assets;
	}

	public String getCreatedBy()
	{
		return createdBy;
	}

	public void setCreatedBy(String createdBy)
	{
		this.createdBy = createdBy;
	}

	public String getUpdatedBy()
	{
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy)
	{
		this.updatedBy = updatedBy;
	}

	public String getEntityStatus()
	{
		return entityStatus;
	}

	public void setEntityStatus(String entityStatus)
	{
		this.entityStatus = entityStatus;
	}

	public String[] getAltProductCodes()
	{
		return altProductCodes;
	}

	public void setAltProductCodes(String[] altProductCodes)
	{
		this.altProductCodes = altProductCodes;
	}

	public boolean getHasContractualDigitalRights()
	{
		return hasContractualDigitalRights;
	}

	public void setHasContractualDigitalRights(boolean hasContractualDigitalRights)
	{
		this.hasContractualDigitalRights = hasContractualDigitalRights;
	}

	/**
	 * @return the modified
	 */
	public String getModified()
	{
		return modified;
	}

	/**
	 * @param modified the modified to set
	 */
	public void setModified(String modified)
	{
		this.modified = modified;
	}

	public boolean getHasContractualPhysicalRights()
	{
		return hasContractualPhysicalRights;
	}

	public void setHasContractualPhysicalRights(boolean hasContractualPhysicalRights)
	{
		this.hasContractualPhysicalRights = hasContractualPhysicalRights;
	}

	
	
	}
	public ProductsInfo_old acceptProducts(ProductsInfo_old products) {
		return products;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getChangeindicator() {
		return changeindicator;
	}
	public void setChangeindicator(String changeindicator) {
		this.changeindicator = changeindicator;
	}
	public Data getData() {
		return data;
	}
	public void setData(Data data) {
		this.data = data;
	}
	public String getBatchId()
	{
		return batchId;
	}
	public void setBatchId(String batchId)
	{
		this.batchId = batchId;
		//System.out.println("batch id"+batchId);
	}

	
}
