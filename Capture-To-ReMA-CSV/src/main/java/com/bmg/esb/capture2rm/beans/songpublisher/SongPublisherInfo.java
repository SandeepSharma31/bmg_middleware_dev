
package com.bmg.esb.capture2rm.beans.songpublisher;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class SongPublisherInfo {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("idx")
    private Integer idx;
    @JsonProperty("publisher")
    private Publisher publisher;
    @JsonProperty("country")
    private Country country;
    @JsonProperty("share")
    private Double share;
    @JsonProperty("iMaestro")
    private Boolean iMaestro;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The idx
     */
    @JsonProperty("idx")
    public Integer getIdx() {
        return idx;
    }

    /**
     * 
     * @param idx
     *     The idx
     */
    @JsonProperty("idx")
    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    /**
     * 
     * @return
     *     The publisher
     */
    @JsonProperty("publisher")
    public Publisher getPublisher() {
        return publisher;
    }

    /**
     * 
     * @param publisher
     *     The publisher
     */
    @JsonProperty("publisher")
    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    /**
     * 
     * @return
     *     The country
     */
    @JsonProperty("country")
    public Country getCountry() {
        return country;
    }

    /**
     * 
     * @param country
     *     The country
     */
    @JsonProperty("country")
    public void setCountry(Country country) {
        this.country = country;
    }

    /**
     * 
     * @return
     *     The share
     */
    @JsonProperty("share")
    public Double getShare() {
        return share;
    }

    /**
     * 
     * @param share
     *     The share
     */
    @JsonProperty("share")
    public void setShare(Double share) {
        this.share = share;
    }

    /**
     * 
     * @return
     *     The iMaestro
     */
    @JsonProperty("iMaestro")
    public Boolean getIMaestro() {
        return iMaestro;
    }

    /**
     * 
     * @param iMaestro
     *     The iMaestro
     */
    @JsonProperty("iMaestro")
    public void setIMaestro(Boolean iMaestro) {
        this.iMaestro = iMaestro;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
