
package com.bmg.esb.capture2rm.beans.products.child;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import com.bmg.esb.capture2rm.beans.products.ProductsInfo;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


@JsonIgnoreProperties(ignoreUnknown = true)


public class ProductChild {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("idx")
    private Integer idx;
    @JsonProperty("child")
    private Child child;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The idx
     */
    @JsonProperty("idx")
    public Integer getIdx() {
        return idx;
    }

    /**
     * 
     * @param idx
     *     The idx
     */
    @JsonProperty("idx")
    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    /**
     * 
     * @return
     *     The child
     */
    @JsonProperty("child")
    public Child getChild() {
        return child;
    }

    /**
     * 
     * @param child
     *     The child
     */
    @JsonProperty("child")
    public void setChild(Child child) {
        this.child = child;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String toString()
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectMapper mapper = new ObjectMapper();

		try 
		{
			mapper.writeValue(baos, this);
		} 
		catch (Exception e) 
		{
			throw new RuntimeException("Unable to serialize ProductChild object to a Json-"+e.getMessage());
		}
		return baos.toString();
	}
	
	public static ProductChild fromString(String strProductChildData) throws Exception
	{
		
		ObjectMapper mapper = new ObjectMapper();
		ProductChild objProductChildren = mapper.readValue(strProductChildData,new TypeReference<ProductChild>() {});
		return objProductChildren;
	}

    
}
