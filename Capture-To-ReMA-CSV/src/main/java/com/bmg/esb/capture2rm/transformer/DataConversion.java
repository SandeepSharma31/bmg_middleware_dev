package com.bmg.esb.capture2rm.transformer;

import com.bmg.esb.capture2rm.beans.products.ProductsInfo;
import com.bmg.esb.capture2rm.beans.products.child.ProductChild;
import com.bmg.esb.capture2rm.beans.tracks.TracksInfo;

import java.io.IOException;
import java.text.ParseException;
// import java.lang.reflect.Array;
import java.util.List;

import javax.sql.DataSource;

import org.apache.camel.Exchange;
/*import org.codehaus.jackson.JsonParseException;
 import org.codehaus.jackson.map.JsonMappingException;
 import org.codehaus.jackson.map.ObjectMapper;
 import org.codehaus.jackson.type.TypeReference;*/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bmg.esb.capture2rm.beans.recordings.RecordingsInfo;
import com.bmg.esb.capture2rm.beans.recordings.song.RecordingSongInfo;
//import com.bmg.esb.capture2rm.beans.PackageTrueProductsInfo;
import com.bmg.esb.capture2rm.beans.songpublisher.SongPublisherInfo;
//import com.bmg.esb.capture2rm.beans.ProductsInfo_old;
// import com.bmg.esb.capture2rm.beans.ProductsAPIInfo;
//import com.bmg.esb.capture2rm.beans.SongPublisherInfo;
import com.bmg.esb.capture2rm.beans.songs.SongsInfo;
import com.bmg.esb.capture2rm.processors.CalcTrackandSideSum;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
//import com.bmg.esb.capture2rm.beans.TrackInfo;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataConversion {
	private static final Logger DataConversionLogger = LoggerFactory.getLogger(DataConversion.class);

	private int sideNumForRec = 0;

	public void processSongListResponseJSON(Exchange exchange) throws JsonParseException, JsonMappingException, IOException 
	{
			ObjectMapper mapper = new ObjectMapper();
			List<SongPublisherInfo> songPubList = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<SongPublisherInfo>>() {});
			int len = songPubList.size();
			DataConversionLogger.info("SongPublisher List value "+len);

			if(songPubList.size()>0)
			{
				DataConversionLogger.info("SongPublisher List avaialble ");
				exchange.getIn().setBody(songPubList);
				
			}
			else
			{
				exchange.getIn().setBody(null);
				DataConversionLogger.info("List is not available for SongPublisher" );	
			}
	}
	


	
	public void processProductListJSONPackageTrue(Exchange exchange) throws JsonParseException, JsonMappingException, IOException
	{
		int iSideSum = 0;
		int iTrackSum = 0;
		
		ObjectMapper mapper = new ObjectMapper();
		List<ProductChild> productsPackageList = mapper.readValue(exchange.getIn().getBody(String.class),new TypeReference<List<ProductChild>>() {});
			
		if(productsPackageList.size()>0)
		{
			
			exchange.getIn().setBody(productsPackageList);
			
		}
		else
		{
			exchange.getIn().setBody(null);
			DataConversionLogger.info("List is not available for products package true" );	
		}
		exchange.getIn().setHeader("childProductCount",""+productsPackageList.size());
		
		for(ProductChild pc:productsPackageList)
		{
			iSideSum = pc.getChild().getFormat().getNumberOfSides()+iSideSum;
			iTrackSum = pc.getIdx() + iTrackSum;
			DataConversionLogger.info("Isidesum value in products package true" +iSideSum );
			DataConversionLogger.info("Itracksum value in products package true" +iTrackSum );
		}
		
		exchange.setProperty(CalcTrackandSideSum.SIDE_COUNT, ""+iSideSum);
		exchange.setProperty(CalcTrackandSideSum.TRACK_COUNT, ""+iTrackSum);
		exchange.setProperty("initSize", 500);
		DataConversionLogger.info("Isidesum value in products package true after calling calculate method" +iSideSum );
		DataConversionLogger.info("Itracksum value in products package trueafter calling calculate method" +iTrackSum );
		
	/*	if((exchange.getIn().getHeader("MaxsideNum")!=null))
		{
		int sideForTrack=Integer.parseInt((exchange.getIn().getHeader("MaxsideNum").toString()));
		}
		else
		{
			
		}*/
		
	}
	
	
	
	public void processTrackListPackageTrueJSON (Exchange exchange) throws JsonParseException, JsonMappingException, IOException
	{
		ObjectMapper mapper = new ObjectMapper();

		List<TracksInfo> productsTrackList = mapper.readValue(exchange.getIn().getBody(String.class), new TypeReference<List<TracksInfo>>() {});

		if(productsTrackList.size()>0)
		{
			exchange.getIn().setBody(productsTrackList);
		}
		else
		{
			exchange.getIn().setBody(null);
			DataConversionLogger.info("List is not available for Tracks for products package true" );	
		}
	}

	public void processProductListResponseJSON(Exchange exchange)
			throws JsonParseException, JsonMappingException, IOException, ParseException {
		ObjectMapper mapper = new ObjectMapper();
		int finalSideNumber = 0, finaltrackNum = 0;
		ProductsInfo objProd = (ProductsInfo) exchange.getProperty(
				"productbody");
		String strBatchId = (String) exchange.getIn().getHeader("batchId");
		JSonToDB objJSONDB = new JSonToDB();

		objJSONDB.setDataSource(null);

	

			List<ProductChild> productsPackageList = mapper.readValue(exchange
					.getIn().getBody(String.class),
					new TypeReference<List<ProductChild>>() {
					});

			int childLenght = productsPackageList.size();
			DataConversionLogger.info("Childs are ---------->" + childLenght);
			int[] anArrayChild = new int[childLenght];
			if (childLenght != 0) {

				for (int i = 0; i < productsPackageList.size(); i++) {
					DataConversionLogger
							.info("In products Package true and childs are --------->"
									+ i);

					int numSide = productsPackageList.get(i).getChild()
							.getFormat().getNumberOfSides();
					finalSideNumber = finalSideNumber + numSide;

					int trackNum = productsPackageList.get(i).getChild()
							.getNumberOfTracks();
					finaltrackNum = finaltrackNum + trackNum;
					DataConversionLogger.info("In products Package true for "
							+ i + "and trackNum and SideNum are  --------->"
							+ trackNum + "," + numSide);

					DataConversionLogger
							.info("Calculation done for track Num and Side Num for product: "
									+ i
									+ "and values are -->"
									+ finaltrackNum
									+ "," + finalSideNumber);

				}
				objJSONDB.executeInsertProductPackageTrue(objProd, strBatchId,
								null, finalSideNumber, finaltrackNum,
								childLenght);

				exchange.getIn().setBody(productsPackageList);

			}

			else {

				DataConversionLogger
						.info("In products Package true and having no childs returing empty data of products");
			}

		
	}

	public void processTrackListResponseJSON(Exchange exchange)
			throws Exception {

		ObjectMapper mapper = new ObjectMapper();

		List<TracksInfo> productsTrackList = mapper.readValue(exchange.getIn()
				.getBody(String.class), new TypeReference<List<TracksInfo>>() {
		});

		int childLenght = productsTrackList.size();
		DataConversionLogger.info("Childs are for tracks---------->"
				+ childLenght);

		if (childLenght != 0) {

			ProductsInfo objProd = (ProductsInfo) exchange.getProperty(
					"productbody");
			String strBatchId = (String) exchange.getIn().getHeader("batchId");
			boolean packagestatus = (Boolean) exchange.getIn().getHeader(
					"packageStatus");
			JSonToDB objJSONDB = new JSonToDB();

			objJSONDB.setDataSource(null);

			for (int i = 0; i < productsTrackList.size(); i++) {

				if (packagestatus == false) {
					DataConversionLogger
							.info("packageStatus is false --------->" + i);
					int trackNum = productsTrackList.get(i).getIdx();
					// finalSideNumber=finalSideNumber+numSide;

					int sideNum = productsTrackList.get(i).getSide();
					
					trackNum=(trackNum==0)?trackNum++:trackNum;
					sideNum= (sideNum==0)?sideNum++:sideNum;
					DataConversionLogger.info("track Num and Side Num"
							+ trackNum + "::" + sideNum);

					objJSONDB.executeInsertTrack(objProd,
							productsTrackList.get(i), strBatchId, trackNum,
							sideNum);
					DataConversionLogger
							.info("end of packageStatus false condition");

				}

			}
			// objJSONDB.executeInsertProductPackageTrue(objProd,
			// strBatchId,dataSource,finalSideNumber,finaltrackNum,childLenght);

		} else {

			DataConversionLogger.info("In track returing empty data for track");
		}
	}

	public void processTrackListPackageTrueResponseJSON(Exchange exchange)
			throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();

		List<TracksInfo> productsTrackListPackageTrue = mapper.readValue(
				exchange.getIn().getBody(String.class),
				new TypeReference<List<TracksInfo>>() {
				});
		int CurrentSide;

		int prodId = (Integer) exchange.getIn().getHeader("prodId");

		int childLenght = productsTrackListPackageTrue.size();
		DataConversionLogger.info("Childs are for tracks---------->"
				+ childLenght);

		String strBatchId = (String) exchange.getIn().getHeader("batchId");
		ProductChild objProd = (ProductChild) exchange.getIn().getHeader(
				"prodbodyTrue");

		if (childLenght != 0) {

			JSonToDB objJSONDB = new JSonToDB();

			objJSONDB.setDataSource(null);

			for (int i = 0; i < productsTrackListPackageTrue.size(); i++) {
				DataConversionLogger
						.info("for loop of products Package true--------->" + i);

				int trackNum = productsTrackListPackageTrue.get(i).getIdx();

				CurrentSide = sideNumForRec;
				DataConversionLogger
						.info("track Num and Side Num where Side number is same"
								+ trackNum + "::" + CurrentSide);
				objJSONDB.executeInsertTrackPackageTrue(objProd,
						productsTrackListPackageTrue.get(i), strBatchId,
						trackNum, CurrentSide, prodId);

			}
			sideNumForRec++;

		} else {

			DataConversionLogger.info("In track returing empty data for track");
		}

	}

	public void processRecordingListResponseJSON(Exchange exchange)
			throws JsonParseException, JsonMappingException, IOException, ParseException {

		ObjectMapper mapper = new ObjectMapper();

		

			List<RecordingSongInfo> recSongList = mapper.readValue(exchange
					.getIn().getBody(String.class),
					new TypeReference<List<RecordingSongInfo>>() {
					});
			int len = recSongList.size();
			DataConversionLogger
					.info("Song list Associated with Recordings and  length ---------->"
							+ len);

			String strBatchId = (String) exchange.getIn().getHeader("batchId");
			DataConversionLogger.debug("batch id is for Recordings:::::::::"
					+ strBatchId);
			RecordingsInfo objRecording = (RecordingsInfo) exchange.getProperty("recordingBody");

			if (len >0) {

				/*for (RecordingSongInfo spiTemp : recSongList) {

					DataConversionLogger
							.info("In recordingSong childs:::::::::");
					JSonToDB objJSONDB = new JSonToDB();

					DataConversionLogger
							.debug("DataSource in recordingSongList ::::::"
									+ dataSource);
					objJSONDB.setDataSource(dataSource);

					DataConversionLogger
							.debug("After setting dataSource::::::::");
					objJSONDB.executeInsertRecording(objRecording, strBatchId,
							spiTemp, dataSource);
					DataConversionLogger
							.info("After RecordingSong invoke :::::::::");

				}
								
*/
				//for (RecordingSongInfo spiTemp : recSongList) {

				int SongId=recSongList.get(0).getSong().getId();
				//RecordingSongInfo spiTemp
					DataConversionLogger
							.info("In recordingSong childs:::::::::");
					JSonToDB objJSONDB = new JSonToDB();

					DataConversionLogger
							.debug("DataSource in recordingSongList ::::::"
									+ null);
					objJSONDB.setDataSource(null);

					DataConversionLogger
							.debug("After setting dataSource::::::::");
					objJSONDB.executeInsertRecording(objRecording, strBatchId,
							SongId, null);
					DataConversionLogger
							.info("After RecordingSong invoke :::::::::");

				//}

			} else {

				DataConversionLogger
						.info("RecordingSong List is Empty from API");
			}
		

	}
	
	
	
	public void recordingSongList(Exchange exchange)throws JsonParseException, JsonMappingException, IOException, ParseException
	{
		ObjectMapper mapper = new ObjectMapper();

		List<RecordingSongInfo> recSongList = mapper.readValue(exchange.getIn().getBody(String.class),
				new TypeReference<List<RecordingSongInfo>>() {
				});
		int len = recSongList.size();
		DataConversionLogger
				.info("Song list Associated with Recordings and  length ---------->"
						+ len);
		if (len >0)
		{
			int songId=recSongList.get(0).getSong().getId();
			
			//exchange.getIn().setBody(recSongList);
			exchange.getIn().setHeader("recSongId", songId);
			
		}
		else{
			exchange.getIn().setBody(null);
			//exchange.getIn().setHeader("recSongId", null);
			DataConversionLogger
			.info("RecordingSong List is Empty from API");
		}
	}
	
	public ProductsInfo createProductInfoFromString(String strProdInfo) throws Exception
	{
		return ProductsInfo.fromString(strProdInfo);
	}
	
	
	public RecordingsInfo createRecordingInfoFromString(String strRecordingInfo) throws Exception
	{
		return RecordingsInfo.fromString(strRecordingInfo);
	}
	
	public void processTrackListProductFalse(Exchange exchange)throws JsonParseException, JsonMappingException, IOException, ParseException
	{
	
		ObjectMapper mapper = new ObjectMapper();

		List<TracksInfo> productsTrackList = mapper.readValue(exchange.getIn().getBody(String.class), new TypeReference<List<TracksInfo>>() {});
		if (productsTrackList.size() >0)
		{
			exchange.getIn().setBody(productsTrackList);
		}
		else
		{
			exchange.getIn().setBody(null);
			DataConversionLogger.info("product tracK List is Empty from API");
		}

/*
		if (childLenght != 0) {
		
			ProductsInfo objProd = (ProductsInfo) exchange.getIn().getHeader(
					"productbody");
			String strBatchId = (String) exchange.getIn().getHeader("batchId");
			boolean packagestatus = (Boolean) exchange.getIn().getHeader(
					"packageStatus");
			
			
			for (int i = 0; i < productsTrackList.size(); i++) {

				if (packagestatus == false) {
					
					int trackNum = productsTrackList.get(i).getIdx();
					// finalSideNumber=finalSideNumber+numSide;

					int sideNum = productsTrackList.get(i).getSide();
					
					trackNum=(trackNum==0)?trackNum++:trackNum;
					sideNum= (sideNum==0)?sideNum++:sideNum;
					
					objJSONDB.executeInsertTrack(objProd,
							productsTrackList.get(i), strBatchId, trackNum,
							sideNum);
					

				}

			}
			*/
		}
	
	
public String replaceCommaWithDoller(String sdata)
{
	String strdata="";
	if(sdata!=null)
	{
		if(sdata.contains(","))
		{
			strdata=sdata.replaceAll(",", "\\$");
		}
		else
		{
			strdata=sdata;
		}
	}
	else
	{
		strdata=sdata;
	}
	return strdata;
}
	
	
}
