package com.bmg.esb.capture2rm.beans;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = ",", skipFirstLine = true)
public class ProductApprovalDataBean {
	
	@DataField(pos = 1,trim=true)
	private String batch_id;
	
	@DataField(pos = 2,trim=true)
	private String ADOADD;
	
	@DataField(pos = 3,trim=true)
	private String IM_PRODUCTAPPROVALS_SEQ;
	
	@DataField(pos = 4,trim=true)
	private String ERRORCODE;
	
	@DataField(pos = 5,trim=true)
	private String ERRORCODEI;
	
	@DataField(pos = 6,trim=true)
	private String AIMPORTBLANKS;
	
	@DataField(pos = 7,trim=true)
	private String aProductSite;
	
	@DataField(pos = 8,trim=true)
	private String aProductCode;
	
	@DataField(pos = 9,trim=true)
	private String aApproved;
	
	@DataField(pos = 10,trim=true)
	private String aUpdateICR;
	
	public String getBatch_id() {
		return batch_id;
	}

	public void setBatch_id(String batch_id) {
		this.batch_id = batch_id;
	}

	public String getADOADD() {
		return ADOADD;
	}

	public void setADOADD(String aDOADD) {
		ADOADD = aDOADD;
	}

	public String getIM_PRODUCTAPPROVALS_SEQ() {
		return IM_PRODUCTAPPROVALS_SEQ;
	}

	public void setIM_PRODUCTAPPROVALS_SEQ(String iM_PRODUCTAPPROVALS_SEQ) {
		IM_PRODUCTAPPROVALS_SEQ = iM_PRODUCTAPPROVALS_SEQ;
	}

	public String getERRORCODE() {
		return ERRORCODE;
	}

	public void setERRORCODE(String eRRORCODE) {
		ERRORCODE = eRRORCODE;
	}

	public String getERRORCODEI() {
		return ERRORCODEI;
	}

	public void setERRORCODEI(String eRRORCODEI) {
		ERRORCODEI = eRRORCODEI;
	}

	public String getAIMPORTBLANKS() {
		return AIMPORTBLANKS;
	}

	public void setAIMPORTBLANKS(String aIMPORTBLANKS) {
		AIMPORTBLANKS = aIMPORTBLANKS;
	}

	public String getaProductSite() {
		return aProductSite;
	}

	public void setaProductSite(String aProductSite) {
		this.aProductSite = aProductSite;
	}

	public String getaProductCode() {
		return aProductCode;
	}

	public void setaProductCode(String aProductCode) {
		this.aProductCode = aProductCode;
	}

	public String getaApproved() {
		return aApproved;
	}

	public void setaApproved(String aApproved) {
		this.aApproved = aApproved;
	}

	public String getaUpdateICR() {
		return aUpdateICR;
	}

	public void setaUpdateICR(String aUpdateICR) {
		this.aUpdateICR = aUpdateICR;
	}

	@Override
	public String toString() {
		return "ProductApprovalDataBean [batch_id=" + batch_id + ", ADOADD="
				+ ADOADD + ", IM_PRODUCTAPPROVALS_SEQ="
				+ IM_PRODUCTAPPROVALS_SEQ + ", ERRORCODE=" + ERRORCODE
				+ ", ERRORCODEI=" + ERRORCODEI + ", AIMPORTBLANKS="
				+ AIMPORTBLANKS + ", aProductSite=" + aProductSite
				+ ", aProductCode=" + aProductCode + ", aApproved=" + aApproved
				+ ", aUpdateICR=" + aUpdateICR + "]";
	}

}
