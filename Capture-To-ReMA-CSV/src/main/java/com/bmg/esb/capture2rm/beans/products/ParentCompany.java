
package com.bmg.esb.capture2rm.beans.products;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ParentCompany {

    @JsonProperty("name")
    private String name;
    @JsonProperty("code")
    private String code;
    @JsonProperty("orderCodeSAP")
    private String orderCodeSAP;
    @JsonProperty("orderNameSAP")
    private String orderNameSAP;
    @JsonProperty("isOwnedByBMG")
    private Boolean isOwnedByBMG;
    @JsonProperty("isRepertoireOwner")
    private Boolean isRepertoireOwner;
    @JsonProperty("isContracting")
    private Boolean isContracting;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("created")
    private String created;
    @JsonProperty("modified")
    private String modified;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The code
     */
    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The orderCodeSAP
     */
    @JsonProperty("orderCodeSAP")
    public String getOrderCodeSAP() {
        return orderCodeSAP;
    }

    /**
     * 
     * @param orderCodeSAP
     *     The orderCodeSAP
     */
    @JsonProperty("orderCodeSAP")
    public void setOrderCodeSAP(String orderCodeSAP) {
        this.orderCodeSAP = orderCodeSAP;
    }

    /**
     * 
     * @return
     *     The orderNameSAP
     */
    @JsonProperty("orderNameSAP")
    public String getOrderNameSAP() {
        return orderNameSAP;
    }

    /**
     * 
     * @param orderNameSAP
     *     The orderNameSAP
     */
    @JsonProperty("orderNameSAP")
    public void setOrderNameSAP(String orderNameSAP) {
        this.orderNameSAP = orderNameSAP;
    }

    /**
     * 
     * @return
     *     The isOwnedByBMG
     */
    @JsonProperty("isOwnedByBMG")
    public Boolean getIsOwnedByBMG() {
        return isOwnedByBMG;
    }

    /**
     * 
     * @param isOwnedByBMG
     *     The isOwnedByBMG
     */
    @JsonProperty("isOwnedByBMG")
    public void setIsOwnedByBMG(Boolean isOwnedByBMG) {
        this.isOwnedByBMG = isOwnedByBMG;
    }

    /**
     * 
     * @return
     *     The isRepertoireOwner
     */
    @JsonProperty("isRepertoireOwner")
    public Boolean getIsRepertoireOwner() {
        return isRepertoireOwner;
    }

    /**
     * 
     * @param isRepertoireOwner
     *     The isRepertoireOwner
     */
    @JsonProperty("isRepertoireOwner")
    public void setIsRepertoireOwner(Boolean isRepertoireOwner) {
        this.isRepertoireOwner = isRepertoireOwner;
    }

    /**
     * 
     * @return
     *     The isContracting
     */
    @JsonProperty("isContracting")
    public Boolean getIsContracting() {
        return isContracting;
    }

    /**
     * 
     * @param isContracting
     *     The isContracting
     */
    @JsonProperty("isContracting")
    public void setIsContracting(Boolean isContracting) {
        this.isContracting = isContracting;
    }

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The created
     */
    @JsonProperty("created")
    public String getCreated() {
        return created;
    }

    /**
     * 
     * @param created
     *     The created
     */
    @JsonProperty("created")
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * 
     * @return
     *     The modified
     */
    @JsonProperty("modified")
    public String getModified() {
        return modified;
    }

    /**
     * 
     * @param modified
     *     The modified
     */
    @JsonProperty("modified")
    public void setModified(String modified) {
        this.modified = modified;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
