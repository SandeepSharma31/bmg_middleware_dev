
package com.bmg.esb.capture2rm.beans.product.prodchild;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "id",
    "idx",
    "child"
})
public class ProductChild {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("idx")
    private Integer idx;
    @JsonProperty("child")
    private Child child;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The idx
     */
    @JsonProperty("idx")
    public Integer getIdx() {
        return idx;
    }

    /**
     * 
     * @param idx
     *     The idx
     */
    @JsonProperty("idx")
    public void setIdx(Integer idx) {
        this.idx = idx;
    }

    /**
     * 
     * @return
     *     The child
     */
    @JsonProperty("child")
    public Child getChild() {
        return child;
    }

    /**
     * 
     * @param child
     *     The child
     */
    @JsonProperty("child")
    public void setChild(Child child) {
        this.child = child;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
