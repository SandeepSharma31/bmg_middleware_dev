
package com.bmg.esb.capture2rm.beans.products;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductsInfo {

    @JsonProperty("type")
    private String type;
    @JsonProperty("changeindicator")
    private String changeindicator;
    @JsonProperty("data")
    private Data data;
    // added as a part of MFD - 105
    @JsonProperty("includesDownloadCard")
    private String includesDownloadCard;
    
    public String getIncludesDownloadCard() {
		return includesDownloadCard;
	}

	public void setIncludesDownloadCard(String includesDownloadCard) {
		this.includesDownloadCard = includesDownloadCard;
	}

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The changeindicator
     */
    @JsonProperty("changeindicator")
    public String getChangeindicator() {
        return changeindicator;
    }

    /**
     * 
     * @param changeindicator
     *     The changeindicator
     */
    @JsonProperty("changeindicator")
    public void setChangeindicator(String changeindicator) {
        this.changeindicator = changeindicator;
    }

    /**
     * 
     * @return
     *     The data
     */
    @JsonProperty("data")
    public Data getData() {
        return data;
    }

    /**
     * 
     * @param data
     *     The data
     */
    @JsonProperty("data")
    public void setData(Data data) {
        this.data = data;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
    
    
    public String toString()
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectMapper mapper = new ObjectMapper();

		try 
		{
			mapper.writeValue(baos, this);
		} 
		catch (Exception e) 
		{
			throw new RuntimeException("Unable to serialize ProductsInfo object to a Json-"+e.getMessage());
		}
		return baos.toString();
	}
	
	public static ProductsInfo fromString(String strProductData) throws Exception
	{
		
		ObjectMapper mapper = new ObjectMapper();
		ProductsInfo objProducts = mapper.readValue(strProductData,new TypeReference<ProductsInfo>() {});
		return objProducts;
	}

}
