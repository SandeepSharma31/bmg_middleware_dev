package com.bmg.esb.capture2rm.beans;

import java.util.Date;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = ",", skipFirstLine = true)
public class DataBean {
	
	@DataField(pos = 1,trim=true)
	private String batch_id;
	
	@DataField(pos = 2,trim=true)
	private String ADOADD;
	
	@DataField(pos = 3,trim=true)
	private String IM_PRODUCT_SEQ;
	
	@DataField(pos = 4,trim=true)
	private String ERRORCODE;
	
	@DataField(pos = 5,trim=true)
	private String ERRORCODEI;
	
	@DataField(pos = 6,trim=true)
	private String AIMPORTBLANKS;
	
	@DataField(pos = 7,trim=true)
	private String aSite;
	
	@DataField(pos = 8,trim=true)
	private String aCode;
	
	@DataField(pos = 9,trim=true)
	private String aBarCode;
	
	@DataField(pos = 10,trim=true)
	private String aTitle;
	
	@DataField(pos = 11,trim=true)
	private String aArtist	;
	
	@DataField(pos = 12,trim=true)
	private String aCompany;
	
	@DataField(pos = 13,trim=true)
	private int aComponents;
	
	@DataField(pos = 14,trim=true)
	private int aSides;
	
	@DataField(pos = 15,trim=true)
	private String aTracks;
	
	@DataField(pos = 16,trim=true)
	private String aPriceCategory;
	
	@DataField(pos = 17,trim=true)
	private String aConfiguration;
	
	@DataField(pos = 18,trim=true)
	private Integer aTotalPlayingTime;
	
	@DataField(pos = 19,trim=true)
	private Date aReleaseDate;
	
	@DataField(pos = 20,trim=true)
	private Date aItReleaseDate;
	
	@DataField(pos = 21,trim=true)
	private Date aRereleaseDate;
	
	@DataField(pos = 22,trim=true)
	private Date aDeletionDate;
	
	@DataField(pos = 23,trim=true)
	private String aTVAdvertised;
	
	@DataField(pos = 24,trim=true)
	private String aPayCopyright;
	
	@DataField(pos = 25,trim=true)
	private Integer aCopyrightProductType;
	
	@DataField(pos = 26,trim=true)
	private String aCopyrightCapType;
	
	@DataField(pos = 27,trim=true)
	private Integer aCopyrightCapSongs;
	
	@DataField(pos = 28,trim=true)
	private Float aCopyrightCapValue;
	
	@DataField(pos = 29,trim=true)
	private Float aCopyrightCapPerc;
	
	@DataField(pos = 30,trim=true)
	private Date aCopyrightCapFreezeDate;
	
	@DataField(pos = 31,trim=true)
	private String aSociety;
	
	@DataField(pos = 32,trim=true)
	private String aSocietyNumber;
	
	@DataField(pos = 33,trim=true)
	private Date aTVAdFrom;
	
	@DataField(pos = 34,trim=true)
	private Date aTVAdTo;
	
	@DataField(pos = 35,trim=true)
	private String aInternal;
	
	@DataField(pos = 36,trim=true)
	private Integer aPriceLevel;
	
	@DataField(pos = 37,trim=true)
	private String aTerritory;
	
	@DataField(pos = 38,trim=true)
	private String aLabel;
	
	@DataField(pos = 39,trim=true)
	private String aHoldReserves;
	
	@DataField(pos = 40,trim=true)
	private Integer aCopyrightTracks;
	
	@DataField(pos = 41,trim=true)
	private String aCompilation;
	
	@DataField(pos = 42,trim=true)
	private String aDigitalDownload;
	
	@DataField(pos = 43,trim=true)
	private String aAllowFree;
	
	@DataField(pos = 44,trim=true)
	private String aRoyaltyInfoComplete;
	
	@DataField(pos = 45,trim=true)
	private String aPromo;
	
	@DataField(pos = 46,trim=true)
	private String aLanguage;
	
	@DataField(pos = 47,trim=true)
	private String aType;
	
	@DataField(pos = 48,trim=true)
	private String aRecType;
	
	@DataField(pos = 49,trim=true)
	private String aTech;
	
	@DataField(pos = 50,trim=true)
	private String aStyle;
	
	@DataField(pos = 51,trim=true)
	private String aVocal;
	
	@DataField(pos = 52,trim=true)
	private String aPayCSI;
	
	@DataField(pos = 53,trim=true)
	private String aPaySOCAN;
	
	@DataField(pos = 54,trim=true)
	private String aProjectCode;
	
	@DataField(pos = 55,trim=true)
	private String aGridCode;
	
	@DataField(pos = 56,trim=true)
	private String aExplicitContent;
	
	@DataField(pos = 57,trim=true)
	private String aProductClearedTerrName;
	
	@DataField(pos = 58,trim=true)
	private String aProductExcludedTerrName;
	
	@DataField(pos = 59,trim=true)
	private String aPYear;
	
	@DataField(pos = 60,trim=true)
	private String aPNote;
	
	@DataField(pos = 61,trim=true)
	private String aCYear;
	
	@DataField(pos = 62,trim=true)
	private String aCNote;
	
	@DataField(pos = 63,trim=true)
	private String aCatCoEnabled;
	
	@DataField(pos = 64,trim=true)
	private String aCatCoResend;
	
	@DataField(pos = 65,trim=true)
	private String aAutoCreateAKACodes;
	
	@DataField(pos = 66,trim=true)
	private String aNotes;
	
	@DataField(pos = 67,trim=true)
	private String aExternalCode;
	
	@DataField(pos = 68,trim=true)
	private String aUpdateUserCode;
	
	@DataField(pos = 69,trim=true)
	private String aUpdateTerrBarcodes;
	
	@DataField(pos = 70,trim=true)
	private String aExternalLabelCode;
	
	@DataField(pos = 71,trim=true)
	private String aGenreGroup1;
	
	@DataField(pos = 72,trim=true)
	private String aGenre1;
	
	@DataField(pos = 73,trim=true)
	private String aGenreGroup2;
	
	@DataField(pos = 74,trim=true)
	private String aGenre2;

	
	public String getBatch_id() {
		return batch_id;
	}

	public void setBatch_id(String batch_id) {
		this.batch_id = batch_id;
	}

	public String getADOADD() {
		return ADOADD;
	}

	public void setADOADD(String aDOADD) {
		ADOADD = aDOADD;
	}

	public String getIM_PRODUCT_SEQ() {
		return IM_PRODUCT_SEQ;
	}

	public void setIM_PRODUCT_SEQ(String iM_PRODUCT_SEQ) {
		IM_PRODUCT_SEQ = iM_PRODUCT_SEQ;
	}

	public String getERRORCODE() {
		return ERRORCODE;
	}

	public void setERRORCODE(String eRRORCODE) {
		ERRORCODE = eRRORCODE;
	}

	public String getERRORCODEI() {
		return ERRORCODEI;
	}

	public void setERRORCODEI(String eRRORCODEI) {
		ERRORCODEI = eRRORCODEI;
	}

	public String getAIMPORTBLANKS() {
		return AIMPORTBLANKS;
	}

	public void setAIMPORTBLANKS(String aIMPORTBLANKS) {
		AIMPORTBLANKS = aIMPORTBLANKS;
	}

	public String getaSite() {
		return aSite;
	}

	public void setaSite(String aSite) {
		this.aSite = aSite;
	}

	public String getaCode() {
		return aCode;
	}

	public void setaCode(String aCode) {
		this.aCode = aCode;
	}

	public String getaBarCode() {
		return aBarCode;
	}

	public void setaBarCode(String aBarCode) {
		this.aBarCode = aBarCode;
	}

	public String getaTitle() {
		return aTitle;
	}

	public void setaTitle(String aTitle) {
		this.aTitle = aTitle;
	}

	public String getaArtist() {
		return aArtist;
	}

	public void setaArtist(String aArtist) {
		this.aArtist = aArtist;
	}

	public String getaCompany() {
		return aCompany;
	}

	public void setaCompany(String aCompany) {
		this.aCompany = aCompany;
	}

	public int getaComponents() {
		return aComponents;
	}

	public void setaComponents(int aComponents) {
		this.aComponents = aComponents;
	}

	public int getaSides() {
		return aSides;
	}

	public void setaSides(int aSides) {
		this.aSides = aSides;
	}

	public String getaTracks() {
		return aTracks;
	}

	public void setaTracks(String aTracks) {
		this.aTracks = aTracks;
	}

	public String getaPriceCategory() {
		return aPriceCategory;
	}

	public void setaPriceCategory(String aPriceCategory) {
		this.aPriceCategory = aPriceCategory;
	}

	public String getaConfiguration() {
		return aConfiguration;
	}

	public void setaConfiguration(String aConfiguration) {
		this.aConfiguration = aConfiguration;
	}

	public Integer getaTotalPlayingTime() {
		return aTotalPlayingTime;
	}

	public void setaTotalPlayingTime(Integer aTotalPlayingTime) {
		this.aTotalPlayingTime = aTotalPlayingTime;
	}
	public Date getaReleaseDate() {
		return aReleaseDate;
	}

	public void setaReleaseDate(Date aReleaseDate) {
		this.aReleaseDate = aReleaseDate;
	}

	public Date getaItReleaseDate() {
		return aItReleaseDate;
	}

	public void setaItReleaseDate(Date aItReleaseDate) {
		this.aItReleaseDate = aItReleaseDate;
	}

	public Date getaRereleaseDate() {
		return aRereleaseDate;
	}

	public void setaRereleaseDate(Date aRereleaseDate) {
		this.aRereleaseDate = aRereleaseDate;
	}

	public Date getaDeletionDate() {
		return aDeletionDate;
	}

	public void setaDeletionDate(Date aDeletionDate) {
		this.aDeletionDate = aDeletionDate;
	}
	
	public String getaTVAdvertised() {
		return aTVAdvertised;
	}

	public void setaTVAdvertised(String aTVAdvertised) {
		this.aTVAdvertised = aTVAdvertised;
	}

	public String getaPayCopyright() {
		return aPayCopyright;
	}

	public void setaPayCopyright(String aPayCopyright) {
		this.aPayCopyright = aPayCopyright;
	}

	public Integer getaCopyrightProductType() {
		return aCopyrightProductType;
	}

	public void setaCopyrightProductType(Integer aCopyrightProductType) {
		this.aCopyrightProductType = aCopyrightProductType;
	}

	public String getaCopyrightCapType() {
		return aCopyrightCapType;
	}

	public void setaCopyrightCapType(String aCopyrightCapType) {
		this.aCopyrightCapType = aCopyrightCapType;
	}

	public Integer getaCopyrightCapSongs() {
		return aCopyrightCapSongs;
	}

	public void setaCopyrightCapSongs(Integer aCopyrightCapSongs) {
		this.aCopyrightCapSongs = aCopyrightCapSongs;
	}

	public Float getaCopyrightCapValue() {
		return aCopyrightCapValue;
	}

	public void setaCopyrightCapValue(Float aCopyrightCapValue) {
		this.aCopyrightCapValue = aCopyrightCapValue;
	}

	public Float getaCopyrightCapPerc() {
		return aCopyrightCapPerc;
	}

	public void setaCopyrightCapPerc(Float aCopyrightCapPerc) {
		this.aCopyrightCapPerc = aCopyrightCapPerc;
	}

	public Date getaCopyrightCapFreezeDate() {
		return aCopyrightCapFreezeDate;
	}

	public void setaCopyrightCapFreezeDate(Date aCopyrightCapFreezeDate) {
		this.aCopyrightCapFreezeDate = aCopyrightCapFreezeDate;
	}

	public String getaSociety() {
		return aSociety;
	}

	public void setaSociety(String aSociety) {
		this.aSociety = aSociety;
	}

	public String getaSocietyNumber() {
		return aSocietyNumber;
	}

	public void setaSocietyNumber(String aSocietyNumber) {
		this.aSocietyNumber = aSocietyNumber;
	}

	public Date getaTVAdFrom() {
		return aTVAdFrom;
	}

	public void setaTVAdFrom(Date aTVAdFrom) {
		this.aTVAdFrom = aTVAdFrom;
	}

	public Date getaTVAdTo() {
		return aTVAdTo;
	}

	public void setaTVAdTo(Date aTVAdTo) {
		this.aTVAdTo = aTVAdTo;
	}

	public String getaInternal() {
		return aInternal;
	}

	public void setaInternal(String aInternal) {
		this.aInternal = aInternal;
	}

	public Integer getaPriceLevel() {
		return aPriceLevel;
	}

	public void setaPriceLevel(Integer aPriceLevel) {
		this.aPriceLevel = aPriceLevel;
	}

	public String getaTerritory() {
		return aTerritory;
	}

	public void setaTerritory(String aTerritory) {
		this.aTerritory = aTerritory;
	}

	public String getaLabel() {
		return aLabel;
	}

	public void setaLabel(String aLabel) {
		this.aLabel = aLabel;
	}

	public String getaHoldReserves() {
		return aHoldReserves;
	}

	public void setaHoldReserves(String aHoldReserves) {
		this.aHoldReserves = aHoldReserves;
	}

	public Integer getaCopyrightTracks() {
		return aCopyrightTracks;
	}

	public void setaCopyrightTracks(Integer aCopyrightTracks) {
		this.aCopyrightTracks = aCopyrightTracks;
	}

	public String getaCompilation() {
		return aCompilation;
	}

	public void setaCompilation(String aCompilation) {
		this.aCompilation = aCompilation;
	}

	public String getaDigitalDownload() {
		return aDigitalDownload;
	}

	public void setaDigitalDownload(String aDigitalDownload) {
		this.aDigitalDownload = aDigitalDownload;
	}

	public String getaAllowFree() {
		return aAllowFree;
	}

	public void setaAllowFree(String aAllowFree) {
		this.aAllowFree = aAllowFree;
	}

	public String getaRoyaltyInfoComplete() {
		return aRoyaltyInfoComplete;
	}

	public void setaRoyaltyInfoComplete(String aRoyaltyInfoComplete) {
		this.aRoyaltyInfoComplete = aRoyaltyInfoComplete;
	}

	public String getaPromo() {
		return aPromo;
	}

	public void setaPromo(String aPromo) {
		this.aPromo = aPromo;
	}

	public String getaLanguage() {
		return aLanguage;
	}

	public void setaLanguage(String aLanguage) {
		this.aLanguage = aLanguage;
	}

	public String getaType() {
		return aType;
	}

	public void setaType(String aType) {
		this.aType = aType;
	}

	public String getaRecType() {
		return aRecType;
	}

	public void setaRecType(String aRecType) {
		this.aRecType = aRecType;
	}

	public String getaTech() {
		return aTech;
	}

	public void setaTech(String aTech) {
		this.aTech = aTech;
	}

	public String getaStyle() {
		return aStyle;
	}

	public void setaStyle(String aStyle) {
		this.aStyle = aStyle;
	}

	public String getaVocal() {
		return aVocal;
	}

	public void setaVocal(String aVocal) {
		this.aVocal = aVocal;
	}

	public String getaPayCSI() {
		return aPayCSI;
	}

	public void setaPayCSI(String aPayCSI) {
		this.aPayCSI = aPayCSI;
	}

	public String getaPaySOCAN() {
		return aPaySOCAN;
	}

	public void setaPaySOCAN(String aPaySOCAN) {
		this.aPaySOCAN = aPaySOCAN;
	}

	public String getaProjectCode() {
		return aProjectCode;
	}

	public void setaProjectCode(String aProjectCode) {
		this.aProjectCode = aProjectCode;
	}

	public String getaGridCode() {
		return aGridCode;
	}

	public void setaGridCode(String aGridCode) {
		this.aGridCode = aGridCode;
	}

	public String getaExplicitContent() {
		return aExplicitContent;
	}

	public void setaExplicitContent(String aExplicitContent) {
		this.aExplicitContent = aExplicitContent;
	}

	public String getaProductClearedTerrName() {
		return aProductClearedTerrName;
	}

	public void setaProductClearedTerrName(String aProductClearedTerrName) {
		this.aProductClearedTerrName = aProductClearedTerrName;
	}

	public String getaProductExcludedTerrName() {
		return aProductExcludedTerrName;
	}

	public void setaProductExcludedTerrName(String aProductExcludedTerrName) {
		this.aProductExcludedTerrName = aProductExcludedTerrName;
	}

	public String getaPYear() {
		return aPYear;
	}

	public void setaPYear(String aPYear) {
		this.aPYear = aPYear;
	}

	public String getaPNote() {
		return aPNote;
	}

	public void setaPNote(String aPNote) {
		this.aPNote = aPNote;
	}

	public String getaCYear() {
		return aCYear;
	}

	public void setaCYear(String aCYear) {
		this.aCYear = aCYear;
	}

	public String getaCNote() {
		return aCNote;
	}

	public void setaCNote(String aCNote) {
		this.aCNote = aCNote;
	}

	public String getaCatCoEnabled() {
		return aCatCoEnabled;
	}

	public void setaCatCoEnabled(String aCatCoEnabled) {
		this.aCatCoEnabled = aCatCoEnabled;
	}

	public String getaCatCoResend() {
		return aCatCoResend;
	}

	public void setaCatCoResend(String aCatCoResend) {
		this.aCatCoResend = aCatCoResend;
	}

	public String getaAutoCreateAKACodes() {
		return aAutoCreateAKACodes;
	}

	public void setaAutoCreateAKACodes(String aAutoCreateAKACodes) {
		this.aAutoCreateAKACodes = aAutoCreateAKACodes;
	}

	public String getaNotes() {
		return aNotes;
	}

	public void setaNotes(String aNotes) {
		this.aNotes = aNotes;
	}

	public String getaExternalCode() {
		return aExternalCode;
	}

	public void setaExternalCode(String aExternalCode) {
		this.aExternalCode = aExternalCode;
	}

	public String getaUpdateUserCode() {
		return aUpdateUserCode;
	}

	public void setaUpdateUserCode(String aUpdateUserCode) {
		this.aUpdateUserCode = aUpdateUserCode;
	}

	public String getaUpdateTerrBarcodes() {
		return aUpdateTerrBarcodes;
	}

	public void setaUpdateTerrBarcodes(String aUpdateTerrBarcodes) {
		this.aUpdateTerrBarcodes = aUpdateTerrBarcodes;
	}

	public String getaExternalLabelCode() {
		return aExternalLabelCode;
	}

	public void setaExternalLabelCode(String aExternalLabelCode) {
		this.aExternalLabelCode = aExternalLabelCode;
	}

	public String getaGenreGroup1() {
		return aGenreGroup1;
	}

	public void setaGenreGroup1(String aGenreGroup1) {
		this.aGenreGroup1 = aGenreGroup1;
	}

	public String getaGenre1() {
		return aGenre1;
	}

	public void setaGenre1(String aGenre1) {
		this.aGenre1 = aGenre1;
	}

	public String getaGenreGroup2() {
		return aGenreGroup2;
	}

	public void setaGenreGroup2(String aGenreGroup2) {
		this.aGenreGroup2 = aGenreGroup2;
	}

	public String getaGenre2() {
		return aGenre2;
	}

	public void setaGenre2(String aGenre2) {
		this.aGenre2 = aGenre2;
	}

	@Override
	public String toString() {
		return "DataBean [batch_id=" + batch_id + ", ADOADD=" + ADOADD
				+ ", IM_PRODUCT_SEQ=" + IM_PRODUCT_SEQ + ", ERRORCODE="
				+ ERRORCODE + ", ERRORCODEI=" + ERRORCODEI + ", AIMPORTBLANKS="
				+ AIMPORTBLANKS + ", aSite=" + aSite + ", aCode=" + aCode
				+ ", aBarCode=" + aBarCode + ", aTitle=" + aTitle
				+ ", aArtist=" + aArtist + ", aCompany=" + aCompany
				+ ", aComponents=" + aComponents + ", aSides=" + aSides
				+ ", aTracks=" + aTracks + ", aPriceCategory=" + aPriceCategory
				+ ", aConfiguration=" + aConfiguration + ", aTotalPlayingTime="
				+ aTotalPlayingTime + ", aReleaseDate=" + aReleaseDate
				+ ", aItReleaseDate=" + aItReleaseDate + ", aRereleaseDate="
				+ aRereleaseDate + ", aDeletionDate=" + aDeletionDate
				+ ", aTVAdvertised=" + aTVAdvertised + ", aPayCopyright="
				+ aPayCopyright + ", aCopyrightProductType="
				+ aCopyrightProductType + ", aCopyrightCapType="
				+ aCopyrightCapType + ", aCopyrightCapSongs="
				+ aCopyrightCapSongs + ", aCopyrightCapValue="
				+ aCopyrightCapValue + ", aCopyrightCapPerc="
				+ aCopyrightCapPerc + ", aCopyrightCapFreezeDate="
				+ aCopyrightCapFreezeDate + ", aSociety=" + aSociety
				+ ", aSocietyNumber=" + aSocietyNumber + ", aTVAdFrom="
				+ aTVAdFrom + ", aTVAdTo=" + aTVAdTo + ", aInternal="
				+ aInternal + ", aPriceLevel=" + aPriceLevel + ", aTerritory="
				+ aTerritory + ", aLabel=" + aLabel + ", aHoldReserves="
				+ aHoldReserves + ", aCopyrightTracks=" + aCopyrightTracks
				+ ", aCompilation=" + aCompilation + ", aDigitalDownload="
				+ aDigitalDownload + ", aAllowFree=" + aAllowFree
				+ ", aRoyaltyInfoComplete=" + aRoyaltyInfoComplete
				+ ", aPromo=" + aPromo + ", aLanguage=" + aLanguage
				+ ", aType=" + aType + ", aRecType=" + aRecType + ", aTech="
				+ aTech + ", aStyle=" + aStyle + ", aVocal=" + aVocal
				+ ", aPayCSI=" + aPayCSI + ", aPaySOCAN=" + aPaySOCAN
				+ ", aProjectCode=" + aProjectCode + ", aGridCode=" + aGridCode
				+ ", aExplicitContent=" + aExplicitContent
				+ ", aProductClearedTerrName=" + aProductClearedTerrName
				+ ", aProductExcludedTerrName=" + aProductExcludedTerrName
				+ ", aPYear=" + aPYear + ", aPNote=" + aPNote + ", aCYear="
				+ aCYear + ", aCNote=" + aCNote + ", aCatCoEnabled="
				+ aCatCoEnabled + ", aCatCoResend=" + aCatCoResend
				+ ", aAutoCreateAKACodes=" + aAutoCreateAKACodes + ", aNotes="
				+ aNotes + ", aExternalCode=" + aExternalCode
				+ ", aUpdateUserCode=" + aUpdateUserCode
				+ ", aUpdateTerrBarcodes=" + aUpdateTerrBarcodes
				+ ", aExternalLabelCode=" + aExternalLabelCode
				+ ", aGenreGroup1=" + aGenreGroup1 + ", aGenre1=" + aGenre1
				+ ", aGenreGroup2=" + aGenreGroup2 + ", aGenre2=" + aGenre2
				+ "]";
	}

	/*@Override
	public String toString() {
		return "DataBean [batch_id=" + batch_id + ", ADOADD=" + ADOADD
				+ ", IM_PRODUCT_SEQ=" + IM_PRODUCT_SEQ + ", ERRORCODE="
				+ ERRORCODE + ", ERRORCODEI=" + ERRORCODEI + ", AIMPORTBLANKS="
				+ AIMPORTBLANKS + ", aSite=" + aSite + ", aCode=" + aCode
				+ ", aBarCode=" + aBarCode + ", aTitle=" + aTitle
				+ ", aArtist=" + aArtist + ", aCompany=" + aCompany
				+ ", aComponents=" + aComponents + ", aSides=" + aSides
				+ ", aTracks=" + aTracks + ", aPriceCategory=" + aPriceCategory
				+ ", aConfiguration=" + aConfiguration + ", aTotalPlayingTime="
				+ aTotalPlayingTime + ", aReleaseDate=" + aReleaseDate
				+ ", aItReleaseDate=" + aItReleaseDate + ", aRereleaseDate="
				+ aRereleaseDate + ", aDeletionDate=" + aDeletionDate
				+ ", aTVAdvertised=" + aTVAdvertised + ", aPayCopyright="
				+ aPayCopyright + ", aCopyrightProductType="
				+ aCopyrightProductType + ", aCopyrightCapType="
				+ aCopyrightCapType + ", aCopyrightCapSongs="
				+ aCopyrightCapSongs + ", aCopyrightCapValue="
				+ aCopyrightCapValue + ", aCopyrightCapPerc="
				+ aCopyrightCapPerc + ", aCopyrightCapFreezeDate="
				+ aCopyrightCapFreezeDate + ", aSociety=" + aSociety
				+ ", aSocietyNumber=" + aSocietyNumber + ", aTVAdFrom="
				+ aTVAdFrom + ", aTVAdTo=" + aTVAdTo + ", aInternal="
				+ aInternal + ", aPriceLevel=" + aPriceLevel + ", aTerritory="
				+ aTerritory + ", aLabel=" + aLabel + ", aHoldReserves="
				+ aHoldReserves + ", aCopyrightTracks=" + aCopyrightTracks
				+ ", aCompilation=" + aCompilation + ", aDigitalDownload="
				+ aDigitalDownload + ", aAllowFree=" + aAllowFree
				+ ", aRoyaltyInfoComplete=" + aRoyaltyInfoComplete
				+ ", aPromo=" + aPromo + ", aLanguage=" + aLanguage
				+ ", aType=" + aType + ", aRecType=" + aRecType + ", aTech="
				+ aTech + ", aStyle=" + aStyle + ", aVocal=" + aVocal
				+ ", aPayCSI=" + aPayCSI + ", aPaySOCAN=" + aPaySOCAN
				+ ", aProjectCode=" + aProjectCode + ", aGridCode=" + aGridCode
				+ ", aExplicitContent=" + aExplicitContent
				+ ", aProductClearedTerrName=" + aProductClearedTerrName
				+ ", aProductExcludedTerrName=" + aProductExcludedTerrName
				+ ", aPYear=" + aPYear + ", aPNote=" + aPNote + ", aCYear="
				+ aCYear + ", aCNote=" + aCNote + ", aCatCoEnabled="
				+ aCatCoEnabled + ", aCatCoResend=" + aCatCoResend
				+ ", aAutoCreateAKACodes=" + aAutoCreateAKACodes + ", aNotes="
				+ aNotes + ", aExternalCode=" + aExternalCode
				+ ", aUpdateUserCode=" + aUpdateUserCode
				+ ", aUpdateTerrBarcodes=" + aUpdateTerrBarcodes
				+ ", aExternalLabelCode=" + aExternalLabelCode
				+ ", aGenreGroup1=" + aGenreGroup1 + ", aGenre1=" + aGenre1
				+ ", aGenreGroup2=" + aGenreGroup2 + ", aGenre2=" + aGenre2
				+ ", getBatch_id()=" + getBatch_id() + ", getADOADD()="
				+ getADOADD() + ", getIM_PRODUCT_SEQ()=" + getIM_PRODUCT_SEQ()
				+ ", getERRORCODE()=" + getERRORCODE() + ", getERRORCODEI()="
				+ getERRORCODEI() + ", getAIMPORTBLANKS()="
				+ getAIMPORTBLANKS() + ", getaSite()=" + getaSite()
				+ ", getaCode()=" + getaCode() + ", getaBarCode()="
				+ getaBarCode() + ", getaTitle()=" + getaTitle()
				+ ", getaArtist()=" + getaArtist() + ", getaCompany()="
				+ getaCompany() + ", getaComponents()=" + getaComponents()
				+ ", getaSides()=" + getaSides() + ", getaTracks()="
				+ getaTracks() + ", getaPriceCategory()=" + getaPriceCategory()
				+ ", getaConfiguration()=" + getaConfiguration()
				+ ", getaTotalPlayingTime()=" + getaTotalPlayingTime()
				+ ", getaReleaseDate()=" + getaReleaseDate()
				+ ", getaItReleaseDate()=" + getaItReleaseDate()
				+ ", getaRereleaseDate()=" + getaRereleaseDate()
				+ ", getaDeletionDate()=" + getaDeletionDate()
				+ ", getaTVAdvertised()=" + getaTVAdvertised()
				+ ", getaPayCopyright()=" + getaPayCopyright()
				+ ", getaCopyrightProductType()=" + getaCopyrightProductType()
				+ ", getaCopyrightCapType()=" + getaCopyrightCapType()
				+ ", getaCopyrightCapSongs()=" + getaCopyrightCapSongs()
				+ ", getaCopyrightCapValue()=" + getaCopyrightCapValue()
				+ ", getaCopyrightCapPerc()=" + getaCopyrightCapPerc()
				+ ", getaCopyrightCapFreezeDate()="
				+ getaCopyrightCapFreezeDate() + ", getaSociety()="
				+ getaSociety() + ", getaSocietyNumber()="
				+ getaSocietyNumber() + ", getaTVAdFrom()=" + getaTVAdFrom()
				+ ", getaTVAdTo()=" + getaTVAdTo() + ", getaInternal()="
				+ getaInternal() + ", getaPriceLevel()=" + getaPriceLevel()
				+ ", getaTerritory()=" + getaTerritory() + ", getaLabel()="
				+ getaLabel() + ", getaHoldReserves()=" + getaHoldReserves()
				+ ", getaCopyrightTracks()=" + getaCopyrightTracks()
				+ ", getaCompilation()=" + getaCompilation()
				+ ", getaDigitalDownload()=" + getaDigitalDownload()
				+ ", getaAllowFree()=" + getaAllowFree()
				+ ", getaRoyaltyInfoComplete()=" + getaRoyaltyInfoComplete()
				+ ", getaPromo()=" + getaPromo() + ", getaLanguage()="
				+ getaLanguage() + ", getaType()=" + getaType()
				+ ", getaRecType()=" + getaRecType() + ", getaTech()="
				+ getaTech() + ", getaStyle()=" + getaStyle()
				+ ", getaVocal()=" + getaVocal() + ", getaPayCSI()="
				+ getaPayCSI() + ", getaPaySOCAN()=" + getaPaySOCAN()
				+ ", getaProjectCode()=" + getaProjectCode()
				+ ", getaGridCode()=" + getaGridCode()
				+ ", getaExplicitContent()=" + getaExplicitContent()
				+ ", getaProductClearedTerrName()="
				+ getaProductClearedTerrName()
				+ ", getaProductExcludedTerrName()="
				+ getaProductExcludedTerrName() + ", getaPYear()="
				+ getaPYear() + ", getaPNote()=" + getaPNote()
				+ ", getaCYear()=" + getaCYear() + ", getaCNote()="
				+ getaCNote() + ", getaCatCoEnabled()=" + getaCatCoEnabled()
				+ ", getaCatCoResend()=" + getaCatCoResend()
				+ ", getaAutoCreateAKACodes()=" + getaAutoCreateAKACodes()
				+ ", getaNotes()=" + getaNotes() + ", getaExternalCode()="
				+ getaExternalCode() + ", getaUpdateUserCode()="
				+ getaUpdateUserCode() + ", getaUpdateTerrBarcodes()="
				+ getaUpdateTerrBarcodes() + ", getaExternalLabelCode()="
				+ getaExternalLabelCode() + ", getaGenreGroup1()="
				+ getaGenreGroup1() + ", getaGenre1()=" + getaGenre1()
				+ ", getaGenreGroup2()=" + getaGenreGroup2()
				+ ", getaGenre2()=" + getaGenre2() + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

			*/
	
		
}