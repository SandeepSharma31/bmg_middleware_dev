package com.bmg.esb.capture2rm.transformer;

import java.sql.DataTruncation;
import java.sql.Types;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.sql.DataSource;

import org.apache.camel.Header;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;





//import com.bmg.esb.capture2rm.beans.PublisherInfo;
//import com.bmg.esb.capture2rm.beans.PackageTrueProductsInfo;
//import com.bmg.esb.capture2rm.beans.ProductsInfo_old;
//import com.bmg.esb.capture2rm.beans.PublisherInfo;
//import com.bmg.esb.capture2rm.beans.RecordingsInfo;
//import com.bmg.esb.capture2rm.beans.SongPublisherInfo;
//import com.bmg.esb.capture2rm.beans.SongsInfo;
//import com.bmg.esb.capture2rm.beans.TrackInfo;
import com.bmg.esb.capture2rm.beans.products.ProductsInfo;
import com.bmg.esb.capture2rm.beans.products.child.ProductChild;
import com.bmg.esb.capture2rm.beans.publisher.PublisherInfo;
import com.bmg.esb.capture2rm.beans.recordings.RecordingsInfo;
import com.bmg.esb.capture2rm.beans.recordings.song.RecordingSongInfo;
import com.bmg.esb.capture2rm.beans.songpublisher.SongPublisherInfo;
import com.bmg.esb.capture2rm.beans.songs.SongsInfo;
import com.bmg.esb.capture2rm.beans.tracks.TracksInfo;
import com.bmg.esb.capture2rm.utility.Mailer;

public class JSonToDB {
	private DataSource dataSource;

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	JdbcTemplate template;
	private static final Logger JSonToDBLogger = LoggerFactory
			.getLogger(JSonToDB.class);

	public String executeInsertSong(SongsInfo objsong,
			@Header("batchId") String songBatchID,
			@Header("songId") String songId) {

			template = new JdbcTemplate(this.dataSource);

			String codesong = objsong.getData().getSite().getCode() != null ? objsong
					.getData().getSite().getCode()
					: " ";
			String batch_id = songBatchID;
			String composerName = objsong.getData().getComposerName() != null ? objsong
					.getData().getComposerName() : " ";
			String lyricist = " ";
			String publicDomain = objsong.getData().getIsPublicDomain() == false ? "N"
					: "Y";
			String genSoundRec = "N";// objsong.getData().isGenerateSoundRec()
										// == false ? "N" : "Y";
			String iswcvalue = objsong.getData().getIswc() != null ? objsong
					.getData().getIswc() : null;
			String iswc = objsong.getData() != null ? iswcvalue : null;
			String title = objsong.getData() != null ? objsong.getData()
					.getTitle() : null;
			int songidv = objsong.getData().getId() != 0 ? objsong.getData()
					.getId() : 0;
			int id = objsong.getData() != null ? songidv : 0;

			final String sqlInsert = "INSERT INTO MDG_SONG(batch_id,aSite,aTitle,aComposer,aISWC,aPublicDomain,aGenerateSoundRec,aExternalCode,aLyricist) VALUES (?,?,?,?,?,?,?,?,?)";

			JSonToDBLogger.debug("Query String for Song " + sqlInsert);
			Object[] paramsInsert = new Object[] { batch_id, codesong, title,
					composerName, iswc, publicDomain, genSoundRec,
					"BMGS_" + id, lyricist };
			JSonToDBLogger.debug("ParamData for Song length "
					+ paramsInsert.length);

			int[] typesInsertSong = new int[] { Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };

			int countSong = template.update(sqlInsert, paramsInsert,
					typesInsertSong);
			JSonToDBLogger
					.info("Records Inserted to Song table successfully,number of records are"
							+ countSong);

		
		return "Inserted";

	}

	// insert into SongPublisher table
	public String executeInsertSongPublisher(SongPublisherInfo objsongpub,
			String songPubBatchID, SongsInfo objsong, DataSource dataSource) {

		JSonToDBLogger
				.debug("In SongPublisher method for inserting into table");
		JSonToDBLogger.debug("dataSource value in method $$$$$$" + dataSource);
		template = new JdbcTemplate(dataSource);

		

			String codeSong = objsong.getData().getSite().getCode() != null ? objsong
					.getData().getSite().getCode()
					: " ";
			String songpubCountry = objsongpub.getCountry() != null ? objsongpub
					.getCountry().getCode() : " ";
			String batch_id = songPubBatchID;
			int pubid = objsongpub.getPublisher() != null ? objsongpub
					.getPublisher().getId() : 0;

			int SongCodeAuto = 0;
			// JSonToDBLogger.info("After intialising ::::::");

			String pubcode = objsongpub.getPublisher().getSite().getCode() != null ? objsongpub
					.getPublisher().getSite().getCode()
					: "NA";
			String updateShare = "N";
			final String sqlInsertSongpub =

			"INSERT INTO MDG_SONGPUB(batch_id,aSongSite,aSongCode,aTerr,aPubSite,aPubCode,aShare,aUpdateLicenseShare,aExternalSongCode,aExternalPubCode) VALUES (?,?,?,?,?,?,?,?,?,?)";

			int[] typesInsertSongpub = new int[] { Types.VARCHAR,
					Types.VARCHAR, Types.INTEGER,Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR, Types.DOUBLE, Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR };

			Object[] paramsInsertSongpub = new Object[] { batch_id, codeSong,SongCodeAuto,
					songpubCountry, songpubCountry, pubcode,
					objsongpub.getShare(), updateShare,
					"BMGS_" + objsong.getData().getId(), "BMGPB_" + pubid };

			int count = template.update(sqlInsertSongpub, paramsInsertSongpub,
					typesInsertSongpub);

			JSonToDBLogger
					.info("Records Inserted to SongPublisher table successfully and number of records are::"
							+ count);
		

		return "Inserted";

	}

	public String executeInsertProduct(ProductsInfo objprd,
			@Header("batchId") String prodBatchID) throws ParseException {

		template = new JdbcTemplate(dataSource);

	try{
			
		String batch_id = prodBatchID;
			String carriercheck;
			
			JSonToDBLogger.info("In ExecuteProduct Method product Package false>>>>>>>>>>>>>>>>>>>");
			

			String code = objprd.getData().getSite().getCode() != null ? objprd
					.getData().getSite().getCode() : " ";
					JSonToDBLogger.info("After code accept in product Package false>>>>>>>>>>>>>>>>>>>");
					
					if((objprd.getData().getFormat().getProductCarrierType().getCarrierName()!=null)&&(objprd.getData().getFormat().getProductCarrierType().getCarrierName().equalsIgnoreCase("Unknown")))
							{
						 carriercheck = null;
							}
					else{
						carriercheck=objprd.getData().getFormat().getProductCarrierType().getCarrierName()!=null?objprd.getData().getFormat().getProductCarrierType().getCarrierName():null;
									
						
					}
					JSonToDBLogger.info("After Carrier TypeName in product Package false>>>>>>>>>>>>>>>>>>>");
					
				String formatcarrierName=carriercheck;
		//	int labelid = objprd.getData().getLabel() != null ? objprd.getData().getLabel().getId() : 0;
			String formatAudioVedio = objprd.getData().getFormat() != null ? (objprd
					.getData().getFormat().getAudioVisual().startsWith("A") ? "A"
					: "V")
					: null;
			String parentaladvLyrics =objprd.getData()
					.getParentalAdvisoryLyricsIndicator()!=null?( objprd.getData()
					.getParentalAdvisoryLyricsIndicator().startsWith("N") ? "N"
					: "Y"):null;
			String digiDownload =objprd.getData().getFormat().getIsPhysical()!=null?( objprd.getData().getFormat().getIsPhysical() == true ? "Y"
					: "N"):null;
			String compilationind =objprd.getData().getIsCompilation()!=null? (objprd.getData().getIsCompilation() == false ? "N"
					: "Y"):null;
			String promo =objprd.getData().getIsPromo()!=null?( objprd.getData().getIsPromo() == false ? "N" : "Y"):null;
			String catCoEnabled = "N";
			String catCoResend = "N";
			String autoCreateAKA = "Y";
			String updateuserCode = "Y";
			String updateTerrBarcode = "N";
			Integer components = 1;
			int sidenumber = objprd.getData().getFormat().getNumberOfSides()!=null?objprd.getData().getFormat().getNumberOfSides():null;
			int tracknumber = objprd.getData().getNumberOfTracks()!=null?objprd.getData().getNumberOfTracks():null;

			String strdate = objprd.getData().getReleaseDate()!=null?objprd.getData().getReleaseDate():"1111-01-01T11:11:11";
					
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			java.util.Date releaseDate = df.parse(strdate);
			
			JSonToDBLogger.info("After releaseDate in product Package false>>>>>>>>>>>>>>>>>>>");
			
			String company =objprd.getData()
					.getRepertoireOwnerCompany()!=null? objprd.getData()
					.getRepertoireOwnerCompany().getCode():null;
					JSonToDBLogger.info("After company in product Package false>>>>>>>>>>>>>>>>>>>");
			String labelId = objprd
					.getData().getLabel()!=null?Integer
					.toString(objprd
							.getData().getLabel().getId()	):null;
					JSonToDBLogger.info("After LabelId in product Package false>>>>>>>>>>>>>>>>>>>");
			String barcode =objprd.getData().getBarcode()!=null?objprd.getData().getBarcode():null;

			String prodCode = objprd.getData().getProductCode() != null ? objprd
					.getData().getProductCode() : barcode;
					
			String ExternalCode = objprd.getData().getId()!=null?"BMGP_" + objprd.getData().getId():null;
			JSonToDBLogger.info("before grid in product Package false>>>>>>>>>>>>>>>>>>>");
			String grid=objprd.getData().getGrId()!=null?objprd.getData().getGrId():null;
			JSonToDBLogger.info("After grid in product Package false>>>>>>>>>>>>>>>>>>>");
			String title= objprd.getData().getProductTitle()!=null? objprd.getData().getProductTitle():null;
			String duration=objprd.getData().getDuration()!=null?Integer.toString(objprd.getData().getDuration()):null;
			String artistName=objprd.getData().getArtistName()!=null?objprd.getData().getArtistName():"NA";
			String cpYear=objprd.getData().getCopyrightYear()!=null?Integer.toString(objprd.getData().getCopyrightYear()):null;
			String cpNotice=objprd.getData().getCopyrightNotice()!=null?objprd.getData().getCopyrightNotice():null;
			String artworkYr=objprd.getData().getArtworkYear()!=null?Integer.toString(objprd.getData().getArtworkYear()):null;
			String artworkNotice=objprd.getData().getArtworkNotice()!=null?objprd.getData().getArtworkNotice():null;
			JSonToDBLogger
					.info("Set all values in Product Table where ::");
							
			final String sqlInsertProduct =

			"INSERT INTO MDG_PRODUCT(batch_id,aSite,aCode,aBarCode,aTitle,aArtist,aCompany,aComponents,aSides,aTracks,aConfiguration,aTotalPlayingTime,aIntReleaseDate,"
					+ "aCopyrightProductType,aLabel,aCompilation,aDigitalDownload,aPromo,"
					+ "aType,aRecType,aTech,aGridCode,aExplicitContent,aPYear,aPNote,aCYear,aCNote,aCatCoEnabled,aCatCoResend,aAutoCreateAKACodes,aExternalCode,"
					+ "aUpdateUserCode,aUpdateTerrBarcodes,aExternalLabelCode) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			Object[] paramsInsert = new Object[] { batch_id, code, prodCode,
					barcode,title,
					artistName, company, components,
					sidenumber, tracknumber, formatcarrierName,
					duration,
					(new java.sql.Date(releaseDate.getTime())), 0, labelId,
					compilationind, digiDownload, promo, formatAudioVedio, "S",
					"D", grid, parentaladvLyrics,cpYear,cpNotice,artworkYr,
					artworkNotice, catCoEnabled,
					catCoResend, autoCreateAKA, ExternalCode, updateuserCode,
					updateTerrBarcode, labelId };

			int[] typesInsertProduct = new int[] { Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER,
					Types.INTEGER, Types.VARCHAR, Types.INTEGER, Types.DATE,
					Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR };

			int count = template.update(sqlInsertProduct, paramsInsert,
					typesInsertProduct);

			JSonToDB objJsondb = new JSonToDB();

			String prodcode = objprd.getData().getProductCode() != null ? objprd
					.getData().getProductCode() : barcode;

			//if (prodcode != null) {
				JSonToDBLogger.debug("prodcode  and prodsiteCode" + prodcode,
						code);

				objJsondb.executeInsertProductApproval(prodBatchID, code,
						prodcode, template);

				JSonToDBLogger.info("Records Inserted to ProductsApproval Table successfully with number of records");
				List altcode = objprd.getData().getAltProductCodes();
				if (altcode != null && altcode.isEmpty() == false) {
					objJsondb.executeInsertProductAKA(prodBatchID, code,
							prodcode, objprd.getChangeindicator(), altcode,
							template);

					JSonToDBLogger
							.info("Records Inserted to ProductAKA Table successfully with number of records");
				} else {
					JSonToDBLogger
							.info("Records not Inserted for ProductAKA Table as AltProdCode is null");
				}
	
		
	

			/*} else {

				JSonToDBLogger
						.info("No records for ProdeuctApproval and ProductAKA Table as ProductCode is null");
			}*/

			JSonToDBLogger
					.info("Records Inserted to Product table successfully with number of records"
							+ count);
			
			}catch(Exception ex)
			{
				JSonToDBLogger
				.info("Exception in product package false$$$$$$$$"+ex);
			}

		return "Inserted";

	}

	public String executeInsertProductApproval(String prodapprBatchID,
			String code, String prodcode,
			org.springframework.jdbc.core.JdbcTemplate

			template) {

		
			String batch_id = prodapprBatchID;
			String prodCode = prodcode;
			String siteCode = code != null ? code : "NA";
			String apprStatus = "Y";
			String updateICR = "N";

			final String sqlInsertProdcutAppr = "INSERT INTO MDG_PRODUCTAPPROVALS(batch_id,aProductSite,aProductCode,aApproved,aUpdateICR) VALUES (?,?,?,?,?)";

			Object[] paramsInsertAppr = new Object[] { batch_id, siteCode,
					prodCode, apprStatus, updateICR };

			int[] typesInsertProductAppr = new int[]

			{ Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR };

			int count = template.update(sqlInsertProdcutAppr, paramsInsertAppr,
					typesInsertProductAppr);

		
		return "Inserted";

	}

	public String executeInsertProductAKA(String prodAKABatchID,
			String prodsiteCode, String prodcode, String changeIndicator,
			List altProdCode, org.springframework.jdbc.core.JdbcTemplate

			template) {

		
			JSonToDBLogger.info("In ProductAKA table ");
			String batch_id = prodAKABatchID;

			String deleteAKA = "N";
			List akaCode = altProdCode;

			for (int i = 0; i < akaCode.size(); i++) {
				String primaryAKA;

				if (i == 0) {
					primaryAKA = "Y";

				} else {
					primaryAKA = "N";
				}

				String tempaltProdCode = (String) akaCode.get(i);
				JSonToDBLogger.debug("After getting AKA code  ");

				final String sqlInsertProdcutAKA = "INSERT INTO MDG_PRODAKA(batch_id,aProdSite,aProdCode,aAKASite,aAKACode,aDeleteAKA,aPrimaryAKA) VALUES (?,?,?,?,?,?,?)";

				Object[] paramsInsertprdAKA = new Object[] { batch_id,
						prodsiteCode, prodcode, prodsiteCode, tempaltProdCode,
						deleteAKA, primaryAKA };

				int[] typesInsertProdcutAKA = new int[] { Types.VARCHAR,
						Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
						Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };

				int count = template.update(sqlInsertProdcutAKA,
						paramsInsertprdAKA, typesInsertProdcutAKA);
				JSonToDBLogger
						.info("Records count for AltPordcode in  ProductAKA table "
								+ i);

			}

		

		return "Inserted";

	}

	// Insert into Publisher table Controlled field mapping remaing

	public String executeInsertPublisher(PublisherInfo objPub,
			@Header("batchId") String pubBatchID) {

		template = new JdbcTemplate(dataSource);

		

			String batch_id = pubBatchID;
			String sitecode = objPub.getData().getSite() != null ? objPub
					.getData().getSite().getCode() : "NA";
			String controlled = "N";

			
			String name = objPub.getData().getName();
			String extcode = "BMGPB_" + objPub.getData().getId();
			int unitType = 1;

			final String sqlInsertpub = "INSERT INTO MDG_PUBLISHER(batch_id,aSite,aName,aControlled,aUnitType,aExternalCode) VALUES (?,?,?,?,?,?)";

			Object[] paramsInsertPub = new Object[] { batch_id, sitecode, name,
					controlled, unitType, extcode };
			int[] typesInsertpub = new int[] { Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.VARCHAR };

			int count = template.update(sqlInsertpub, paramsInsertPub,
					typesInsertpub);

			JSonToDBLogger
					.info("Records Inserted to Publisher Table successfully with number of records"
							+ count);

	
		return "Inserted into Pub";

	}

	// Insert into sound Recording table
	public String executeInsertRecording(RecordingsInfo objRec,
			@Header("batchId") String recBatchID,
			int songId, DataSource dataSource) throws ParseException {

		template = new JdbcTemplate(dataSource);

	

			String codeRec = objRec.getData().getSite() != null ? objRec
					.getData().getSite().getCode() : "NA";
			String batch_id = recBatchID;

			String songSite = objRec.getData().getIsrc() != null ? objRec
					.getData().getSite().getCode() : " ";

			String isrc = objRec.getData().getIsrc() != null ? objRec.getData()
					.getIsrc() : null;
			String style = "P";
			String rType = "S";

			String type = objRec.getData().getRecordingType() != null ? (objRec
					.getData().getRecordingType().getName().startsWith("A") ? "A"
					: "V")
					: null;
			String recLangName = objRec.getData().getRecordingLanguage() != null ? objRec
					.getData().getRecordingLanguage().getCode2(): null;
			String category = objRec.getData().getCategory()!=null?(objRec.getData().getCategory() == "VOCAL" ? "V"
					: "I"):null;
			String cpYear = String
					.valueOf((objRec.getData().getCopyrightYear().SIZE != 0 ? objRec
							.getData().getCopyrightYear() : 0));

			String cpNotice = objRec.getData().getCopyrightNotice() != null ? objRec
					.getData().getCopyrightNotice() : null;
			String ddEnabled = objRec.getChangeindicator() == "I" ? "Y" : "N";

			String ddcode = ddEnabled == "Y" ? objRec.getData().getIsrc() : " ";
			String explicit =objRec.getData().getIsExplicit()!=null?( objRec.getData().getIsExplicit() ? "Y" : "N"):null;
			String ismedley = objRec.getData().getIsMedley()!=null?(objRec.getData().getIsMedley() ? "Y" : "N"):null;

			SimpleDateFormat sdfDestination = new SimpleDateFormat(
					"yyyy-MM-dd'T'HH:mm:ss");

			java.util.Date recDate = sdfDestination.parse(objRec.getData()
					.getRecordingDate());
			String submix = objRec.getData().getVersionTitle() != null ? objRec
					.getData().getVersionTitle() : null;
			String company = objRec.getData().getRepertoireOwnerCompany()
					.getCode() != null ? objRec.getData()
					.getRepertoireOwnerCompany().getCode() : null;
					String duration=objRec.getData().getDuration()!=null?Integer.toString(objRec.getData().getDuration()):null;
					String artistName= objRec.getData().getArtistName()!=null? objRec.getData().getArtistName():"NA";
					int recId=objRec.getData().getId()!=null?objRec.getData().getId():null;
			//int songId = objrecSongbody.getSong().getId();
			final String sqlInsertsoundRec = "INSERT INTO MDG_SOUNDREC(batch_id,aSite,aSongSite,aArtist,aSubmix,aISRC,aType,aStyle,aRType,aVocal,aPYear,aPNote,aTime,aUpdateProductTracks,aUpdateProductTime,aTech,"
					+ "						aRecordingDate,aLang,aCompany,aDDEnabled,aDDCode,aExplicitContent,aExternalCode,aExternalSongCode) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			JSonToDBLogger.debug("Query String forRecording "
					+ sqlInsertsoundRec);
			Object[] paramsInsertsoundRec = new Object[] { batch_id, codeRec,
					songSite,artistName, submix, isrc,
					type, style, rType, category, cpYear, cpNotice,
					duration, "Y", "Y", "D",
					(new java.sql.Date(recDate.getTime())), recLangName,
					company, ddEnabled, ddcode, explicit,"BMGR_" + recId,
					"BMGS_" + songId };

			int[] typesInsertsoundRec = new int[] { Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.INTEGER,
					Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.DATE,
					Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };

			int count = template.update(sqlInsertsoundRec,
					paramsInsertsoundRec, typesInsertsoundRec);

			JSonToDBLogger
					.info("Records Inserted to Recordings table successfully with number of records"
							+ count);

		

		return "Inserted";

	}

	public String executeInsertProductPackageTrue(ProductsInfo objprd,
			String prodBatchID, DataSource dataSource, int sideNum,
			int trackNum, int childComponent) throws ParseException {

			template = new JdbcTemplate(dataSource);
			

				String carriercheck;

				String batch_id = prodBatchID;

				String code = objprd.getData().getSite().getCode() != null ? objprd
						.getData().getSite().getCode() : " ";
						
						if((objprd.getData().getFormat().getProductCarrierType().getCarrierName()!=null)&&(objprd.getData().getFormat().getProductCarrierType().getCarrierName().equalsIgnoreCase("Unknown")))
						{
					 carriercheck = null;
						}
				else{
					carriercheck=objprd.getData().getFormat().getProductCarrierType().getCarrierName()!=null?objprd.getData().getFormat().getProductCarrierType().getCarrierName():null;
								
					
				}
			String formatcarrierName=carriercheck;
			
			JSonToDBLogger.info("CarrierName in product Table"+ formatcarrierName);
			
				int labelid = objprd.getData().getLabel() != null ? objprd
						.getData().getLabel().getId() : null;
				String formatAudioVedio = objprd.getData().getFormat().getAudioVisual()!=null?(objprd.getData().getFormat().getAudioVisual().startsWith("A") ? "A": "V"):null;
						
				String parentaladvLyrics =  objprd.getData()
						.getParentalAdvisoryLyricsIndicator()!=null?(objprd.getData()
						.getParentalAdvisoryLyricsIndicator().startsWith("N") ? "N"
						: "Y"):null;
				String digiDownload = objprd.getData().getFormat().getIsPhysical()!=null?(objprd.getData().getFormat().getIsPhysical() == true ? "Y"
						: "N"):null;
				String compilationind = (objprd.getData().getIsCompilation()!=null?(objprd.getData().getIsCompilation() == false ? "N"
						: "Y"):null);
				String promo = objprd.getData().getIsPromo() == false ? "N" : "Y";
				String catCoEnabled = "N";
				String catCoResend = "N";
				String autoCreateAKA = "Y";
				String updateuserCode = "Y";
				String updateTerrBarcode = "N";
				Integer components = childComponent;

				int sidenumber = sideNum;
				int tracknumber = trackNum;

				String strdate = objprd.getData().getReleaseDate();

				DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				java.util.Date releaseDate = df.parse(objprd.getData()
						.getReleaseDate());
				String company = objprd.getData().getRepertoireOwnerCompany()
						.getCode() != null ? objprd.getData()
						.getRepertoireOwnerCompany().getCode() :null;
				String labelId = objprd.getData().getLabel()!=null?Integer
						.toString(objprd.getData().getLabel().getId()):null;
				String barcode = objprd.getData().getBarcode() != null ? objprd
						.getData().getBarcode() : null;
				String prodCode = objprd.getData().getProductCode() != null ? objprd
						.getData().getProductCode() : barcode;
				String ExternalCode = "BMGP_" + objprd.getData().getId();
				String grid=objprd.getData().getGrId()!=null?objprd.getData().getGrId():null;
				
				String title= objprd.getData().getProductTitle()!=null? objprd.getData().getProductTitle():null;
				String duration=objprd.getData().getDuration()!=null?Integer.toString(objprd.getData().getDuration()):null;
				String artistName=objprd.getData().getArtistName()!=null?objprd.getData().getArtistName():"NA";
				String cpYear=Integer.toString(objprd.getData().getCopyrightYear());
				String cpNotice=objprd.getData().getCopyrightNotice()!=null?objprd.getData().getCopyrightNotice():null;
				String artworkYr=objprd.getData().getArtworkYear()!=null?Integer.toString(objprd.getData().getArtworkYear()):null;
				String artworkNotice=objprd.getData().getArtworkNotice()!=null?objprd.getData().getArtworkNotice():null;
				
				
				
				
				JSonToDBLogger.info("All values set in product Table$$$$$$$");
				
				final String sqlInsertProduct =

				"INSERT INTO MDG_PRODUCT(batch_id,aSite,aCode,aBarCode,aTitle,aArtist,aCompany,aComponents,aSides,aTracks,aConfiguration,aTotalPlayingTime,aIntReleaseDate,"
						+ "aCopyrightProductType,aLabel,aCompilation,aDigitalDownload,aPromo,"
						+ "aType,aRecType,aTech,aGridCode,aExplicitContent,aPYear,aPNote,aCYear,aCNote,aCatCoEnabled,aCatCoResend,aAutoCreateAKACodes,aExternalCode,"
						+ "aUpdateUserCode,aUpdateTerrBarcodes,aExternalLabelCode) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

				Object[] paramsInsert = new Object[] { batch_id, code, prodCode,
						barcode, title,
						artistName, company, components,
						sidenumber, tracknumber,formatcarrierName,
						duration,
						(new java.sql.Date(releaseDate.getTime())), 0, labelId,
						compilationind, digiDownload, promo, formatAudioVedio, "S",
						"D", grid, parentaladvLyrics,
						cpYear,
						cpNotice,artworkYr,
						artworkNotice, catCoEnabled,
						catCoResend, autoCreateAKA, ExternalCode, updateuserCode,
						updateTerrBarcode, Integer.toString(labelid) };

				int[] typesInsertProduct = new int[] { Types.VARCHAR,
						Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
						Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER,
						Types.INTEGER, Types.VARCHAR, Types.INTEGER, Types.DATE,
						Types.INTEGER, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
						Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
						Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
						Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
						Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
						Types.VARCHAR };

				int count = template.update(sqlInsertProduct, paramsInsert,
						typesInsertProduct);

				JSonToDBLogger
						.info("Records Inserted to Product table successfully with number of records"
								+ count);
				JSonToDB objJsondb = new JSonToDB();

				String prodcode = objprd.getData().getProductCode() != null ? objprd
						.getData().getProductCode() : barcode;

				//if (prodcode != null) {
					JSonToDBLogger.info("prodcode  and prodsiteCode" + prodcode,
							code);

					objJsondb.executeInsertProductApproval(prodBatchID, code,
							prodcode, template);

					JSonToDBLogger
							.info("Records Inserted to ProductsApproval Table successfully with number of records");
					List altcode = objprd.getData().getAltProductCodes();
					if (altcode != null && altcode.isEmpty() == false) {
						objJsondb.executeInsertProductAKA(prodBatchID, code,
								prodcode, objprd.getChangeindicator(), altcode,
								template);

						JSonToDBLogger
								.info("Records Inserted to ProductAKA Table successfully with number of records");
					} else {
						JSonToDBLogger
								.info("Records not Inserted for ProductAKA Table as AltProdCode is null");
					}

				/*} else {

					JSonToDBLogger
							.info("No records for ProductApproval and ProductAKA Table as ProductCode is null");
				}*/
		
			
		
		return "Inserted";

	}

	public String executeInsertTrack(ProductsInfo objprd, TracksInfo objtrack,
			@Header("batchId") String trackBatchID, int trackNum, int sideNum) throws Exception {
		template = new JdbcTemplate(dataSource);
		

			String batch_id = trackBatchID;
			//String medleyTitle = (objprd.getChangeindicator() == "D") ? "ToBeDeleted"	: " ";
			
			//String medleyTitle =null;
			
			JSonToDBLogger.info("in track method");

			String soundrecSite = objtrack.getRecording().getSite().getCode() != null ? objtrack
					.getRecording().getSite().getCode()
					: null;
					JSonToDBLogger.info("after soundrecSite");

			/*String soundrecCode = objtrack.getRecording().getIsrc() != null ? "000"
					: (objtrack.getRecording().getSite() != null ? objtrack
							.getRecording().getSite().getId() : "00");*/

			String sitecode = objprd.getData().getSite() != null ? objprd
					.getData().getSite().getCode() : null;
			String isrc = objtrack.getRecording().getIsrc()!=null? objtrack.getRecording().getIsrc():null;
			String prdId = objtrack.getProduct() != null ? "BMGP_"
					+ objtrack.getProduct().getId() : null;
			String recId = "BMGR_" + objtrack.getRecording().getId();
			String parentLink = "N";
			// objtrack.getParentLink()!=null?objtrack.getParentLink():" ";
			int duration = objtrack.getRecording().getDuration();
/*
			final String sqlInserttrack =

			"INSERT INTO MDG_TRACK(batch_id,aProductSite,aProductCode,aSoundrecSite,aSoundrecCode,aSide,aTrack,aSub,aTime,aSoundrecISRC,aParentLink,aMedleyTitle,aExProductCode,aExSoundRecCode) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			Object[] paramsInsert = new Object[] { batch_id, sitecode, "NA",
					soundrecSite, Integer.parseInt(soundrecCode), sideNum,
					trackNum, "NA", duration, isrc, parentLink, "NA", prdId,
					recId };

			int[] typesInserttrack = new int[] { Types.VARCHAR, Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.INTEGER, Types.INTEGER,
					Types.INTEGER, Types.VARCHAR, Types.INTEGER, Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };

			int count = template.update(sqlInserttrack, paramsInsert,
					typesInserttrack);*/
			
			final String sqlInserttrack =

					"INSERT INTO MDG_TRACK(batch_id,aProductSite,aSoundrecSite,aSide,aTrack,aTime,aSoundrecISRC,aParentLink,aExProductCode,aExSoundRecCode) VALUES (?,?,?,?,?,?,?,?,?,?)";

			Object[] paramsInsert = new Object[] { batch_id, sitecode,
					soundrecSite, sideNum,
					trackNum, duration, isrc, parentLink, prdId,
					recId };

			int[] typesInserttrack = new int[] { Types.VARCHAR, Types.VARCHAR,
					 Types.VARCHAR, Types.INTEGER,
					Types.INTEGER, Types.INTEGER,Types.VARCHAR,
					Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };

			int count = template.update(sqlInserttrack, paramsInsert,
					typesInserttrack);

			
			 /*if (objprd.getChangeindicator() == "D") {
				 
				 Mailer objMailer=new Mailer();
			 
				 String Subject="Message for track reduction";
				 
				 objMailer.sendEmailToSupportTeam(Subject,soundrecCode,sideNum,trackNum);
				 

					JSonToDBLogger
							.info("Email sent for track deletion");
			
			 	}*/
			 

			JSonToDBLogger
					.info("Records Inserted to Track Table successfully with number of records"
							+ count);
		

		return "Inserted";

	}

	public String executeInsertTrackPackageTrue(ProductChild objprd,
			TracksInfo objtrack, @Header("batchId") String trackBatchID,
			int trackNum, int sideNum, int prodId) {
		template = new JdbcTemplate(dataSource);

	

			
				String batch_id = trackBatchID;

				//String medleyTitle = "NA";
				String soundrecSite = objtrack.getRecording().getSite().getCode() != null ? objtrack
						.getRecording().getSite().getCode()	: null;

				//String soundrecCode = objtrack.getRecording().getIsrc() != null ? null: (objtrack.getRecording().getSite() != null ? objtrack.getRecording().getSite().getId() : "00");

				String sitecode = objprd.getChild().getSite() != null ? objprd
						.getChild().getSite().getCode() : null;
				String isrc = objtrack.getRecording().getIsrc()!=null? objtrack.getRecording().getIsrc():null;

		
				String prdId = "BMGP_" + prodId;
				String recId = "BMGR_" + objtrack.getRecording().getId();
				String parentLink = "N";
				

				int duration = objtrack.getRecording().getDuration()!=null?objtrack.getRecording().getDuration():null;
				

				JSonToDBLogger
						.info("Duration value in Track While inserting into table::"
								+ duration);

				/*final String sqlInserttrack =

				"INSERT INTO MDG_TRACK(batch_id,aProductSite,aProductCode,aSoundrecSite,aSoundrecCode,aSide,aTrack,aSub,aTime,aSoundrecISRC,aParentLink,aMedleyTitle,aExProductCode,aExSoundRecCode) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
*/
				
				final String sqlInserttrack =

						"INSERT INTO MDG_TRACK(batch_id,aProductSite,aSoundrecSite,aSide,aTrack,aTime,aSoundrecISRC,aParentLink,aExProductCode,aExSoundRecCode) VALUES (?,?,?,?,?,?,?,?,?,?)";

				Object[] paramsInsert = new Object[] { batch_id, sitecode,
						soundrecSite, sideNum,
						trackNum, duration, isrc, parentLink, prdId,
						recId };

				int[] typesInserttrack = new int[] { Types.VARCHAR, Types.VARCHAR,
						 Types.VARCHAR, Types.INTEGER,
						Types.INTEGER, Types.INTEGER, Types.VARCHAR,
						Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };

				int count = template.update(sqlInserttrack, paramsInsert,
						typesInserttrack);

				// if (objprd.getChangeindicator() == "D")
				// {

				// sent email logic as track is deleted
				/*
				 * Message for track reduction Hi,
				 * 
				 * Below mentioned track data has been removed from Capture system.
				 * 
				 * Sound Recording Code : {SoundRecordingCode} Side Number :
				 * {SideNumber} Track Number : {TrackNumber}
				 * 
				 * Regards, BMG Support team
				 */

				// }

				JSonToDBLogger
						.info("Records Inserted to Track Table successfully with number of records"
								+ count);
			
		

		return "Inserted";

	}

	String userName = "ggbmg04";
	String password = "Summer#87";

	public String fetchAuthenticationCredentials(
			String authenticationCredentials) {
		String temp = this.userName + ":" + this.password;

		String basicAuth = Base64.encodeBase64String(temp.getBytes());
		return basicAuth;
	}

}
