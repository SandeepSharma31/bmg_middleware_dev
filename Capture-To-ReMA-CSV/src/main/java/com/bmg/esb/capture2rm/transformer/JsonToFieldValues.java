package com.bmg.esb.capture2rm.transformer;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bmg.esb.capture2rm.beans.products.ProductsInfo;
import com.bmg.esb.capture2rm.beans.products.child.ProductChild;
import com.bmg.esb.capture2rm.beans.recordings.RecordingsInfo;
import com.bmg.esb.capture2rm.beans.songs.SongsInfo;
import com.bmg.esb.capture2rm.beans.tracks.TracksInfo;
import com.bmg.esb.capture2rm.processors.CalcTrackandSideSum;
import com.bmg.esb.capture2rm.test.ContainerBean;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonToFieldValues {

	private static final Logger JsonToFieldValues = LoggerFactory
			.getLogger(JsonToFieldValues.class);

	// int aSideNum=0,iaSideNum;
	private static int iaSideNum;
	// int aSide=0;
	private ContainerBean containerBean;

	public ContainerBean getContainerBean() {
		return containerBean;
	}

	public void setContainerBean(ContainerBean containerBean) {
		this.containerBean = containerBean;
	}

	/*@SuppressWarnings("null")
	public Map<String, Object> createPublisherDataMapping(
			PublisherInfo pubData, @Header("batchId") String pubBatchID) {
		DataConversion objdataConv = new DataConversion();
		String batch_id = pubBatchID;
		String ADOADD = null;

		Float IM_PUBLISHER_SEQ = null;

		String ERRORCODE = null;
		// float ERRORCODEI=(Float) null;
		String AIMPORTBLANKS = null;

		String aSite = pubData.getData().getSite() != null ? (pubData.getData()
				.getSite().getCode() != null ? pubData.getData().getSite()
				.getCode() : null) : null;
		String aName = pubData.getData().getName() != null ? objdataConv
				.replaceCommaWithDoller(pubData.getData().getName()) : null; // made
																				// changes
																				// for
																				// Publisher
																				// field
		String aControlled = "N";
		int aUnitType = 1;
		String aExternalCode = pubData.getData().getId() != null ? "BMGPB_"
				+ pubData.getData().getId() : null;
		String aCode = null;
		String aCompany = null;
		String aCurrency = null;
		// float aExRate= (Float) null;
		String aTaxID = null;
		String aAgency = null;
		String aCrossMechCode = null;
		String aCrossPrintCode = null;
		String aNegCycle = null;
		// int aPriceType=(Integer) null;
		// float aMinCheque=(Float) null;
		String aHoldRes = null;
		String aHoldPay = null;
		String aPayVAT = null;
		String aFreeGoods = null;
		String aPaymentFreq = null;
		String aContactName = null;
		String aStreet1 = null;
		String aStreet2 = null;
		String aTown = null;
		String aCounty = null;
		String aPostcode = null;
		String aCountry = null;
		String aPhone = null;
		String aFax = null;
		String aEmail = null;
		String aEmailCC = null;
		String aAddrTypeCode = null;
		String aAddrNotes = null;
		String aSendStatement = null;
		String aAccountingSystem = null;
		String aCMRRAPublCode = null;

		Map<String, Object> publisherData = new LinkedHashMap<String, Object>();
		publisherData.put("batch_id", batch_id);
		publisherData.put("ADOADD", ADOADD);
		publisherData.put("IM_PUBLISHER_SEQ", null);
		publisherData.put("ERRORCODE", ERRORCODE);
		publisherData.put("ERRORCODEI", null);
		publisherData.put("AIMPORTBLANKS", AIMPORTBLANKS);
		publisherData.put("aSite", aSite);
		publisherData.put("aCode", aCode);
		publisherData.put("aName", aName);
		publisherData.put("aCompany", aCompany);
		publisherData.put("aCurrency", aCurrency);
		publisherData.put("aExRate", null);
		publisherData.put("aTaxID", aTaxID);
		publisherData.put("aControlled", aControlled);
		publisherData.put("aAgency", aAgency);
		publisherData.put("aCrossMechCode", aCrossMechCode);
		publisherData.put("aCrossPrintCode", aCrossPrintCode);
		publisherData.put("aNegCycle", aNegCycle);
		publisherData.put("aPriceType", null);
		publisherData.put("aUnitType", aUnitType);
		publisherData.put("aMinCheque", null);
		publisherData.put("aHoldRes", aHoldRes);
		publisherData.put("aHoldPay", aHoldPay);
		publisherData.put("aPayVAT", aPayVAT);
		publisherData.put("aFreeGoods", aFreeGoods);
		publisherData.put("aPaymentFreq", aPaymentFreq);
		publisherData.put("aContactName", aContactName);
		publisherData.put("aStreet1", aStreet1);
		publisherData.put("aStreet2", aStreet2);
		publisherData.put("aTown", aTown);
		publisherData.put("aCounty", aCounty);
		publisherData.put("aPostcode", aPostcode);
		publisherData.put("aCountry", aCountry);
		publisherData.put("aPhone", aPhone);
		publisherData.put("aFax", aFax);
		publisherData.put("aEmail", aEmail);
		publisherData.put("aEmailCC", aEmailCC);
		publisherData.put("aAddrTypeCode", aAddrTypeCode);
		publisherData.put("aAddrNotes", aAddrNotes);
		publisherData.put("aSendStatement", aSendStatement);
		publisherData.put("aAccountingSystem", aAccountingSystem);
		publisherData.put("aCMRRAPublCode", aCMRRAPublCode);
		publisherData.put("aExternalCode", aExternalCode);

		return publisherData;
	}*/

	public Map<String, Object> createSongDataMapping(String strSongBody,
			@Header("batchId") String songBatchID) throws Exception {
		SongsInfo allData = SongsInfo.fromString(strSongBody);
		DataConversion objdataConv = new DataConversion();
		String codeSong = allData.getData().getSite().getCode() != null ? allData
				.getData().getSite().getCode()
				: null;
		int v = allData.getData().getId() != 0 ? allData.getData().getId() : 0;
		int id = allData.getData() != null ? v : 0;
		String batch_id = songBatchID;

		String title = allData.getData() != null ? allData.getData().getTitle()
				: null;
		String stitle = objdataConv.replaceCommaWithDoller(title);
		String composerName = allData.getData().getComposerName() != null ? allData
				.getData().getComposerName() : null;
		String scomposerName = objdataConv.replaceCommaWithDoller(composerName);
		String iswcvalue = allData.getData().getIswc() != null ? allData
				.getData().getIswc() : null;
		String iswc = allData.getData() != null ? iswcvalue : " ";
		String publicDomain = allData.getData().getIsPublicDomain() != null ? (allData
				.getData().getIsPublicDomain() == false ? "N" : "Y") : null;
		String generateSoundRec = "N";
		String externalCode = "BMGS_" + id;
		String lyricist = "NULL";

		@SuppressWarnings("null")
		// int aCode=(Integer) null;
		String aSoundrecISRC = null;
		String aCMRRASongCode = allData.getData().getIsIMaestro() != null ? (allData
				.getData().getIsIMaestro() == true ? "im."
				+ allData.getData().getiMaestroCode() : null) : null;

		String aArtist = null;
		String ERRORCODE = null;

		Map<String, Object> songData = new LinkedHashMap<String, Object>();
		songData.put("batch_id", batch_id);
		songData.put("ADOADD", null);
		songData.put("IM_SONG_SEQ", null);
		songData.put("ERRORCODE", null);
		songData.put("ERRORCODEI", null);
		songData.put("AIMPORTBLANKS", null);
		songData.put("aSite", codeSong);
		songData.put("aCode", null);
		songData.put("aTitle", stitle);
		songData.put("aComposer", scomposerName);
		songData.put("aISWC", iswc);
		songData.put("aPublicDomain", publicDomain);
		songData.put("aSoundrecISRC", aSoundrecISRC);
		songData.put("aCMRRASongCode", aCMRRASongCode);
		songData.put("aGenerateSoundRec", generateSoundRec);
		songData.put("aArtist", aArtist);
		songData.put("aExternalCode", externalCode);
		songData.put("aLyricist", lyricist);

		return songData;
	}

	/*public void createSongPublisherDataMapping(Exchange exchange)
			throws Exception {
		if (exchange.getIn().getBody() != null
				&& (exchange.getIn().getBody() instanceof SongPublisherInfo)) {
			JsonToFieldValues
					.info("body is not null and in createSongPublisherDataMapping"
							+ exchange.getIn().getBody());
			Object objTemp = exchange.getIn().getHeader("songbody");
			SongsInfo allData = null;
			if (objTemp instanceof SongsInfo) {
				allData = (SongsInfo) exchange.getIn().getHeader("songbody");
			} else {
				allData = SongsInfo.fromString((String) exchange.getIn()
						.getHeader("songbody"));
			}

			String songBatchID = (String) exchange.getIn().getHeader("batchId");

			SongPublisherInfo songPuballData = (SongPublisherInfo) exchange
					.getIn().getBody();

			String batch_id = songBatchID;
			// String aSongSite=allData.getData().getSite() != null ?
			// (allData.getData().getSite() != null
			// ?allData.getData().getSite().getCode(): null):null;
			String aSongSite = songPuballData.getPublisher().getSite() != null ? (songPuballData
					.getPublisher().getSite().getCode() != null ? songPuballData
					.getPublisher().getSite().getCode()
					: null)
					: null;
			String aTerr = songPuballData.getCountry() != null ? (songPuballData
					.getCountry().getCode() != null ? songPuballData
					.getCountry().getCode() : null) : null;
			String aPubSite = songPuballData.getPublisher().getSite() != null ? (songPuballData
					.getPublisher().getSite().getCode() != null ? songPuballData
					.getPublisher().getSite().getCode()
					: null)
					: null;
			String aPubCode = null;
			Double daShare = songPuballData.getShare() != null ? songPuballData
					.getShare() : null;
			Double aShare = daShare * 100;
			String aUpdateLicenseShare = "N";
			String aExternalSongCode = "BMGS_" + allData.getData().getId();
			Integer pubid = songPuballData.getPublisher() != null ? songPuballData
					.getPublisher().getId() : null;
			String aExternalPubCode = "BMGPB_" + pubid;

			String aExternalSongPubCode = "NULL";

			Map<String, Object> songPubData = new LinkedHashMap<String, Object>();
			songPubData.put("batch_id", batch_id);
			songPubData.put("ADOADD", null);
			songPubData.put("IM_SONGPUB_SEQ", null);
			songPubData.put("ERRORCODE", null);
			songPubData.put("ERRORCODEI", null);
			songPubData.put("AIMPORTBLANKS", null);
			songPubData.put("aSongSite", aSongSite);
			songPubData.put("aSongCode", null);
			songPubData.put("aTerr", aTerr);
			songPubData.put("aPubSite", aPubSite);
			songPubData.put("aPubCode", aPubCode);
			songPubData.put("aShare", aShare);
			songPubData.put("aUpdateLicenseShare", aUpdateLicenseShare);
			songPubData.put("aExternalSongCode", aExternalSongCode);
			songPubData.put("aExternalPubCode", aExternalPubCode);
			songPubData.put("aExternalSongPubCode", aExternalSongPubCode);
			exchange.getIn().setBody(songPubData);
		} else {
			JsonToFieldValues.info("SongPublisher body is Empty");
		}
	}*/

	/*
	 * public Map<String, Object> createRecordingDataMapping(RecordingsInfo
	 * recordingData,@Header("batchId") String recBatchID) throws ParseException
	 * {
	 */

	public void createRecordingDataMapping(Exchange exchange) throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		DataConversion objdataConv = new DataConversion();
		String batch_id = (String) exchange.getIn().getHeader("batchId");
		String fileGenValue = (String) exchange.getIn()
				.getHeader("fileGenflag");
		/*
		 * List<RecordingSongInfo> recSongList = mapper.readValue(exchange
		 * .getIn().getBody(String.class), new
		 * TypeReference<List<RecordingSongInfo>>() { });
		 */
		// RecordingSongInfo recSongList=(RecordingSongInfo)
		// exchange.getIn().getBody();
		Object objTemp = exchange.getProperty("recordingBody");
		RecordingsInfo recordingData = null;
		if (objTemp instanceof RecordingsInfo) {
			recordingData = (RecordingsInfo) exchange.getProperty(
					"recordingBody");
		} else {
			recordingData = RecordingsInfo.fromString((String) exchange.getProperty("recordingBody"));
		}

		// String songId = (String)
		// (exchange.getIn().getHeader("recSongId")!=null?
		// exchange.getIn().getHeader("recSongId"):"");
		int songId = (Integer) exchange.getIn().getHeader("recSongId");

		String aSite = recordingData.getData().getSite() != null ? (recordingData
				.getData().getSite().getCode() != null ? recordingData
				.getData().getSite().getCode() : null)
				: null;
		String aSongSite = recordingData.getData().getIsrc() != null ? recordingData
				.getData().getSite().getCode()
				: null;
		String aArtist = recordingData.getData().getArtistName() != null ? recordingData
				.getData().getArtistName() : null;
		String saArtist = aArtist != null ? objdataConv
				.replaceCommaWithDoller(aArtist) : null;
		String aSubmix = recordingData.getData().getVersionTitle() != null ? recordingData
				.getData().getVersionTitle() : null;
		String saSubmix = aSubmix != null ? objdataConv
				.replaceCommaWithDoller(aSubmix) : null;

		String aISRC = recordingData.getData().getIsrc() != null ? recordingData
				.getData().getIsrc() : null;
		String aType = recordingData.getData().getRecordingType() != null ? (recordingData
				.getData().getRecordingType().getName().startsWith("A") ? "A"
				: "V") : null;
		String aStyle = "P";
		String aRType = "S";
		String aVocal = recordingData.getData().getCategory() != null ? (recordingData
				.getData().getCategory().startsWith("V") ? "V" : "I")
				: null;
		String aPYear = recordingData.getData().getCopyrightYear() != null ? String
				.valueOf(recordingData.getData().getCopyrightYear()) : null;
		String aPNote = recordingData.getData().getCopyrightNotice() != null ? recordingData
				.getData().getCopyrightNotice() : null;
		String saPNote = aPNote != null ? objdataConv
				.replaceCommaWithDoller(aPNote) : null;
		Integer aTime = recordingData.getData().getDuration() != null ? recordingData
				.getData().getDuration() : null;
		String aUpdateProductTracks = "Y";
		String aUpdateProductTime = "Y";
		String aTech = "D";
		SimpleDateFormat sdfDestination = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss");
		java.sql.Date aRecordingDate;
		if (recordingData.getData().getRecordingDate() != null) {
			java.util.Date recDate1 = sdfDestination.parse(recordingData
					.getData().getRecordingDate());
			aRecordingDate = (new java.sql.Date(recDate1.getTime()));
		} else {
			aRecordingDate = null;

		}

		String aLang = recordingData.getData().getRecordingLanguage() != null ? (recordingData
				.getData().getRecordingLanguage().getCode2() != null ? recordingData
				.getData().getRecordingLanguage().getCode2().toUpperCase()
				: null)
				: null;
		String aOrigin = null;
		String aLabel = recordingData.getData().getLabel() != null ? (recordingData
				.getData().getLabel().getLabelCode() != null ? recordingData
				.getData().getLabel().getLabelCode() : null)
				: null;
		// recordingData.getData().getLabel()!=null?(recordingData.getData().getLabel().getParentCompany()!=null?(recordingData.getData().getLabel().getParentCompany().getCode()!=null?recordingData.getData().getLabel().getParentCompany().getCode():
		// null):null):null;
		String aCompany = recordingData.getData().getRepertoireOwnerCompany() != null ? (recordingData
				.getData().getRepertoireOwnerCompany().getCode() != null ? recordingData
				.getData().getRepertoireOwnerCompany().getCode()
				: null)
				: null;
		// commented for Enhancement SoudRec Digitization new mappings
		// String aDDEnabled =recordingData.getChangeindicator()!=null?(
		// recordingData.getChangeindicator()=="I" ? "Y": "N"):null;
		/*
		 * changes made for new sound rec requirement 14/4/2016
		 * https://bmgconfluence
		 * .atlassian.net/wiki/display/LD/SoundRec+Digitisation for 2nd file
		 */
		String aDDEnabled;
		String aDDCode;
		if (fileGenValue.equals("First")) {
			aDDEnabled = "N";
			JsonToFieldValues.info("SoundRecording aDDEnabled value"
					+ aDDEnabled);
			aDDCode = null;
			JsonToFieldValues.info("SoundRecording aDDCode value" + aDDCode);
		} else {
			aDDEnabled = "Y";
			JsonToFieldValues.info("SoundRecording aDDEnabled value"
					+ aDDEnabled);
			aDDCode = aISRC;
			JsonToFieldValues.info("SoundRecording aDDCode value" + aDDCode);
		}

		// commented for Enhancement SoudRec Digitization new mappings
		// String aDDCode = (aDDEnabled == "Y") ?
		// recordingData.getData().getIsrc(): null;

		String aSociety = null;
		Integer aCopyrightProductType = (Integer) null;
		String aPayCopyright = "Y";//changes Done on 11 Aug as per Confluence
		String aSoleRecTerr = null;
		String aFinalMixTerr = null;
		String aCommissionTerr = null;
		String aPerformanceTerr = null;
		String aPayCSI = null;
		String aPaySOCAN = null;
		Integer aLevelOfInterest = (Integer) null;
		Date aOwnershipDate = null;
		Integer aOwnershipType = (Integer) null;
		String aRightsHolderCode = null;
		Integer aRightsHoldingType = (Integer) null;
		Float aRightsHoldingPerc = (Float) null;
		Date aRightsHoldingEndDate = null;
		String aCYear = null;
		String aCNote = null;
		String aGridCode = null;
		String aExplicitContent = recordingData.getData().getIsExplicit() != null ? (recordingData
				.getData().getIsExplicit() ? "Y" : "N") : null;
		String aMedley = "N";
		String aMedleyType = null;
		String aMedleyTitle = null;
		String aLabelCatNo = null;
		String aProjectCode = null;
		String aExternalCode = "BMGR_" + recordingData.getData().getId();
		// int songId=0;//recordingDataSongbody.getSong().getId();
		String aExternalSongCode = "BMGS_" + songId;

		Integer aCode = (Integer) null;

		Integer aSongCode = (Integer) null;

		Date aDeliveredDate = null;
		Date aReleaseDate = null;
		String aTerr = null;

		String aExternalLabelCode = "NULL";

		Map<String, Object> recData = new LinkedHashMap<String, Object>();
		recData.put("batch_id", batch_id);
		recData.put("ADOADD", null);
		recData.put("IM_SOUNDREC_SEQ", null);
		recData.put("ERRORCODE", null);
		recData.put("ERRORCODEI", null);
		recData.put("AIMPORTBLANKS", null);
		recData.put("aSite", aSite);
		recData.put("aCode", aCode);
		recData.put("aSongSite", aSongSite);
		recData.put("aSongCode", aSongCode);
		recData.put("aArtist", saArtist);
		recData.put("aSubmix", saSubmix);
		recData.put("aISRC", aISRC);
		recData.put("aType", aType);
		recData.put("aStyle", aStyle);
		recData.put("aRType", aRType);
		recData.put("aVocal", aVocal);
		recData.put("aPYear", aPYear);
		recData.put("aPNote", saPNote);
		recData.put("aTime", aTime);
		recData.put("aUpdateProductTracks", aUpdateProductTracks);
		recData.put("aUpdateProductTime", aUpdateProductTime);
		recData.put("aTech", aTech);
		recData.put("aRecordingDate", aRecordingDate);
		recData.put("aDeliveredDate", aDeliveredDate);
		recData.put("aReleaseDate", aReleaseDate);
		recData.put("aTerr", aTerr);
		recData.put("aLang", aLang);
		recData.put("aOrigin", aOrigin);
		recData.put("aLabel", aLabel);
		recData.put("aCompany", aCompany);
		/* recData.put("aDDEnabled", aDDEnabled); */
		/*
		 * changes made for new sound rec requirement 14/4/2016
		 * https://bmgconfluence
		 * .atlassian.net/wiki/display/LD/SoundRec+Digitisation
		 */
		
		recData.put("aDDEnabled", aDDEnabled);
		recData.put("aDDCode", aDDCode);

		recData.put("aSociety", aSociety);
		recData.put("aCopyrightProductType", aCopyrightProductType);
		recData.put("aPayCopyright", aPayCopyright);
		recData.put("aSoleRecTerr", aSoleRecTerr);
		recData.put("aFinalMixTerr", aFinalMixTerr);
		recData.put("aCommissionTerr", aCommissionTerr);
		recData.put("aPerformanceTerr", aPerformanceTerr);
		recData.put("aPayCSI", aPayCSI);
		recData.put("aPaySOCAN", aPaySOCAN);
		recData.put("aLevelOfInterest", aLevelOfInterest);
		recData.put("aOwnershipDate", aOwnershipDate);
		recData.put("aOwnershipType", aOwnershipType);
		recData.put("aRightsHolderCode", aRightsHolderCode);
		recData.put("aRightsHoldingType", aRightsHoldingType);
		recData.put("aRightsHoldingPerc", aRightsHoldingPerc);
		recData.put("aRightsHoldingEndDate", aRightsHoldingEndDate);
		recData.put("aCYear", aCYear);
		recData.put("aCNote", aCNote);
		recData.put("aGridCode", aGridCode);
		recData.put("aExplicitContent", aExplicitContent);
		recData.put("aMedley", aMedley);
		recData.put("aMedleyType", aMedleyType);
		recData.put("aMedleyTitle", aMedleyTitle);
		recData.put("aLabelCatNo", aLabelCatNo);
		recData.put("aProjectCode", aProjectCode);
		recData.put("aExternalCode", aExternalCode);
		recData.put("aExternalSongCode", aExternalSongCode);
		recData.put("aExternalLabelCode", aExternalLabelCode);

		exchange.getIn().setBody(recData);

	}

	@SuppressWarnings("unused")
	public void createProductDataMappingForProductPackageFalse(Exchange exchange)
			throws Exception {

		String batch_id = (String) exchange.getIn().getHeader("batchId");
		DataConversion objdataConv = new DataConversion();
		ProductsInfo prdData = null;
		Object objTemp = exchange.getProperty("productbody");
		if (objTemp instanceof ProductsInfo) {
			prdData = (ProductsInfo) exchange.getProperty("productbody");
		} else {
			prdData = ProductsInfo.fromString((String) exchange.getProperty("productbody"));
		}

		String aSite = prdData.getData().getSite().getCode() != null ? (prdData
				.getData().getSite().getCode() != null ? prdData.getData()
				.getSite().getCode() : null) : null;
		String aBarCode = prdData.getData().getBarcode() != null ? (prdData
				.getData().getBarcode() != null ? prdData.getData()
				.getBarcode() : null) : null;
		String aCode = prdData.getData().getProductCode() != null ? prdData
				.getData().getProductCode() : aBarCode;

		String aTitle = prdData.getData().getProductTitle() != null ? prdData
				.getData().getProductTitle() : null;
		String saTitle = aTitle != null ? objdataConv
				.replaceCommaWithDoller(aTitle) : null;
				
		String aArtist = prdData.getData().getArtistName() != null ? prdData
				.getData().getArtistName() : null;
		String saArtist = aArtist != null ? objdataConv
				.replaceCommaWithDoller(aArtist) : null;
		String aCompany = prdData.getData().getRepertoireOwnerCompany() != null ? (prdData
				.getData().getRepertoireOwnerCompany().getCode() != null ? prdData
				.getData().getRepertoireOwnerCompany().getCode()
				: null)
				: null;
		int aComponents = 1;
		int aSides = prdData.getData().getFormat().getNumberOfSides();
		int aTracks = prdData.getData().getNumberOfTracks();
		String aConfiguration = prdData.getData().getFormat() != null ? prdData
				.getData().getFormat().getReMaConfigCode() : null;
		Integer aTotalPlayingTime = prdData.getData().getDuration() != null ? prdData
				.getData().getDuration() : null;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

		SimpleDateFormat sdfDestination = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss");
		java.util.Date releaseDate;
		java.sql.Date aIntReleaseDate;
		if (prdData.getData().getReleaseDate() != null) {
			releaseDate = sdfDestination.parse(prdData.getData()
					.getReleaseDate());
			aIntReleaseDate = (new java.sql.Date(releaseDate.getTime()));
		} else {
			aIntReleaseDate = null;

		}

		// String aLabel=Integer.toString((prdData.getData().getLabel().getId()
		// != null ? prdData.getData().getLabel().getId(): 0));
		String aLabel = prdData.getData().getLabel() != null ? (prdData
				.getData().getLabel().getLabelCode() != null ? prdData
				.getData().getLabel().getLabelCode() : null) : null;// prdData.getData().getLabel()!=null?(prdData.getData().getLabel().getParentCompany()!=null?(prdData.getData().getLabel().getParentCompany().getCode()!=null?prdData.getData().getLabel().getParentCompany().getCode():null):null):null;
		JsonToFieldValues.info("product Package false ----------aLabel value"
				+ aLabel);
		String aCompilation = prdData.getData().getIsCompilation() != null ? ((prdData
				.getData().getIsCompilation() == false) ? "N" : "Y") : null;
		//added by Jyothi
		if(aCompilation=="Y"){
			
			/*if(prdData.getData().getTitle() == "RAMiami" || prdData.getData().getTitle() == "RAMsterdam" || prdData.getData().getTitle() == "RAMLife" || prdData.getData().getTitle()== "RAM Raid" || prdData.getData().getTitle() == "RAM Drum"){*/
			if(prdData.getData().getTitle().contains("RAMiami") || prdData.getData().getTitle().contains("RAMsterdam") || prdData.getData().getTitle().contains("RAMLife") || prdData.getData().getTitle().contains("RAM Raid") || prdData.getData().getTitle().contains("RAM Drum")){
				
				prdData.getData().setPriceCategoryCode("COMP");
				
				JsonToFieldValues.info("product Package false ----------aPriceCategoryCategory value"
						+ prdData.getData().getPriceCategoryCode());
					//String aPriceCategory = "COMP";
		}else{
			prdData.getData().setPriceCategoryCode("");
			JsonToFieldValues.info("product Package false else condition ----------aPriceCategoryCategory value"
					+ prdData.getData().getPriceCategoryCode());
			//String aPriceCategory = null;
		}
		}
		//ended by Jyothi
		String aDigitalDownload = prdData.getData().getFormat() != null ? (((prdData
				.getData().getFormat().getIsPhysical() != null) ? ((prdData
				.getData().getFormat().getIsPhysical() == true ? "N" : "Y"))
				: null)) : null;
		JsonToFieldValues
				.info("product Package false aDigitalDownload ----------aDigitalDownload value"
						+ aDigitalDownload);
		String aPromo = prdData.getData().getIsPromo() == false ? "N" : "Y";
		String aType = prdData.getData().getFormat() != null ? (prdData
				.getData().getFormat().getAudioVisual() != null ? (prdData
				.getData().getFormat().getAudioVisual().startsWith("A") ? "A"
				: "V") : null) : null;
		String aRecType = "S";
		String aTech = "D";
		String aGridCode = prdData.getData().getGrId() != null ? prdData
				.getData().getGrId() : null;
		String aExplicitContent = prdData.getData()
				.getParentalAdvisoryLyricsIndicator() != null ? (prdData
				.getData().getParentalAdvisoryLyricsIndicator().startsWith("N") ? "N"
				: "Y")
				: null;
		String aPYear = prdData.getData().getCopyrightYear() != null ? Integer
				.toString(prdData.getData().getCopyrightYear()) : null;
		String aPNote = prdData.getData().getCopyrightNotice() != null ? prdData
				.getData().getCopyrightNotice() : null;
		String saPNote = aPNote != null ? objdataConv
				.replaceCommaWithDoller(aPNote) : null;
		String aCYear = prdData.getData().getArtworkYear() != null ? Integer
				.toString(prdData.getData().getArtworkYear()) : null;
		String aCNote = prdData.getData().getArtworkNotice() != null ? prdData
				.getData().getArtworkNotice() : null;
		String saCNote = aCNote != null ? objdataConv
				.replaceCommaWithDoller(aCNote) : null;
		String aCatCoEnabled = "N";
		String aCatCoResend = "N";
		String aAutoCreateAKACodes = "Y";
		String aExternalCode = prdData.getData().getId() != null ? "BMGP_"
				+ prdData.getData().getId() : null;
		String aUpdateUserCode = "Y";
		String aUpdateTerrBarcodes = "N";
		// String
		// aExternalLabelCode=Integer.toString(prdData.getData().getLabel() !=
		// null ? prdData.getData().getLabel().getId() : 0);
		String aExternalLabelCode = null;
		//String aPriceCategory = null;
		Date aReleaseDate = null;
		Date aRereleaseDate = null;

		Date aDeletionDate = null;
		String aTVAdvertised = null;
		String aPayCopyright = "Y";//changes Done on 11 Aug as per Confluence
		Integer aCopyrightProductType = null;

		String aCopyrightCapType = null;
		Integer aCopyrightCapSongs = null;
		Float aCopyrightCapValue = null;
		Float aCopyrightCapPerc = null;
		Date aCopyrightCapFreezeDate = null;
		String aSociety = null;
		String aSocietyNumber = null;
		Date aTVAdFrom = null;
		Date aTVAdTo = null;
		String aInternal = null;
		Integer aPriceLevel = null;
		String aTerritory = null;

		String aHoldReserves = null;
		Integer aCopyrightTracks = null;

		String aAllowFree = null;
		String aRoyaltyInfoComplete = null;

		String aLanguage = null;

		String aStyle = null;
		String aVocal = null;
		String aPayCSI = null;
		String aPaySOCAN = null;
		String aProjectCode = null;

		String aProductClearedTerrName = null;
		String aProductExcludedTerrName = null;
		String aNotes = null;
		String aGenreGroup1 = null;
		String aGenre1 = null;
		String aGenreGroup2 = null;
		String aGenre2 = "NULL";
		

		Map<String, Object> productData = new LinkedHashMap<String, Object>();
		productData.put("batch_id", batch_id);
		productData.put("ADOADD", null);
		productData.put("IM_PRODUCT_SEQ", null);
		productData.put("ERRORCODE", null);
		productData.put("ERRORCODEI", null);
		productData.put("AIMPORTBLANKS", null);
		productData.put("aSite", aSite);
		productData.put("aCode", aCode);
		productData.put("aBarCode", aBarCode);
		productData.put("aTitle", saTitle);
		productData.put("aArtist", saArtist);
		productData.put("aCompany", aCompany);
		productData.put("aComponents", aComponents);
		productData.put("aSides", aSides);
		productData.put("aTracks", aTracks);
		//productData.put("aPriceCategory", aPriceCategory);added by Jyothi
		productData.put("aPriceCategory", prdData.getData().getPriceCategoryCode());
		productData.put("aConfiguration", aConfiguration);
		productData.put("aTotalPlayingTime", aTotalPlayingTime);
		productData.put("aReleaseDate", aReleaseDate);
		productData.put("aIntReleaseDate", aIntReleaseDate);
		productData.put("aRereleaseDate", aRereleaseDate);
		productData.put("aDeletionDate", aDeletionDate);
		productData.put("aTVAdvertised", aTVAdvertised);
		productData.put("aPayCopyright", aPayCopyright);
		productData.put("aCopyrightProductType", aCopyrightProductType);
		productData.put("aCopyrightCapType", aCopyrightCapType);
		productData.put("aCopyrightCapSongs", aCopyrightCapSongs);
		productData.put("aCopyrightCapValue", aCopyrightCapValue);
		productData.put("aCopyrightCapPerc", aCopyrightCapPerc);
		productData.put("aCopyrightCapFreezeDate", aCopyrightCapFreezeDate);
		/**/
		productData.put("aSociety", aSociety);
		productData.put("aSocietyNumber", aSocietyNumber);
		productData.put("aTVAdFrom", aTVAdFrom);
		productData.put("aTVAdTo", aTVAdTo);
		productData.put("aInternal", aInternal);
		productData.put("aPriceLevel", aPriceLevel);
		productData.put("aTerritory", aTerritory);
		productData.put("aLabel", aLabel);
		productData.put("aHoldReserves", aHoldReserves);
		productData.put("aCopyrightTracks", aCopyrightTracks);
		productData.put("aCompilation", aCompilation);
		productData.put("aDigitalDownload", aDigitalDownload);
		productData.put("aAllowFree", aAllowFree);
		productData.put("aRoyaltyInfoComplete", aRoyaltyInfoComplete);
		productData.put("aPromo", aPromo);
		productData.put("aLanguage", aLanguage);
		productData.put("aType", aType);

		productData.put("aRecType", aRecType);
		productData.put("aTech", aTech);
		productData.put("aStyle", aStyle);
		productData.put("aVocal", aVocal);
		productData.put("aPayCSI", aPayCSI);
		productData.put("aPaySOCAN", aPaySOCAN);
		productData.put("aProjectCode", aProjectCode);
		productData.put("aGridCode", aGridCode);
		productData.put("aExplicitContent", aExplicitContent);

		productData.put("aProductClearedTerrName", aProductClearedTerrName);
		productData.put("aProductExcludedTerrName", aProductExcludedTerrName);

		productData.put("aPYear", aPYear);
		productData.put("aPNote", saPNote);
		productData.put("aCYear", aCYear);
		productData.put("aCNote", saCNote);
		productData.put("aCatCoEnabled", aCatCoEnabled);
		productData.put("aCatCoResend", aCatCoResend);
		productData.put("aAutoCreateAKACodes", aAutoCreateAKACodes);
		productData.put("aNotes", aNotes);
		productData.put("aExternalCode", aExternalCode);
		productData.put("aUpdateUserCode", aUpdateUserCode);
		productData.put("aUpdateTerrBarcodes", aUpdateTerrBarcodes);
		productData.put("aExternalLabelCode", aExternalLabelCode);
		productData.put("aGenreGroup1", aGenreGroup1);
		productData.put("aGenre1", aGenre1);
		productData.put("aGenreGroup2", aGenreGroup2);
		productData.put("aGenre2", aGenre2);
		//added by Jyothi
	//	productData.put("title", title);

		exchange.getIn().setBody(productData);
	}

	public void createProductDataMappingForProductPackageTrue(Exchange exchange)
			throws Exception {

		/*
		 * System.out.println("2-Side Count: "+exchange.getProperty(
		 * CalcTrackandSideSum.SIDE_COUNT));
		 * System.out.println("2-Track Count: "
		 * +exchange.getProperty(CalcTrackandSideSum.TRACK_COUNT));
		 */
		DataConversion objdataConv = new DataConversion();

		String strSideCount = (String) exchange
				.getProperty(CalcTrackandSideSum.SIDE_COUNT);
		String strTrackCount = (String) exchange
				.getProperty(CalcTrackandSideSum.TRACK_COUNT);
		String strChildProductCount = (String) exchange.getIn().getHeader(
				"childProductCount");

		String batch_id = (String) exchange.getIn().getHeader("batchId");

		ProductsInfo prdData = null;
		Object objTemp = exchange.getProperty("productbody");
		if (objTemp instanceof ProductsInfo) {
			prdData = (ProductsInfo) exchange.getProperty("productbody");
		} else {
			prdData = ProductsInfo.fromString((String) exchange.getProperty("productbody"));
		}

		String aSite = prdData.getData().getSite().getCode() != null ? (prdData
				.getData().getSite().getCode() != null ? prdData.getData()
				.getSite().getCode() : null) : null;
		String aBarCode = prdData.getData().getBarcode() != null ? (prdData
				.getData().getBarcode() != null ? prdData.getData()
				.getBarcode() : null) : null;
		String aCode = prdData.getData().getProductCode() != null ? prdData
				.getData().getProductCode() : aBarCode;

		String aTitle = prdData.getData().getProductTitle() != null ? prdData
				.getData().getProductTitle() : null;
		String saTitle = aTitle != null ? objdataConv
				.replaceCommaWithDoller(aTitle) : null;
		String aArtist = prdData.getData().getArtistName() != null ? prdData
				.getData().getArtistName() : null;
		String saArtist = aArtist != null ? objdataConv
				.replaceCommaWithDoller(aArtist) : null;
		String aCompany = prdData.getData().getRepertoireOwnerCompany() != null ? (prdData
				.getData().getRepertoireOwnerCompany().getCode() != null ? prdData
				.getData().getRepertoireOwnerCompany().getCode()
				: null)
				: null;
		int aComponents = Integer.parseInt(strChildProductCount);
		int aSides = Integer.parseInt(strSideCount);
		int aTracks = Integer.parseInt(strTrackCount);
		String aConfiguration = prdData.getData().getFormat() != null ? prdData
				.getData().getFormat().getReMaConfigCode() : null;
		Integer aTotalPlayingTime = prdData.getData().getDuration() != null ? prdData
				.getData().getDuration() : null;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

		SimpleDateFormat sdfDestination = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss");
		java.util.Date releaseDate;
		java.sql.Date aIntReleaseDate;
		if (prdData.getData().getReleaseDate() != null) {
			releaseDate = sdfDestination.parse(prdData.getData()
					.getReleaseDate());
			aIntReleaseDate = (new java.sql.Date(releaseDate.getTime()));
		} else {
			aIntReleaseDate = null;

		}

		// String aLabel=Integer.toString((prdData.getData().getLabel().getId()
		// != null ? prdData.getData().getLabel().getId(): 0));
		String aLabel = prdData.getData().getLabel() != null ? (prdData
				.getData().getLabel().getLabelCode() != null ? prdData
				.getData().getLabel().getLabelCode() : null) : null;// prdData.getData().getLabel()!=null?(prdData.getData().getLabel().getParentCompany()!=null?(prdData.getData().getLabel().getParentCompany().getCode()!=null?prdData.getData().getLabel().getParentCompany().getCode():null):null):null;
		JsonToFieldValues.info("product Package True aLabel ---------- value"
				+ aLabel);

		String aCompilation = prdData.getData().getIsCompilation() != null ? ((prdData
				.getData().getIsCompilation() == false) ? "N" : "Y") : null;
		//added by Jyothi
				if(aCompilation=="Y"){
					
					if(prdData.getData().getTitle().contains("RAMiami" ) || prdData.getData().getTitle().contains("RAMsterdam") || prdData.getData().getTitle().contains("RAMLife") || prdData.getData().getTitle().contains("RAM Raid") || prdData.getData().getTitle().contains("RAM Drum")){
						
						prdData.getData().setPriceCategoryCode("COMP");
							//String aPriceCategory = "COMP";
						JsonToFieldValues.info("product Package True ----------aPriceCategoryCategory value"
								+ prdData.getData().getPriceCategoryCode());
				}else{
					prdData.getData().setPriceCategoryCode("");
					
					prdData.getData().setPriceCategoryCode("");
					JsonToFieldValues.info("product Package true else condition ----------aPriceCategoryCategory value"
							+ prdData.getData().getPriceCategoryCode());
					//String aPriceCategory = null;
				}
				}
				//ended by Jyothi
		String aDigitalDownload = prdData.getData().getFormat() != null ? (((prdData
				.getData().getFormat().getIsPhysical() != null) ? ((prdData
				.getData().getFormat().getIsPhysical() == true ? "N" : "Y"))
				: null)) : null;
		JsonToFieldValues
				.info("product Package True aDigitalDownload ---------- value"
						+ aDigitalDownload);

		String aPromo = prdData.getData().getIsPromo() == false ? "N" : "Y";
		String aType = prdData.getData().getFormat() != null ? (prdData
				.getData().getFormat().getAudioVisual() != null ? (prdData
				.getData().getFormat().getAudioVisual().startsWith("A") ? "A"
				: "V") : null) : null;
		String aRecType = "S";
		String aTech = "D";
		String aGridCode = prdData.getData().getGrId() != null ? prdData
				.getData().getGrId() : null;
		String aExplicitContent = prdData.getData()
				.getParentalAdvisoryLyricsIndicator() != null ? (prdData
				.getData().getParentalAdvisoryLyricsIndicator().startsWith("N") ? "N"
				: "Y")
				: null;
		String aPYear = prdData.getData().getCopyrightYear() != null ? Integer
				.toString(prdData.getData().getCopyrightYear()) : null;
		String aPNote = prdData.getData().getCopyrightNotice() != null ? prdData
				.getData().getCopyrightNotice() : null;

		String saPNote = aPNote != null ? objdataConv
				.replaceCommaWithDoller(aPNote) : null;
		String aCYear = prdData.getData().getArtworkYear() != null ? Integer
				.toString(prdData.getData().getArtworkYear()) : null;
		String aCNote = prdData.getData().getArtworkNotice() != null ? prdData
				.getData().getArtworkNotice() : null;
		String saCNote = aCNote != null ? objdataConv
				.replaceCommaWithDoller(aCNote) : null;
		String aCatCoEnabled = "N";
		String aCatCoResend = "N";
		String aAutoCreateAKACodes = "Y";
		String aExternalCode = prdData.getData().getId() != null ? "BMGP_"
				+ prdData.getData().getId() : null;
		String aUpdateUserCode = "Y";
		String aUpdateTerrBarcodes = "N";
		// String
		// aExternalLabelCode=Integer.toString(prdData.getData().getLabel() !=
		// null ? prdData.getData().getLabel().getId() : 0);
		String aExternalLabelCode = null;
		//String aPriceCategory = null;
		Date aReleaseDate = null;
		Date aRereleaseDate = null;

		Date aDeletionDate = null;
		String aTVAdvertised = null;
		String aPayCopyright = "Y";//changes Done on 11 Aug as per Confluence
		Integer aCopyrightProductType = null;

		String aCopyrightCapType = null;
		Integer aCopyrightCapSongs = null;
		Float aCopyrightCapValue = null;
		Float aCopyrightCapPerc = null;
		Date aCopyrightCapFreezeDate = null;
		String aSociety = null;
		String aSocietyNumber = null;
		Date aTVAdFrom = null;
		Date aTVAdTo = null;
		String aInternal = null;
		Integer aPriceLevel = null;
		String aTerritory = null;

		String aHoldReserves = null;
		Integer aCopyrightTracks = null;

		String aAllowFree = null;
		String aRoyaltyInfoComplete = null;

		String aLanguage = null;

		String aStyle = null;
		String aVocal = null;
		String aPayCSI = null;
		String aPaySOCAN = null;
		String aProjectCode = null;

		String aProductClearedTerrName = null;
		String aProductExcludedTerrName = null;
		String aNotes = null;
		String aGenreGroup1 = null;
		String aGenre1 = null;
		String aGenreGroup2 = null;
		String aGenre2 = "NULL";

		Map<String, Object> productData = new LinkedHashMap<String, Object>();
		productData.put("batch_id", batch_id);
		productData.put("ADOADD", null);
		productData.put("IM_PRODUCT_SEQ", null);
		productData.put("ERRORCODE", null);
		productData.put("ERRORCODEI", null);
		productData.put("AIMPORTBLANKS", null);
		productData.put("aSite", aSite);
		productData.put("aCode", aCode);
		productData.put("aBarCode", aBarCode);
		productData.put("aTitle", saTitle);
		productData.put("aArtist", saArtist);
		productData.put("aCompany", aCompany);
		productData.put("aComponents", aComponents);
		productData.put("aSides", aSides);
		productData.put("aTracks", aTracks);
		//productData.put("aPriceCategory", aPriceCategory);added by jyothi
		productData.put("aPriceCategory",prdData.getData().getPriceCategoryCode());
		productData.put("aConfiguration", aConfiguration);
		productData.put("aTotalPlayingTime", aTotalPlayingTime);
		productData.put("aReleaseDate", aReleaseDate);
		productData.put("aIntReleaseDate", aIntReleaseDate);
		productData.put("aRereleaseDate", aRereleaseDate);
		productData.put("aDeletionDate", aDeletionDate);
		productData.put("aTVAdvertised", aTVAdvertised);
		productData.put("aPayCopyright", aPayCopyright);
		productData.put("aCopyrightProductType", aCopyrightProductType);
		productData.put("aCopyrightCapType", aCopyrightCapType);
		productData.put("aCopyrightCapSongs", aCopyrightCapSongs);
		productData.put("aCopyrightCapValue", aCopyrightCapValue);
		productData.put("aCopyrightCapPerc", aCopyrightCapPerc);
		productData.put("aCopyrightCapFreezeDate", aCopyrightCapFreezeDate);
		/**/
		productData.put("aSociety", aSociety);
		productData.put("aSocietyNumber", aSocietyNumber);
		productData.put("aTVAdFrom", aTVAdFrom);
		productData.put("aTVAdTo", aTVAdTo);
		productData.put("aInternal", aInternal);
		productData.put("aPriceLevel", aPriceLevel);
		productData.put("aTerritory", aTerritory);
		productData.put("aLabel", aLabel);
		productData.put("aHoldReserves", aHoldReserves);
		productData.put("aCopyrightTracks", aCopyrightTracks);
		productData.put("aCompilation", aCompilation);
		productData.put("aDigitalDownload", aDigitalDownload);
		productData.put("aAllowFree", aAllowFree);
		productData.put("aRoyaltyInfoComplete", aRoyaltyInfoComplete);
		productData.put("aPromo", aPromo);
		productData.put("aLanguage", aLanguage);
		productData.put("aType", aType);

		productData.put("aRecType", aRecType);
		productData.put("aTech", aTech);
		productData.put("aStyle", aStyle);
		productData.put("aVocal", aVocal);
		productData.put("aPayCSI", aPayCSI);
		productData.put("aPaySOCAN", aPaySOCAN);
		productData.put("aProjectCode", aProjectCode);
		productData.put("aGridCode", aGridCode);
		productData.put("aExplicitContent", aExplicitContent);

		productData.put("aProductClearedTerrName", aProductClearedTerrName);
		productData.put("aProductExcludedTerrName", aProductExcludedTerrName);

		productData.put("aPYear", aPYear);
		productData.put("aPNote", saPNote);
		productData.put("aCYear", aCYear);
		productData.put("aCNote", saCNote);
		productData.put("aCatCoEnabled", aCatCoEnabled);
		productData.put("aCatCoResend", aCatCoResend);
		productData.put("aAutoCreateAKACodes", aAutoCreateAKACodes);
		productData.put("aNotes", aNotes);
		productData.put("aExternalCode", aExternalCode);
		productData.put("aUpdateUserCode", aUpdateUserCode);
		productData.put("aUpdateTerrBarcodes", aUpdateTerrBarcodes);
		productData.put("aExternalLabelCode", aExternalLabelCode);
		productData.put("aGenreGroup1", aGenreGroup1);
		productData.put("aGenre1", aGenre1);
		productData.put("aGenreGroup2", aGenreGroup2);
		productData.put("aGenre2", aGenre2);

		exchange.getIn().setBody(productData);
	}

	public void createProductAKADataMapping(Exchange exchange) throws Exception {

		String batch_id = (String) exchange.getIn().getHeader("batchId");
		String sflagCheck = (String) exchange.getIn().getHeader("flagCheck");
		JsonToFieldValues.info("ProductAKA flagCheck" + sflagCheck);
		ProductsInfo prdData = null;
		Object objTemp = exchange.getProperty("productbody");
		if (objTemp instanceof ProductsInfo) {
			JsonToFieldValues.info("ProductAKA if condition$$$$$$$$$");
			prdData = (ProductsInfo) exchange.getProperty("productbody");
		} else {
			JsonToFieldValues.info("ProductAKA else condition$$$$$$$$$");
			prdData = ProductsInfo.fromString((String) exchange.getProperty("productbody"));
		}

		String aProdSite = (prdData.getData().getSite() != null) ? (prdData
				.getData().getSite().getCode() != null ? prdData.getData()
				.getSite().getCode() : null) : null;
		String barcode = prdData.getData().getBarcode() != null ? prdData
				.getData().getBarcode() : null;
		String aProdCode = prdData.getData().getProductCode() != null ? prdData
				.getData().getProductCode() : barcode;
		String aAKASite = prdData.getData().getSite().getCode() != null ? prdData
				.getData().getSite().getCode()
				: null;

		List<String> aAKACode = new ArrayList<String>();
		aAKACode.addAll(prdData.getData().getAltProductCodes());
		String aDeleteAKA = "N";
		String aPrimaryAKA;
		String AAKATYPECODE = "NULL";
		JsonToFieldValues.info("ProductAKA Altcode check$$$$$$$$$");

		if (aAKACode != null && aAKACode.isEmpty() == false) {
			ArrayList<Map<String, Object>> a = new ArrayList<Map<String, Object>>();
			exchange.getIn().setHeader("flagCheck", "C");
			JsonToFieldValues
					.info("ProductAKA in after AlcodeCheck ####$$$$$$$$$");
			for (int i = 0; i < aAKACode.size(); i++) {
				JsonToFieldValues.info("ProductAKA in for loop ####$$$$$$$$$");
				if (i == 0) {

					aPrimaryAKA = "Y";
				} else {
					aPrimaryAKA = "N";
				}

				Map<String, Object> prodAKAData = new LinkedHashMap<String, Object>();
				prodAKAData.put("batch_id", batch_id);
				prodAKAData.put("ADOADD", null);
				prodAKAData.put("IM_PRODAKA_SEQ", null);
				prodAKAData.put("ERRORCODE", null);
				prodAKAData.put("ERRORCODEI", null);
				prodAKAData.put("AIMPORTBLANKS", null);
				prodAKAData.put("aProdSite", aProdSite);
				prodAKAData.put("aProdCode", aProdCode);
				prodAKAData.put("aAKASite", aAKASite);
				prodAKAData.put("aAKACode", aAKACode.get(i));
				prodAKAData.put("aDeleteAKA", aDeleteAKA);
				prodAKAData.put("aPrimaryAKA", aPrimaryAKA);
				prodAKAData.put("AAKATYPECODE", AAKATYPECODE);

				a.add(i, prodAKAData);
			}

			exchange.getIn().setBody(a);
		} else {
			Map<String, Object> b = new LinkedHashMap<String, Object>();
			exchange.getIn().setHeader("flagCheck", "D");
			exchange.getIn().setBody(b);
		}

	}

	public void createProductApprovalsDataMapping(Exchange exchange)
			throws Exception {

		String batch_id = (String) exchange.getIn().getHeader("batchId");

		ProductsInfo prdData = null;
		Object objTemp = exchange.getProperty("productbody");
		if (objTemp instanceof ProductsInfo) {
			prdData = (ProductsInfo) exchange.getProperty("productbody");
		} else {
			prdData = ProductsInfo.fromString((String) exchange.getProperty("productbody"));
		}

		String aProductSite = (prdData.getData().getSite() != null) ? (prdData
				.getData().getSite().getCode() != null ? prdData.getData()
				.getSite().getCode() : null) : null;
		String barcode = prdData.getData().getBarcode() != null ? prdData
				.getData().getBarcode() : null;
		String aProductCode = prdData.getData().getProductCode() != null ? prdData
				.getData().getProductCode() : barcode;
		String aApproved = "Y";
		String aUpdateICR = "N";

		Map<String, Object> prodApprovalData = new LinkedHashMap<String, Object>();
		prodApprovalData.put("batch_id", batch_id);
		prodApprovalData.put("ADOADD", null);
		prodApprovalData.put("IM_PRODUCTAPPROVALS_SEQ", null);
		prodApprovalData.put("ERRORCODE", null);
		prodApprovalData.put("ERRORCODEI", null);
		prodApprovalData.put("AIMPORTBLANKS", null);
		prodApprovalData.put("aProductSite", aProductSite);
		prodApprovalData.put("aProductCode", aProductCode);
		prodApprovalData.put("aApproved", aApproved);
		prodApprovalData.put("aUpdateICR", aUpdateICR);
		exchange.getIn().setBody(prodApprovalData);

	}

	public void createTrackDataMappingForProductPackageFalse(Exchange exchange)
			throws Exception {
		String batch_id = (String) exchange.getIn().getHeader("batchId");

		ProductsInfo prodData = null;
		Object objTemp = exchange.getProperty("productbody");
		if (objTemp instanceof ProductsInfo) {
			prodData = (ProductsInfo) exchange.getProperty("productbody");
		} else {
			prodData = ProductsInfo.fromString((String) exchange.getProperty("productbody"));
		}
		TracksInfo trackallData = (TracksInfo) exchange.getIn().getBody();

		String aProductSite = prodData.getData().getSite() != null ? (prodData
				.getData().getSite().getCode() != null ? prodData.getData()
				.getSite().getCode() : null) : null;
		String aProductCode = null;

		String aSoundrecSite = trackallData.getRecording().getSite() != null ? ((trackallData
				.getRecording().getSite().getCode() != null) ? trackallData
				.getRecording().getSite().getCode() : null)
				: null;

		/*
		 * if(!(StringUtils.equals(trackallData.getRecording().getSite().getCode(
		 * ), null))) {
		 * aSoundrecSite=trackallData.getRecording().getSite().getCode(); } else
		 * { aSoundrecSite=null; }
		 */

		Integer aSoundrecCode = null;

		int aSide = trackallData.getSide() != null ? trackallData.getSide() + 1
				: null;
		int aTrack = trackallData.getIdx() != null ? trackallData.getIdx() + 1
				: null;
		/*
		 * if(aSide==0) { aSide=aSide+1; }
		 * 
		 * if(aTrack==0) { aTrack=aTrack+1; }
		 */

		String aSub = null;
		Integer aTime = trackallData.getRecording().getDuration() != null ? trackallData
				.getRecording().getDuration() : null;
		String aSoundrecISRC = trackallData.getRecording().getIsrc() != null ? trackallData
				.getRecording().getIsrc() : null;
		String aParentLin = "N";
		String aMedleyTitle = null;
		String aExProductCode = trackallData.getProduct() != null ? (trackallData
				.getProduct().getId() != null ? "BMGP_"
				+ trackallData.getProduct().getId() : null) : null;
		String aExSoundRecCode = trackallData.getRecording().getId() != null ? "BMGR_"
				+ trackallData.getRecording().getId()
				: null;

		Map<String, Object> trackData = new LinkedHashMap<String, Object>();
		trackData.put("batch_id", batch_id);
		trackData.put("ADOADD", null);
		trackData.put("IM_TRACK_SEQ", null);
		trackData.put("ERRORCODE", null);
		trackData.put("ERRORCODEI", null);
		trackData.put("AIMPORTBLANKS", null);
		trackData.put("aProductSite", aProductSite);
		trackData.put("aProductCode", aProductCode);
		trackData.put("aSoundrecSite", aSoundrecSite);
		trackData.put("aSoundrecCode", aSoundrecCode);
		trackData.put("aSide", aSide);
		trackData.put("aTrack", aTrack);
		trackData.put("aSub", aSub);
		trackData.put("aTime", aTime);
		trackData.put("aSoundrecISRC", aSoundrecISRC);
		trackData.put("aParentLink", aParentLin);
		trackData.put("aMedleyTitle", aMedleyTitle);
		trackData.put("aExProductCode", aExProductCode);
		trackData.put("aExSoundRecCode", aExSoundRecCode);

		exchange.getIn().setBody(trackData);

	}

	public Map<String, Object> createProductStatusTrueDataMapping(
			Exchange exchange) throws JsonParseException, JsonMappingException,
			IOException, ParseException {
		ObjectMapper mapper = new ObjectMapper();

		DataConversion objdataConv = new DataConversion();

		List<ProductChild> productsPackageList = mapper.readValue(exchange
				.getIn().getBody(String.class),
				new TypeReference<List<ProductChild>>() {
				});

		ProductsInfo prdData = (ProductsInfo) exchange.getProperty(
				"productbody");
		String prdBatchID = (String) exchange.getIn().getHeader("batchId");

		String batch_id = prdBatchID;
		String aSite = prdData.getData().getSite().getCode() != null ? prdData
				.getData().getSite().getCode() : " ";
		String aBarCode = prdData.getData().getBarcode() != null ? prdData
				.getData().getBarcode() : " ";
		String aCode = prdData.getData().getProductCode() != null ? prdData
				.getData().getProductCode() : aBarCode;

		String aTitle = prdData.getData().getProductTitle();

		String saTitle = aTitle != null ? objdataConv
				.replaceCommaWithDoller(aTitle) : null;
		String aArtist = prdData.getData().getArtistName() != null ? prdData
				.getData().getArtistName() : null;
		String saArtist = aArtist != null ? objdataConv
				.replaceCommaWithDoller(aArtist) : null;
		String aCompany = prdData.getData().getRepertoireOwnerCompany()
				.getCode() != null ? prdData.getData()
				.getRepertoireOwnerCompany().getCode() : null;
		int aComponents = 1;
		int aSides = prdData.getData().getFormat().getNumberOfSides();
		int aTracks = prdData.getData().getNumberOfTracks();
		String aConfiguration = prdData.getData().getFormat() != null ? prdData
				.getData().getFormat().getReMaConfigCode() : null;
		int aTotalPlayingTime = prdData.getData().getDuration();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		java.util.Date releaseDate = df.parse(prdData.getData()
				.getReleaseDate());
		long aIntReleaseDate = releaseDate.getTime();// (new
														// java.sql.Date(releaseDate.getTime()))
		int aCopyrightProductType = 0;
		String aLabel = prdData.getData().getLabel() != null ? (prdData
				.getData().getLabel().getLabelCode() != null ? prdData
				.getData().getLabel().getLabelCode() : null) : null;// prdData.getData().getLabel()!=null?(prdData.getData().getLabel().getParentCompany()!=null?(prdData.getData().getLabel().getParentCompany().getCode()!=null?prdData.getData().getLabel().getParentCompany().getCode():null):null):null;

		String aCompilation = (prdData.getData().getIsCompilation() == false) ? "N"
				: "Y";
		String aDigitalDownload = prdData.getData().getFormat().getIsPhysical() == true ? "N"
				: "Y";
		String aPromo = prdData.getData().getIsPromo() == false ? "N" : "Y";
		String aType = prdData.getData().getFormat() != null ? (prdData
				.getData().getFormat().getAudioVisual().startsWith("A") ? "A"
				: "V") : " ";
		String aRecType = "S";
		String aTech = "D";
		String aGridCode = prdData.getData().getGrId() != null ? prdData
				.getData().getGrId() : null;
		String aExplicitContent = prdData.getData()
				.getParentalAdvisoryLyricsIndicator().startsWith("N") ? "N"
				: "Y";
		String aPYear = Integer.toString(prdData.getData().getCopyrightYear());
		String aPNote = prdData.getData().getCopyrightNotice();
		String aCYear = Integer.toString(prdData.getData().getArtworkYear());
		String aCNote = prdData.getData().getArtworkNotice();
		String aCatCoEnabled = "N";
		String aCatCoResend = "N";
		String aAutoCreateAKACodes = "Y";
		String aExternalCode = "BMGP_" + prdData.getData().getId();
		String aUpdateUserCode = "Y";
		String aUpdateTerrBarcodes = "N";
		String aExternalLabelCode = Integer.toString(prdData.getData()
				.getLabel() != null ? prdData.getData().getLabel().getId() : 0);

		String aPriceCategory = null;

		Date aReleaseDate = null;

		Date aRereleaseDate = null;

		Date aDeletionDate = null;
		String aTVAdvertised = null;
		String aPayCopyright = "Y";//changes Done on 11 Aug as per Confluence

		String aCopyrightCapType = null;
		int aCopyrightCapSongs = (Integer) null;
		float aCopyrightCapValue = (Float) null;
		float aCopyrightCapPerc = (Float) null;
		Date aCopyrightCapFreezeDate = null;
		String aSociety = null;
		String aSocietyNumber = null;
		Date aTVAdFrom = null;
		Date aTVAdTo = null;
		String aInternal = null;
		int aPriceLevel = (Integer) null;
		String aTerritory = null;

		String aHoldReserves = null;
		int aCopyrightTracks = (Integer) null;

		String aAllowFree = null;
		String aRoyaltyInfoComplete = null;

		String aLanguage = null;

		String aStyle = null;
		String aVocal = null;
		String aPayCSI = null;
		String aPaySOCAN = null;
		String aProjectCode = null;

		String aProductClearedTerrName = null;
		String aProductExcludedTerrName = null;
		String aNotes = null;
		String aGenreGroup1 = null;
		String aGenre1 = null;
		String aGenreGroup2 = null;
		String aGenre2 = null;

		Map<String, Object> productData = new LinkedHashMap<String, Object>();
		productData.put("batch_id", batch_id);
		productData.put("ADOADD", null);
		productData.put("IM_PRODUCT_SEQ", null);
		productData.put("ERRORCODE", null);
		productData.put("ERRORCODEI", null);
		productData.put("AIMPORTBLANKS", null);
		productData.put("aSite", aSite);
		productData.put("aCode", aCode);
		productData.put("aBarCode", aBarCode);
		productData.put("aTitle", aTitle);
		productData.put("aArtist", aArtist);
		productData.put("aCompany", aCompany);
		productData.put("aComponents", aComponents);
		productData.put("aSides", aSides);
		productData.put("aTracks", aTracks);
		productData.put("aPriceCategory", aPriceCategory);
		productData.put("aConfiguration", aConfiguration);
		productData.put("aTotalPlayingTime", aTotalPlayingTime);
		productData.put("aReleaseDate", aReleaseDate);
		productData.put("aIntReleaseDate", aIntReleaseDate);
		productData.put("aRereleaseDate", aRereleaseDate);
		productData.put("aDeletionDate", aDeletionDate);
		productData.put("aTVAdvertised", aTVAdvertised);
		productData.put("aPayCopyright", aPayCopyright);
		productData.put("aCopyrightProductType", aCopyrightProductType);
		productData.put("aCopyrightCapType", aCopyrightCapType);
		productData.put("aCopyrightCapSongs", aCopyrightCapSongs);
		productData.put("aCopyrightCapValue", aCopyrightCapValue);
		productData.put("aCopyrightCapPerc", aCopyrightCapPerc);
		productData.put("aCopyrightCapFreezeDate", aCopyrightCapFreezeDate);
		productData.put("aSociety", aSociety);
		productData.put("aSocietyNumber", aSocietyNumber);
		productData.put("aTVAdFrom", aTVAdFrom);
		productData.put("aTVAdTo", aTVAdTo);
		productData.put("aInternal", aInternal);
		productData.put("aPriceLevel", aPriceLevel);
		productData.put("aTerritory", aTerritory);

		productData.put("aLabel", aLabel);
		productData.put("aHoldReserves", aHoldReserves);
		productData.put("aCopyrightTracks", aCopyrightTracks);
		productData.put("aCompilation", aCompilation);
		productData.put("aDigitalDownload", aDigitalDownload);
		productData.put("aAllowFree", aAllowFree);
		productData.put("aRoyaltyInfoComplete", aRoyaltyInfoComplete);
		productData.put("aPromo", aPromo);
		productData.put("aLanguage", aLanguage);
		productData.put("aType", aType);

		productData.put("aRecType", aRecType);
		productData.put("aTech", aTech);
		productData.put("aStyle", aStyle);
		productData.put("aVocal", aVocal);
		productData.put("aPayCSI", aPayCSI);
		productData.put("aPaySOCAN", aPaySOCAN);
		productData.put("aProjectCode", aProjectCode);
		productData.put("aGridCode", aGridCode);
		productData.put("aExplicitContent", aExplicitContent);

		productData.put("aProductClearedTerrName", aProductClearedTerrName);
		productData.put("aProductExcludedTerrName", aProductExcludedTerrName);

		productData.put("aPYear", aPYear);
		productData.put("aPNote", aPNote);
		productData.put("aCYear", aCYear);
		productData.put("aCNote", aCNote);
		productData.put("aCatCoEnabled", aCatCoEnabled);
		productData.put("aCatCoResend", aCatCoResend);
		productData.put("aAutoCreateAKACodes", aAutoCreateAKACodes);
		productData.put("aNotes", aNotes);
		productData.put("aExternalCode", aExternalCode);
		productData.put("aUpdateUserCode", aUpdateUserCode);
		productData.put("aUpdateTerrBarcodes", aUpdateTerrBarcodes);
		productData.put("aExternalLabelCode", aExternalLabelCode);
		productData.put("aGenreGroup1", aGenreGroup1);
		productData.put("aGenre1", aGenre1);
		productData.put("aGenreGroup2", aGenreGroup2);
		productData.put("aGenre2", aGenre2);

		return productData;

	}

	/*
	 * public Map<String, Object> createProductPackageTrueDataMapping( SongsInfo
	 * allData) {
	 * 
	 * }
	 * 
	 * public Map<String, Object> createTrackPackageTrueDataMapping( SongsInfo
	 * allData) {
	 * 
	 * }
	 */

	/*
	 * public void processSongListResponseJSON(Exchange exchange) throws
	 * JsonParseException, JsonMappingException, IOException { ObjectMapper
	 * mapper = new ObjectMapper();
	 * 
	 * 
	 * 
	 * List<SongPublisherInfo> songPubList = mapper.readValue(exchange
	 * .getIn().getBody(String.class), new
	 * TypeReference<List<SongPublisherInfo>>() { }); int len =
	 * songPubList.size();
	 * JsonToFieldValues.info("ParamData for Songpublisher length ---------->" +
	 * len); SongsInfo objSongInfo = (SongsInfo) exchange.getIn().getHeader(
	 * "songbody"); if (len > 0) {
	 * 
	 * JsonToFieldValues.info("SongPublisher List from API having size::"+len);
	 * exchange.setIn((Message) objSongInfo);;
	 * 
	 * 
	 * } else {
	 * 
	 * JsonToFieldValues.info("SongPublisher List is Empty from API"); }
	 * 
	 * }
	 */

	int aSide;
	int aSidecopy;
	static int max;
	static String prod = "";
	static String child = "";
	static String sideC = "";
	static String update = "";

	public void resetSideVal() {

		aSide = 0;
	}

	public void createTrackDataMappingForProductPackageTrue(Exchange exchange,
			@Header("childId") String childId, @Header("prodId") String prodId,
			@Header("MaxsideNum") String maxsideNum,
			@Header("aSideValTest") String asideValTest) throws Exception { // /
																			// ,

		JsonToFieldValues
				.info("aSide value at start of method---------------------"
						+ aSide);

		TracksInfo trackallDataes = (TracksInfo) exchange.getIn().getBody();
		JsonToFieldValues.info("" + trackallDataes.getId()
				+ "---------------------" + trackallDataes.getIdx()
				+ "-----------");
		/*
		 * if (trackallDataes.getIdx() == 0 && trackallDataes.getSide() == 0) {
		 * exchange.setProperty("sideMaxProperty", 0); aSide = 0; }
		 */

		if ((String) exchange.getProperty("sideC") != null) {
			sideC = (String) exchange.getProperty("sideC");
		}
		JsonToFieldValues
		.info("sideC value at start of method---------------------"
				+ sideC);
		// int aSidecopy;
		String batch_id = (String) exchange.getIn().getHeader("batchId");
		if (exchange.getProperty("sideMaxProperty") != null) {
			aSidecopy = (Integer) exchange.getProperty("sideMaxProperty");
		}

		int iSide;

		if (exchange.getProperty("sideMaxProperty") != null) {

			JsonToFieldValues.info("Entering the not null for property  "
					+ childId);
			
			String sSide = (String.valueOf(exchange
					.getProperty("sideMaxProperty")));
			JsonToFieldValues
					.info("sideMAXProperty inside track method $$$$$$$$$$$$$$$$$$$$$$$$$$  "
							+ sSide);
			iSide = Integer.parseInt(sSide);
		} else {
			JsonToFieldValues.info("Null condition Entering the for property  "
					+ childId);
			exchange.setProperty("sideMaxProperty", 1);

		}
		int aSideCount = 0;

		ProductsInfo prodData = null;
		Object objTemp = exchange.getProperty("productbody");
		if (objTemp instanceof ProductsInfo) {
			prodData = (ProductsInfo) exchange.getProperty("productbody");
		} else {
			prodData = ProductsInfo.fromString((String) exchange.getProperty("productbody"));
		}

		TracksInfo trackallData = (TracksInfo) exchange.getIn().getBody();

		String aProductSite = prodData.getData().getSite() != null ? prodData
				.getData().getSite().getCode() : null;
		String aProductCode = null;
		String aSoundrecSite = trackallData.getRecording().getSite() != null ? ((trackallData
				.getRecording().getSite().getCode() != null) ? trackallData
				.getRecording().getSite().getCode() : null)
				: null;

		exchange.setProperty("sideC", sideC);
		
		JsonToFieldValues.info("Null condition Entering the for property sideC value  "
				+ sideC);
		if (!(prod == prodId || prod.equals(prodId))) {
			aSide = 0;
			JsonToFieldValues.info("Null condition Entering the for property prod value  "
					+ prod);
			JsonToFieldValues.info("Null condition Entering the for property  prodid value"
					+ prodId);
		}

		ProductsInfo prdData = (ProductsInfo) exchange.getProperty(
				"productbody");
		String updateId = prdData.getData().getUpdated();

		if (!(update == updateId || update.equals(updateId))) {
			aSide = 0;
			JsonToFieldValues.info("Null condition Entering the for property  update value"
					+ update);
			
			JsonToFieldValues.info("Null condition Entering the for property  updateid value"
					+ updateId);
		}

		else {

			JsonToFieldValues.info("------------------------" + updateId
					+ "&&&&&&&&&&&&&&&&&&&&&&&&&&&&" + update);
		}
		prod = prodId;
		update = updateId;

		if (child == childId || child.equals(childId)) {

			if (!(sideC.equals(String.valueOf(trackallData.getSide())))) {
				aSide = aSide + 1;
				JsonToFieldValues.info("Thay are equal conditon aSide value"
						+ aSide);
				JsonToFieldValues
						.info("---------------------------They are equal------------------child:::"
								+ child
								+ "------------childId-----"
								+ childId
								+ "-----------------" + aSide);
			}

		} else {

			aSide = aSide + trackallData.getSide() + 1;
			
			JsonToFieldValues.info("Thay are not equal conditon aSide value"
					+ aSide);
			JsonToFieldValues
					.info("---------------------------They are not equal------------------child:::"
							+ child
							+ "------------childId----"
							+ childId
							+ "-----------------" + aSide);
		}

		sideC = String.valueOf(trackallData.getSide());
		
		JsonToFieldValues.info("After they are not equal condition  sideC value"
				+ sideC);
		max++;
		
		JsonToFieldValues.info("After they are not equal condition  max++ value"
				+ max);
		
		JsonToFieldValues.info("MaxSide value for child  " + childId + "$$$"
				+ Integer.parseInt(maxsideNum));
		aSideCount = aSideCount + Integer.parseInt(maxsideNum);
		int trackNum = trackallData.getIdx();

		int aTrack = trackNum + 1;

		String aSub = null;
		Integer aTime = trackallData.getRecording().getDuration() != null ? trackallData
				.getRecording().getDuration() : null;
		String aSoundrecISRC = trackallData.getRecording().getIsrc() != null ? trackallData
				.getRecording().getIsrc() : null;

		String aParentLin = "N";
		String aMedleyTitle = null;
		String aExProductCode = "BMGP_" + prodId;

		String aExSoundRecCode = trackallData.getRecording().getId() != null ? "BMGR_"
				+ trackallData.getRecording().getId()
				: null;

		Map<String, Object> trackData = new LinkedHashMap<String, Object>();
		trackData.put("batch_id", batch_id);
		trackData.put("ADOADD", null);
		trackData.put("IM_TRACK_SEQ", null);
		trackData.put("ERRORCODE", null);
		trackData.put("ERRORCODEI", null);
		trackData.put("AIMPORTBLANKS", null);
		trackData.put("aProductSite", aProductSite);
		trackData.put("aProductCode", aProductCode);
		trackData.put("aSoundrecSite", aSoundrecSite);
		trackData.put("aSoundrecCode", null);
		trackData.put("aSide", aSide);
		trackData.put("aTrack", aTrack);
		trackData.put("aSub", aSub);
		trackData.put("aTime", aTime);
		trackData.put("aSoundrecISRC", aSoundrecISRC);
		trackData.put("aParentLink", aParentLin);
		trackData.put("aMedleyTitle", aMedleyTitle);
		trackData.put("aExProductCode", aExProductCode);
		trackData.put("aExSoundRecCode", aExSoundRecCode);

		exchange.getIn().setHeader("MaxsideNum",
				Integer.parseInt(maxsideNum) + 1);
		
		exchange.setProperty("sideMaxProperty", aSide);
		/*
		 * JsonToFieldValues.info("aSideVal value at End of method-"+aSide+"_____"
		 * + exchange.getProperty("aSideVal")); exchange.setProperty("aSideVal",
		 * aSide); exchange.getIn().setHeader("aSideValTest",
		 * Integer.parseInt(asideValTest) + 1);
		 * JsonToFieldValues.info("aSideValTest value at End of method-"
		 * +asideValTest+"_____"+
		 * exchange.getIn().getHeader("aSideValTest")+"-----getting property"
		 * +exchange.getProperty("aSideVal"));
		 */
		JsonToFieldValues
				.info("aSide value at Endd of method---------------------"
						+ aSide);
		exchange.getIn().setBody(trackData);
		child = childId;
		
		JsonToFieldValues.info("childid end of method value"
				+ childId);
		
		
	}
	
//	 MFD -104 code changes starts here
	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * Method to populate the fields in Licencing Track csv
	 */
	public void createLicencingTrackDataMapping(Exchange exchange)
			throws Exception {


		ProductsInfo prdData = null;
		Object objTemp = exchange.getProperty("productbody");
		if (objTemp instanceof ProductsInfo) {
			prdData = (ProductsInfo) exchange.getProperty("productbody");
		} else {
			prdData = ProductsInfo.fromString((String) exchange.getProperty("productbody"));
		}

		String aProductSite = (prdData.getData().getSite() != null) ? (prdData
				.getData().getSite().getCode() != null ? prdData.getData()
				.getSite().getCode() : null) : null;
		String barcode = prdData.getData().getBarcode() != null ? prdData
				.getData().getBarcode() : null;
		String aProductCode = prdData.getData().getProductCode() != null ? prdData
				.getData().getProductCode() : barcode;

		Map<String, Object> licencingTrackData = new LinkedHashMap<String, Object>();
		licencingTrackData.put("Product Site", null);
		licencingTrackData.put("Product Code", null);
		licencingTrackData.put("Sound Recording Site", null);
		licencingTrackData.put("Sound Recording Code", null);
		licencingTrackData.put("Side Number", "1");
		licencingTrackData.put("Track Number", "trackList.sequenceNumber");
		licencingTrackData.put("Subtrack Code", null);
		licencingTrackData.put("Track Time", null);
		licencingTrackData.put("Sound Recording ISRC", null);
		licencingTrackData.put("Link Parent Product", "N");
		licencingTrackData.put("Medley Title", null);
		licencingTrackData.put("External Product Code", "BMGL_<products.product.productId>");
		licencingTrackData.put("External Sound Recording Code", "BMGR_<CaptureRecordingId>");
		exchange.getIn().setBody(licencingTrackData);

	}
	
	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * Method to populate the fields in Licencing ProducatAKA csv
	 */
	public void createLicencingProductAkaDataMapping(Exchange exchange)
			throws Exception {


		ProductsInfo prdData = null;
		Object objTemp = exchange.getProperty("productbody");
		if (objTemp instanceof ProductsInfo) {
			prdData = (ProductsInfo) exchange.getProperty("productbody");
		} else {
			prdData = ProductsInfo.fromString((String) exchange.getProperty("productbody"));
		}

		String aProductSite = (prdData.getData().getSite() != null) ? (prdData
				.getData().getSite().getCode() != null ? prdData.getData()
				.getSite().getCode() : null) : null;
		String barcode = prdData.getData().getBarcode() != null ? prdData
				.getData().getBarcode() : null;
		String aProductCode = prdData.getData().getProductCode() != null ? prdData
				.getData().getProductCode() : barcode;

		Map<String, Object> licencingTrackData = new LinkedHashMap<String, Object>();
		licencingTrackData.put("Product Site", null);
		licencingTrackData.put("Product Code", null);
		licencingTrackData.put("Sound Recording Site", null);
		licencingTrackData.put("Sound Recording Code", null);
		licencingTrackData.put("Side Number", "1");
		licencingTrackData.put("Track Number", "trackList.sequenceNumber");
		licencingTrackData.put("Subtrack Code", null);
		licencingTrackData.put("Track Time", null);
		licencingTrackData.put("Sound Recording ISRC", null);
		licencingTrackData.put("Link Parent Product", "N");
		licencingTrackData.put("Medley Title", null);
		licencingTrackData.put("External Product Code", "BMGL_<products.product.productId>");
		licencingTrackData.put("External Sound Recording Code", "BMGR_<CaptureRecordingId>");
		exchange.getIn().setBody(licencingTrackData);

	}
	/**
	 * 
	 * @param exchange
	 * @throws Exception
	 * Method to populate the fields in Licencing Product csv
	 */
	public void createLicencingProductDataMapping(Exchange exchange)
			throws Exception {


		ProductsInfo prdData = null;
		Object objTemp = exchange.getProperty("productbody");
		if (objTemp instanceof ProductsInfo) {
			prdData = (ProductsInfo) exchange.getProperty("productbody");
		} else {
			prdData = ProductsInfo.fromString((String) exchange.getProperty("productbody"));
		}

		String aProductSite = (prdData.getData().getSite() != null) ? (prdData
				.getData().getSite().getCode() != null ? prdData.getData()
				.getSite().getCode() : null) : null;
		String barcode = prdData.getData().getBarcode() != null ? prdData
				.getData().getBarcode() : null;
		String aProductCode = prdData.getData().getProductCode() != null ? prdData
				.getData().getProductCode() : barcode;

		Map<String, Object> licencingTrackData = new LinkedHashMap<String, Object>();
		licencingTrackData.put("Product Site", null);
		licencingTrackData.put("Product Code", null);
		licencingTrackData.put("Sound Recording Site", null);
		licencingTrackData.put("Sound Recording Code", null);
		licencingTrackData.put("Side Number", "1");
		licencingTrackData.put("Track Number", "trackList.sequenceNumber");
		licencingTrackData.put("Subtrack Code", null);
		licencingTrackData.put("Track Time", null);
		licencingTrackData.put("Sound Recording ISRC", null);
		licencingTrackData.put("Link Parent Product", "N");
		licencingTrackData.put("Medley Title", null);
		licencingTrackData.put("External Product Code", "BMGL_<products.product.productId>");
		licencingTrackData.put("External Sound Recording Code", "BMGR_<CaptureRecordingId>");
		exchange.getIn().setBody(licencingTrackData);

	}

}