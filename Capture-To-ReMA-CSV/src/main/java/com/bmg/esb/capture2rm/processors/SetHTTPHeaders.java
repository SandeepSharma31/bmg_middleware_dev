package com.bmg.esb.capture2rm.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.codec.binary.Base64;

public class SetHTTPHeaders implements Processor
{
	private String userName;
	private String password;
	@Override
	public void process(Exchange arg0) throws Exception 
	{
		String basicAuth = "Basic " +Base64.encodeBase64String((getUserName() + ":" + getPassword()).getBytes());

		arg0.getIn().setHeader("Authorization",basicAuth);
		arg0.getIn().setHeader(Exchange.HTTP_METHOD,"GET");
		arg0.getIn().setHeader(Exchange.CONTENT_TYPE,"application/json");
		arg0.getIn().setHeader(Exchange.ACCEPT_CONTENT_TYPE,"application/json");
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
