package com.bmg.esb.capture2rm.beans;



public class SongsInfo {

	// song
	// {

	private String type = "";
	private String changeindicator = "";
	private Data data;
	private String batchId="";

	public class Data {
		private Site site;
		private String title = "";// : "My First Recording",
		private String composerName = "";// : "Steve Harris",
		private boolean isIMaestro = false;// : false,
		private boolean isPublicDomain = false;// : false,
		private String complianceStatus = "";// : "NON_COMPLIANT",
		private String supplyChainStatus = "";// : "UNAVAILABLE",
		private int id = 0;// : 1

		private String iswc = ""; // created feild as per the mapping

		private String soundrecISRC = "";// as per mapping

		private String cmrraSongCode = "";// as per mapping
		private boolean generateSoundRec = false; // as per mapping
		private String artist = ""; // as per mapping
		private String lyricist = "";// as per mapping
		private boolean isImportBlanks = false;

		private String publisherName = "";
		private String created="";
		private String updated="";
		private String entityStatus;
		private String createdBy;

		// }

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getComposerName() {
			return composerName;
		}

		public void setComposerName(String composerName) {
			this.composerName = composerName;
		}

		public boolean getIsIMaestro() {
			return isIMaestro;
		}

		public void setIsIMaestro(boolean isIMaestro) {
			this.isIMaestro = isIMaestro;
		}

		public boolean getIsPublicDomain() {
			return isPublicDomain;
		}

		public void setIsPublicDomain(boolean isPublicDomain) {
			this.isPublicDomain = isPublicDomain;
		}

		public String getComplianceStatus() {
			return complianceStatus;
		}

		public void setComplianceStatus(String complianceStatus) {
			this.complianceStatus = complianceStatus;
		}

		public String getSupplyChainStatus() {
			return supplyChainStatus;
		}

		public void setSupplyChainStatus(String supplyChainStatus) {
			this.supplyChainStatus = supplyChainStatus;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public Site getSite() {
			return site;
		}

		public void setSite(Site site) {
			this.site = site;
		}

		public class Site// : {
		{
			private String id;// : "ba7e81f5-79cd-4ccd-a040-cb60ecf3ad02",
			private String name;// : "BMG Rights Management UK",
			private String code;// : "BGUK",
			private String country;// : "GB"

			public String getId() {
				return id;
			}

			public void setId(String id) {
				this.id = id;
			}

			public String getName() {
				return name;
			}

			public void setName(String name) {
				this.name = name;
			}

			public String getCode() {
				return code;
			}

			public void setCode(String code) {
				this.code = code;
			}

			public String getCountry() {
				return country;
			}

			public void setCountry(String country) {
				this.country = country;
			}

		}

		public String getIswc() {
			return iswc;
		}

		public void setIswc(String iswc) {
			this.iswc = iswc;
		}

		public String getSoundrecISRC() {
			return soundrecISRC;
		}

		public void setSoundrecISRC(String soundrecISRC) {
			this.soundrecISRC = soundrecISRC;
		}

		public String getArtist() {
			return artist;
		}

		public void setArtist(String artist) {
			this.artist = artist;
		}

		public String getLyricist() {
			return lyricist;
		}

		public void setLyricist(String lyricist) {
			this.lyricist = lyricist;
		}

		public String getCmrraSongCode() {
			return cmrraSongCode;
		}

		public void setCmrraSongCode(String cmrraSongCode) {
			this.cmrraSongCode = cmrraSongCode;
		}

		public boolean getGenerateSoundRec() {
			return generateSoundRec;
		}

		public void setGenerateSoundRec(boolean generateSoundRec) {
			this.generateSoundRec = generateSoundRec;
		}

		public boolean getIsImportBlanks() {
			return isImportBlanks;
		}

		public void setIsImportBlanks(boolean isImportBlanks) {
			this.isImportBlanks = isImportBlanks;
		}

		public String getPublisherName() {
			return publisherName;
		}

		public void setPublisherName(String publisherName) {
			this.publisherName = publisherName;
		}

		public String getCreated()
		{
			return created;
		}

		public void setCreated(String created)
		{
			this.created = created;
		}

		public String getUpdated()
		{
			return updated;
		}

		public void setUpdated(String updated)
		{
			this.updated = updated;
		}

		public String getEntityStatus()
		{
			return entityStatus;
		}

		public void setEntityStatus(String entityStatus)
		{
			this.entityStatus = entityStatus;
		}

		public String getCreatedBy()
		{
			return createdBy;
		}

		public void setCreatedBy(String createdBy)
		{
			this.createdBy = createdBy;
		}

	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getChangeindicator() {
		return changeindicator;
	}

	public void setChangeindicator(String changeindicator) {
		this.changeindicator = changeindicator;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public SongsInfo acceptSong(SongsInfo song) {
		return song;
	}

	public String getBatchId()
	{
		return batchId;
	}

	public void setBatchId(String batchId)
	{
		this.batchId = batchId;
	}

}
