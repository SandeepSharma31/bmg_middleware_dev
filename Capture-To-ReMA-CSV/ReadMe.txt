Camel Router Project for Blueprint (OSGi)
=========================================

To build this project use

    mvn install

To run the project you can execute the following Maven goal

    mvn camel:run

To deploy the project in OSGi. For example using Apache ServiceMix
or Apache Karaf. You can run the following command from its shell:

    osgi:install -s mvn:com.bmg.esb.capture2rm/Capture-To-ReMa/1.0.0-SNAPSHOT

For more help see the Apache Camel documentation

    http://camel.apache.org/

    
    CMRRA code mapping change, Royalti compliance indicator added and removed entityStatus
    9.6.7 version updated with '{body.getData.getFormat.getIsPackage}' earlier it was {body.getData.getIsPackage} as capture has made changes in Json message structure
    
    9.6.8 updated with SoundRecording related changes -to avoid blank file creation
    9.6.9 updated track issue listing for new product(aSideVal header added and done rest at packaged product level)
    9.7.0 uopdated with new Aggregator strategy an impleted for first product split for packed product and setting header in that.
    9.7.1 updated for common exception.
    9.7.2 updated with new value in aggregator
    9.7.7 track issue changes done(new method added and reset value for aSide) 
    9.7.8 side value chages only for changes child product. assigning childId to child.
    9.7.9 SoudRecording Digitised new enhancement for creating two files.
    9.8.0- this is working version in STaging
    9.8.1- Printing Request Headers.
    9.8.2- Changed http request url with header
    9.8.3- added log message and removed showheader option in uri for recordings
    9.8.5- updated "aPayCopyRight" to "Y" for recordings and Product
    9.8.7- updated aPriceCategory to "COMP" for both packaged and non-packaged product if acompilation is true
    
    