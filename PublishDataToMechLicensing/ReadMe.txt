Camel Router Project for Blueprint (OSGi)
=========================================

To build this project use

    mvn install

To run the project you can execute the following Maven goal

    mvn camel:run

To deploy the project in OSGi. For example using Apache ServiceMix
or Apache Karaf. You can run the following command from its shell:

    osgi:install -s mvn:com.bmg.esb/PublishDataToMechLicensing/1.0.0-SNAPSHOT

For more help see the Apache Camel documentation

    http://camel.apache.org/
    1.3.8 changed code for long(line 29 in setrangevalue method)
    
    1.4.1 made changes to database configuration details
    1.4.2 added changes to artist
    1.4.3 error for changing artifact id
    1.4.4 error resolution
    1.4.5 new version
    1.4.6 changes to log tabledata
    1.4.7 error resolution
    1.4.8 added songstatus field to SongEntity.
    1.5.0 changes made as per navin's suggestion.
    1.5.5 changes done for MechArtist for mapping issue(i.e,name)
    1.5.6 changes done ID as id 
    1.5.7 changes done Artist entity for column change
    1.5.8 added one more Entity(Composer)
    1.6.8 resolved coding issues for new entity(Composer)
    1.7.0 Made changes to Query for Artist and Composer(forgot to put top 1in first query)
    1.7.5 addes two more entities(client&Linksongclient)  
    1.7.6 changed the persistent-id in the blue print
    1.7.7 added IV_CHAIN_ID column to mechlinksongpublisher