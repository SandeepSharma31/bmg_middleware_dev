package com.bmg.esb.PublishDataToMechLicensing.transformer;

import com.bmg.esb.PublishDataToMechLicensing.beans.MechArtist;
import com.bmg.esb.PublishDataToMechLicensing.beans.MechClient;
import com.bmg.esb.PublishDataToMechLicensing.beans.MechComposer;
import com.bmg.esb.PublishDataToMechLicensing.beans.MechLinkSongClient;
import com.bmg.esb.PublishDataToMechLicensing.beans.MechLinkSongPublisher;
import com.bmg.esb.PublishDataToMechLicensing.beans.MechPublisher;
import com.bmg.esb.PublishDataToMechLicensing.beans.MechSong;
import com.google.gson.Gson;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;







public class DBToJSon
{
  private static final Logger dbToJSonLogger = LoggerFactory.getLogger(DBToJSon.class);
  
  public DBToJSon() {}
  
  public String convertSongToJSon(MechSong allData) {
    String json = "";
    dbToJSonLogger.debug("MecLic:MechSong:in convertSongToJSon");
    

    Map<String, Object> songData = new HashMap();
    
    try
    {
      Map<String, Object> map = new LinkedHashMap();
      map.put("type", "MechSong");
      
      dbToJSonLogger.debug("MecLic:MechSong:Message before creatSongDataMapping conversion to JSon data{}");
      songData = creatSongDataMapping(allData);
      map.put("data", songData);
      Gson gson = new Gson();
      json = gson.toJson(map);
      map = null;
      dbToJSonLogger.debug("MecLic:MechSong:Message after conversion to JSon::" + json);

    }
    catch (Exception e)
    {

      dbToJSonLogger.error("MecLic:MechSong:In Exception block of convertSongToJSon while creating json message" + e.getMessage());
    }
    return json;
  }
  


  private Map<String, Object> creatSongDataMapping(MechSong allData)
  {
    Map<String, Object> songData = new HashMap();
    dbToJSonLogger.debug("MecLic:MechSong:in creatSongDataMapping");
    
    songData.put("songId", Long.valueOf(allData.getSNGDTL_SONG_CODE()));
    songData.put("name", allData.getSNGDTL_SONG_TITLE());
    songData.put("composer", allData.getSNGDTL_COMPOSERS());
    songData.put("territory", allData.getSNGDTL_OWNING_SYS_TERR());
    songData.put("songstatus", allData.getSNGDTL_SONG_STATUS());
    songData.put("id", Long.valueOf(allData.getID()));
    
    dbToJSonLogger.debug("MecLic:MechSong:creatSongDataMapping:After putting into Map of Song");
    return songData;
  }
  
  public String convertPublisherToJSon(MechPublisher allData)
  {
    Map<String, Object> publisherData = new HashMap();
    String json = "";
    dbToJSonLogger.debug("MecLic:MechPublisher:in convertPublisherToJSon");
    
    try
    {
      Map<String, Object> map = new LinkedHashMap();
      map.put("type", "MechPublisher");
      
      dbToJSonLogger.debug("MecLic:MechPublisher:convertPublisherToJSon:Message before convertPublisherToJSon conversion to JSon data{}");
      publisherData = creatPublisherDataMapping(allData);
      map.put("data", publisherData);
      Gson gson = new Gson();
      json = gson.toJson(map);
      map = null;
      dbToJSonLogger.debug("MecLic:MechPublisher:convertPublisherToJSon after conversion to JSon::" + json);

    }
    catch (Exception e)
    {

      dbToJSonLogger.error("MecLic:MechPublisher:convertPublisherToJSon: Exception block of convertPublisherToJSon while creating json message" + e.getMessage());
    }
    return json;
  }
  


  private Map<String, Object> creatPublisherDataMapping(MechPublisher allData)
  {
    Map<String, Object> publisherdata = new HashMap();
    dbToJSonLogger.debug("MecLic:MechPublisher:In creatPublisherDataMapping");
    
    publisherdata.put("pub_id", Long.valueOf(allData.getIdPub()));
    publisherdata.put("name", allData.getName());
    publisherdata.put("id", Long.valueOf(allData.getID()));
    
    dbToJSonLogger.debug("MecLic:MechPublisher:creatPublisherDataMapping: putting into Map of Publisher");
    return publisherdata;
  }
  
  public String convertLinkSongPublisherToJSon(MechLinkSongPublisher allData)
  {
    Map<String, Object> linkSongPublsherData = new HashMap();
    String json = "";
    dbToJSonLogger.debug("MecLic:MechLinkSongPublisher:in convertLinkSongPublisherToJSon");
    
    try
    {
      Map<String, Object> map = new LinkedHashMap();
      map.put("type", "MechLinkSongPublisher");
      
      dbToJSonLogger.debug("MecLic:MechLinkSongPublisher:Message before convertLinkSongPublisherToJSon conversion to JSon data{}");
      linkSongPublsherData = creatLinkSongPublisherDataMapping(allData);
      map.put("data", linkSongPublsherData);
      Gson gson = new Gson();
      json = gson.toJson(map);
      map = null;
      dbToJSonLogger.debug("MecLic:MechLinkSongPublisher:Message after conversion to JSon::" + json);

    }
    catch (Exception e)
    {

      dbToJSonLogger.error("MecLic:MechLinkSongPublisher:In Exception block of convertSongIPToJSon while creating json message" + e.getMessage());
    }
    return json;
  }
  


  private Map<String, Object> creatLinkSongPublisherDataMapping(MechLinkSongPublisher linkSong)
  {
    dbToJSonLogger.debug("MecLic:MechLinkSongPublisher:in creatLinkSongPublisherDataMapping");
    Map<String, Object> linkSongPublsherData = new HashMap();
    linkSongPublsherData.put("song_id", Long.valueOf(linkSong.getSongCode()));
    linkSongPublsherData.put("publisher_id", Long.valueOf(linkSong.getIpCode1()));
    linkSongPublsherData.put("original_id", linkSong.getIsOriginatingTerritory());
    linkSongPublsherData.put("territory_id", linkSong.getTerritoryId());
    linkSongPublsherData.put("ip_type", linkSong.getIpType1());
    linkSongPublsherData.put("sips_controlled_id", linkSong.getIsControlled());
    linkSongPublsherData.put("mech_share", linkSong.getMechControlledShare());
    linkSongPublsherData.put("chain_id", Long.valueOf(linkSong.getChainId()));
    linkSongPublsherData.put("id", Long.valueOf(linkSong.getID()));
    
    dbToJSonLogger.debug("MecLic:MechLinkSongPublisher:creatSongIPDataMapping: putting into Map of SongIP");
    
    return linkSongPublsherData;
  }
  


  public String convertArtistToJSon(MechArtist allData)
  {
    Map<String, Object> artistData = new HashMap();
    String json = "";
    dbToJSonLogger.debug("MecLic:MechArtist:in convertArtistToJSon");
    
    try
    {
      Map<String, Object> map = new LinkedHashMap();
      map.put("type", "MechArtist");
      
      dbToJSonLogger.debug("MecLic:MechArtist:convertArtistToJSon:Message before convertArtistToJSon conversion to JSon data{}");
      artistData = createArtistDataMapping(allData);
      map.put("data", artistData);
      Gson gson = new Gson();
      json = gson.toJson(map);
      map = null;
      dbToJSonLogger.debug("MecLic:MechArtist:convertArtistToJSon after conversion to JSon::" + json);

    }
    catch (Exception e)
    {

      dbToJSonLogger.error("MecLic:MechArtist:convertArtistToJSon: Exception block of convertArtistToJSon while creating json message" + e.getMessage());
    }
    return json;
  }
  


  private Map<String, Object> createArtistDataMapping(MechArtist allData)
  {
    Map<String, Object> artistData = new HashMap();
    dbToJSonLogger.debug("MecLic:MechArtist:In createArtistDataMapping");
    


    artistData.put("id", Long.valueOf(allData.getID()));
    artistData.put("name", allData.getDG_ARTA_ARTIST_NAME());
    
    dbToJSonLogger.debug("MecLic:MechArtist:In createArtistDataMapping: putting into Map of Artist");
    return artistData;
  }
  



  public String convertComposerToJSon(MechComposer allData)
  {
    Map<String, Object> composerData = new HashMap();
    String json = "";
    dbToJSonLogger.debug("MecLic:MechComposer:in convertComposerToJSon");
    
    try
    {
      Map<String, Object> map = new LinkedHashMap();
      map.put("type", "MechComposer");
      
      dbToJSonLogger.debug("MecLic:MechComposer:convertComposerToJSon:Message before convertComposerToJSon conversion to JSon data{}");
      composerData = createComposerDataMapping(allData);
      map.put("data", composerData);
      Gson gson = new Gson();
      json = gson.toJson(map);
      map = null;
      dbToJSonLogger.debug("MecLic:MechComposer:convertComposerToJSon after conversion to JSon::" + json);

    }
    catch (Exception e)
    {

      dbToJSonLogger.error("MecLic:MechComposer:convertComposerToJSon: Exception block of convertComposerToJSon while creating json message" + e.getMessage());
    }
    return json;
  }
  


  private Map<String, Object> createComposerDataMapping(MechComposer allData)
  {
    Map<String, Object> composerData = new HashMap();
    dbToJSonLogger.debug("MecLic:MechComposer:In createComposerDataMapping");
    



    try
    {
      composerData.put("comp_id", Long.valueOf(allData.getIdComp()));
      composerData.put("surname", allData.getSurName());
      composerData.put("firstname", allData.getFirstName());
      composerData.put("lastname", allData.getLastName());
      composerData.put("id", Long.valueOf(allData.getID()));
      dbToJSonLogger.debug("MecLic:MechComposer:In createComposerDataMapping:putting into Map of Composer");
    }
    catch (Exception e) {
      dbToJSonLogger.debug("MecLic:MechComposer:In createComposerDataMapping:Error putting into Map of Composer");
    }
    
    return composerData;
  }
  
  public String convertClientToJSon(MechClient allData)
  {
    Map<String, Object> clientData = new HashMap();
    String json = "";
    dbToJSonLogger.debug("MecLic:MechClient:in convertClientToJSon");
    
    try
    {
      Map<String, Object> map = new LinkedHashMap();
      map.put("type", "MechClient");
      
      dbToJSonLogger.debug("MecLic:MechClient:convertClientToJSon:Message before convertClientToJSon conversion to JSon data{}");
      clientData = createClientDataMapping(allData);
      map.put("data", clientData);
      Gson gson = new Gson();
      json = gson.toJson(map);
      map = null;
      dbToJSonLogger.debug("MecLic:MechClient:convertClientToJSon after conversion to JSon::" + json);

    }
    catch (Exception e)
    {

      dbToJSonLogger.error("MecLic:MechClient:convertClientToJSon: Exception block of convertClientToJSon while creating json message" + e.getMessage());
    }
    return json;
  }
  


  private Map<String, Object> createClientDataMapping(MechClient allData)
  {
    Map<String, Object> clientData = new HashMap();
    dbToJSonLogger.debug("MecLic:MechClient:In createClientDataMapping");
    



    try
    {
      clientData.put("client_id", Long.valueOf(allData.getClientCode()));
      clientData.put("clientName", allData.getClientName());
      clientData.put("addedDate", Long.valueOf(allData.getAddedDate()));
      clientData.put("addedBy", allData.getAddedBy());
      clientData.put("amendedDate", Long.valueOf(allData.getAmendedDate()));
      clientData.put("amendedBy", allData.getAmendedBy());
      clientData.put("territory", allData.getTerritory());
      clientData.put("id", Long.valueOf(allData.getID()));
      dbToJSonLogger.debug("MecLic:MechClient:In createClientDataMapping:putting into Map of Client");
    }
    catch (Exception e) {
      dbToJSonLogger.debug("MecLic:MechClient:In createClientDataMapping:Error putting into Map of Client");
    }
    
    return clientData;
  }
  

  public String convertLinkSongClientToJSon(MechLinkSongClient allData)
  {
    Map<String, Object> linkSongClientData = new HashMap();
    String json = "";
    dbToJSonLogger.debug("MecLic:MechLinkSongClient:in convertLinkSongClientToJSon");
    
    try
    {
      Map<String, Object> map = new LinkedHashMap();
      map.put("type", "MechLinkSongClient");
      
      dbToJSonLogger.debug("MecLic:MechLinkSongClient:convertLinkSongClientToJSon:Message before convertLinkSongClientToJSon conversion to JSon data{}");
      linkSongClientData = createLinkSongClientDataMapping(allData);
      map.put("data", linkSongClientData);
      Gson gson = new Gson();
      json = gson.toJson(map);
      map = null;
      dbToJSonLogger.debug("MecLic:MechLinkSongClient:convertLinkSongClientToJSon after conversion to JSon::" + json);

    }
    catch (Exception e)
    {

      dbToJSonLogger.error("MecLic:MechLinkSongClient:convertLinkSongClientToJSon:Exception block of convertLinkSongClient while creating json message" + e.getMessage());
    }
    return json;
  }
  


  private Map<String, Object> createLinkSongClientDataMapping(MechLinkSongClient allData)
  {
    Map<String, Object> linkSongClientData = new HashMap();
    dbToJSonLogger.debug("MecLic:MechLinkSongClient:In createClientDataMapping");
    



    try
    {
      linkSongClientData.put("song_id", Long.valueOf(allData.getSongCode()));
      linkSongClientData.put("client_id", Long.valueOf(allData.getClientCode()));
      linkSongClientData.put("territory", allData.getTerritory());
      linkSongClientData.put("mechFraction", Long.valueOf(allData.getMechFraction()));
      linkSongClientData.put("id", Long.valueOf(allData.getID()));
      dbToJSonLogger.debug("MecLic:MechLinkSongClient:In createClientDataMapping:putting into Map of Client");
    }
    catch (Exception e) {
      dbToJSonLogger.debug("MecLic:MechLinkSongClient:In createClientDataMapping:Error putting into Map of Client");
    }
    
    return linkSongClientData;
  }
}