package com.bmg.esb.PublishDataToMechLicensing.beans;

public class MechPublisher implements Comparable<MechPublisher> {
  private long idPub;
  private String name;
  private long ID;
  private long id_md;
  
  public MechPublisher() {}
  
  public long getId_md() {
    return id_md;
  }
  
  public void setId_md(long id_md) {
    this.id_md = id_md;
  }
  
  public int compareTo(MechPublisher pub)
  {
    return (int)(id_md - id_md);
  }
  



  public long getIdPub()
  {
    return idPub;
  }
  
  public void setIdPub(long idPub) {
    this.idPub = idPub;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public long getID() {
    return ID;
  }
  
  public void setID(long iD) {
    ID = iD;
  }
}