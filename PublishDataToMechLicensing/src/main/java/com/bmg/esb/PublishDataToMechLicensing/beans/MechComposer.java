package com.bmg.esb.PublishDataToMechLicensing.beans;

public class MechComposer implements Comparable<MechComposer>
{
  private long idComp;
  private String firstName;
  private String lastName;
  private String surName;
  private long ID;
  private long id_md;
  
  public MechComposer() {}
  
  public long getId_md()
  {
    return id_md;
  }
  
  public void setId_md(long id_md) {
    this.id_md = id_md;
  }
  
  public int compareTo(MechComposer com)
  {
    return (int)(id_md - id_md);
  }
  


  public long getIdComp()
  {
    return idComp;
  }
  
  public void setIdComp(long idComp) {
    this.idComp = idComp;
  }
  
  public String getFirstName() {
    return firstName;
  }
  
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  
  public String getLastName() {
    return lastName;
  }
  
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  
  public String getSurName() {
    return surName;
  }
  
  public void setSurName(String surName) {
    this.surName = surName;
  }
  
  public long getID() {
    return ID;
  }
  
  public void setID(long iD) {
    ID = iD;
  }
}