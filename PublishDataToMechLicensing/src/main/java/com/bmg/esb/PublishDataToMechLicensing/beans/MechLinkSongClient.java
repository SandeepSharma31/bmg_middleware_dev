package com.bmg.esb.PublishDataToMechLicensing.beans;

public class MechLinkSongClient { private long songCode;
  private long clientCode;
  private String territory;
  private long mechFraction;
  private long ID;
  
  public MechLinkSongClient() {}
  
  public long getSongCode() { return songCode; }
  
  public void setSongCode(long songCode)
  {
    this.songCode = songCode;
  }
  
  public long getClientCode() {
    return clientCode;
  }
  
  public void setClientCode(long clientCode) {
    this.clientCode = clientCode;
  }
  
  public String getTerritory() {
    return territory;
  }
  
  public void setTerritory(String territory) {
    this.territory = territory;
  }
  
  public long getMechFraction() {
    return mechFraction;
  }
  
  public void setMechFraction(long mechFraction) {
    this.mechFraction = mechFraction;
  }
  
  public long getID() {
    return ID;
  }
  
  public void setID(long iD) {
    ID = iD;
  }
}