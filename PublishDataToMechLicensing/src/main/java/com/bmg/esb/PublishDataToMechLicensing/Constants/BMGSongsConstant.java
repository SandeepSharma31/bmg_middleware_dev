package com.bmg.esb.PublishDataToMechLicensing.Constants;

public class BMGSongsConstant
{
  public static final String ENTITY_MECHSONG = "MECHSONGENTITY";
  public static final String ENTITY_MECHARTIST = "MECHARTISTENTITY";
  public static final String ENTITY_MECHPUBLISHER = "MECHPUBLISHERENTITY";
  public static final String ENTITY_MECHLINKSONGPUBLISHER = "MECHLINKSONGPUBLISHER";
  public static final String ENTITY_MECHCOMPOSER = "MECHCOMPOSERENTITY";
  public static final String ENTITY_MECHCLIENT = "MECHCLIENTENTITY";
  public static final String ENTITY_MECHLINKSONGCLIENT = "MECHLINKSONGCLIENTENTITY";
  
  public BMGSongsConstant() {}
}