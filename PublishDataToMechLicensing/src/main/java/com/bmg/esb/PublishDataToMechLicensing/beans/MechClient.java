package com.bmg.esb.PublishDataToMechLicensing.beans;

public class MechClient { private long clientCode;
  private String clientName;
  private long addedDate;
  private String addedBy;
  private long amendedDate;
  private String amendedBy;
  private String territory;
  private long ID;
  
  public MechClient() {}
  
  public long getClientCode() { return clientCode; }
  
  public void setClientCode(long clientCode)
  {
    this.clientCode = clientCode;
  }
  
  public String getClientName() {
    return clientName;
  }
  
  public void setClientName(String clientName) {
    this.clientName = clientName;
  }
  
  public long getAddedDate() {
    return addedDate;
  }
  
  public void setAddedDate(long addedDate) {
    this.addedDate = addedDate;
  }
  
  public String getAddedBy() {
    return addedBy;
  }
  
  public void setAddedBy(String addedBy) {
    this.addedBy = addedBy;
  }
  
  public long getAmendedDate() {
    return amendedDate;
  }
  
  public void setAmendedDate(long amendedDate) {
    this.amendedDate = amendedDate;
  }
  
  public String getAmendedBy() {
    return amendedBy;
  }
  
  public void setAmendedBy(String amendedBy) {
    this.amendedBy = amendedBy;
  }
  
  public String getTerritory() {
    return territory;
  }
  
  public void setTerritory(String territory) {
    this.territory = territory;
  }
  
  public long getID() {
    return ID;
  }
  
  public void setID(long iD) {
    ID = iD;
  }
}