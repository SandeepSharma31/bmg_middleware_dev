package com.bmg.esb.PublishDataToMechLicensing.beans;

public class MechArtist implements Comparable<MechArtist> {
  private long DG_ARTA_ARTIST_CODE;
  private String DG_ARTA_ARTIST_NAME;
  private long ID;
  private long id_md;
  
  public MechArtist() {}
  
  public int compareTo(MechArtist artist) {
    return (int)(id_md - id_md);
  }
  

  public long getDG_ARTA_ARTIST_CODE()
  {
    return DG_ARTA_ARTIST_CODE;
  }
  
  public void setDG_ARTA_ARTIST_CODE(long dG_ARTA_ARTIST_CODE) {
    DG_ARTA_ARTIST_CODE = dG_ARTA_ARTIST_CODE;
  }
  
  public String getDG_ARTA_ARTIST_NAME() {
    return DG_ARTA_ARTIST_NAME;
  }
  
  public void setDG_ARTA_ARTIST_NAME(String dG_ARTA_ARTIST_NAME) {
    DG_ARTA_ARTIST_NAME = dG_ARTA_ARTIST_NAME;
  }
  
  public long getID() {
    return ID;
  }
  
  public void setID(long iD) {
    ID = iD;
  }
  
  public long getId_md() {
    return id_md;
  }
  
  public void setId_md(long id_md) {
    this.id_md = id_md;
  }
}