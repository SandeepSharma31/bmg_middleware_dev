package com.bmg.esb.PublishDataToMechLicensing.beans;

public class MechSong implements Comparable<MechSong> {
  private long SNGDTL_SONG_CODE;
  private String SNGDTL_SONG_TITLE;
  private String SNGDTL_COMPOSERS;
  private String SNGDTL_OWNING_SYS_TERR;
  private String SNGDTL_SONG_STATUS;
  private long ID;
  private long id_md;
  
  public MechSong() {}
  
  public long getId_md() {
    return id_md;
  }
  
  public void setId_md(long id_md) {
    this.id_md = id_md;
  }
  
  public int compareTo(MechSong sng)
  {
    return (int)(id_md - id_md);
  }
  



  public long getSNGDTL_SONG_CODE()
  {
    return SNGDTL_SONG_CODE;
  }
  
  public void setSNGDTL_SONG_CODE(long sNGDTL_SONG_CODE) {
    SNGDTL_SONG_CODE = sNGDTL_SONG_CODE;
  }
  
  public String getSNGDTL_SONG_TITLE() {
    return SNGDTL_SONG_TITLE;
  }
  
  public void setSNGDTL_SONG_TITLE(String sNGDTL_SONG_TITLE) {
    SNGDTL_SONG_TITLE = sNGDTL_SONG_TITLE;
  }
  
  public String getSNGDTL_COMPOSERS() {
    return SNGDTL_COMPOSERS;
  }
  
  public void setSNGDTL_COMPOSERS(String sNGDTL_COMPOSERS) {
    SNGDTL_COMPOSERS = sNGDTL_COMPOSERS;
  }
  
  public String getSNGDTL_OWNING_SYS_TERR() {
    return SNGDTL_OWNING_SYS_TERR;
  }
  
  public void setSNGDTL_OWNING_SYS_TERR(String sNGDTL_OWNING_SYS_TERR) {
    SNGDTL_OWNING_SYS_TERR = sNGDTL_OWNING_SYS_TERR;
  }
  
  public String getSNGDTL_SONG_STATUS() {
    return SNGDTL_SONG_STATUS;
  }
  
  public void setSNGDTL_SONG_STATUS(String sNGDTL_SONG_STATUS) {
    SNGDTL_SONG_STATUS = sNGDTL_SONG_STATUS;
  }
  
  public long getID() {
    return ID;
  }
  
  public void setID(long iD) {
    ID = iD;
  }
}