package com.bmg.esb.PublishDataToMechLicensing.transformer;

import java.util.List;
import java.util.Map;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;





public class logTableData
{
  private static final Logger logTableData = LoggerFactory.getLogger(logTableData.class);
  
  public logTableData() {}
  
  public void setRangeValue(List<Map<String, Object>> countList, Exchange ex) {
    logTableData.info("in createRangeValue");
    

    Long start_range = Long.valueOf(0L);
    logTableData.info("in createRangeValue" + countList.size());
    if (countList.size() > 0)
    {

      start_range = (Long)((Map)countList.get(0)).get("startCount");
    }
    





    logTableData.info("in createRangeValue" + start_range);
    if (ex.getIn().getHeader("entityType").equals("MECHSONGENTITY"))
    {
      logTableData.info("MECHSONG and value for start range is " + start_range);
      ex.getIn().setHeader("songStartRange", start_range);
    }
    else if (ex.getIn().getHeader("entityType").equals("MECHARTISTENTITY"))
    {
      logTableData.info("MECHARTIST and value for start range is " + start_range);
      ex.getIn().setHeader("artistStartRange", start_range);
    }
    else if (ex.getIn().getHeader("entityType").equals("MECHPUBLISHERENTITY"))
    {
      logTableData.info("MECHPUBLISHER and value for start range is " + start_range);
      ex.getIn().setHeader("publisherStartRange", start_range);
    }
    else if (ex.getIn().getHeader("entityType").equals("MECHLINKSONGPUBLISHER"))
    {
      logTableData.info("MECHLINKSONGPUBLISHER and value for start range is " + start_range);
      ex.getIn().setHeader("linksongpublisherStartRange", start_range);


    }
    else if (ex.getIn().getHeader("entityType").equals("MECHCOMPOSERENTITY"))
    {
      logTableData.info("MECHCOMPOSER and value for start range is " + start_range);
      ex.getIn().setHeader("composerStartRange", start_range);


    }
    else if (ex.getIn().getHeader("entityType").equals("MECHCLIENTENTITY"))
    {
      logTableData.info("MECHCLIENT and value for start range is " + start_range);
      ex.getIn().setHeader("clientStartRange", start_range);

    }
    else if (ex.getIn().getHeader("entityType").equals("MECHLINKSONGCLIENTENTITY"))
    {
      logTableData.info("MECHLINKSONGCLIENT and value for start range is " + start_range);
      ex.getIn().setHeader("linksongclientStartRange", start_range);
    }
  }
}