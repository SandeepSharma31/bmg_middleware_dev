package com.bmg.esb.PublishDataToMechLicensing.transformer;

import com.bmg.esb.PublishDataToMechLicensing.beans.MechArtist;
import com.bmg.esb.PublishDataToMechLicensing.beans.MechClient;
import com.bmg.esb.PublishDataToMechLicensing.beans.MechComposer;
import com.bmg.esb.PublishDataToMechLicensing.beans.MechLinkSongClient;
import com.bmg.esb.PublishDataToMechLicensing.beans.MechLinkSongPublisher;
import com.bmg.esb.PublishDataToMechLicensing.beans.MechPublisher;
import com.bmg.esb.PublishDataToMechLicensing.beans.MechSong;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;








public class ChangeFormat
{
  private static final Logger ChangeFormatLogger = LoggerFactory.getLogger(ChangeFormat.class);
  
  public ChangeFormat() {}
  
  public void generateJSONEntityWise(List<Map<String, Object>> inputList, Exchange ex)
  {
    if (inputList.size() > 0)
    {

      if (ex.getIn().getHeader("entityType").equals("MECHSONGENTITY"))
      {


        ArrayList<MechSong> alRet = new ArrayList();
        




        if (inputList.size() > 0) {
          ChangeFormatLogger.debug("MecLic:MechSong:generateJSONEntityWise:calling method");
          getSongByID(alRet, inputList);
        }
        


        Collections.sort(alRet);
        ChangeFormatLogger.info("MecLic:MechSong:generateJSONEntityWise:MechSong Sorting$$$$$ startEntityID  --" + ((MechSong)alRet.get(0)).getId_md() + "--endEntityID--" + ((MechSong)alRet.get(alRet.size() - 1)).getId_md());
        
        if (alRet.size() > 0) {
          int deltaSize = alRet.size();
          
          ex.getIn().setHeader("startEntityID", Long.valueOf(((MechSong)alRet.get(0)).getId_md()));
          
          ChangeFormatLogger.debug("MecLic:MechSong:generateJSONEntityWise:ex.getIn().getHeader(startEntityID):" + 
            ex.getIn().getHeader("startEntityID"));
          ex.getIn().setHeader("endEntityID", Long.valueOf(((MechSong)alRet.get(alRet.size() - 1)).getId_md()));
          
          ChangeFormatLogger.debug("MecLic:MechSong:generateJSONEntityWise:ex.getIn(),getHeader(endEntityID):" + 
            ex.getIn().getHeader("endEntityID"));
        }
        else {
          ex.getIn().setHeader("startEntityID", "NODATA");
          ChangeFormatLogger.info("MecLic:MechSong:generateJSONEntityWise:::If list null then we are setting flag and not processing further");
        }
        

        ChangeFormatLogger.info("MecLic:MechSong:generateJSONEntityWise:MechSong Entity Size of the list before conversion to JSon::" + alRet.size());
        
        ex.getIn().setBody(alRet);
        alRet = null;



      }
      else if (ex.getIn().getHeader("entityType").equals("MECHPUBLISHERENTITY"))
      {

        ArrayList<MechPublisher> alRet = new ArrayList();
        




        if (inputList.size() > 0) {
          ChangeFormatLogger.debug("MecLic:MechPublisher:generateJSONEntityWise:calling method");
          getPublisherByID(alRet, inputList);
        }
        

        Collections.sort(alRet);
        ChangeFormatLogger.info("MecLic:MechPublisher:generateJSONEntityWise:MechPublisher Sorting$$$$$ startEntityID --" + ((MechPublisher)alRet.get(0)).getId_md() + "--endEntityID--" + ((MechPublisher)alRet.get(alRet.size() - 1)).getId_md());
        if (alRet.size() > 0) {
          int deltaSize = alRet.size();
          
          ex.getIn().setHeader("startEntityID", Long.valueOf(((MechPublisher)alRet.get(0)).getId_md()));
          ChangeFormatLogger
            .debug("MecLic:MechPublisher:generateJSONEntityWise:ex.getIn(),getHeader(startEntityID):" + 
            ex.getIn().getHeader("startEntityID"));
          
          ex.getIn().setHeader("endEntityID", Long.valueOf(((MechPublisher)alRet.get(alRet.size() - 1)).getId_md()));
          ChangeFormatLogger
            .debug("MecLic:MechPublisher:generateJSONEntityWise:ex.getIn(),getHeader(endEntityID):" + 
            ex.getIn().getHeader("endEntityID"));
        }
        else {
          ex.getIn().setHeader("startEntityID", "NODATA");
          ChangeFormatLogger.info("MecLic:MechPublisher:generateJSONEntityWise:If list null then we are setting flag and not processing further");
        }
        

        ChangeFormatLogger.info("MecLic:MechPublisher:generateJSONEntityWise:Size of the list before conversion to JSon::" + alRet.size());
        
        ex.getIn().setBody(alRet);
        alRet = null;



      }
      else if (ex.getIn().getHeader("entityType").equals("MECHLINKSONGPUBLISHER"))
      {

        ArrayList<MechLinkSongPublisher> alRet = new ArrayList();
        




        if (inputList.size() > 0) {
          ChangeFormatLogger.debug("MecLic:MechLinkSongPublisher:generateJSONEntityWise:calling method");
          
          getMechLinkSongByID(alRet, inputList);
        }
        

        Collections.sort(alRet);
        ChangeFormatLogger.info("MecLic:MechLinkSongPublisher:generateJSONEntityWise:MechLinkSongPublisher Sorting$$$$$ startEntityID --" + ((MechLinkSongPublisher)alRet.get(0)).getId_md() + "--endEntityID--" + ((MechLinkSongPublisher)alRet.get(alRet.size() - 1)).getId_md());
        
        if (alRet.size() > 0) {
          int deltaSize = alRet.size();
          





          ex.getIn().setHeader("startEntityID", Long.valueOf(((MechLinkSongPublisher)alRet.get(0)).getId_md()));
          ChangeFormatLogger
            .debug("MecLic:MechLinkSongPublisher:generateJSONEntityWise:ex.getIn(),getHeader(startEntityID):" + 
            ex.getIn().getHeader("startEntityID"));
          
          ex.getIn().setHeader("endEntityID", Long.valueOf(((MechLinkSongPublisher)alRet.get(alRet.size() - 1)).getId_md()));
          ChangeFormatLogger
            .debug("MecLic:MechLinkSongPublisher:generateJSONEntityWise:ex.getIn(),getHeader(endEntityID):" + 
            ex.getIn().getHeader("endEntityID"));
        }
        else {
          ex.getIn().setHeader("startEntityID", "NODATA");
          ChangeFormatLogger.info("MecLic:MechLinkSongPublisher:generateJSONEntityWise:If list null then we are setting flag and not processing further");
        }
        

        ChangeFormatLogger.info("MecLic:MechLinkSongPublisher:generateJSONEntityWise: Size of the list before conversion to JSon::" + 
          alRet.size());
        
        ex.getIn().setBody(alRet);
        alRet = null;




      }
      else if (ex.getIn().getHeader("entityType").equals("MECHARTISTENTITY"))
      {

        ArrayList<MechArtist> alRet = new ArrayList();
        




        if (inputList.size() > 0) {
          ChangeFormatLogger.debug("MecLic:MechArtist:generateJSONEntityWise:calling method");
          getArtistByID(alRet, inputList);
        }
        

        Collections.sort(alRet);
        ChangeFormatLogger.info("MecLic:MechArtist:generateJSONEntityWise:MechArtist Sorting$$$$$ startEntityID --" + ((MechArtist)alRet.get(0)).getId_md() + "--endEntityID--" + ((MechArtist)alRet.get(alRet.size() - 1)).getId_md());
        if (alRet.size() > 0) {
          int deltaSize = alRet.size();
          
          ex.getIn().setHeader("startEntityID", Long.valueOf(((MechArtist)alRet.get(0)).getId_md()));
          ChangeFormatLogger
            .debug("MecLic:MechArtist:generateJSONEntityWise:ex.getIn(),getHeader(startEntityID):" + 
            ex.getIn().getHeader("startEntityID"));
          
          ex.getIn().setHeader("endEntityID", Long.valueOf(((MechArtist)alRet.get(alRet.size() - 1)).getId_md()));
          ChangeFormatLogger
            .debug("MecLic:MechArtist:generateJSONEntityWise:ex.getIn(),getHeader(endEntityID):" + 
            ex.getIn().getHeader("endEntityID"));
        }
        else {
          ex.getIn().setHeader("startEntityID", "NODATA");
          ChangeFormatLogger.info("MecLic:MechArtist:generateJSONEntityWise:If list null then we are setting flag and not processing further");
        }
        

        ChangeFormatLogger.info("MecLic:MechArtist:generateJSONEntityWise:Size of the list before conversion to JSon::" + alRet.size());
        
        ex.getIn().setBody(alRet);
        alRet = null;





      }
      else if (ex.getIn().getHeader("entityType").equals("MECHCOMPOSERENTITY")) {
        ArrayList<MechComposer> alRet = new ArrayList();
        
        if (inputList.size() > 0) {
          ChangeFormatLogger.debug("MecLic:MechComposer:generateJSONEntityWise:calling method");
          getComposerByID(alRet, inputList);
        }
        

        Collections.sort(alRet);
        ChangeFormatLogger.info("MecLic:MechComposer:generateJSONEntityWise:MechComposer Sorting$$$$$ startEntityID --" + ((MechComposer)alRet.get(0)).getId_md() + "--endEntityID--" + ((MechComposer)alRet.get(alRet.size() - 1)).getId_md());
        if (alRet.size() > 0) {
          int deltaSize = alRet.size();
          
          ex.getIn().setHeader("startEntityID", Long.valueOf(((MechComposer)alRet.get(0)).getId_md()));
          ChangeFormatLogger
            .debug("MecLic:MechComposer:generateJSONEntityWise:ex.getIn(),getHeader(startEntityID):" + 
            ex.getIn().getHeader("startEntityID"));
          
          ex.getIn().setHeader("endEntityID", Long.valueOf(((MechComposer)alRet.get(alRet.size() - 1)).getId_md()));
          ChangeFormatLogger
            .debug("MecLic:MechComposer:generateJSONEntityWise:ex.getIn(),getHeader(endEntityID):" + 
            ex.getIn().getHeader("endEntityID"));
        }
        else {
          ex.getIn().setHeader("startEntityID", "NODATA");
          ChangeFormatLogger.info("MecLic:MechComposer:generateJSONEntityWise:If list null then we are setting flag and not processing further");
        }
        

        ChangeFormatLogger.info("MecLic:MechComposer:generateJSONEntityWise:Size of the list before conversion to JSon::" + alRet.size());
        
        ex.getIn().setBody(alRet);
        alRet = null;




      }
      else if (ex.getIn().getHeader("entityType").equals("MECHCLIENTENTITY"))
      {
        List<Map<String, Object>> deltaClientList = (List)ex.getIn().getHeader("MechclientData");
        ArrayList<MechClient> alRet = new ArrayList();
        
        for (Map<String, Object> inputMap : inputList)
        {

          if (inputMap.size() > 0)
          {
            ChangeFormatLogger.debug("MecLic:MechClient:generateJSONEntityWise:inputMap having values ");
            ChangeFormatLogger.debug("MecLic:MechClient:generateJSONEntityWise:calling method");
            alRet.add(getClientByID(inputMap));
          }
        }
        
        if (deltaClientList.size() > 0)
        {
          int deltaSize = deltaClientList.size();
          ex.getIn().setHeader("startEntityID", ((Map)deltaClientList.get(0)).get("id"));
          

          ChangeFormatLogger.debug("MecLic:MechClient:generateJSONEntityWise:ex.getIn(),getHeader(startEntityID):" + 
            ex.getIn().getHeader("startEntityID"));
          
          ex.getIn().setHeader("endEntityID", ((Map)deltaClientList.get(deltaSize - 1)).get("id"));
          
          ChangeFormatLogger.debug("MecLic:MechClient:generateJSONEntityWise:ex.getIn(),getHeader(endEntityID):" + 
            ex.getIn().getHeader("endEntityID"));
        }
        else
        {
          ex.getIn().setHeader("startEntityID", "NODATA");
          ChangeFormatLogger.info("MecLic:MechClient:generateJSONEntityWise:If list null and list empty then we are setting flag and not processing further");
        }
        

        ChangeFormatLogger.info("MecLic:MechClient:generateJSONEntityWise:Size of the list before conversion to JSon::" + alRet.size());
        
        ex.getIn().setBody(alRet);
        deltaClientList = null;
        alRet = null;





      }
      else if (ex.getIn().getHeader("entityType").equals("MECHLINKSONGCLIENTENTITY"))
      {
        List<Map<String, Object>> deltaSongClientLinkList = (List)ex.getIn().getHeader("MechLinkSongClientdata");
        ArrayList<MechLinkSongClient> alRet = new ArrayList();
        
        for (Map<String, Object> inputMap : inputList)
        {

          if (inputMap.size() > 0)
          {
            ChangeFormatLogger.debug("MecLic:MechLinkSongClient:generateJSONEntityWise:inputMap having values ");
            ChangeFormatLogger.debug("MecLic:MechLinkSongClient:generateJSONEntityWise:calling method");
            alRet.add(getLinkSongClientByID(inputMap));
          }
        }
        
        if (deltaSongClientLinkList.size() > 0)
        {
          int deltaSize = deltaSongClientLinkList.size();
          ex.getIn().setHeader("startEntityID", ((Map)deltaSongClientLinkList.get(0)).get("id"));
          
          ChangeFormatLogger.debug("MecLic:MechLinkSongClient:generateJSONEntityWise:ex.getIn(),getHeader(startEntityID):" + 
            ex.getIn().getHeader("startEntityID"));
          
          ex.getIn().setHeader("endEntityID", ((Map)deltaSongClientLinkList.get(deltaSize - 1)).get("id"));
          
          ChangeFormatLogger.debug("MecLic:MechLinkSongClient:generateJSONEntityWise:ex.getIn(),getHeader(endEntityID):" + 
            ex.getIn().getHeader("endEntityID"));
        }
        else
        {
          ex.getIn().setHeader("startEntityID", "NODATA");
          ChangeFormatLogger.info("MecLic:MechLinkSongClient:generateJSONEntityWise:If list null and list empty then we are setting flag and not processing further");
        }
        

        ChangeFormatLogger.info("MecLic:MechLinkSongClient:generateJSONEntityWise:Size of the list before conversion to JSon::" + alRet.size());
        
        ex.getIn().setBody(alRet);
        deltaSongClientLinkList = null;
        alRet = null;
      }
    }
  }
  





  private void getSongByID(ArrayList<MechSong> alRet, List<Map<String, Object>> inputList)
  {
    for (Map<String, Object> inputMap : inputList)
    {


      ChangeFormatLogger.debug("MecLic:MechSong:getSongByID");
      
      ChangeFormatLogger
        .debug("MecLic:MechSong:getSongByID:getting values from map and setting to");
      MechSong sdNew = new MechSong();
      


      long id = ((Long)inputMap.get("id_md")).longValue();
      sdNew.setId_md(id);
      



      BigDecimal songCode = (BigDecimal)inputMap.get("SNGDTL_SONG_CODE");
      if (songCode != null) { sdNew.setSNGDTL_SONG_CODE(songCode.longValue());
      }
      
      if (inputMap.get("SNGDTL_SONG_TITLE") != null) {
        sdNew.setSNGDTL_SONG_TITLE((String)inputMap.get("SNGDTL_SONG_TITLE"));
      } else {
        sdNew.setSNGDTL_SONG_TITLE("");
      }
      
      if (inputMap.get("SNGDTL_COMPOSERS") != null) {
        sdNew.setSNGDTL_COMPOSERS((String)inputMap.get("SNGDTL_COMPOSERS"));
      } else {
        sdNew.setSNGDTL_COMPOSERS("");
      }
      if (inputMap.get("SNGDTL_OWNING_SYS_TERR") != null) {
        sdNew.setSNGDTL_OWNING_SYS_TERR((String)inputMap.get("SNGDTL_OWNING_SYS_TERR"));
      }
      else {
        sdNew.setSNGDTL_OWNING_SYS_TERR("");
      }
      if (inputMap.get("SNGDTL_SONG_STATUS") != null) {
        sdNew.setSNGDTL_SONG_STATUS((String)inputMap.get("SNGDTL_SONG_STATUS"));
      } else {
        sdNew.setSNGDTL_SONG_STATUS("");
      }
      
      long ID = ((Long)inputMap.get("ID")).longValue();
      sdNew.setID(ID);
      


      alRet.add(sdNew);
      


      ChangeFormatLogger.debug("MecLic:MechSong:getSongByID:After setting all values to new map");
    }
  }
  





  private void getPublisherByID(ArrayList<MechPublisher> alRet, List<Map<String, Object>> inputList)
  {
    for (Map<String, Object> inputMap : inputList)
    {

      ChangeFormatLogger.debug("MecLic:MechPublisher:In getPublisherByID");
      

      MechPublisher pubdata = new MechPublisher();
      


      long id = ((Long)inputMap.get("id_md")).longValue();
      pubdata.setId_md(id);
      

      if (inputMap.get("idPub") != null) {
        pubdata.setIdPub(((BigDecimal)inputMap.get("idPub")).longValue());
      }
      

      if (inputMap.get("name") != null) {
        pubdata.setName((String)inputMap.get("name"));
      }
      else {
        pubdata.setName("");
      }
      long ID = ((Long)inputMap.get("ID")).longValue();
      pubdata.setID(ID);
      


      alRet.add(pubdata);
      
      ChangeFormatLogger.debug("MecLic:MechPublisher:getPublisherByID:After setting all values to new map");
    }
  }
  




  private void getMechLinkSongByID(ArrayList<MechLinkSongPublisher> alRet, List<Map<String, Object>> inputList)
  {
    for (Map<String, Object> inputMap : inputList)
    {

      ChangeFormatLogger.debug("MecLic:MechLinkSongPublisher:In getSongIPByID");
      
      MechLinkSongPublisher mechLinkSongdata = new MechLinkSongPublisher();
      



      long id = ((Long)inputMap.get("id_md")).longValue();
      mechLinkSongdata.setId_md(id);
      

      if (inputMap.get("songCode") != null)
      {
        mechLinkSongdata.setSongCode(((BigDecimal)inputMap.get("songCode")).longValue());
      }
      if (inputMap.get("ipCode1") != null)
      {
        mechLinkSongdata.setIpCode1(((BigDecimal)inputMap.get("ipCode1")).longValue());
      }
      

      if (inputMap.get("isOriginatingTerritory") != null) {
        mechLinkSongdata.setIsOriginatingTerritory(
          (String)inputMap.get("isOriginatingTerritory"));
      } else {
        mechLinkSongdata.setIsOriginatingTerritory("");
      }
      if (inputMap.get("territoryId") != null) {
        mechLinkSongdata.setTerritoryId(
          (String)inputMap.get("territoryId"));
      }
      


      if (inputMap.get("ipType1") != null) {
        mechLinkSongdata.setIpType1((String)inputMap.get("ipType1"));
      }
      
      if (inputMap.get("isControlled") != null) {
        mechLinkSongdata.setIsControlled((String)inputMap.get("isControlled"));
      }
      
      if (inputMap.get("mechControlledShare") != null) {
        mechLinkSongdata.setMechControlledShare(
          Double.valueOf(((BigDecimal)inputMap.get("mechControlledShare")).doubleValue()));
      }
      


      if (inputMap.get("chainId") != null)
      {
        mechLinkSongdata.setChainId(((BigDecimal)inputMap.get("chainId")).longValue());
      }
      


      if (inputMap.get("ID") != null) {
        mechLinkSongdata.setID(((Long)inputMap.get("ID")).longValue());
      }
      



      alRet.add(mechLinkSongdata);
      
      ChangeFormatLogger.debug("MecLic:MechLinkSongPublisher:getMechLinkSongByID:After setting all values to new map");
    }
  }
  





  private void getArtistByID(ArrayList<MechArtist> alRet, List<Map<String, Object>> inputList)
  {
    for (Map<String, Object> inputMap : inputList)
    {


      ChangeFormatLogger.debug("MecLic:MechArtist:In getArtistByID");
      

      MechArtist artistdata = new MechArtist();
      


      long id = ((Long)inputMap.get("id_md")).longValue();
      artistdata.setId_md(id);
      


      if (inputMap.get("DG_ARTA_ARTIST_NAME") != null) {
        artistdata.setDG_ARTA_ARTIST_NAME((String)inputMap.get("DG_ARTA_ARTIST_NAME"));
      }
      else {
        artistdata.setDG_ARTA_ARTIST_NAME("");
      }
      long ID = ((Long)inputMap.get("ID")).longValue();
      artistdata.setID(ID);
      


      alRet.add(artistdata);
      
      ChangeFormatLogger.debug("MecLic:MechArtist:getArtistByID:After setting all values to new map");
    }
  }
  






  private void getComposerByID(ArrayList<MechComposer> alRet, List<Map<String, Object>> inputList)
  {
    for (Map<String, Object> inputMap : inputList)
    {


      ChangeFormatLogger.debug("MecLic:MechComposer:In getComposerByID");
      

      MechComposer composerdata = new MechComposer();
      


      long id = ((Long)inputMap.get("id_md")).longValue();
      composerdata.setId_md(id);
      


      if (inputMap.get("idComp") != null)
      {
        composerdata.setIdComp(((BigDecimal)inputMap.get("idComp")).longValue());
      }
      
      if (inputMap.get("surName") != null) {
        composerdata.setSurName((String)inputMap.get("surName"));
      }
      else {
        composerdata.setSurName("");
      }
      
      if (inputMap.get("firstName") != null) {
        composerdata.setFirstName((String)inputMap.get("firstName"));
      }
      
      if (inputMap.get("lastName") != null) {
        composerdata.setLastName((String)inputMap.get("lastName"));
      }
      

      long ID = ((Long)inputMap.get("ID")).longValue();
      composerdata.setID(ID);
      



      alRet.add(composerdata);
      
      ChangeFormatLogger.debug("MecLic:MechComposer:getComposerByID:After setting all values to new map");
    }
  }
  


  private MechClient getClientByID(Map<String, Object> inputMap)
  {
    MechClient clientdata = new MechClient();
    
    ChangeFormatLogger.debug("MecLic:MechClient:In getClientByID");
    







    if (inputMap.get("clientCode") != null)
    {
      clientdata.setClientCode(((BigDecimal)inputMap.get("clientCode")).longValue());
    }
    
    if (inputMap.get("clientName") != null) {
      clientdata.setClientName((String)inputMap.get("clientName"));
    }
    else {
      clientdata.setClientName("");
    }
    if (inputMap.get("addedDate") != null)
    {
      clientdata.setAddedDate(((BigDecimal)inputMap.get("addedDate")).longValue());
    }
    
    if (inputMap.get("addedBy") != null) {
      clientdata.setAddedBy((String)inputMap.get("addedBy"));
    }
    else {
      clientdata.setAddedBy("");
    }
    if (inputMap.get("amendedDate") != null)
    {
      clientdata.setAmendedDate(((BigDecimal)inputMap.get("amendedDate")).longValue());
    }
    

    if (inputMap.get("amendedBy") != null) {
      clientdata.setAmendedBy((String)inputMap.get("amendedBy"));
    }
    
    if (inputMap.get("territory") != null) {
      clientdata.setTerritory((String)inputMap.get("territory"));
    }
    

    long ID = ((Long)inputMap.get("ID")).longValue();
    clientdata.setID(ID);
    


    ChangeFormatLogger.debug("MecLic:MechClient:getClientByID:After setting all values to new map");
    return clientdata;
  }
  

  private MechLinkSongClient getLinkSongClientByID(Map<String, Object> inputMap)
  {
    MechLinkSongClient sngClientdata = new MechLinkSongClient();
    ChangeFormatLogger.debug("MecLic:MechLinkSongClient:In getLinkSongClientByID");
    




    if (inputMap.get("songCode") != null)
    {
      sngClientdata.setSongCode(((BigDecimal)inputMap.get("songCode")).longValue());
    }
    
    if (inputMap.get("clientCode") != null)
    {
      sngClientdata.setClientCode(((BigDecimal)inputMap.get("clientCode")).longValue());
    }
    
    if (inputMap.get("territory") != null)
    {
      sngClientdata.setTerritory((String)inputMap.get("territory"));
    }
    else
    {
      sngClientdata.setTerritory("");
    }
    
    if (inputMap.get("mechFraction") != null)
    {
      sngClientdata.setMechFraction(((BigDecimal)inputMap.get("mechFraction")).longValue());
    }
    
    long ID = ((Long)inputMap.get("ID")).longValue();
    sngClientdata.setID(ID);
    
    ChangeFormatLogger.debug("MecLic:MechLinkSongClient:In getLinkSongClientByID:After setting all values to new map");
    
    return sngClientdata;
  }
}