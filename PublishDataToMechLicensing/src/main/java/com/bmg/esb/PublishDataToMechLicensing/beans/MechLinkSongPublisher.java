package com.bmg.esb.PublishDataToMechLicensing.beans;

public class MechLinkSongPublisher implements Comparable<MechLinkSongPublisher>
{
  private long songCode;
  private long ipCode1;
  private String isOriginatingTerritory;
  private String territoryId;
  private String ipType1;
  
  public MechLinkSongPublisher() {}
  
  public long getChainId()
  {
    return chainId;
  }
  
  public void setChainId(long chainId) {
    this.chainId = chainId;
  }
  
  private String isControlled;
  private Double mechControlledShare;
  private long chainId;
  private long ID;
  private long id_md;
  public long getId_md()
  {
    return id_md;
  }
  
  public void setId_md(long id_md) {
    this.id_md = id_md;
  }
  
  public int compareTo(MechLinkSongPublisher link)
  {
    return (int)(id_md - id_md);
  }
  



  public long getSongCode()
  {
    return songCode;
  }
  
  public void setSongCode(long songCode) {
    this.songCode = songCode;
  }
  
  public long getIpCode1() {
    return ipCode1;
  }
  
  public void setIpCode1(long ipCode1) {
    this.ipCode1 = ipCode1;
  }
  
  public String getIsOriginatingTerritory() {
    return isOriginatingTerritory;
  }
  
  public void setIsOriginatingTerritory(String isOriginatingTerritory) {
    this.isOriginatingTerritory = isOriginatingTerritory;
  }
  
  public String getTerritoryId() {
    return territoryId;
  }
  
  public void setTerritoryId(String territoryId) {
    this.territoryId = territoryId;
  }
  
  public String getIpType1() {
    return ipType1;
  }
  
  public void setIpType1(String ipType1) {
    this.ipType1 = ipType1;
  }
  
  public String getIsControlled() {
    return isControlled;
  }
  
  public void setIsControlled(String isControlled) {
    this.isControlled = isControlled;
  }
  
  public Double getMechControlledShare() {
    return mechControlledShare;
  }
  
  public void setMechControlledShare(Double mechControlledShare) {
    this.mechControlledShare = mechControlledShare;
  }
  
  public long getID() {
    return ID;
  }
  
  public void setID(long iD) {
    ID = iD;
  }
}