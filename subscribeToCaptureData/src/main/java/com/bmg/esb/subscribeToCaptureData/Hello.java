package com.bmg.esb.subscribeToCaptureData;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
