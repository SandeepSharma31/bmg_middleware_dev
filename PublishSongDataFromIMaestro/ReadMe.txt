Camel Router Project for Blueprint (OSGi)
=========================================

To build this project use

    mvn install

To run the project you can execute the following Maven goal

    mvn camel:run

To deploy the project in OSGi. For example using Apache ServiceMix
or Apache Karaf. You can run the following command from its shell:

    osgi:install -s mvn:com.bmg.esb.PublishSongDataFromIMaestro/PublishSongDataFromIMaestro/1.0.0-SNAPSHOT

For more help see the Apache Camel documentation

    http://camel.apache.org/

   
   1.	Pick up the latest entry for an entity. From the record, we need to extract the maximum id. This would give us the last processed record id for the entity.
2.	Fetch next batch of 1000 records from the delta table. This batch will have id starting from next count of the id retrieved in step 1. 
3.	Fetch remaining data from “tbl” table using the key retrieved in step 2.
4.	Log above id range in the log table along with timestamp.
   
   Working interface......
    
    Staging Code version 2.0.7deployed on 24thApr
    Song entity
    
    Staging Code version 2.0.8deployed on 4thMay
    Song entity
    SongStatus
    SongShare
    SongIP
    
    Staging code version 3.0.6 deployed on 8th May
     Song entity
    SongStatus
    SongShare
    SongIP
    Composer
    Publisher
    
   TISHeirachy
   
   All Entites code
   Queries change for fetching 1000 records per min per entity,SongGroup Entity fetched for 'SO' records changes done.
   Integrated with Common logger and exception handler component(version 6.0.5)-Not deployed
   version 6.0.6- updated changes for songStatus field to status(Song Entity),TISCode updated to TisCode,added new field aliasSongCode in SongAKA. Mfd,89,90,91 updated
   
   
   6.0.7- updated common logger changes for Song entity
   6.0.8- without Common logger and updated changes for mfd-89,90,91
   6.0.9- without common logger and SongSharestart range value missing and updated in this
   6.1.0- SongGrouplink related Query change.
   6.1.1- All Queries changed to 'top 1000 and greater than count ', and updated mfd93
   6.1.2- updated mfd-91 and changed timer for SongIP to 75s earlier it was 60s. also changed SongIP record count to 750
   6.1.3- updated Param count to 1
   6.1.4- Internal AMQ related changes done so that it should connect to Queue.
   6.1.6- Join query on songIp to test  performance improvement
   6.1.7- Join query on 12 entities which have delta views to test  performance improvement
   6.2.5- Comparable interface implemetation for sorting Arraylist for 11 entities where delta views are used.(Records getting skip issue.)
   6.2.8- Made changes only to song entity to add extra two columns.
   6.2.9- Made changes to columns not be null
   6.3.2- Made chages to columns to be null
   6.3.3- columns names has been changed by DB
   6.3.6- handled null condition for two columns
   