package com.bmg.esb.PublishSongDataFromIMaestro.beans;

public class SongStatus implements Comparable<SongStatus>{
	
	private String songStatusCode;
	private String description;
	private String ChangeIndicator;
	private long ID;
	private long id_md;
	
	
	//code addeed for sorting
	
		public long getId_md() {
			return id_md;
		}
		public void setId_md(long id_md) {
			this.id_md = id_md;
		}
		
		
		public int compareTo(SongStatus sng){
			
			return (int) (this.id_md - sng.id_md);
			
			
		}
		
		//ends here

	
	public String getSongStatusCode() {
		return songStatusCode;
	}
	public void setSongStatusCode(String songStatusCode) {
		this.songStatusCode = songStatusCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getChangeIndicator() {
		return ChangeIndicator;
	}
	public void setChangeIndicator(String changeIndicator) {
		ChangeIndicator = changeIndicator;
	}
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}
	
}
