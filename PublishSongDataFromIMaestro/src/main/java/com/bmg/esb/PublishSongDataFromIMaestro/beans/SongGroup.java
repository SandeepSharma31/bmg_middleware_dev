package com.bmg.esb.PublishSongDataFromIMaestro.beans;

public class SongGroup {
	
	
private String groupCode;
private String groupName;
private String groupShortName;
private String ChangeIndicator;
private long ID;

public String getGroupCode() {
	return groupCode;
}
public void setGroupCode(String groupCode) {
	this.groupCode = groupCode;
}
public String getGroupName() {
	return groupName;
}
public void setGroupName(String groupName) {
	this.groupName = groupName;
}
public String getGroupShortName() {
	return groupShortName;
}
public void setGroupShortName(String groupShortName) {
	this.groupShortName = groupShortName;
}
public String getChangeIndicator() {
	return ChangeIndicator;
}
public void setChangeIndicator(String changeIndicator) {
	ChangeIndicator = changeIndicator;
}
public long getID() {
	return ID;
}
public void setID(long iD) {
	ID = iD;
}
}
