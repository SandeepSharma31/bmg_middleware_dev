package com.bmg.esb.PublishSongDataFromIMaestro.beans;

public class SongShare implements Comparable<SongShare> {

	private long songCode;
	private String tisCode;
	private Double mechOwnershipShare;
	private Double perfOwnershipShare;
	private Double synchOwnershipShare;
	private Double mechControlledShare;
	private Double perfControlledShare;
	private Double synchControlledShare;
	private String isControlled;
	private String ChangeIndicator;
	private long ID;
	private long id_md;
	//code addeed for sorting
	
		public long getId_md() {
			return id_md;
		}
		public void setId_md(long id_md) {
			this.id_md = id_md;
		}
		
		
		public int compareTo(SongShare sng){
			
			return (int) (this.id_md - sng.id_md);
			
			
		}
		
		//ends here

	
	public String getChangeIndicator() {
		return ChangeIndicator;
	}
	public void setChangeIndicator(String changeIndicator) {
		ChangeIndicator = changeIndicator;
	}
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}
	
	
	public long getSongCode() {
		return songCode;
	}
	public void setSongCode(long songCode) {
		this.songCode = songCode;
	}
	public String getTisCode() {
		return tisCode;
	}
	public void setTisCode(String tisCode) {
		this.tisCode = tisCode;
	}
	public Double getMechOwnershipShare() {
		return mechOwnershipShare;
	}
	public void setMechOwnershipShare(Double mechOwnershipShare) {
		this.mechOwnershipShare = mechOwnershipShare;
	}
	public Double getPerfOwnershipShare() {
		return perfOwnershipShare;
	}
	public void setPerfOwnershipShare(Double perfOwnershipShare) {
		this.perfOwnershipShare = perfOwnershipShare;
	}
	public Double getSynchOwnershipShare() {
		return synchOwnershipShare;
	}
	public void setSynchOwnershipShare(Double synchOwnershipShare) {
		this.synchOwnershipShare = synchOwnershipShare;
	}
	public Double getMechControlledShare() {
		return mechControlledShare;
	}
	public void setMechControlledShare(Double mechControlledShare) {
		this.mechControlledShare = mechControlledShare;
	}
	public Double getPerfControlledShare() {
		return perfControlledShare;
	}
	public void setPerfControlledShare(Double perfControlledShare) {
		this.perfControlledShare = perfControlledShare;
	}
	public Double getSynchControlledShare() {
		return synchControlledShare;
	}
	public void setSynchControlledShare(Double synchControlledShare) {
		this.synchControlledShare = synchControlledShare;
	}
	public String getIsControlled() {
		return isControlled;
	}
	public void setIsControlled(String isControlled) {
		this.isControlled = isControlled;
	}
	
	
}
