package com.bmg.esb.PublishSongDataFromIMaestro.beans;

public class TISCode {
	
	private String tisCode;
	private String tisDesc;
	private String ChangeIndicator;
	private long ID;
	public String getTisCode() {
		return tisCode;
	}
	public void setTisCode(String tisCode) {
		this.tisCode = tisCode;
	}
	public String getTisDesc() {
		return tisDesc;
	}
	public void setTisDesc(String tisDesc) {
		this.tisDesc = tisDesc;
	}
	public String getChangeIndicator() {
		return ChangeIndicator;
	}
	public void setChangeIndicator(String changeIndicator) {
		ChangeIndicator = changeIndicator;
	}
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}

}
