package com.bmg.esb.PublishSongDataFromIMaestro.beans;

public class SongAka implements Comparable<SongAka>{
	
	private String name;
	private Integer songCode;
	private Integer aliasSongCode;
	private String ChangeIndicator;
	private long ID;
	private long id_md;
	//code addeed for sorting
	
	public long getId_md() {
		return id_md;
	}
	public void setId_md(long id_md) {
		this.id_md = id_md;
	}
	
	
	public int compareTo(SongAka sng){
		
		return (int) (this.id_md - sng.id_md);
		//return 1;
		
	}
	
	//ends here
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getSongCode() {
		return songCode;
	}
	public void setSongCode(Integer songCode) {
		this.songCode = songCode;
	}
	public String getChangeIndicator() {
		return ChangeIndicator;
	}
	public void setChangeIndicator(String changeIndicator) {
		ChangeIndicator = changeIndicator;
	}
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}
	public Integer getAliasSongCode() {
		return aliasSongCode;
	}
	public void setAliasSongCode(Integer aliasSongCode) {
		this.aliasSongCode = aliasSongCode;
	}

}
