package com.bmg.esb.PublishSongDataFromIMaestro.beans;

public class Society implements Comparable<Society>{

	private String idsociety;
	private String name;
	private String cisacCode;
	private String abbreviation;
	private String territoryName;
	private String countryName;
	private String ChangeIndicator;
	private long ID;
private long id_md;
	

	
	//code addeed for sorting
	
	public long getId_md() {
		return id_md;
	}
	public void setId_md(long id_md) {
		this.id_md = id_md;
	}
	
	
	public int compareTo(Society sng){
		
		return (int) (this.id_md - sng.id_md);
				
	}
	
	//ends here
	
	
	public String getIdsociety() {
		return idsociety;
	}
	public void setIdsociety(String idsociety) {
		this.idsociety = idsociety;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCisacCode() {
		return cisacCode;
	}
	public void setCisacCode(String cisacCode) {
		this.cisacCode = cisacCode;
	}
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public String getTerritoryName() {
		return territoryName;
	}
	public void setTerritoryName(String territoryName) {
		this.territoryName = territoryName;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getChangeIndicator() {
		return ChangeIndicator;
	}
	public void setChangeIndicator(String changeIndicator) {
		ChangeIndicator = changeIndicator;
	}
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}

	
	
	
	
}
