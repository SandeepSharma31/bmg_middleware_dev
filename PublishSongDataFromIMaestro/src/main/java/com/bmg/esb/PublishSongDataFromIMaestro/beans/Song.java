package com.bmg.esb.PublishSongDataFromIMaestro.beans;

public class Song implements Comparable<Song>
{
	
	
	
	private long SNGDTL_SONG_CODE;
	private String SNGDTL_SONG_TITLE;
	private String  SNGDTL_TERRITORY_CONTD;
	private String SNGDTL_COMPOSERS;
	private String SNGDTL_ISWC_CODE;
	private String SNGDTL_DELETE_IN;
	private String SNGDTL_SONG_STATUS;
	private String ChangeIndicator;
	private long ID;
	private long id_md;
	
	//added by jyothi
	private long SNGDTL_DATE_CREATED ;
	
	private long SNGDTL_DATE_ORIG_PUBL; 
	
	//ended by jyothi
	

	
	//code addeed for sorting
	
	public long getId_md() {
		return id_md;
	}
	public void setId_md(long id_md) {
		this.id_md = id_md;
	}
	
	
	public int compareTo(Song sng){
		
		return (int) (this.id_md - sng.id_md);
		//return 1;
		
	}
	
	//ends here
	
	
	
	public String getChangeIndicator() {
		return ChangeIndicator;
	}
	public void setChangeIndicator(String changeIndicator) {
		ChangeIndicator = changeIndicator;
	}
	
	public String getSNGDTL_SONG_TITLE() {
		return SNGDTL_SONG_TITLE;
	}
	public void setSNGDTL_SONG_TITLE(String sNGDTL_SONG_TITLE) {
		SNGDTL_SONG_TITLE = sNGDTL_SONG_TITLE;
	}
	public String getSNGDTL_TERRITORY_CONTD() {
		return SNGDTL_TERRITORY_CONTD;
	}
	public void setSNGDTL_TERRITORY_CONTD(String sNGDTL_TERRITORY_CONTD) {
		SNGDTL_TERRITORY_CONTD = sNGDTL_TERRITORY_CONTD;
	}
	public String getSNGDTL_COMPOSERS() {
		return SNGDTL_COMPOSERS;
	}
	public void setSNGDTL_COMPOSERS(String sNGDTL_COMPOSERS) {
		SNGDTL_COMPOSERS = sNGDTL_COMPOSERS;
	}
	public String getSNGDTL_ISWC_CODE() {
		return SNGDTL_ISWC_CODE;
	}
	public void setSNGDTL_ISWC_CODE(String sNGDTL_ISWC_CODE) {
		SNGDTL_ISWC_CODE = sNGDTL_ISWC_CODE;
	}
	public String getSNGDTL_DELETE_IN() {
		return SNGDTL_DELETE_IN;
	}
	public void setSNGDTL_DELETE_IN(String sNGDTL_DELETE_IN) {
		SNGDTL_DELETE_IN = sNGDTL_DELETE_IN;
	}
	public String getSNGDTL_SONG_STATUS() {
		return SNGDTL_SONG_STATUS;
	}
	public void setSNGDTL_SONG_STATUS(String sNGDTL_SONG_STATUS) {
		SNGDTL_SONG_STATUS = sNGDTL_SONG_STATUS;
	
	}
	public long getSNGDTL_SONG_CODE() {
		return SNGDTL_SONG_CODE;
	}
	public void setSNGDTL_SONG_CODE(long sNGDTL_SONG_CODE) {
		SNGDTL_SONG_CODE = sNGDTL_SONG_CODE;
	}
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}
	
	//add by jyothi
	public long getSNGDTL_DATE_CREATED() {
		return SNGDTL_DATE_CREATED;
	}
	public void setSNGDTL_DATE_CREATED(long sNGDTL_DATE_CREATED) {
		SNGDTL_DATE_CREATED = sNGDTL_DATE_CREATED;
	}
	public long getSNGDTL_DATE_ORIG_PUBL() {
		return SNGDTL_DATE_ORIG_PUBL;
	}
	public void setSNGDTL_DATE_ORIG_PUBL(long sNGDTL_DATE_ORIG_PUBL) {
		SNGDTL_DATE_ORIG_PUBL = sNGDTL_DATE_ORIG_PUBL;
	}
 //end by jyothi
}
