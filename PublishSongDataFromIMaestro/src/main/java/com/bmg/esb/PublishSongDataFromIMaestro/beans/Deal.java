package com.bmg.esb.PublishSongDataFromIMaestro.beans;

public class Deal {
	
	
	private String sysTerr;
	private String dealNo;
	private String dealName;
	private String ChangeIndicator;
	private long ID;
	public String getSysTerr() {
		return sysTerr;
	}
	public void setSysTerr(String sysTerr) {
		this.sysTerr = sysTerr;
	}
	public String getDealNo() {
		return dealNo;
	}
	public void setDealNo(String dealNo) {
		this.dealNo = dealNo;
	}
	public String getDealName() {
		return dealName;
	}
	public void setDealName(String dealName) {
		this.dealName = dealName;
	}
	public String getChangeIndicator() {
		return ChangeIndicator;
	}
	public void setChangeIndicator(String changeIndicator) {
		ChangeIndicator = changeIndicator;
	}
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}
	
	

}
