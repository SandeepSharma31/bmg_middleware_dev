package com.bmg.esb.PublishSongDataFromIMaestro.beans;

public class Territory implements Comparable<Territory>{
	
	
	private String idTerritory;
	private String name;
	private String incCodes;
	private String excCodes;
	private String ChangeIndicator;
	private long ID;
	
private long id_md;
	

	
	//code addeed for sorting
	
	public long getId_md() {
		return id_md;
	}
	public void setId_md(long id_md) {
		this.id_md = id_md;
	}
	
	
	public int compareTo(Territory sng){
		
		return (int) (this.id_md - sng.id_md);
		//return 1;
		
	}
	
	//ends here
	
	
	public String getIdTerritory() {
		return idTerritory;
	}
	public void setIdTerritory(String idTerritory) {
		this.idTerritory = idTerritory;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIncCodes() {
		return incCodes;
	}
	public void setIncCodes(String incCodes) {
		this.incCodes = incCodes;
	}
	public String getExcCodes() {
		return excCodes;
	}
	public void setExcCodes(String excCodes) {
		this.excCodes = excCodes;
	}
	public String getChangeIndicator() {
		return ChangeIndicator;
	}
	public void setChangeIndicator(String changeIndicator) {
		ChangeIndicator = changeIndicator;
	}
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}

}
