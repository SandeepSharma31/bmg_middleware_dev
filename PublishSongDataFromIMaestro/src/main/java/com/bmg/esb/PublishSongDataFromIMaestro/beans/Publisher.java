package com.bmg.esb.PublishSongDataFromIMaestro.beans;

public class Publisher implements Comparable<Publisher>{

	
	private long idPub;
	private String name;
	private String ipi;
	private String mechanicalSocietyId;
	private String performanceSocietyId;
	private String isControlled; 
	private String ChangeIndicator;
	private long ID;
private long id_md;
	

	
	//code addeed for sorting
	
	public long getId_md() {
		return id_md;
	}
	public void setId_md(long id_md) {
		this.id_md = id_md;
	}
	
	
	public int compareTo(Publisher sng){
		
		return (int) (this.id_md - sng.id_md);
		//return 1;
		
	}
	
	//ends here
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIpi() {
		return ipi;
	}
	public void setIpi(String ipi) {
		this.ipi = ipi;
	}
	public String getMechanicalSocietyId() {
		return mechanicalSocietyId;
	}
	public void setMechanicalSocietyId(String mechanicalSocietyId) {
		this.mechanicalSocietyId = mechanicalSocietyId;
	}
	public String getPerformanceSocietyId() {
		return performanceSocietyId;
	}
	public void setPerformanceSocietyId(String performanceSocietyId) {
		this.performanceSocietyId = performanceSocietyId;
	}
	public String getIsControlled() {
		return isControlled;
	}
	public void setIsControlled(String isControlled) {
		this.isControlled = isControlled;
	}

	public String getChangeIndicator() {
		return ChangeIndicator;
	}
	public void setChangeIndicator(String changeIndicator) {
		ChangeIndicator = changeIndicator;
	}
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}
	public long getIdPub() {
		return idPub;
	}
	public void setIdPub(long idPub) {
		this.idPub = idPub;
	}

	
	
}
