package com.bmg.esb.PublishSongDataFromIMaestro.beans;

public class Composer implements Comparable<Composer>{
	
	private long idComp;
	private String firstName;
	private String middleName;
	private String lastName;
	private String name;
	private String ipi;
	private String mechanicalSocietyId;
	private String performanceSocietyId;
	private String isControlled;
	private String ChangeIndicator;
	private long ID;
	private long id_md;
	

	
	//code addeed for sorting
	
	public long getId_md() {
		return id_md;
	}
	public void setId_md(long id_md) {
		this.id_md = id_md;
	}
	
	
	public int compareTo(Composer sng){
		
		return (int) (this.id_md - sng.id_md);
		//return 1;
		
	}
	
	//ends here
	
	
	public String getChangeIndicator() {
		return ChangeIndicator;
	}
	public void setChangeIndicator(String changeIndicator) {
		ChangeIndicator = changeIndicator;
	}
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIpi() {
		return ipi;
	}
	public void setIpi(String ipi) {
		this.ipi = ipi;
	}
	public String getMechanicalSocietyId() {
		return mechanicalSocietyId;
	}
	public void setMechanicalSocietyId(String mechanicalSocietyId) {
		this.mechanicalSocietyId = mechanicalSocietyId;
	}
	public String getPerformanceSocietyId() {
		return performanceSocietyId;
	}
	public void setPerformanceSocietyId(String performanceSocietyId) {
		this.performanceSocietyId = performanceSocietyId;
	}
	public String getIsControlled() {
		return isControlled;
	}
	public void setIsControlled(String isControlled) {
		this.isControlled = isControlled;
	}
	public long getIdComp() {
		return idComp;
	}
	public void setIdComp(long idComp) {
		this.idComp = idComp;
	}

	
	
	

}
