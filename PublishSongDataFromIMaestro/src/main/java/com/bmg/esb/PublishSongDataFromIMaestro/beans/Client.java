package com.bmg.esb.PublishSongDataFromIMaestro.beans;

public class Client {

	private Integer clientCode;
	private String clientName;
	private String ChangeIndicator;
	private long ID;
	public Integer getClientCode() {
		return clientCode;
	}
	public void setClientCode(Integer clientCode) {
		this.clientCode = clientCode;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getChangeIndicator() {
		return ChangeIndicator;
	}
	public void setChangeIndicator(String changeIndicator) {
		ChangeIndicator = changeIndicator;
	}
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}
}
