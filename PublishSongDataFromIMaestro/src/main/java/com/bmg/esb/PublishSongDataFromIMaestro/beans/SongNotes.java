package com.bmg.esb.PublishSongDataFromIMaestro.beans;

public class SongNotes implements Comparable<SongNotes>{

	private Integer songCode;
	private String type;
	private Integer sequence;
	private String note;
	private String system;
	private String ChangeIndicator;
	private long ID;
	
private long id_md;
	

	
	//code addeed for sorting
	
	public long getId_md() {
		return id_md;
	}
	public void setId_md(long id_md) {
		this.id_md = id_md;
	}
	
	
	public int compareTo(SongNotes sng){
		
		return (int) (this.id_md - sng.id_md);
		//return 1;
		
	}
	
	//ends here
	
	
	
	
	public Integer getSongCode() {
		return songCode;
	}
	public void setSongCode(Integer songCode) {
		this.songCode = songCode;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getSequence() {
		return sequence;
	}
	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getSystem() {
		return system;
	}
	public void setSystem(String system) {
		this.system = system;
	}
	public String getChangeIndicator() {
		return ChangeIndicator;
	}
	public void setChangeIndicator(String changeIndicator) {
		ChangeIndicator = changeIndicator;
	}
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}
	
}
