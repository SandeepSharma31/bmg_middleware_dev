package com.bmg.esb.subscribeToBmgSmartSuspense;

/**
 * An interface for implementing Hello services.
 */
public interface Hello {

    String hello();
	
}
